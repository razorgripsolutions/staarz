describe('Sanity', () => {
  beforeAll(async () => {
    await device.launchApp({
      permissions: {notifications: 'YES'},
    });
  });

  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('[NO TOKEN] should show login page after notification permission', async () => {
    await waitFor(element(by.id('login-page')))
      .toExist()
      .withTimeout(3000);
    await expect(element(by.id('login-page'))).toBeVisible();
  });
});
