import React, {
  createContext,
  Fragment,
  useContext,
  useEffect,
  useMemo,
  useReducer,
} from 'react';
import {Provider} from 'react-native-paper';
import {ReactQueryConfigurationProvider} from './src/api/react-query-config';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {theme} from './src/core/theme';
import store, {
  // USER_REGISTRATION_DETAILS_IDENTIFIER,
  USER_TOKEN_DETAILS_IDENTIFIER,
  USER_TOKEN_IDENTIFIER,
} from './src/api/store';
import {AuthReducer} from './src/context/auth-reducer';
import {AuthStack, CoreStack} from './src/navigation';
import Loader from './src/screens/shared';
import Toast from 'react-native-toast-message';
import {BasicEmailSignupRequest} from './src/api/hooks/email_signup';
import {UserProfileResponse} from './src/api/hooks/email_login';
import PubNub from 'pubnub';
import {PubNubProvider} from 'pubnub-react';
import {
  PUBNUB_PUBLISH_KEY,
  PUBNUB_SECRET_KEY,
  PUBNUB_SUBSCRIBE_KEY,
} from './src/constants/constant';

const pubnub = new PubNub({
  publishKey: PUBNUB_PUBLISH_KEY,
  subscribeKey: PUBNUB_SUBSCRIBE_KEY,
  uuid: PUBNUB_SECRET_KEY,
});

type RegistrationFiles = {
  galleryImage?: string;
  cameraImage?: string;
};
type RegistrationDetails = BasicEmailSignupRequest & RegistrationFiles;
export interface IUserContext extends UserContextControllerProps {
  userState: IUserState;
}

export interface IUserState {
  userToken: string | null;
  isLoading: boolean;
  userData: UserProfileResponse | null;
  registrationData: RegistrationDetails | null;
}

export type UserContextControllerProps = {
  signIn: (
    userToken: string | null,
    userData: UserProfileResponse | null,
  ) => Promise<void>;
  signOut: () => Promise<void>;
  register: (user: RegistrationDetails) => Promise<void>;
  endRegistration: () => Promise<void>;
};

declare enum ActionEnum {
  RESTORE_TOKEN,
  SIGN_IN,
  SIGN_OUT,
  REGISTER,
  END_REGISTRATION,
}

export type ActionsTypes = keyof typeof ActionEnum;

export type Reducers = {
  userToken: string | null;
  type: ActionsTypes;
  userData: UserProfileResponse | null;
  registrationData: RegistrationDetails | null;
};

const initialState = {
  isLoading: true,
  userData: null,
  userToken: null,
  registrationData: null,
};

const Stack = createStackNavigator();

const AuthContext = createContext<IUserContext>({
  signIn: async () => {},
  signOut: async () => {},
  register: async () => {},
  endRegistration: async () => {},
  userState: initialState,
});

function App() {
  const [state, dispatch] = useReducer(AuthReducer, initialState);

  const authController = useMemo(
    () => ({
      signIn: async (
        userToken: string | null,
        userData: UserProfileResponse | null,
      ) => {
        try {
          await store.storeToken<any>(USER_TOKEN_IDENTIFIER, userToken);
          await store.storeToken<any>(USER_TOKEN_DETAILS_IDENTIFIER, userData);
          dispatch({
            type: 'SIGN_IN',
            userToken: userToken,
            userData: userData,
            registrationData: null,
          });
        } catch (e) {
          console.warn(e);
        }
      },
      register: async (user: RegistrationDetails | null) => {
        try {
          dispatch({
            type: 'REGISTER',
            userToken: null,
            userData: null,
            registrationData: user,
          });
        } catch (e) {
          console.warn(e);
        }
      },
      signOut: async () => {
        try {
          await store.deleteToken(USER_TOKEN_IDENTIFIER);
          await store.deleteToken(USER_TOKEN_DETAILS_IDENTIFIER);
          dispatch({
            type: 'SIGN_OUT',
            userData: null,
            userToken: null,
            registrationData: null,
          });
        } catch (e) {
          console.log(e);
        }
      },
      endRegistration: async () => {
        try {
          dispatch({
            type: 'END_REGISTRATION',
            userData: null,
            userToken: null,
            registrationData: null,
          });
        } catch (e) {
          console.log(e);
        }
      },
    }),
    [],
  );

  useEffect(() => {
    (async () => {
      let userToken: string;
      let userDetails: UserProfileResponse;
      try {
        userToken = await store.getToken(USER_TOKEN_IDENTIFIER);
        userDetails = await store.getToken(USER_TOKEN_DETAILS_IDENTIFIER);
        if (userToken) {
          dispatch({
            type: 'RESTORE_TOKEN',
            userToken: userToken,
            userData: userDetails,
            registrationData: null,
          });
        } else {
          dispatch({
            type: 'RESTORE_TOKEN',
            userToken: null,
            userData: null,
            registrationData: null,
          });
          authController.signOut();
          authController.endRegistration();
        }
      } catch (error) {
        console.warn(error);
      }
    })();
  }, [authController]);

  return (
    <Fragment>
      <PubNubProvider client={pubnub}>
        <ReactQueryConfigurationProvider>
          {state.isLoading ? (
            <Loader />
          ) : (
            <AuthContext.Provider
              value={{
                userState: state,
                ...authController,
              }}
            >
              <Provider theme={theme}>
                <NavigationContainer>
                  <Stack.Navigator
                    initialRouteName="Auth"
                    screenOptions={{
                      headerShown: false,
                    }}
                  >
                    {!state.userData || !state.userToken ? (
                      <Stack.Screen name="Auth" component={AuthStack} />
                    ) : (
                      <Stack.Screen name="Core" component={CoreStack} />
                    )}
                  </Stack.Navigator>
                </NavigationContainer>
              </Provider>
            </AuthContext.Provider>
          )}
        </ReactQueryConfigurationProvider>
      </PubNubProvider>

      <Toast position="top" />
    </Fragment>
  );
}

export const useAuthProvider = () => useContext(AuthContext);
export default App;
