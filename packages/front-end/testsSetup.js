import mockRNCNetInfo from '@react-native-community/netinfo/jest/netinfo-mock';
import {NativeModules} from 'react-native';
// eslint-disable-next-line import/no-extraneous-dependencies
import ws from 'ws';

// Reset the mocks before each test
global.beforeEach(() => {
  jest.resetAllMocks();
});

// Required for jest to properly run
function mockNativeModules() {
  // Mock the ImagePickerManager native module to allow us to unit test the JavaScript code
  NativeModules.ImagePickerManager = {
    showImagePicker: jest.fn(),
    launchCamera: jest.fn(),
    launchImageLibrary: jest.fn(),
  };
  jest.mock('react-native/Libraries/EventEmitter/NativeEventEmitter');
  jest.mock('@react-native-community/netinfo', () => mockRNCNetInfo);
  jest.mock('@react-native-google-signin/google-signin', () => {});
  jest.mock('react-native-agora', () => ({
    __esModule: true,
    default: {
      create: jest.fn(() => ({
        addListener: jest.fn(),
        destroy: jest.fn(),
        enableVideo: jest.fn(),
        removeListener: jest.fn(),
        // Add any additional methods on RtcEngine that you use
      })),
    },
  }));
  jest.mock('agora-react-native-rtm', () => {});
  jest.mock('@ptomasroos/react-native-multi-slider', () => {});
  jest.mock('react-native-sound', () => 'Sound');
  jest.mock('react-native-background-timer', () => ({
    start: jest.fn(),
  }));
  jest.mock('@stripe/stripe-react-native', () => ({
    StripeProvider: jest.fn(({children}) => children),
    CardField: jest.fn(() => null),
    presentPaymentSheet: jest.fn(),
    initPaymentSheet: jest.fn(),
  }));
  jest.mock('react-native/Libraries/Animated/NativeAnimatedHelper');
}

function mockWebSockets() {
  Object.assign(global, {WebSocket: ws});
}

mockNativeModules();
mockWebSockets();
