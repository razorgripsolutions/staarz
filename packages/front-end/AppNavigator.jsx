import DropdownAlert from 'react-native-dropdownalert';
import React, {Component} from 'react';
import {View, Text} from 'react-native';

export default class AppNavigator extends Component {
  componentDidMount() {
    this.dropDownAlertRef.alertWithType('error', 'Error', 'hello');
  }

  render() {
    // Make sure DropdownAlert is the last component in the document tree.
    return (
      <View>
        <Text>hello</Text>
        <DropdownAlert
          ref={(ref) => {
            this.dropDownAlertRef = ref;
          }}
        />
      </View>
    );
  }
}
