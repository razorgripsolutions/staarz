export const FontStyles = {
  bold: 'SFUIText-Bold',
  semiBold: 'SFUIText-Semibold',
  regular: 'SFUIText-regular',
  medium: 'SFUIText-Medium',
};
