import * as React from 'react';
import {checkConnection} from '../components/checkConnection';
import {InterconnectionHookReturnType} from './types';

function useHasInternetConnection(): InterconnectionHookReturnType {
  const [hasInternetConnection, setHasInternetConnection] =
    React.useState(false);

  const handleCheckInternetConnection = React.useCallback(async () => {
    const result = await checkConnection();
    setHasInternetConnection(result as boolean);
  }, [setHasInternetConnection]);

  React.useEffect(() => {
    handleCheckInternetConnection();
  }, []);

  return {
    handleCheckInternetConnection,
    hasInternetConnection,
  };
}

export {useHasInternetConnection};
