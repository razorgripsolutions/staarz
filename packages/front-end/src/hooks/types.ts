export type InterconnectionHookReturnType = {
  hasInternetConnection: boolean;
  handleCheckInternetConnection: () => Promise<void>;
};
