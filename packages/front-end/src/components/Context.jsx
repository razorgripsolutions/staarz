import React, {createContext, useReducer} from 'react';
// import { Provider } from 'react-native-paper/lib/typescript/core/settings';

export const Store = createContext();
// export const GlobalProvider = ({ children }) => {
//     return (
//         <Appcontext.Provider value={initialState}>
//             {children}
//         </Appcontext.Provider>
//     )
// }

export function GlobalProvider({children}) {
  const [activeCall, setActiveCall] = useReducer(
    (state, action = {}) => action,
    true,
  );

  // eslint-disable-next-line react/jsx-no-constructed-context-values
  const rootReducer = {
    activeCall,
    setActiveCall,
  };
  return <Store.Provider value={rootReducer}>{children}</Store.Provider>;
}
