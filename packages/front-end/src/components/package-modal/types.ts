export type PayPackage = {
    packageName: string;
    paymentPlans: PaymentPlan[];
    features: string[];
  };
  
  export type PaymentPlan = {
    id: number;
    header: string;
    isActive?: boolean;
    numOfMonths: number;
    price: number;
    total: number;
  };
  