import { View, StyleSheet, Text } from "react-native";
import React from "react";
import { Colors } from "../../constants/colors";
import { PaymentPlan } from "./types";

export type PaymentCardProp = {
  color?: string;
  isActive?: boolean;
  header: string;
  paymentPlan: PaymentPlan
};

const activeColor = "#222E40";
const cardHeaderColor = "#C4CDDA";

const PaymentCard = ({ color, isActive, header,paymentPlan }: PaymentCardProp) => {
  return (
    <View style={[styles.card, { marginTop: isActive ? 3 : 7 }]}>
      <View style={{ backgroundColor: isActive ? color : cardHeaderColor }}>
        <Text
          style={{
            color: Colors.white,
            textAlign: "center",
            margin:3,
            fontSize: isActive ? 12 : 10,
          }}
        >
          {header}
        </Text>
      </View>
      <View style={styles.details}>
        <Text
          style={[styles.textCenter,{ color: isActive ? activeColor : Colors.lightTintColor,fontSize: isActive ? 22 : 16, fontWeight: isActive? "bold":"normal" }]}
        >
          {paymentPlan.numOfMonths}
        </Text>
        <Text
          style={[styles.textCenter,{  color: isActive ? activeColor : Colors.lightTintColor,fontSize: isActive ? 22 : 16, fontWeight: isActive? "500":"normal" }]}
        >
          Month
        </Text>

        <View style={styles.flexHorizontal}>
          <Text
            style={[styles.textCenter,{
              color: isActive ? activeColor : Colors.lightTintColor,
            }]}
          >
            ${paymentPlan.price}
          </Text>
          <Text style={[styles.textCenter,{       
              fontSize:12
            }]}>/mo</Text>
        </View>

        <View style={styles.flexHorizontal}>
          <Text style={{ color: isActive ? activeColor : Colors.lightTintColor }}>Total:</Text>
          <Text
            style={[styles.textCenter,{       
              color: color,
              fontSize:12
            }]}
          >
            ${paymentPlan.total}
          </Text>
        </View>
      </View>
    </View>
  );
};

export default PaymentCard;

const styles = StyleSheet.create({
  textCenter: { textAlign: "center"},
  top: {},
  flexHorizontal: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
  },
  details: {
    textAlign: "center",
    height: "100%",
  },
  card: {
    width: 100,
    height: 150,
    borderRadius: 10,
    display: "flex",
    overflow: "hidden",
    elevation: 1,
  },
});
