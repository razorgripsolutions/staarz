import React, { useEffect, useState } from "react";
import {
  Modal,
  StyleSheet,
  Text,
  Image,
  View,
  TouchableOpacity,
  ImageSourcePropType,
} from "react-native";
import Button from "../Button";
import LinearGradient from "react-native-linear-gradient";
import { Colors } from "../../constants/colors";
import { PaymentPlan, PayPackage } from "./types";

import PaymentCard from "./PaymentCard";

type PaymentPlanModal = {
  colors: string[];
  visible: boolean;
  onClose: () => void;
  iconSource: ImageSourcePropType;
  payPackage: PayPackage;
  bulletsColor: string;
};

const bulletCharCode = "\u2B24";

const PaymentPlanModal = ({
  colors,
  visible,
  onClose,
  iconSource,
  payPackage,
  bulletsColor,
}: PaymentPlanModal) => {

  const [mainColor, setMainColor] = useState<string>();
  const [paymentPlans, setPaymentPlans] = useState<PaymentPlan[]>();

  useEffect(() => {
    setMainColor(colors[1]);
  }, [colors]);

  useEffect(() => {
    setPaymentPlans(payPackage.paymentPlans);
  }, [payPackage]);

  const setSelectedPayPlan = (id: number) => {
    if (!paymentPlans) return;

    const updatedPayments = paymentPlans.map((p) => ({
      ...p,
      isActive: p.id === id,
    }));

    setPaymentPlans(updatedPayments);
  };

  const continueHandler = () => {};

  return (
    <Modal
      transparent={true}
      animationType="fade"
      visible={visible}
      presentationStyle="overFullScreen"
      onRequestClose={() => {}}
    >
      <View style={styles.modalView}>
        <LinearGradient colors={colors} style={{ flex: 1 }}>
          <View style={[styles.topModal]}>
            <Image style={styles.topImage} source={iconSource} />
            <Text style={styles.topTitle}>{payPackage.packageName}</Text>
            <View style={styles.featuresList}>
              {payPackage.features.map((item) => (
                <View style={styles.featureContainer} key={item}>
                  <Text style={[styles.bulletStyle, { color: bulletsColor }]}>
                    {bulletCharCode}
                  </Text>
                  <Text style={styles.featureText}>{item}</Text>
                </View>
              ))}
            </View>
          </View>
        </LinearGradient>
        <View style={styles.bottomModal}>
          <View style={styles.cardsContainer}>
            {paymentPlans?.map((plan) => (
              <TouchableOpacity
                key={plan.id}
                onPress={() => setSelectedPayPlan(plan.id)}
              >
                <PaymentCard
                  header={plan.header}
                  color={mainColor}
                  isActive={plan.isActive}
                  paymentPlan={plan}
                />
              </TouchableOpacity>
            ))}
          </View>
          <View style={bottomButton.bottomView}>
            <Button
              style={bottomButton.continue}
              mode="contained"
              onPress={continueHandler}
            >
              <Text style={bottomButton.text}>Continue</Text>
            </Button>
          </View>

          <View style={styles.noThankContainer}>
            <Text>No Thank,</Text>
            <TouchableOpacity onPress={onClose}>
              <Text style={{ color: mainColor }}> I'll do it later</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
};

const bottomButton = StyleSheet.create({
  bottomView: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  continue: {
    marginBottom: 10 ,
    borderRadius: 8,
    width: "80%",
    backgroundColor: Colors.buttonColor,
  },
  text: {
    textTransform: "capitalize",
    fontWeight: "bold",
    fontSize: 18,
    color: "white",
  },
});

const styles = StyleSheet.create({
  featuresList: { display: "flex", flexDirection: "column" },
  featureContainer: { display: "flex", flexDirection: "row", marginTop: 5 },
  featureText: { color: "white" },
  bulletStyle: { marginRight: 7 },
  topTitle: {
    color: "white",
    fontWeight: "bold",
    fontSize: 22,
    marginTop: 5,
  },
  topImage: {
    width: 90,
    height: 90,
  },
  listBullet: {},
  doItLaterText: {
    color: "red",
  },
  noThankContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
  },
  cardsContainer: {
    marginTop: 20,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-evenly",
  },

  root: {
    alignItems: "center",
    flex: 1,
    justifyContent: "center",
  },

  modalView: {
    marginTop: 55,
    marginLeft: 20,
    width: "90%",
    height: "80%",
    borderRadius: 20,
    overflow: "hidden",
  },
  topModal: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  bottomModal: {
    flex: 1,
    backgroundColor: "white",
  },
  featuresContainer: {
    flex: 1,
  },
  featuresItem: {},
});

export default PaymentPlanModal;
