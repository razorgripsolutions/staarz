import React, {useState} from 'react';
import {
  ImageBackground,
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  SafeAreaView,
  StatusBar,
} from 'react-native';
// import { TouchableOpacity } from 'react-native-gesture-handler';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';

import AntDesign from 'react-native-vector-icons/AntDesign';
import {Stopwatch} from 'react-native-stopwatch-timer';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';

const {width} = Dimensions.get('window'); // full width
const {height} = Dimensions.get('window');
const image = {uri: 'https://reactjs.org/logo-og.png'};
function RecevierScreen({
  onPressSpeaker,
  onPressMute,
  onPress,
  username,
  muteCallstate,
  speakerCallState,
  url,
  ...rest
}) {
  const [isTimerStart, setIsTimerStart] = useState(false);
  const [isStopwatchStart, setIsStopwatchStart] = useState(true);
  const [timerDuration, setTimerDuration] = useState(90000);
  const [resetTimer, setResetTimer] = useState(false);
  const [resetStopwatch, setResetStopwatch] = useState(false);
  // const [muteIcon,setmuteIcon] = useState(false)

  return (
    <SafeAreaView style={{flex: 1}}>
      <StatusBar backgroundColor="#008080" />
      <View
        style={{
          flex: 1,
          backgroundColor: '#000',
        }}
      >
        <ImageBackground source={url} style={styles.image}>
          <View style={styles.HederCall}>
            <Text
              style={{
                fontWeight: 'bold',
                textAlign: 'center',
                color: '#fff',
                fontSize: 20,
                alignSelf: 'center',
                letterSpacing: 0.5,
                marginTop: 10,
              }}
            >
              {username}
            </Text>
            <Text
              style={{
                fontWeight: '400',
                textAlign: 'center',
                color: '#fff',
                fontSize: 16,
                alignSelf: 'center',
                marginTop: 3,
              }}
            >
              audio call
            </Text>
            <View style={styles.sectionStyle}>
              <Stopwatch
                start={isStopwatchStart}
                // To start
                reset={resetStopwatch}
                // To reset
                options={options}
                // Options for the styling
                getTime={(time) => {
                  console.log(time);
                }}
              />
            </View>
          </View>
          <View style={{flexDirection: 'row'}} />
          <View
            style={{
              flex: 1,
              backgroundColor: 'black',
              borderTopLeftRadius: 15,
              borderTopRightRadius: 15,
              marginTop: hp('85'),
              alignItems: 'center',
              opacity: 0.8,
            }}
          >
            <View
              style={{
                flex: 0.8,
                backgroundColor: 'black',
                marginTop: 20,
                flexDirection: 'row',
                alignItems: 'center',
                alignContent: 'center',
              }}
            >
              <TouchableOpacity style={styles.circle} onPress={onPressSpeaker}>
                {speakerCallState === true ? (
                  <AntDesign
                    name="sound"
                    color="white"
                    size={25}
                    style={{marginTop: 18}}
                  />
                ) : (
                  <Entypo
                    name="sound"
                    color="white"
                    size={25}
                    style={{marginTop: 18}}
                  />
                )}
              </TouchableOpacity>

              <TouchableOpacity style={styles.circle1} onPress={onPress}>
                <Entypo name="cross" color="black" size={35} style={{}} />
              </TouchableOpacity>

              <TouchableOpacity style={styles.circle} onPress={onPressMute}>
                {muteCallstate === true ? (
                  <FontAwesome
                    name="microphone-slash"
                    color="white"
                    size={25}
                    style={{marginTop: 18}}
                  />
                ) : (
                  <FontAwesome
                    name="microphone"
                    color="white"
                    size={25}
                    style={{marginTop: 18}}
                  />
                )}
              </TouchableOpacity>
              {/* <TouchableOpacity style={styles.circle} >
     { muteIcon === true?<Ionicons name = "md-camera-reverse-sharp" color="white" size={25}  style ={{marginTop:18}}/>:
     <Ionicons name = "md-camera-reverse-sharp" color="white" size={30}  style ={{marginTop:15}}/>}
     </TouchableOpacity> */}
            </View>
          </View>
        </ImageBackground>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    //   justifyContent: "center"
  },
  text: {
    color: 'white',
    fontSize: 42,
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: '#000000a0',
  },
  Cllbuttonview: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    height: 80,
    backgroundColor: '#008080',
    position: 'absolute',
    // top:height/1.3
    // top:"80%"
    bottom: 0,
  },
  HederCall: {
    width: '100%',
    // flexDirection:"row",
    // justifyContent:"space-around",
    alignItems: 'center',
    height: 80,
    backgroundColor: '#008080',
    position: 'absolute',
    // top:height/1.3
    // top:"65%"
    top: 0,
  },
  Center: {
    // top:"75%"
    justifyContent: 'space-around',
    marginTop: height / 1.5,
    flexDirection: 'row',
  },

  circle: {
    alignSelf: 'center',
    width: 60,
    height: 60,
    borderRadius: 60 / 2,
    backgroundColor: '#5b5353',
    marginHorizontal: 10,
    alignItems: 'center',

    // opacity:0.6
  },
  circle1: {
    alignSelf: 'center',
    width: 60,
    height: 60,
    borderRadius: 60 / 2,
    backgroundColor: 'red',
    marginHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
    // opacity:0.6
  },
  sectionStyle: {
    flex: 1,
    // marginTop: ,
    // alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 0,
  },
});

const options = {
  container: {
    // backgroundColor: '#FF0000',
    // padding: 5,
    borderRadius: 5,
    width: 200,
    left: -3,
    alignItems: 'center',
    bottom: 2,
  },
  text: {
    fontSize: 15,
    color: '#FFF',
    marginLeft: 7,
  },
};
export default RecevierScreen;
