import React from 'react';
import {Image, StyleSheet} from 'react-native';

function Logo() {
  return (
    <Image source={require('../assets/starzlogo.png')} style={styles.image} />
  );
}

const styles = StyleSheet.create({
  image: {
    width: 119,
    height: 133,
    // marginBottom: 8,
  },
});

export default Logo;
