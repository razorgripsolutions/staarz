/* eslint-disable no-nested-ternary */
// import React from 'react'
// import { View, Text, Modal, Dimensions, ImageBackground, Image, TouchableOpacity } from 'react-native'
// import LinearGradient from 'react-native-linear-gradient';
// import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
// var width = Dimensions.get('window').width; //full width
// var height = Dimensions.get('window').height;
// const About_ExpireModel = ({ UserDetails, ExpireDate, modalAbout_ExpireModel, aboutDismiss, packageType, packName, onPressRewindPackage
// }) => {
//     return (
//         <Modal
//             animationType="slide"
//             transparent={true}
//             visible={modalAbout_ExpireModel}>
//             <View style={{ alignItems: 'center', alignSelf: "center", height: "100%", width: "100%", position: 'absolute', justifyContent: "center", zIndex: 10 }}>
//                 <LinearGradient colors={packageType === '1' ? ['#000000', '#535353'] : packageType === '2' ? ['#7643F8', '#6843C5'] : ['#F2E586', '#D2AF4F']} style={{ width: "85%", height: height < 700 ? "42%" : "40%", alignItems: "center", borderTopRightRadius: 10, borderTopLeftRadius: 10 }}>

//                     {/* <View style={{ width:"75%", backgroundColor: "#000000", alignItems: "center", borderTopRightRadius: 10, borderTopLeftRadius: 10 }}> */}
//                     <Image source={packageType === '1' ? require('../assets/E_Plus.png') : packageType === '2' ? require('../assets/E_Purpal.png') : require('../assets/E_Gold.png')} style={{ height: height < 700 ? 60 : 70, width: height < 700 ? 60 : 70, resizeMode: "cover", marginTop: 20 }} />
//                     <Text style={{ color: "#fff", textAlign: "center", fontSize: height < 700 ? 20 : 24, fontFamily: "MPLUS1p-Bold", fontWeight: "800", marginTop: 10 }}>{ExpireDate === "0" ? "Oh no" : "Yalla"}</Text>
//                     {ExpireDate === "0" ?
//                         <Text style={{ color: "#fff", textAlign: "center", fontSize: height < 700 ? 12 : 16, fontFamily: "MPLUS1p-Bold", fontWeight: "800", lineHeight: height < 700 ? 20 : 30 }}>it seems you lost your spark!{'\n'} Click here to renew your package.</Text>

//                         :
//                         <Text style={{ color: "#fff", textAlign: "center", fontSize: height < 700 ? 12 : 16, fontFamily: "MPLUS1p-Bold", fontWeight: "800", lineHeight: height < 700 ? 20 : 30 }}>your light is about to run out! {'\n'} Renew your stellar package today!</Text>
//                     }
//                     {ExpireDate === "0" ?
//                         <Text style={{ color: "#fff", textAlign: "center", fontSize: height < 700 ? 16 : 20, fontFamily: "MPLUS1p-Bold", fontWeight: "800", marginTop: height < 700 ? 15 : 30 }}>Your Starzz {UserDetails.pakageName} is</Text>
//                         :
//                         <Text style={{ color: "#fff", textAlign: "center", fontSize: height < 700 ? 16 : 20, fontFamily: "MPLUS1p-Bold", fontWeight: "800", marginTop: height < 700 ? 15 : 30 }}>Your Starzz {UserDetails.pakageName} {'\n'}is about to expire in</Text>

//                     }
//                     <Text style={{ color: "#fff", textAlign: "center", fontSize: wp(7), fontFamily: "MPLUS1p-ExtraBold", fontWeight: "900", marginTop: height < 700 ? 10 : 20, marginBottom: 10 }}>{ExpireDate === "0" ? null : ExpireDate} {ExpireDate === "0" ? "Expired" : ExpireDate === "1" ? "DAY" : "DAYS"}</Text>

//                     {/* </View> */}
//                 </LinearGradient>

//                 <View style={{ width: "85%", height: height < 700 ? "28%" : "24%", backgroundColor: "#fff", borderBottomRightRadius: 10, borderBottomLeftRadius: 10 }}>
//                     <Text style={{ color: "#253443", textAlign: "center", fontSize: height < 700 ? 12 : 14, fontFamily: "MPLUS1p-Bold", fontWeight: "800", marginTop: height < 700 ? 12 : 14 }}>You’ll no longer have access to:</Text>
//                     <Text style={{ color: "#253443", textAlign: "center", fontSize: height < 700 ? 12 : 14, fontFamily: "MPLUS1p-ExtraBold", marginTop: height < 700 ? 12 : 14, lineHeight: 25 }}>{UserDetails.unlimitedLikes} Unlimited Likes
//                         <Text style={{ fontSize: 8, color: '#7643F8', textAlign: "center" }}>  ⬤  </Text>
//                         {UserDetails.rewind} Rewind
//                         <Text style={{ fontSize: 8, color: '#7643F8', textAlign: "center" }}>  ⬤  </Text>
//                         {UserDetails.isAds} No ads{"\n"}
//                         {UserDetails.superLikesPerDay} Super Likes a day
//                         <Text style={{ fontSize: 8, color: '#7643F8', textAlign: "center" }}>  ⬤  </Text>
//                         {UserDetails.passport} Passport
//                     </Text>

//                     <TouchableOpacity onPress={onPressRewindPackage}>
//                         <LinearGradient colors={packageType === '1' ? ['#000000', '#535353'] : packageType === '2' ? ['#7643F8', '#6843C5'] : ['#F2E586', '#D2AF4F']} style={{ width: "90%", alignItems: "center", borderRadius: 10, alignSelf: "center", marginTop: height < 700 ? 12 : 18 }}>

//                             <Text style={{ color: "#fff", textAlign: "center", fontSize: height < 700 ? 13 : 16, fontFamily: "MPLUS1p-ExtraBold", lineHeight: 25, paddingVertical: height < 700 ? 5 : 8 }}>
//                                 Renew package
//                             </Text>
//                         </LinearGradient>
//                     </TouchableOpacity>
//                     <Text style={{ color: "#8A98AC", textAlign: "center", fontSize: height < 700 ? 14 : 16, fontFamily: "MPLUS1p-Bold", fontWeight: "800", marginTop: 8, }} onPress={aboutDismiss}>Back to staarz</Text>

//                 </View>
//             </View >
//         </Modal>
//     )
// }
// export default About_ExpireModel;

import React from 'react';
import {
  View,
  Text,
  Modal,
  Dimensions,
  Image,
  TouchableOpacity,
  Platform,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

const {width} = Dimensions.get('window'); // full width
const {height} = Dimensions.get('window');
function About_ExpireModel({
  UserDetails,
  ExpireDate,
  modalAbout_ExpireModel,
  aboutDismiss,
  packageType,
  packName,
  onPressRewindPackage,
}) {
  return (
    <Modal animationType="slide" transparent visible={modalAbout_ExpireModel}>
      <View
        style={{
          alignItems: 'center',
          alignSelf: 'center',
          height: '100%',
          width: '100%',
          position: 'absolute',
          justifyContent: 'center',
          zIndex: 10,
          margintTop: 100,
        }}
      >
        <LinearGradient
          colors={
            // eslint-disable-next-line no-nested-ternary
            packageType === '1'
              ? ['#000000', '#535353']
              : packageType === '2'
              ? ['#7643F8', '#6843C5']
              : ['#F2E586', '#D2AF4F']
          }
          style={{
            width: '85%',
            height: '45%',
            alignItems: 'center',
            borderTopRightRadius: 10,
            borderTopLeftRadius: 10,
            marginTop: Platform.OS === 'ios' ? '15%' : '20%',
          }}
        >
          <Image
            source={
              // eslint-disable-next-line no-nested-ternary
              packageType === '1'
                ? require('../assets/E_Plus.png')
                : packageType === '2'
                ? require('../assets/E_Purpal.png')
                : require('../assets/E_Gold.png')
            }
            style={{
              height: 65,
              width: 65,
              borderRadius: 60 / 2,
              resizeMode: 'cover',
              position: 'absolute',
              bottom: '75%',
            }}
          />

          <Text
            style={{
              color: '#fff',
              textAlign: 'center',
              fontSize: height < 700 ? 20 : 24,
              fontFamily: 'MPLUS1p-Bold',
              fontWeight: '800',
              position: 'absolute',
              bottom: '63%',
              alignSelf: 'center',
            }}
          >
            {ExpireDate === '0' ? 'Oh no' : 'Yalla'}
          </Text>
          {ExpireDate === '0' ? (
            <Text
              style={{
                color: '#fff',
                textAlign: 'center',
                fontSize: height < 700 ? 12 : 16,
                fontFamily: 'MPLUS1p-Bold',
                fontWeight: '800',
                lineHeight: height < 700 ? 20 : 30,
                position: 'absolute',
                bottom: '43%',
                alignSelf: 'center',
              }}
            >
              it seems you lost your spark!{'\n'} Click here to renew your
              package.
            </Text>
          ) : (
            <Text
              style={{
                color: '#fff',
                textAlign: 'center',
                fontSize: height < 700 ? 12 : 16,
                fontFamily: 'MPLUS1p-Bold',
                fontWeight: '800',
                lineHeight: height < 700 ? 20 : 30,
                position: 'absolute',
                bottom: '43%',
                alignSelf: 'center',
              }}
            >
              your light is about to run out! {'\n'} Renew your stellar package
              today!
            </Text>
          )}
          {ExpireDate === '0' ? (
            <Text
              style={{
                color: '#fff',
                textAlign: 'center',
                fontSize: height < 700 ? 16 : 20,
                fontFamily: 'MPLUS1p-Bold',
                fontWeight: '800',
                position: 'absolute',
                bottom: '20%',
                alignSelf: 'center',
              }}
            >
              Your Starzz {UserDetails.pakageName} is
            </Text>
          ) : (
            <Text
              style={{
                color: '#fff',
                textAlign: 'center',
                fontSize: height < 700 ? 16 : 20,
                fontFamily: 'MPLUS1p-Bold',
                fontWeight: '800',
                position: 'absolute',
                bottom: '20%',
                alignSelf: 'center',
              }}
            >
              Your Starzz {UserDetails.pakageName} {'\n'}is about to expire in
            </Text>
          )}
          <Text
            style={{
              color: '#fff',
              textAlign: 'center',
              fontSize: wp(7),
              fontFamily: 'MPLUS1p-ExtraBold',
              fontWeight: '900',
              position: 'absolute',
              bottom: '5%',
              alignSelf: 'center',
            }}
          >
            {ExpireDate === '0' ? null : ExpireDate}{' '}
            {ExpireDate === '0'
              ? 'Expired'
              : ExpireDate === '1'
              ? 'DAY'
              : 'DAYS'}
          </Text>
        </LinearGradient>

        <View
          style={{
            width: '85%',
            height: '30%',
            backgroundColor: '#fff',
            borderBottomRightRadius: 10,
            borderBottomLeftRadius: 10,
            elevation: 10,
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.5,
            shadowRadius: 3.84,
          }}
        >
          <Text
            style={{
              color: '#253443',
              textAlign: 'center',
              fontSize: height < 700 ? 12 : 14,
              fontFamily: 'MPLUS1p-Bold',
              fontWeight: '800',
              position: 'absolute',
              top: '8%',
              alignSelf: 'center',
            }}
          >
            You’ll no longer have access to:
          </Text>
          <Text
            style={{
              color: '#253443',
              textAlign: 'center',
              fontSize: height < 700 ? 12 : 14,
              fontFamily: 'MPLUS1p-ExtraBold',
              position: 'absolute',
              bottom: '55%',
              alignSelf: 'center',
              lineHeight: 20,
            }}
          >
            {' '}
            Unlimited Likes
            <Text style={{fontSize: 8, color: '#7643F8', textAlign: 'center'}}>
              {' '}
              ⬤{' '}
            </Text>
            {UserDetails.rewind} Rewind
            <Text style={{fontSize: 8, color: '#7643F8', textAlign: 'center'}}>
              {' '}
              ⬤{' '}
            </Text>
            {UserDetails.isAds} No ads{'\n'}
            {UserDetails.superLikesPerDay} Super Likes a day
            <Text style={{fontSize: 8, color: '#7643F8', textAlign: 'center'}}>
              {' '}
              ⬤{' '}
            </Text>
            {UserDetails.passport} Passport
          </Text>
          <Text
            style={{
              color: '#8A98AC',
              textAlign: 'center',
              fontSize: height < 700 ? 14 : 16,
              fontFamily: 'MPLUS1p-Bold',
              fontWeight: '800',
              bottom: 20,
              position: 'absolute',
              alignSelf: 'center',
            }}
            onPress={aboutDismiss}
          >
            Back to staarz
          </Text>
          <TouchableOpacity
            onPress={onPressRewindPackage}
            style={{
              position: 'absolute',
              bottom: '25%',
              width: '90%',
              alignSelf: 'center',
            }}
          >
            <LinearGradient
              colors={
                packageType === '1'
                  ? ['#000000', '#535353']
                  : packageType === '2'
                  ? ['#7643F8', '#6843C5']
                  : ['#F2E586', '#D2AF4F']
              }
              style={{
                width: '90%',
                alignItems: 'center',
                borderRadius: 10,
                alignSelf: 'center',
              }}
            >
              <Text
                style={{
                  color: '#fff',
                  textAlign: 'center',
                  fontSize: height < 700 ? 13 : 16,
                  fontFamily: 'MPLUS1p-ExtraBold',
                  lineHeight: 25,
                  paddingVertical: height < 700 ? 5 : 8,
                }}
              >
                Renew package
              </Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
}
export default About_ExpireModel;
