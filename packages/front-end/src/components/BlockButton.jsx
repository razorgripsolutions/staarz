import React from 'react';
import {
  Text,
  TouchableOpacity,
  StyleSheet,
  View,
  Dimensions,
  Image,
} from 'react-native';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';

const {width} = Dimensions.get('window'); // full width
const {height} = Dimensions.get('window');
function BlockButton({iconType, title, color, url, buttonContainer, ...rest}) {
  return (
    <View style={{width: '100%', alignItems: 'center'}}>
      <TouchableOpacity style={styles.buttonContainer} {...rest}>
        {/* {iconType === "cross" ?
          <Entypo name={iconType} size={width === 320 || height === 610.6666666666666 ? iconType === "cross" ? 25 : 18 : iconType === "cross" ? 32 : 25} color={color} style={{}} />
          : */}

        <Image
          source={url}
          style={{
            width: hp(7),
            height: hp(7),
            resizeMode: 'contain',
            //  backgroundColor: '#F3F6FF',
            //  borderRadius: 15,
            //  borderWidth:1,
            // elevation:10,
            //  borderRadius:15
          }}
        />
        {/* } */}
      </TouchableOpacity>
      <Text style={styles.title}>{title}</Text>
    </View>
  );
}

export default BlockButton;

const styles = StyleSheet.create({
  buttonContainer: {
    // width: hp(6.5),
    // height: hp(6.5),
    borderRadius: 15,
    backgroundColor: '#F3F6FF',

    // backgroundColor: 'red',
    // paddingVertical:8,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    // bottom:1,
    // flexDirection:'row',
  },
  title: {
    fontWeight: '500',
    fontSize: 14,
    marginTop: 5,
    width: '100%',
    lineHeight: 17,
    color: '#000000',
    fontFamily: 'SFUIText-Medium',
    alignSelf: 'center',
    letterSpacing: 0.9,
    //   alignSelf:"center",
    textAlign: 'center',
  },
});
