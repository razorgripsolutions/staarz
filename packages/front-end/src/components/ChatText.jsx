import React from 'react';
import {View, TextInput, StyleSheet} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import {windowHeight, windowWidth} from '../../utilis/Dimentions';

function ChatText({labelValue, placeholderText, iconType, ...rest}) {
  return (
    <View style={styles.inputContainer}>
      <TextInput
        value={labelValue}
        style={styles.input}
        numberOfLines={1}
        placeholder={placeholderText}
        placeholderTextColor="#666"
        {...rest}
      />
      <View style={styles.iconStyle}>
        <Feather name={iconType} size={15} color="#000" />
      </View>
    </View>
  );
}

export default ChatText;

const styles = StyleSheet.create({
  inputContainer: {
    marginTop: 5,
    marginBottom: 10,
    width: '95%',
    height: windowHeight / 16,
    borderColor: '#ccc',
    borderRadius: 3,
    borderWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#b0c4de',
    justifyContent: 'space-between',
  },
  iconStyle: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRightColor: '#ccc',
    margin: 8,
  },
  input: {
    padding: 10,
    flex: 1,
    fontSize: 16,
    color: '#333',
    width: '90%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputField: {
    // padding: 10,
    marginTop: 5,
    marginBottom: 10,
    width: windowWidth / 1.5,
    height: windowHeight / 15,
    fontSize: 16,
    borderRadius: 8,
    borderWidth: 1,
  },
});
