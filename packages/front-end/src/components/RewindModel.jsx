import React from 'react';
import {
  View,
  Text,
  Modal,
  Dimensions,
  ImageBackground,
  Image,
  TouchableOpacity,
} from 'react-native';

import {heightPercentageToDP as hp} from 'react-native-responsive-screen';

const {width} = Dimensions.get('window'); // full width
const {height} = Dimensions.get('window');

function RewindModel({
  modalVisibleRewind,
  CrossOnpress,
  Rewind,
  rewindeDismiss,
}) {
  return (
    <Modal animationType="slide" transparent visible={modalVisibleRewind}>
      <View
        style={{
          alignItems: 'center',
          alignSelf: 'center',
          // eslint-disable-next-line no-nested-ternary
          height: height < 740 ? '55%' : height < 840 ? '50%' : '45%',
          width: '70%',
          position: 'absolute',
          bottom: height < 730 ? height / 3.2 : height / 3,
          justifyContent: 'center',
        }}
      >
        {/* <Text>{height}</Text> */}
        <ImageBackground
          source={require('../assets/rewind_B.png')}
          style={{width: '100%', height: '100%', alignItems: 'center'}}
          imageStyle={{borderRadius: 20}}
        >
          <TouchableOpacity
            style={{
              top: 25,
              left: 20,
              position: 'absolute',
              width: hp(2),
              height: hp(2),
              zIndex: 10,
            }}
            onPress={CrossOnpress}
          >
            <Image
              source={require('../assets/cross.png')}
              style={{width: hp(2), height: hp(2), zIndex: 10}}
            />
          </TouchableOpacity>

          <Image
            style={{
              width: height < 700 ? hp(18) : hp(17),
              height: height < 700 ? hp(18) : hp(17),
              borderRadius: height < 700 ? hp(18) : hp(17) / 2,
              // eslint-disable-next-line no-nested-ternary
              marginTop: height < 740 ? 50 : height < 840 ? 60 : 70,
            }}
            source={require('../assets/USer_p.png')}
          />
          <Image
            style={{
              // eslint-disable-next-line no-nested-ternary
              marginTop: height < 740 ? 135 : height < 840 ? 155 : 175,
              width: hp(8),
              height: hp(9),
              position: 'absolute',
              zIndex: 1,
            }}
            source={require('../assets/rewind_logo.png')}
          />
          <Text
            style={{
              textAlign: 'center',
              fontSize: 18,
              fontWeight: 'bold',
              fontFamily: 'MPLUS1p-Bold',
              color: '#253443',
              marginTop: 40,
            }}
          >
            Oops, you let a star fall
          </Text>
          <TouchableOpacity
            style={{
              marginTop: 10,
              margin: 10,
              backgroundColor: '#8A98AC',
              borderRadius: 8,
              width: '75%',
            }}
            onPress={Rewind}
          >
            <Text
              style={{
                textAlign: 'center',
                fontSize: 20,
                fontWeight: 'bold',
                fontFamily: 'MPLUS1p-ExtraBold',
                color: '#FFFFFF',
                paddingVertical: 9,
              }}
            >
              Rewind
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={rewindeDismiss}
            // () => setmodalVisibleRewind(!modalVisibleRewind)
          >
            <Text
              style={{
                textAlign: 'center',
                lineHeight: 27,
                fontSize: 20,
                fontWeight: '800',
                fontFamily: 'MPLUS1p-ExtraBold',
                color: '#8A98AC',
                paddingVertical: 5,
              }}
            >
              Continue Swiping
            </Text>
          </TouchableOpacity>
        </ImageBackground>
        {/* <Image
            style={{ width: hp(8), height: hp(8), top: -hp(4), zIndex: 10, resizeMode: "contain", marginTop: -2 }}
            source={
              require('./../assets/Union.png')
            }
          />
          <Image
            style={{ width: hp(3), height: hp(3), top: -hp(9.5), zIndex: 10, resizeMode: "contain", marginTop: -2 }}
            source={
              require('./../assets/Group.png')
            }
          /> */}

        {/* <Text style={{ textAlign: "center", lineHeight: 23, fontSize: 15, fontWeight: "800", fontFamily: "MPLUS1p-Bold", color: "#253443", top: 5 }}>Oops, you let a star fall</Text>
          <TouchableOpacity style={{ margin: 10, backgroundColor: "#8A98AC", borderRadius: 8, width: "75%", top: -25 }}>
            <Text style={{ textAlign: "center", lineHeight: 27, fontSize: 18, fontWeight: "800", fontFamily: "MPLUS1p-Bold", color: "#FFFFFF", paddingVertical: 8 }}>Rewind</Text>

          </TouchableOpacity>
          <TouchableOpacity style={{ top: -22 }}>

            <Text style={{ textAlign: "center", lineHeight: 27, fontSize: 18, fontWeight: "800", fontFamily: "MPLUS1p-Bold", color: "#8A98AC", paddingVertical: 5, }}>Continue Swiping</Text>
          </TouchableOpacity>
       */}
      </View>
      {/* </LinearGradient> */}
    </Modal>
  );
}
export default RewindModel;
