import React from 'react';
import {Text, StyleSheet, View, Image, Dimensions} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const {width} = Dimensions.get('window'); // full width
const {height} = Dimensions.get('window');
function ItsMatchMaodal({
  navigation,
  receiverId,
  receiverName,
  receiverPic,
  chennelID,
  url1,
  url2,
  ...rest
}) {
  // const navigation = navigation;
  // alert(height)
  return (
    <View style={{alignItems: 'center'}}>
      <View
        style={{
          flexDirection: 'row',
          alignSelf: 'center',
          marginTop: hp(16.5),
        }}
      >
        <Image style={styles.circle} source={url1} />

        <Image style={styles.circle1} source={url2} />
      </View>
      <Image
        style={{
          height: height < 765 ? 70 : 90,
          width: height < 765 ? 60 : 80,
          // top: Platform.OS === "ios" ? height < 765 ? 190 : (height < 765 ? height === "764.3333333333334" ? 185 : 180 : 222) : height < 700 ? 175 : (height < 765 ? height === "764.3333333333334" ? 218 : 185 : 233),
          bottom: '12%',
          // position: 'absolute'
        }}
        source={require('../assets/itsMatchlogo.png')}
      />

      <View style={{alignItems: 'center', marginTop: hp(0)}}>
        <Text
          style={{
            fontFamily: 'MPLUS1p-Regular',
            color: '#FFFFFF',
            fontSize: 18,
          }}
        >
          {' '}
          You matched with{' '}
        </Text>
        <Text
          numberOfLines={1}
          style={{
            fontFamily: 'MPLUS1p-ExtraBold',
            color: '#FFFFFF',
            fontSize: height < 700 ? 32 : 35,
            marginTop: 10,
            textAlign: 'center',
          }}
        >
          {receiverName}
        </Text>
        {/* <Button style={{ width: 300, marginTop: 30, height: 50, alignItems: 'center', justifyContent: 'center', fontFamily: "MPLUS1p-Bold", fontSize: 25, fontWeight: '900', backgroundColor: 'white' }} mode="contained" onPress={() => console.log('Pressed')}
                    onPress={() => navigation.dispatch(StackActions.push('PrivateMessage', {
                        chnnelId: chennelID, //Agora channel id ......get om message screen
                        profilePicture: receiverPic,
                        username: receiverName,

                        reciverId: receiverId
                    }))} >
                    <Text style={{ color: '#7643F8', fontFamily: "MPLUS1p-ExtraBold", fontSize: 20, fontWeight: '800' }}>SEND A MESSAGE</Text>
                </Button> */}
      </View>
    </View>
  );
}

export default ItsMatchMaodal;

const styles = StyleSheet.create({
  circle: {
    // width: height < 700 ? 140 : 160,
    // height: height < 700 ? 140 : 160,
    // width: height < 765 ? 130 : 158,
    width: wp(42),
    height: wp(42),
    // height: height < 765 ? 130 : 158,
    borderRadius: wp(42) / 2,
    backgroundColor: '#fff',
    borderColor: '#FFFFFF',
    borderWidth: 2,
  },
  circle1: {
    width: wp(42),
    height: wp(42),
    // width: 10,
    // height: 160,
    borderRadius: wp(42) / 2,

    backgroundColor: '#fff',
    borderColor: '#FFFFFF',
    marginLeft: -15,
    // borderRadius: 2,
    borderWidth: 2,
  },
  TextStyle: {
    marginTop: 20,
    textAlign: 'center',
    fontSize: 20,
    textDecorationLine: 'underline',
    color: '#FFFFFF',
    fontFamily: 'MPLUS1p-ExtraBold',
  },
});
