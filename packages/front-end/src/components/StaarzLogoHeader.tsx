import React, {useState} from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity} from 'react-native';

import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import PaymentPlanModal from './package-modal/PaymentPlan_Modal';
import {Colors} from '../constants/colors';
import {PaymentPlan, PayPackage} from './package-modal/types';

export default function StaarzLogoHeader() {
  const [plusPaymentModalVisibility, setPlusPaymentModalVisibility] =
    useState(false);
  const [platinumPaymentModalVisibility, setPlatinumPaymentModalVisibility] =
    useState(false);
  const [goldPaymentModalVisibility, setGoldPaymentModalVisibility] =
    useState(false);

  const closePaymentModals = () => {
    setPlusPaymentModalVisibility(false);
    setPlatinumPaymentModalVisibility(false);
    setGoldPaymentModalVisibility(false);
  };

  const featuresList = [
    'Unlimited Likes',
    'Rewind',
    '5 Super Likes a day',
    'Passport',
    'No ads',
  ];

  const paymentPlans: PaymentPlan[] = [
    {id: 1, header: 'Best value', price: 50, numOfMonths: 12, total: 600},
    {
      id: 2,
      header: 'Most popular',
      isActive: true,
      price: 70,
      numOfMonths: 6,
      total: 520,
    },
    {id: 3, header: 'Classic', price: 110, numOfMonths: 1, total: 110},
  ];
  const packagePlus: PayPackage = {
    packageName: 'Starzz Plus',
    paymentPlans,
    features: featuresList,
  };
  const packagePlatinum: PayPackage = {
    packageName: 'Starzz Platinum',
    paymentPlans,
    features: featuresList,
  };
  const packageGold: PayPackage = {
    packageName: 'Starzz Gold',
    paymentPlans,
    features: featuresList,
  };

  return (
    <View
      style={{
        flexDirection: 'row',
        position: 'absolute',
        top: 0,
        alignSelf: 'center',
        alignItems: 'center',
        zIndex: 100,
      }}
    >
      <Image
        source={require('../assets/starzlogo.png')}
        style={sideLogoStyles.imageLogo}
      />
      <Text
        style={{
          fontSize: wp(6),
          fontFamily: 'MPLUS1p-Bold',
          color: '#243443',
          left: -2,
          lineHeight: 33,
          textAlign: 'center',
          fontWeight: '900',
        }}
      >
        STAARY
      </Text>
      <TouchableOpacity
        onPress={() => {
          closePaymentModals();
          setPlusPaymentModalVisibility(true);
        }}
      >
        <Text style={{color: 'black', fontSize: 30, marginLeft: 10}}>*</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          closePaymentModals();
          setPlatinumPaymentModalVisibility(true);
        }}
      >
        <Text style={{color: '#7643F8', fontSize: 30, marginLeft: 10}}>*</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          closePaymentModals();
          setGoldPaymentModalVisibility(true);
        }}
      >
        <Text style={{color: 'orange', fontSize: 30, marginLeft: 10}}>*</Text>
      </TouchableOpacity>
      <PaymentPlanModal
        visible={plusPaymentModalVisibility}
        colors={[Colors.packages.plus, Colors.packages.plus]}
        onClose={closePaymentModals}
        iconSource={require('../assets/E_Plus.png')}
        payPackage={packagePlus}
        bulletsColor={Colors.packages.packageListBullet}
      />
      <PaymentPlanModal
        visible={platinumPaymentModalVisibility}
        colors={[Colors.packages.platinumLight, Colors.packages.platinumDark]}
        onClose={closePaymentModals}
        iconSource={require('../assets/E_Purpal.png')}
        payPackage={packagePlatinum}
        bulletsColor={Colors.packages.packageListBullet}
      />
      <PaymentPlanModal
        visible={goldPaymentModalVisibility}
        colors={[Colors.packages.goldLight, Colors.packages.goldDark]}
        onClose={closePaymentModals}
        iconSource={require('../assets/E_Gold.png')}
        payPackage={packageGold}
        bulletsColor={Colors.white}
      />
      {/* <Image source={require('../assets/STAARZ_109X19_black.png')} style={sideLogoStyles.image} /> */}
      {/* <Text style={{ textAlign: "center", fontSize: 22, fontFamily: "Montserrat-Bold" }}>STAARZ</Text> */}
      {/* <Image source={require('../assets/STAARZ_109X19_black.png')} style={sideLogoStyles.image} /> */}
    </View>
  );
}
const sideLogoStyles = StyleSheet.create({
  image: {
    // marginTop: 0,
    marginLeft: 20,
    height: 20,
    width: 100,

    resizeMode: 'contain',
  },
  imageLogo: {
    // marginBottom: 8,
    // marginLeft:5,
    left: -4,
    width: wp(7.5),
    height: wp(6.5),
    resizeMode: 'cover',
    // resizeMode: "cover"
  },
  container: {
    position: 'absolute',
    // top: 10 + getStatusBarHeight(),
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 0,
  },
});
