import React from 'react';
import {StyleSheet, View, Image} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

function CallChat({iconType, buttonContainer, ...rest}) {
  return (
    <View style={styles.buttonContainer}>
      {/* <MaterialCommunityIcons name={iconType} size={20} color="#fff" /> */}
      <Image
        source={require('../assets/Shape.png')}
        style={{height: wp(5.5), width: wp(6), resizeMode: 'contain'}}
      />
    </View>
  );
}

export default CallChat;

const styles = StyleSheet.create({
  buttonContainer: {
    width: '100%',
    height: 40,
    borderRadius: 10,
    backgroundColor: '#000',
    paddingVertical: 10,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    flexDirection: 'row',
    bottom: 1,
  },
});
