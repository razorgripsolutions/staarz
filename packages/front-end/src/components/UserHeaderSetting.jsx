import React from 'react';
import {Text, TouchableOpacity, StyleSheet, View, Image} from 'react-native';

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

function UserHeaderSetting({userName, goBack, url}) {
  // const [userName,setuserName] = useState(props.params.userName);
  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 20,
        alignItems: 'center',
        width: '100%',
      }}
    >
      <TouchableOpacity onPress={goBack} style={{width: '5%'}}>
        <FontAwesome name="angle-left" size={25} color="#000" />
      </TouchableOpacity>
      <View
        style={{
          width: '90%',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <Text
          style={{
            fontSize: wp(6),
            fontFamily: 'MPLUS1p-Bold',
            color: '#243443',
            left: 5,
            lineHeight: 33,
            textAlign: 'center',
          }}
        >
          {userName}
        </Text>
      </View>

      <View style={{width: '5%'}}>
        <Image
          source={url}
          style={{width: 30, height: 30, borderRadius: 40 / 2}}
        />
      </View>
    </View>
  );
}

export default UserHeaderSetting;

const styles = StyleSheet.create({});
