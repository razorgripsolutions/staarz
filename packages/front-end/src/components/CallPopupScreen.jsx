import React, {useState} from 'react';
import {
  ImageBackground,
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  SafeAreaView,
  StatusBar,
} from 'react-native';
// import { TouchableOpacity } from 'react-native-gesture-handler';

import Entypo from 'react-native-vector-icons/Entypo';

const {width} = Dimensions.get('window'); // full width
const {height} = Dimensions.get('window');
const image = {uri: 'https://reactjs.org/logo-og.png'};

function CallPopupScreen({
  onPressSpeaker,
  onPressMute,
  onPress,
  username,
  startTimer,
  url,
  ...rest
}) {
  // alert(startTimer)
  const [isTimerStart, setIsTimerStart] = useState(false);
  const [isStopwatchStart, setIsStopwatchStart] = useState(startTimer);
  const [timerDuration, setTimerDuration] = useState(90000);
  const [resetTimer, setResetTimer] = useState(false);
  const [resetStopwatch, setResetStopwatch] = useState(false);

  return (
    <SafeAreaView style={{flex: 1}}>
      <StatusBar backgroundColor="#008080" />
      <View
        style={{
          flex: 1,
          backgroundColor: '#000',
        }}
      >
        <ImageBackground source={url} style={styles.image}>
          <View style={styles.HederCall}>
            <Text
              style={{
                fontWeight: 'bold',
                textAlign: 'center',
                color: '#fff',
                fontSize: 20,
                alignSelf: 'center',
                letterSpacing: 0.5,
                marginTop: 10,
              }}
            >
              {username}
            </Text>
            <Text
              style={{
                fontWeight: '400',
                textAlign: 'center',
                color: '#fff',
                fontSize: 16,
                alignSelf: 'center',
                marginTop: 10,
              }}
            >
              Ringing
            </Text>
          </View>

          <View style={styles.Center}>
            <TouchableOpacity
              onPress={onPress}
              style={{
                width: 60,
                height: 60,
                borderRadius: 60 / 2,
                alignSelf: 'center',
                backgroundColor: 'red',
                alignItems: 'center',
                justifyContent: 'center',
              }}
              {...rest}
            >
              <Entypo name="cross" color="white" size={30} />
            </TouchableOpacity>
          </View>
          {/* <View style={styles.Cllbuttonview}>
    <AntDesign name="sound" color="white" size={25}    onPress={onPressSpeaker} />

    <FontAwesome name="video-camera" color="white" size={25} />

    <FontAwesome name = "microphone" color="white" size={25}  onPress={onPressMute}  />


   </View> */}
        </ImageBackground>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    // height:'95%',
    // width:'90%'
    //   justifyContent: "center"
  },
  text: {
    color: 'white',
    fontSize: 42,
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: '#000000a0',
  },
  Cllbuttonview: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    height: 90,
    backgroundColor: '#008080',
    position: 'absolute',
    // top:height/1.3
    // top:"80%"
    bottom: 0,
  },
  HederCall: {
    width: '100%',
    // flexDirection:"row",
    // justifyContent:"space-around",
    alignItems: 'center',
    height: 80,
    backgroundColor: '#008080',
    position: 'absolute',
    // top:height/1.3
    // top:"65%"
    top: 0,
  },
  Center: {
    position: 'absolute',
    alignSelf: 'center',
    top: '75%',
  },
  sectionStyle: {
    flex: 1,
    // marginTop: ,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: -2,
  },
});

const options = {
  container: {
    // backgroundColor: '#FF0000',
    padding: 5,
    borderRadius: 5,
    width: 200,
    alignItems: 'center',
  },
  text: {
    fontSize: 15,
    color: '#FFF',
    marginLeft: 7,
  },
};
export default CallPopupScreen;
