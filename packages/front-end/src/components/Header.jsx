import React from 'react';
import {View, StyleSheet, TouchableOpacity, Image} from 'react-native';
import {Text} from 'react-native-paper';
import {theme} from '../core/theme';

function Header({HederName, url, color}) {
  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 20,
        alignItems: 'center',
        width: '100%',
        top: -10,
      }}
    >
      <TouchableOpacity style={{width: '5%'}}>
        {/* <FontAwesome name="angle-left" size={25} color="#000" /> */}
      </TouchableOpacity>
      <View
        style={{width: '90%', alignItems: 'center', justifyContent: 'center'}}
      >
        <Text
          style={{
            color,
            fontSize: 54,
            fontWeight: 'bold',
            letterSpacing: 1,
            lineHeight: 74,
          }}
        >
          {HederName}
        </Text>
      </View>

      <View style={{width: '5%'}}>
        <Image
          source={url}
          style={{width: 35, height: 35, borderRadius: 35 / 2}}
        />
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  header: {
    fontSize: 21,
    color: theme.colors.primary,
    fontWeight: 'bold',
    paddingVertical: 12,
  },
});

export default Header;
