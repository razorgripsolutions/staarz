import React from 'react';
import {StyleSheet, View, Image, TouchableOpacity, Text} from 'react-native';
import {RtcLocalView} from 'react-native-agora';
import Entypo from 'react-native-vector-icons/Entypo';

function OutGoingVideo({onPress, username, url, ...rest}) {
  return (
    <View style={styles.container}>
      {/* <RNCamera
        style={{ flex: 1, alignItems: 'center' }}
        ref={ref => {
          this.camera = ref
        }}

        type={RNCamera.Constants.Type.front}
        flashMode={RNCamera.Constants.FlashMode.auto}
      /> */}
      <RtcLocalView.SurfaceView
        style={{flex: 1, alignItems: 'center'}}
        /// // channelId={this.state.channelName}
        // renderMode={VideoRenderMode.Hidden}
      />
      <Text
        style={{
          color: '#fff',
          position: 'absolute',
          top: 100,
          fontWeight: '900',
          alignItems: 'center',
          alignSelf: 'center',
          fontSize: 20,
        }}
      >
        Ringing
      </Text>

      <Image
        style={{
          height: 150,
          width: 150,
          borderRadius: 150 / 2,
          position: 'absolute',
          top: 140,
          alignSelf: 'center',
        }}
        source={url}
      />
      <Text
        style={{
          color: '#fff',
          position: 'absolute',
          top: 320,
          fontWeight: '900',
          alignItems: 'center',
          alignSelf: 'center',
          fontSize: 30,
        }}
      >
        {username}
      </Text>

      <View
        style={{height: 100, position: 'absolute', bottom: 40, width: '100%'}}
      >
        <TouchableOpacity
          onPress={onPress}
          style={{
            height: 60,
            width: 60,
            borderRadius: 60 / 2,
            backgroundColor: 'red',
            marginBottom: 20,
            alignItems: 'center',
            justifyContent: 'center',
            alignSelf: 'center',
            alignContent: 'center',
            position: 'absolute',
            bottom: 20,
          }}
        >
          {/* <Text style={styles.buttonText}> End Call </Text> */}
          {/* <MaterialIcons name="call-end"  size={28}  color="#000"/> */}

          <Entypo name="cross" size={30} color="#000" />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    // backgroundColor: 'black'
  },
});

export default OutGoingVideo;
