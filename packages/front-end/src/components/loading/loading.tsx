import React, {FC} from 'react';
import {Modal, View} from 'react-native';
import {CircleFade} from 'react-native-animated-spinkit';

interface LoadingProps {
  show: boolean;
}

export const LoadingModal: FC<LoadingProps> = ({show}) => {
  return (
    <>
      <Modal transparent visible={show}>
        <>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: 'rgba(0,0,0,0.9)',
            }}
          >
            <CircleFade size={70} color="#fff" />
          </View>
        </>
      </Modal>
    </>
  );
};
