import React from 'react';
import {TouchableOpacity, Image, StyleSheet, Platform} from 'react-native';
import {getStatusBarHeight} from 'react-native-status-bar-height';

function BackButton({goBack}) {
  return (
    <TouchableOpacity onPress={goBack} style={styles.container}>
      <Image
        style={styles.image}
        source={require('../assets/arrow_back.png')}
      />
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: Platform.OS !== 'ios' ? 15 : 10 + getStatusBarHeight(),

    left: 4,
    zIndex: 11,
  },
  image: {
    width: 24,
    height: 24,
  },
});

export default BackButton;
