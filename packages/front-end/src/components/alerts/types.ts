export interface NoInternetComponentProp {
  handleCheckInternetConnection: () => Promise<void>;
}
export interface EmptyComponentProp {
  model: string;
}
