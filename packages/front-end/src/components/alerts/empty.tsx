import * as React from 'react';
import {View, StyleSheet} from 'react-native';
import {Colors} from '../../constants/colors';
import {Text} from 'react-native-paper';
import {EmptyComponentProp} from './types';

function EmptyState({model}: EmptyComponentProp) {
  return (
    <React.Fragment>
      <View style={styles.container}>
        <Text style={styles.titleText}>No {model} found</Text>
        <Text style={styles.descriptionText}>
          Lorem, ipsum dolor sit amet consectetur.
        </Text>
      </View>
    </React.Fragment>
  );
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 0.8,
    paddingHorizontal: 4,
  },
  titleText: {
    color: Colors.gray['800'],
    fontSize: 20,
  },
  descriptionText: {
    color: Colors.gray['500'],
    fontSize: 13,
    textAlign: 'center',
  },
});

export {EmptyState};
