import * as React from 'react';
import {
  Dimensions,
  Image,
  Platform,
  TouchableOpacity,
  View,
  StyleSheet,
} from 'react-native';
import {Text} from 'react-native-paper';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {string as DescriptiveLabels} from '../../const/string';
import {Colors} from '../../constants/colors';
import {NoInternetComponentProp} from './types';

const {height} = Dimensions.get('window');

function NoInternet({handleCheckInternetConnection}: NoInternetComponentProp) {
  return (
    <View style={style.container}>
      <Image
        source={require('../../assets/internat.png')}
        style={style.noInternetImage}
      />
      <Text style={style.noInternetLabel}>{DescriptiveLabels.network}</Text>
      <TouchableOpacity
        style={style.tryAgainButtonContainer}
        onPress={handleCheckInternetConnection}
      >
        <View style={style.tryAgainInnerContainer}>
          {Platform.OS === 'ios' ? (
            <Text style={style.tryAgainText}>
              {DescriptiveLabels.Try_Again}
            </Text>
          ) : (
            <Text style={style.tryAgainText}>
              {DescriptiveLabels.Try_Again}
            </Text>
          )}
        </View>
      </TouchableOpacity>
    </View>
  );
}

const style = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  noInternetImage: {
    height: 50,
    width: 50,
  },
  noInternetLabel: {
    fontSize: 17,
    fontWeight: '700',
    marginTop: 5,
  },
  tryAgainButtonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    top: height * 0.3,
  },
  tryAgainInnerContainer: {
    backgroundColor: Colors.primary,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    paddingHorizontal: 30,
    paddingVertical: 20,
    marginTop: 10,
  },
  tryAgainText: {
    fontSize: hp('2'),
    color: '#fff',
  },
});

export {NoInternet};
