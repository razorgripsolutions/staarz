/* eslint-disable import/extensions */
/* eslint-disable import/no-unresolved */
import {RealTimeAPI} from 'rocket.chat.realtime.api.rxjs';
import {
  ROCKETCHAT_HOST,
  ROCKETCHAT_PORT,
  ROCKETCHAT_USE_SSL,
} from '../core/configuration';

const realTimeAPI = new RealTimeAPI(
  `${
    ROCKETCHAT_USE_SSL ? 'wss' : 'ws'
  }://${ROCKETCHAT_HOST}:${ROCKETCHAT_PORT}/websocket`,
);
// const realTimeAPI =  new RealTimeAPI("ws://localhost:8080/");
const chatClient = connectToServer();
/**
 * connect to chat server and keep alive
 */
function connectToServer() {
  realTimeAPI.connectToServer();

  // console.log("realTimeAPI => "+JSON.stringify(realTimeAPI));
  return realTimeAPI;
}

/**
 * call realtime api for starting session with given token
 *
 * @param {string} token
 */
export function authenticateWithToken(token) {
  connectToServer();
  return chatClient.loginWithAuthToken(token);
}

/**
 * load history of channel
 * @param {string} channel
 * @param {ts} lastTs
 * @param {function} callback
 */
export function loadHistory(channel, lastTs, callback) {
  chatClient
    .callMethod('loadHistory', channel, lastTs, 20)
    .forEach((message) => {
      callback(message);
    });
}

/**
 * subscrip to channel whenever new message comes call on Message
 *
 * @param {string} channel
 * @param {function} onMessage
 */
export function messageSubscription(channel, onNewMessage) {
  try {
    // chatClient.keepAlive().subscribe();
    return chatClient
      .getSubscription('stream-room-messages', channel, false)
      .subscribe(
        (message) => {
          onNewMessage(message);
        },
        (error) => console.error('getSubscription error', error),
        () => console.log('completed OnMessage'),
      );
  } catch (error) {
    // ???
  }
}
export function chatkeepAlive() {
  return chatClient.keepAlive().subscribe();
}
export function sendMessage(channel, message, callback) {
  try {
    chatClient
      .callMethod('sendMessage', {rid: channel, msg: message})
      .forEach((response) => {
        callback(response);
      });
  } catch (error) {
    console.log('error--------rcchat====', error);
  }
}
export function onMessage(channel, message) {
  // alert('work')
  chatClient.onMessage(message);
}

// export function getObservable() {
//     alert('getObservable')
//     RealTimeAPI.getObservable()
//    //return chatClient.getObservable()
// }
export function setStatus(status) {
  chatClient.callMethod('UserPresence:setDefaultStatus', status);
}
export function clear() {
  chatClient.disconnect();
}

export {chatClient};
