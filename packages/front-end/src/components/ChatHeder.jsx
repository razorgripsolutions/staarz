import React from 'react';
import {
  Text,
  TouchableOpacity,
  StyleSheet,
  View,
  Image,
  Dimensions,
} from 'react-native';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {windowWidth} from '../../utilis/Dimentions';

const {width} = Dimensions.get('window'); // full width
const {height} = Dimensions.get('window');
function ChatHeder({userName, url, status, ...rest}) {
  // alert(JSON.stringify(props.params.userName))
  // const [userName,setuserName] = useState(props.params.userName);
  return (
    <>
      <View style={styles.buttonContainer}>
        <View style={styles.chatView}>
          <Image style={styles.userProfile} source={url} />

          <View style={{justifyContent: 'center'}}>
            <Text numberOfLines={1} style={styles.UserName}>
              {userName}
            </Text>
            <Text numberOfLines={1} style={styles.LastSeen}>
              {status === 0 ? 'Offline' : 'Online'}
            </Text>
          </View>
        </View>
        <View style={{width: '20%'}}>
          <TouchableOpacity
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              width: width === 320 || height === 610.6666666666666 ? 25 : 29,
              height: width === 320 || height === 610.6666666666666 ? 25 : 29,
              backgroundColor: '#fff',
              borderRadius: windowWidth / 2,
              alignSelf: 'flex-end',
              elevation: 10,
              shadowColor: '#000',

              shadowOffset: {
                width: 0,
                height: 3,
              },
              shadowOpacity: 0.27,
              shadowRadius: 4.65,

              // elevation: 6,
            }}
            {...rest}
          >
            <MaterialIcons
              name="block"
              size={width === 320 || height === 610.6666666666666 ? 20 : 23}
              color="red"
            />
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
}

export default ChatHeder;

const styles = StyleSheet.create({
  buttonContainer: {
    width: '95%',
    paddingVertical: 12,
    backgroundColor: '#fff',
    alignItems: 'center',
    // marginHorizontal:10,
    borderBottomWidth: 0.8,
    borderBottomColor: '#ccc',
    // marginHorizontal:20,
    zIndex: 100,
    alignSelf: 'center',
    // top: 0,
    paddingHorizontal: 8,
    // position: "absolute",

    flexDirection: 'row',
  },
  UserName: {
    fontSize: 16,
    fontWeight: '700',
    marginLeft: 10,
    color: '#1E2022',
    letterSpacing: 1.09,
    lineHeight: 17,
    marginBottom: 2,
    fontFamily: 'SFUIText-Semibold',
  },
  LastSeen: {
    fontSize: 12,
    // fontWeight: '500',
    marginTop: 2,
    marginLeft: 10,
    color: '#8A98AC',
    letterSpacing: 1.5,
    lineHeight: 14,
    fontFamily: 'SFUIText-Semibold',
  },
  chatView: {
    width: '80%',
    flexDirection: 'row',
  },
  userProfile: {
    width: width === 320 || height === 610.6666666666666 ? hp(8) : hp(7.5),
    height: width === 320 || height === 610.6666666666666 ? hp(8) : hp(7.5),
    borderRadius:
      width === 320 || height === 610.6666666666666 ? hp(8) / 2 : hp(7.5) / 2,
  },
  container1: {
    // position: 'absolute',
    top: 10 + getStatusBarHeight(),
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 0,
  },
  image: {
    marginTop: 0,
    marginLeft: 20,
    // paddingVertical:8,
    width: hp('15'),
    resizeMode: 'stretch',
    height: hp('2.5'),
  },
  imageLogo: {
    marginBottom: 8,
    width: hp('3'),

    resizeMode: 'stretch',
    height: hp('3'),
  },
  cotainer: {
    flex: 1,
    marginTop: 40,
  },
});
