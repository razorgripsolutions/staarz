import React from 'react';
import {Text, StyleSheet, View, Image} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

function Callstart({iconType, title, color, buttonContainer, ...rest}) {
  return (
    <View>
      <View style={styles.buttonContainer}>
        {/* <MaterialIcons name={iconType} size={30} color={color} /> */}
        <Image
          source={iconType}
          style={{height: wp(7), width: wp(7), resizeMode: 'contain'}}
        />
      </View>
      <Text style={styles.title}>{title}</Text>
    </View>
  );
}

export default Callstart;

const styles = StyleSheet.create({
  buttonContainer: {
    width: '100%',
    height: '70%',
    borderRadius: 15,
    backgroundColor: '#F3F6FF',
    // paddingVertical:8,
    alignItems: 'center',
    justifyContent: 'center',
    // bottom:1,
    // flexDirection:'row',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 14,
    marginTop: 5,
    width: '100%',
    lineHeight: 17,
    color: '#000000',
    fontFamily: 'SFUIText-Semibold',
    alignSelf: 'center',
    //   alignSelf:"center",
    textAlign: 'center',
  },
});
