import React from 'react';
import {
  ImageBackground,
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  SafeAreaView,
  StatusBar,
} from 'react-native';
// import { TouchableOpacity } from 'react-native-gesture-handler';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const {width} = Dimensions.get('window'); // full width
const {height} = Dimensions.get('window');
const image = {uri: 'https://reactjs.org/logo-og.png'};
function VideoCallIncoming({onPress, anserer, username, url, ...rest}) {
  return (
    <SafeAreaView style={{flex: 1}}>
      <StatusBar backgroundColor="#008080" />
      <View
        style={{
          flex: 1,
          backgroundColor: '#000',
        }}
      >
        <ImageBackground source={url} style={styles.image}>
          <View style={styles.HederCall}>
            <Text
              style={{
                fontWeight: 'bold',
                textAlign: 'center',
                color: '#fff',
                fontSize: 20,
                alignSelf: 'center',
                letterSpacing: 0.5,
                marginTop: 10,
              }}
            >
              {username}
            </Text>
            <Text
              style={{
                fontWeight: '400',
                textAlign: 'center',
                color: '#fff',
                fontSize: 16,
                alignSelf: 'center',
                letterSpacing: 0.5,
                marginBottom: 10,
              }}
            >
              incoming Video call
            </Text>
          </View>

          <View style={styles.Center}>
            <TouchableOpacity
              onPress={onPress}
              style={{
                width: 60,
                height: 60,
                borderRadius: 60 / 2,
                alignSelf: 'center',
                backgroundColor: 'red',
                alignItems: 'center',
                justifyContent: 'center',
              }}
              {...rest}
            >
              <MaterialIcons name="call-end" color="white" size={25} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={anserer}
              style={{
                width: 60,
                height: 60,
                borderRadius: 60 / 2,
                alignSelf: 'center',
                backgroundColor: 'green',
                alignItems: 'center',
                justifyContent: 'center',
              }}
              {...rest}
            >
              <MaterialCommunityIcons name="video" color="white" size={25} />
            </TouchableOpacity>
          </View>
          {/* <View style={styles.Cllbuttonview}>
<AntDesign name="sound" color="white" size={25} />
<FontAwesome name="video-camera" color="white" size={25} />

<FontAwesome name="microphone" color="white" size={25} />
</View> */}
        </ImageBackground>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    //   justifyContent: "center"
  },
  text: {
    color: 'white',
    fontSize: 42,
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: '#000000a0',
  },
  Cllbuttonview: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    height: 80,
    backgroundColor: '#008080',
    position: 'absolute',
    // top:height/1.3
    // top:"80%"
    bottom: 0,
  },
  HederCall: {
    width: '100%',
    // flexDirection:"row",
    justifyContent: 'space-around',
    alignItems: 'center',
    height: 80,
    backgroundColor: '#008080',
    position: 'absolute',
    // top:height/1.3
    // top:"65%"
    top: 0,
  },
  Center: {
    // top:"75%"
    justifyContent: 'space-around',
    marginTop: height / 1.5,
    flexDirection: 'row',
  },
});
export default VideoCallIncoming;
