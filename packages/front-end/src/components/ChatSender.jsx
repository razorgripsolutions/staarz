import React, {useState} from 'react';
import {StyleSheet, View} from 'react-native';
import VideoCallChat from './VideoCallChat';
import ChatText from './ChatText';
import CallChat from './CallChat';

function ChatSender({buttonTitle, UserName, LastSeen, ...rest}) {
  const [typing, setEmail] = useState({typing: ''});
  return (
    <View style={styles.buttonContainer}>
      <View style={styles.Sendertext}>
        <ChatText
          labelValue={typing}
          onChangeText={(text) => setEmail({typing: text})}
          placeholderText="Type a meaasge"
          iconType="mic"
          keyboardType="email-address"
          autoCapitalize="none"
          autoCorrect={false}
        />
      </View>
      <View style={styles.View2}>
        <View style={{width: '40%'}}>
          <VideoCallChat iconType="videocam" />
        </View>
        <View style={{width: '40%'}}>
          <CallChat iconType="send" />
        </View>
      </View>
    </View>
  );
}

export default ChatSender;

const styles = StyleSheet.create({
  buttonContainer: {
    width: '100%',
    paddingVertical: 8,
    backgroundColor: '#fff',
    borderTopWidth: 0.8,
    borderBottomColor: '#ccc',
    flexDirection: 'row',
  },
  Sendertext: {
    width: '70%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  UserName: {
    fontSize: 16,
    fontWeight: 'bold',
    marginLeft: 10,
    color: '#000',
    letterSpacing: 2,
  },
  View2: {
    width: '30%',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection: 'row',
  },
});
