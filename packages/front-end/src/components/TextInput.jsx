import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  Dimensions,
  Image,
  TouchableOpacity,
} from 'react-native';
import {TextInput as Input} from 'react-native-paper';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {theme} from '../core/theme';

const {width} = Dimensions.get('window'); // full width
const {height} = Dimensions.get('window');

function TextInput({
  errorText,
  description,
  icon,
  onPress,
  colorIcon,
  url,
  ...props
}) {
  return (
    <View style={styles.container}>
      <View style={{width: '100%', flexDirection: 'row', alignSelf: 'center'}}>
        <View style={{width: '100%'}}>
          <Input
            style={styles.input}
            selectionColor={theme.colors.primary}
            underlineColor="transparent"
            mode="outlined"
            dense={
              width === 320 || height === 610.6666666666666 ? 'true' : null
            }
            {...props}
          />
        </View>
        <View
          style={{width: '10%', alignItems: 'center', justifyContent: 'center'}}
        >
          <TouchableOpacity onPress={onPress} style={{zIndex: 110, right: 40}}>
            <Image
              source={url}
              style={{height: wp(6), width: wp(7), zIndex: 10, marginTop: 5}}
            />
          </TouchableOpacity>
          {/* <Entypo style={{ left: -36, top: 23, zIndex: 10 }} name={icon} size={20} color={colorIcon} onPress={onPress} /> */}
        </View>
      </View>
      {description && !errorText ? (
        <Text style={styles.description}>{description}</Text>
      ) : null}
      {errorText ? <Text style={styles.error}>{errorText}</Text> : null}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: width / 1.1,
    marginVertical: '2%',
  },
  input: {
    width: '100%',
  },
  description: {
    fontSize: hp('1.8'),
    color: theme.colors.secondary,
    paddingTop: 8,
  },
  error: {
    fontSize: hp('1.8'),
    color: theme.colors.error,
    paddingTop: 8,
  },
});

export default TextInput;
