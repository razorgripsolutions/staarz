import {BackHandler} from 'react-native';

export default function BackPressHandler(callback) {
  BackHandler.addEventListener('hardwareBackPress', () => {
    callback();
    return true;
  });
}
