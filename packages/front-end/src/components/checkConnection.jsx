import NetInfo from '@react-native-community/netinfo';

export const checkConnection = () =>
  NetInfo.fetch().then((state) => state.isConnected);
