import React from 'react';
import {StyleSheet, View, Image} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

function VideoCallChat({iconType, buttonContainer, ...rest}) {
  return (
    <View style={styles.buttonContainer}>
      {/* <MaterialIcons name={iconType} size={20} color="#fff" /> */}
      <Image
        source={require('../assets/Video.png')}
        style={{height: wp(8), width: wp(7.5), resizeMode: 'contain'}}
      />
    </View>
  );
}

export default VideoCallChat;

const styles = StyleSheet.create({
  buttonContainer: {
    width: '100%',
    height: 40,
    borderRadius: 10,
    backgroundColor: '#90ee90',
    paddingVertical: 8,
    alignItems: 'center',
    justifyContent: 'center',
    bottom: 1,
    flexDirection: 'row',
  },
});
