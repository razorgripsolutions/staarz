import React from 'react';
import {StyleSheet} from 'react-native';
import {Button as PaperButton} from 'react-native-paper';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {theme} from '../core/theme';

function Button({mode, style, ...props}) {
  return (
    <PaperButton
      style={[
        styles.button,
        mode === 'outlined' && {backgroundColor: theme.colors.surface},
        style,
      ]}
      labelStyle={styles.text}
      mode={mode}
      {...props}
    />
  );
}

const styles = StyleSheet.create({
  button: {
    width: '100%',
    marginVertical: hp('1%'),
    paddingVertical: hp('-0.5%'),
  },
  text: {
    fontWeight: 'bold',
    fontSize: 15,
    lineHeight: hp('3%'),
  },
});

export default Button;
