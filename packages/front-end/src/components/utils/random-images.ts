export const handlePickRandomImages = (
  length: number,
  imagesData: string[],
): string[] => {
  const randomImages: string[] = [];
  for (let i = 0; i < length; i++) {
    const randomImage: string =
      imagesData[Math.floor(Math.random() * imagesData.length)];
    randomImages.push(randomImage);
  }
  return randomImages;
};
