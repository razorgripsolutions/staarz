import React from 'react';
import {View, Text, Dimensions} from 'react-native';
import {Colors} from '../constants/colors';
import DropDownPicker from 'react-native-dropdown-picker';

const {width} = Dimensions.get('window');
const {height} = Dimensions.get('window');

export default function DropDown({
  item,
  onChangeItem,
  defaultValue,
  heading,
  max,
  min,
  onChangeItemMultiple,
  multiple,
}) {
  return (
    <View>
      <Text
        style={{
          color: '#101010',
          textAlign: 'left',
          marginBottom: 8,
          marginTop: 15,
          alignContent: 'flex-start',
          fontWeight: '600',
          fontSize: width === 320 || height === 610.6666666666666 ? 14 : 16,
          lineHeight: 21,
          fontFamily: 'SFUIText-Semibold',
        }}
      >
        {heading}
      </Text>
      <DropDownPicker
        items={item}
        multiple={multiple}
        labelStyle={{
          fontSize: 14,
          textAlign: 'left',
          color: Colors.gray['600'],
        }}
        dropDownDirection="TOP"
        defaultValue={defaultValue}
        containerStyle={{height: 50}}
        style={{backgroundColor: '#fff', width: '100%'}}
        itemStyle={{
          justifyContent: 'flex-start',
        }}
        max={max}
        min={min}
        dropDownStyle={{backgroundColor: '#fff'}}
        onChangeItem={(newItem) => onChangeItem?.(newItem)}
        onChangeItemMultiple={(newItem) => onChangeItemMultiple?.({...newItem})}
      />
    </View>
  );
}
