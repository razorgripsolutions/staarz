// const baseUrl = "http://192.168.1.23:6060/";
// export const backendBaseUrl = "http://192.168.1.23:8080/";
// http://52.3.61.123:3000/api/v1/users.delete
const baseUrl = 'http://52.27.46.4:7070/';
export const backendBaseUrl = 'http://loaclhost:7070/';
// const baseUrl = "https://staarz.tomsweb.xyz/";
// export const backendBaseUrl = "https://staarz.tomsweb.xyz/";
export const url = {
  login: `${baseUrl}auth/userSignIn`, // done
  signup: `${baseUrl}auth/userSignUp`, // done
  registerUserBioScreen: `${baseUrl}auth/registerUserBio`, // Done
  socialProfile: `${baseUrl}auth/socialProfile`, // Done
  uploadYourPhoto: `${baseUrl}upload/profilePicPath`, // Done
  setYourPhoto: `${baseUrl}auth/profilePicPath`, // Done
  verifyYourPhoto: `${baseUrl}auth/photoVerificationPath`, // Done
  fetchReligionList: `${baseUrl}auth/fetchReligion`, // Done
  fetchRelationShipType: `${baseUrl}auth/fetchRelationShipType`, // Done
  fetchFavMusicStyleList: `${baseUrl}auth/favouriteMusicStyleList`, // Done
  fetchFavMovieGenreList: `${baseUrl}auth/fetchfavouriteMovieGenre`, // Done
  fetchFavPetList: `${baseUrl}auth/fetchUserPetList`, // Done
  fetchFavTravelStyleList: `${baseUrl}auth/fetchTravelStyle`, // Done
  tellUsMoreAboutYou1Submit: `${baseUrl}auth/moreAbout1`,
  tellUsMoreAboutYou2Submit: `${baseUrl}auth/moreAbout2`,
  tellUsMoreAboutYou3Submit: `${baseUrl}auth/moreAbout3`,
  addPhotoVideoSubmit: `${baseUrl}auth/galleryPhotos`,
  fetchProfileAllProfile: `${baseUrl}auth/fetchAllUserList`,
  fetchProfileForMatch: `${baseUrl}auth/fetchUserWithFilter`,
  uploadYourPicture: `${baseUrl}auth/uploadYourPicture`,
  fbSignUp: `${baseUrl}auth/userFBSignUp`,
  fbSignIn: `${baseUrl}auth/userFBSignIn`,
  googleSignUp: `${baseUrl}auth/userGoogleSignUp`,
  googleSignIn: `${baseUrl}auth/userGoogleSignIn`,
  uploadSingleImage: `${baseUrl}upload/single`,
  verifyUploadSingleImage: `${baseUrl}upload/verifySingle`,
  uploadGalleryMedia: `${baseUrl}upload/galleryImagePath`,
  requestForVerification: `${baseUrl}auth/requestForVerification`,
  fetchInterestList: `${baseUrl}auth/fetchInterestList`, // Done
  swipeUpdate: `${baseUrl}auth/updateMatching`,
  channellist: `${baseUrl}auth/channelList`, // Done
  createChannel: `${baseUrl}auth/createChannel`,
  itsMatch: `${baseUrl}auth/fetchMatchingUsers`,
  LikeStatuschek: `${baseUrl}auth/fetchMatchingStatus`,
  sendMsg: `${baseUrl}auth/sendMessage`,
  pakageList: `${baseUrl}auth/pakageList`, // Done
  addUserSubscribtion: `${baseUrl}auth/addUserSubscribtion`,
  updateUserSubscribtion: `${baseUrl}auth/updateUserSubscribtion`,
  fetchUserSubscribtion: `${baseUrl}auth/fetchUserSubscribtion`,
  blockListByUserId: `${baseUrl}auth/blockListByUserId`,
  unblockUser: `${baseUrl}auth/unblockUser`,
  Payment: `${baseUrl}auth/payment`,
  MyLikes: `${baseUrl}auth/MyLikes`, // Done
  whoLikeMe: `${baseUrl}auth/whoLikeMe`, // Done
  getUserRCIDByUserId: `${baseUrl}getUserRCIDByUserId`,
  getUserPicGallary: `${baseUrl}auth/getUserPicGallary`,
  userSettings: `${baseUrl}auth/userSettings`, // Done
  getUserSettings: `${baseUrl}auth/getUserSettings`, // Done
  setOnlineStatus: `${baseUrl}auth/setOnlineStatus`,
  editUserProfile: `${baseUrl}auth/editUserProfile`, // Done
  userInformation: `${baseUrl}auth/userInformation`, // Done
  getFirebaseTokenByUserId: `${baseUrl}auth/getFirebaseTokenByUserId`,
  userForgotPassword: `${baseUrl}auth/userForgotPassword`,
  addToBlock: `${baseUrl}auth/addToBlock`,
  reasonOfBlockUserList: `${baseUrl}auth/reasonOfBlockUserList`,
  matchEmailVerificationCode: `${baseUrl}auth/matchEmailVerificationCode`,
  setFirebaseTokenByUserId: `${baseUrl}auth/setFirebaseTokenByUserId`,
  rewind: `${baseUrl}auth/rewind`,
  unmatch: `${baseUrl}auth/unmatch`,
  getOccupationList: `${baseUrl}auth/getOccupationList`,
  getEducation: `${baseUrl}auth/getEducation`,
  getOnlineStatus: `${baseUrl}auth/getOnlineStatus`,
  boost: `${baseUrl}auth/boostUser`,
  getBoostuser: `${baseUrl}auth/getBoostUser`,
  readReceipt: `${baseUrl}auth/readReceipt`,
  aboutToExpire: `${baseUrl}auth/aboutToExpire`,
  callActiveStatus: `${baseUrl}auth/ifCallBusy`,
};
export const appId = '117427410258092';
