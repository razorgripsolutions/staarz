import {Dimensions, StyleSheet, Platform} from 'react-native';

const dimensions = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};

export const heding = StyleSheet.create({
  headingText: {
    textAlign: 'left',
    marginTop: 18,
    marginBottom: 8,
    alignContent: 'flex-start',
    color: '#1E263C',
    lineHeight: 22,
    //   letterSpacing:0.9,
    fontWeight: Platform.OS === 'ios' ? '700' : null,
    fontFamily: 'SFUIText-Semibold',
  },
});
