const post = async (urlPath, data) => {
  let response = {};
  await fetch(urlPath, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  })
    .then(async (result) => {
      response = await result.json();
    })
    .catch((error) => {
      console.log(error);
      // alert(error)≥
    });
  return response;
};

const get = async (urlPath) => {
  let response = {};
  await fetch(urlPath, {
    method: 'GET',
  })
    .then(async (result) => {
      response = await result.json();
    })
    .catch((error) => {
      console.log(error);
    });
  return response;
};

export default {get, post};
