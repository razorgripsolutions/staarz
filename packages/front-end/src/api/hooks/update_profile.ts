import {AxiosResponse} from 'axios';
import {useMutation} from 'react-query';
import {patch} from '../helpers';
import {API_ENDPOINTS} from '../routes';
import {UserProfileResponse} from './email_login';

export type UpdateProfileRequest = {
  request: UserProfileResponse;
  userId: number;
};
export type UpdateProfileResponse = {};

export const useUpdateProfile = () =>
  useMutation<
    any,
    any,
    UpdateProfileRequest,
    AxiosResponse<UpdateProfileResponse>
  >((formData) =>
    patch<UserProfileResponse>(
      API_ENDPOINTS.updateProfile(formData?.userId),
      formData?.request,
    ),
  );
