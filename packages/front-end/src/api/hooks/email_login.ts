import {AxiosResponse} from 'axios';
import {useMutation} from 'react-query';
import {post} from '../helpers';
import {API_ENDPOINTS} from '../routes';
import jwt_decode from 'jwt-decode';
import {Gender} from './email_signup';

export type EmailLoginRequest = {
  emailAddress: string;
  password: string;
};

export type EmailLoginResponse = {
  access_token: string;
  message: 'You have to verify your emaill address before you can login !';
};

export type UserProfileResponse = {
  emailAddress: string;
  id: number;
  profilePicture: string | null;
  fullName: string;
  gender: Gender;
  intrestedIn: Gender;
  typeOfRelationship: string;
  height: string;
  occupation: string;
  haveCar: number;
  liveAt: string;
  aboutMe: string;
  smokes: number;
  interests: string[];
  music: string;
  pets: string[];
  movies: string;
  boosted: number;
  travellStyle: string;
  isFirstLogin: boolean;
};

type DecodedTokeResponse = {
  emailAddress: string;
  exp: number;
  fullName: string;
  lat: number;
  id: number;
};

export type SendEmailVerificationCodeRequest = {
  emailAddress: string;
};

export type SendEmailVerificationCodeResponse = {
  message: string;
  otp: string;
};

export type VerifyEmailRequest = {
  emailAddress: string;
  code: string;
};

export type VerifyEmailResponse = {};

export const useBasicEmailLogin = () =>
  useMutation<any, any, EmailLoginRequest, AxiosResponse<EmailLoginResponse>>(
    (formData) =>
      post<EmailLoginRequest>(API_ENDPOINTS.basicEmailLogin, formData),
  );

export const useSendEmailVerification = () =>
  useMutation<
    any,
    any,
    SendEmailVerificationCodeRequest,
    AxiosResponse<SendEmailVerificationCodeResponse>
  >((formData) =>
    post<SendEmailVerificationCodeRequest>(
      API_ENDPOINTS.sendEmailVerificationCode,
      formData,
    ),
  );

export const useVerifyEmail = () =>
  useMutation<any, any, VerifyEmailRequest, AxiosResponse<VerifyEmailResponse>>(
    (formData) => post<VerifyEmailRequest>(API_ENDPOINTS.verifyEmail, formData),
  );

export const getUserProfile = async (
  token: string,
): Promise<UserProfileResponse> => {
  const decoded: DecodedTokeResponse = await jwt_decode(token);
  const response = await fetch(API_ENDPOINTS.userProfile(decoded.id), {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  if (!response.ok) {
    throw new Error(response.statusText);
  }
  const result: UserProfileResponse = await response.json();
  return result;
};
