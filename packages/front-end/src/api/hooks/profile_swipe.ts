import {AxiosResponse} from 'axios';
import {useMutation} from 'react-query';
import {get, post} from '../helpers';
import {API_ENDPOINTS} from '../routes';

export type BoostRequest = {};
export const useGetUsers = () =>
  useMutation<any, any, any, AxiosResponse<any>>('users', () =>
    get(API_ENDPOINTS.profileSwipeUsers),
  );

export const useBoostUser = () =>
  useMutation<any, any, BoostRequest, AxiosResponse<any>>((formData) =>
    post(API_ENDPOINTS.boostUser),
  );

export const useUnwindUser = () =>
  useMutation<any, any, BoostRequest, AxiosResponse<any>>(() =>
    post(API_ENDPOINTS.unwindUser),
  );
