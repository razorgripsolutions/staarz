import {AxiosResponse} from 'axios';
import {useMutation, useQueryClient} from 'react-query';
import {post} from '../helpers';
import {API_ENDPOINTS} from '../routes';

export type BlockUserRequest = {
  userId: number;
};
export type BlockUserResponse = {};

export const useBlockUser = () => {
  const queryClient = useQueryClient();
  return useMutation<
    any,
    any,
    BlockUserRequest,
    AxiosResponse<BlockUserResponse>
  >((formData) => post(API_ENDPOINTS.blockUser(formData.userId)), {
    onSuccess: () => {
      queryClient.invalidateQueries('blocked-users');
    },
  });
};

export const useUnblockUser = () =>
  useMutation<any, any, BlockUserRequest, AxiosResponse<BlockUserResponse>>(
    (formData) => post(API_ENDPOINTS.unblockUser(formData.userId)),
  );
