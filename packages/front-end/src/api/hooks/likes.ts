import {AxiosResponse} from 'axios';
import {useMutation, useQueryClient} from 'react-query';
import {get, post, delete_request} from '../helpers';
import {API_ENDPOINTS} from '../routes';

export type LikesResponse = {
  emailAddress: string;
  id: number;
  profilePicture: string | null;
  fullName: string;
};

export type LikeRequest = {
  userId: number;
};

export const useGetLikes = () =>
  useMutation<any, any, LikeRequest, AxiosResponse<LikesResponse[]>>(
    (formData) => get(API_ENDPOINTS.likes(formData.userId)),
  );

export const useGetMyLikes = () =>
  useMutation<any, any, any, AxiosResponse<LikesResponse[]>>(() =>
    get(API_ENDPOINTS.myLikes),
  );

export const useLikeUser = () => {
  const queryClient = useQueryClient();
  return useMutation<any, any, LikeRequest, AxiosResponse<any>>(
    (formData) => post(API_ENDPOINTS.likeUser(formData.userId)),
    {
      onSuccess() {
        queryClient.invalidateQueries('users');
      },
    },
  );
};

export const useUnLikeUser = () => {
  const queryClient = useQueryClient();
  return useMutation<any, any, LikeRequest, AxiosResponse<any>>(
    (formData) => delete_request(API_ENDPOINTS.likeUser(formData.userId)),
    {
      onSuccess() {
        queryClient.invalidateQueries('users');
      },
    },
  );
};
