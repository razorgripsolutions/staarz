import {AxiosResponse} from 'axios';
import {useMutation} from 'react-query';
import {post} from '../helpers';
import {API_ENDPOINTS} from '../routes';

export type Gender = 'Male' | 'Female';

export type BasicEmailSignupRequest = {
  emailAddress?: string;
  password?: string;
  gender?: Gender;
  religion?: string;
  dob?: string;
  location?: number[];
  fullName?: string;
  intrestedIn: Gender;
  typeOfRelationship: string;
  height: string;
  occupation: string;
  haveCar: number;
  aboutMe: string;
  smokes: number;
  interests: string[];
  musics: string[];
  pets: string[];
  id?: number;
};

export type BasicEmailSignupResponse = {
  emailAddress: string;
};

export type BasicEmailResponseError = {
  message: string;
};

export const useBasicEmailSignup = () =>
  useMutation<
    any,
    any,
    BasicEmailSignupRequest,
    AxiosResponse<BasicEmailSignupResponse | BasicEmailResponseError>
  >((formData) =>
    post<BasicEmailSignupRequest>(API_ENDPOINTS.basicEmailSignup, formData),
  );

export const handleVerifyImages = async (): Promise<boolean> => {
  const response = await fetch(API_ENDPOINTS.verifyProfile, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
    },
  });
  if (!response.ok) {
    throw new Error(response.statusText);
  }
  const results: boolean = await response.json();
  return results;
};
