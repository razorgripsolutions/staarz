import {AxiosResponse} from 'axios';
import {useMutation} from 'react-query';
import {post} from '../helpers';
import {API_ENDPOINTS} from '../routes';

export type FacebookUserDetails = {
  email: string;
  id: string;
  picture: {
    data: {
      url: string;
    };
  };
  last_name: string;
  name: string;
  first_name: string;
  occupation?: string;
};
export type FacebookLoginRequest = {
  access_token: string;
};

export type FacebookLoginResponse = {
  access_token: string;
};

export const useLoginWithFacebook = () =>
  useMutation<
    any,
    any,
    FacebookLoginRequest,
    AxiosResponse<FacebookLoginResponse>
  >((formData) =>
    post<FacebookLoginRequest>(API_ENDPOINTS.loginWithFacebook, formData),
  );
