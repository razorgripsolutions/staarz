import {AxiosResponse} from 'axios';
import {useMutation} from 'react-query';
import {get} from '../helpers';
import {API_ENDPOINTS} from '../routes';
import {Gender} from './email_signup';

export type MatchResponse = {
  emailAddress: string;
  id: number;
  profilePicture: string;
  fullName: string;
  gender: Gender;
  intrestedIn: Gender;
  typeOfRelationship: string;
  height: string;
  occupation: string;
  haveCar: string;
  liveAt: string;
  aboutMe: string;
  smokes: string;
  interests: string[];
  music: string;
  pets: string[];
  movies: string;
  boosted: number;
  travellStyle: string;
};

export type MatchRequest = {
  userId: number;
};
export const useGetMatch = () =>
  useMutation<any, any, MatchRequest, AxiosResponse<MatchResponse[]>>(
    (formData) => get(API_ENDPOINTS.matches(formData.userId)),
  );
