import {BASE_URL} from '../store';

export const API_ENDPOINTS = {
  loginWithFacebook: 'auth/facebook/token',
  basicEmailSignup: 'auth/signUp',
  basicEmailLogin: 'auth/signIn',
  sendEmailVerificationCode: 'auth/sendEmailVerificationCode',
  verifyEmail: 'auth/verifyEmail',
  userProfile: (userId: number) => `${BASE_URL}users/profile/${userId}`,
  verifyProfile: `${BASE_URL}profile/verify`,
  uploadGalleryMedia: `${BASE_URL}users/profile/files`,
  profileSwipeUsers: `${BASE_URL}users/profile`,
  matches: (userId: number) => `users/${userId}/matches`,
  likes: (userId: number) => `users/profile/${userId}/likes`,
  likeUser: (userId: number) => `users/profile/${userId}/likes`,
  updateProfile: (userId: number) => `users/profile/${userId}`,
  unwindUser: 'users/profile/unwind',
  boostUser: 'users/profile/boost',
  myLikes: 'users/profile/my/likes',
  getChat: 'chat/getchat',
  blockUser: (userId: number) => `users/${userId}/block`,
  unblockUser: (userId: number) => `users/${userId}/unblock`,
  getBlockedUsers: 'users/my/blocked',
};
