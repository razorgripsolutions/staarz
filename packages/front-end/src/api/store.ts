import AsyncStorage from '@react-native-async-storage/async-storage';

export const BASE_URL = 'https://lettestest.com/';
export const USER_TOKEN_IDENTIFIER = '@staarz_user_token';
export const USER_TOKEN_DETAILS_IDENTIFIER = '@staarz_user_details';
export const USER_REGISTRATION_DETAILS_IDENTIFIER = '@staarz_registration';
class Store {
  async storeToken<t>(key: string, data: t) {
    try {
      await AsyncStorage.setItem(key, JSON.stringify(data));
    } catch (e) {
      console.warn(e);
    }
  }

  async getToken(key: any) {
    try {
      let token = await AsyncStorage.getItem(key);
      return token != null ? JSON.parse(token) : null;
    } catch (e) {
      console.warn(e);
    }
  }

  async deleteToken(key: string) {
    try {
      await AsyncStorage.removeItem(key);
    } catch (e) {
      console.warn(e);
    }
  }
}

export default new Store();
