import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {
  AddImageScreen,
  Are_You_Sure,
  BottomTabs,
  Call_Screen,
  ChooesOption,
  demo,
  EditProfileScreen,
  Email_Verify,
  ForgotPasswordScreen,
  FullImageScreen,
  Gaming,
  Likes,
  PrivacyPolicy,
  PrivateMessage,
  Profile,
  ProfileSwipe,
  Setting_Profile,
  UserDetails,
  voiceCall,
} from '../screens';
import StripePayemt from '../screens/StripePayemt';
import {BlockedUsers} from '../screens/main/settings/blocked-users';

const Stack = createStackNavigator();

function CoreStack() {
  return (
    <Stack.Navigator
      initialRouteName="BottomTabs"
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="BottomTabs" component={BottomTabs} />
      <Stack.Screen
        name="ProfileSwipe"
        component={ProfileSwipe}
        options={{
          gestureEnabled: false,
        }}
      />
      <Stack.Screen name="Likes" component={Likes} />
      <Stack.Screen name="Profile" component={Profile} />
      <Stack.Screen name="Gaming" component={Gaming} />
      <Stack.Screen name="demo" component={demo} />
      <Stack.Screen name="UserDetails" component={UserDetails} />
      <Stack.Screen
        name="PrivateMessage"
        component={PrivateMessage}
        options={{
          gestureEnabled: false,
        }}
      />
      <Stack.Screen name="voiceCall" component={voiceCall} />
      <Stack.Screen
        name="ForgotPasswordScreen"
        component={ForgotPasswordScreen}
      />
      <Stack.Screen name="Setting_Profile" component={Setting_Profile} />
      <Stack.Screen name="ChooesOption" component={ChooesOption} />
      <Stack.Screen name="Are_You_Sure" component={Are_You_Sure} />
      <Stack.Screen name="UnblockuserScreen" component={BlockedUsers} />
      <Stack.Screen name="StripePayemt" component={StripePayemt} />
      <Stack.Screen name="AddImageScreen" component={AddImageScreen} />
      <Stack.Screen name="EditProfileScreen" component={EditProfileScreen} />
      <Stack.Screen
        name="Call_Screen"
        component={Call_Screen}
        options={{
          gestureEnabled: false,
        }}
      />
      <Stack.Screen name="Email_Verify" component={Email_Verify} />
      <Stack.Screen name="PrivacyPolicy" component={PrivacyPolicy} />
      <Stack.Screen name="FullImageScreen" component={FullImageScreen} />
    </Stack.Navigator>
  );
}

export {CoreStack};
