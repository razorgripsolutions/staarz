import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {
  PendingForVerification,
  RegisterScreen,
  RegisterUserBioScreen,
  TellUsMoreAboutYou1,
  TellUsMoreAboutYou2,
  TellUsMoreAboutYou3,
  uploadYourPicture,
  verifyYourPicture,
} from '../screens';
import LoginScreen from '../screens/auth/login';
import SuccessScreen from '../screens/auth/register/Success';
import AddPhoto from '../screens/auth/register/AddPhotosGrid';
import ResetPasswordStack from '../screens/auth/reset-password';
import OTPScreeen from '../screens/auth/otp';

const Stack = createStackNavigator();

function AuthStack() {
  return (
    <>
      <Stack.Navigator
        initialRouteName="LoginScreen"
        screenOptions={{
          headerShown: false,
        }}
      >
        <Stack.Screen name="LoginScreen" component={LoginScreen} />
        <Stack.Screen name="OTPScreen" component={OTPScreeen} />
        <Stack.Screen
          name="ResetPasswordScreen"
          component={ResetPasswordStack}
        />
        <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
        <Stack.Screen
          name="RegisterUserBioScreen"
          component={RegisterUserBioScreen}
        />
        <Stack.Screen name="UploadYourPicture" component={uploadYourPicture} />
        <Stack.Screen name="VerifyYourPicture" component={verifyYourPicture} />
        <Stack.Screen
          name="PendingForVerification"
          component={PendingForVerification}
        />
        <Stack.Screen name="AddPhotoVideo" component={AddPhoto} />
        <Stack.Screen
          name="TellUsMoreAboutYou1"
          component={TellUsMoreAboutYou1}
        />
        <Stack.Screen
          name="TellUsMoreAboutYou2"
          component={TellUsMoreAboutYou2}
        />
        <Stack.Screen
          name="TellUsMoreAboutYou3"
          component={TellUsMoreAboutYou3}
        />

        <Stack.Screen name="SuccessScreen" component={SuccessScreen} />
      </Stack.Navigator>
    </>
  );
}

export {AuthStack};
