import {IUserState, Reducers} from '../../App';

export const AuthReducer = (prevState: IUserState, action: Reducers) => {
  switch (action.type) {
    case 'RESTORE_TOKEN':
      return {
        ...prevState,
        userToken: action.userToken,
        isLoading: false,
        userData: action.userData,
        registrationData: action.registrationData,
      };
    case 'SIGN_IN':
      return {
        ...prevState,
        isLoading: false,
        userToken: action.userToken,
        userData: action.userData,
        registrationData: action.registrationData,
      };
    case 'SIGN_OUT':
      return {
        ...prevState,
        userToken: null,
        userData: null,
        registrationData: null,
      };
    case 'END_REGISTRATION':
      return {
        ...prevState,
        userToken: null,
        userData: null,
        registrationData: null,
      };
    case 'REGISTER':
      return {
        ...prevState,
        isLoading: false,
        userToken: action.userToken,
        userData: action.userData,
        registrationData: action.registrationData,
      };
  }
};
