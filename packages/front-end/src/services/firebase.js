import firebase from '@react-native-firebase/app';
import messaging from '@react-native-firebase/messaging';

// TODO - ENV vars
// Probably using https://github.com/luggit/react-native-config/
const config = {
  projectId: 'staarz-c3a69',
  appId: '1:740227810745:ios:db96cae617370fa447a268',
  apiKey: 'AIzaSyDVRAJyVJMkjBypP30eiCd1nTaAA-VcCTc',
  databaseURL:
    'com.googleusercontent.apps.740227810745-nvkbhemki9t6f9f6rnje1pv6onp6a7h7',
  storageBucket: 'staarz-c3a69.appspot.com',
  messagingSenderId: '740227810745',
};

/**
 * Init firebase only once, make sure it's initialized
 */
async function getFirebaseService() {
  const app =
    firebase.apps.length === 0 ? firebase.initializeApp(config) : firebase.app;

  return {messaging, app};
}

export {getFirebaseService};
