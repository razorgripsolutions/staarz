import React, {useState, useEffect} from 'react';
import {
  Dimensions,
  Image,
  View,
  StyleSheet,
  ImageBackground,
  KeyboardAvoidingView,
} from 'react-native';
import {Text} from 'react-native-paper';
// import Background from '../components/Background'
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {theme} from '../core/theme';

function Gaming(props) {
  // alert(JSON.stringify(userData));
  const windowWidth = Dimensions.get('window').width;
  const windowHeight = Dimensions.get('window').height;
  // const temp = {"userID":403,"userEmail":"ghdfgdf@dfsgdf.dfg","userPassword":"","username":"","facebookToken":"","googleToken":"","fbAuthStatus":0,"googleAuthStatus":0,"fullName":" Fhfgh gf","dob":"31-03-2021","locationText":"6-E, Nungambakkam High Road 1A, Dr, Thirumurthy Nagar, 1st Street, Nungambakkam, Chennai, Tamil Nadu 600034, India","lat":13.055441,"lon":80.2479924,"gender":"1","religionID":2,"introVideoID":0,"profilePicture":"","height":0,"weight":0,"alternateLocation":"","haveCar":0,"doesSmoke":0,"aboutMe":"","relationshipType":2,"packageID":0,"identifyID":0,"emailVerified":1,"isImageVerified":0,"isVideoVerified":0,"isProfileVerified":0,"facebookID":"","googleID":"","uploadedImage":"","clickedImage":"","fbProfileLink":"Dghgh","instaProfileLink":"Fghhjjjk"}
  const [userData, setUserData] = useState(); // route.params.userData);
  const [accessToken, setAccessToken] = useState(); // route.params.accessToken);
  const [instaProfileLink, setInstaProfileLink] = useState('');
  const [fbProfileLink, setFbProfileLink] = useState('');
  const [isloading, setIsloading] = useState(false);
  const [usersProfile, setUsersProfile] = useState([]);
  // alert(JSON.stringify(userData))
  // console.log(JSON.stringify(userData));

  useEffect(() => {
    // fetchProfileForMatch();
  }, []);

  return (
    // <Background>
    <View style={{alignItems: 'center', justifyContent: 'center'}}>
      <StaarzLogo />

      <Text
        style={{
          alignItems: 'center',
          top: windowHeight * 0.45,
          fontSize: 30,
          fontWeight: '800',
          textAlign: 'center',
        }}
      >
        GAMING UNDER CONSTRUCTION
      </Text>
    </View>
  );
}

const swipeCardStyles = StyleSheet.create({
  container: {
    // top:0,
    // width:"100%",
    top: 10 + getStatusBarHeight(),
  },
  card: {
    flex: 1,
    borderRadius: 4,
    borderWidth: 2,
    width: '100%',
    borderColor: '#E8E8E8',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  text: {
    textAlign: 'center',
    fontSize: 50,
    // backgroundColor: "transparent"
  },
});

function StaarzLogo() {
  return (
    <View style={sideLogoStyles.container}>
      <Image
        source={require('../assets/starzlogo_23X26.png')}
        style={sideLogoStyles.imageLogo}
      />
      <Image
        source={require('../assets/STAARZ_109X19_black.png')}
        style={sideLogoStyles.image}
      />
    </View>
  );
}
const sideLogoStyles = StyleSheet.create({
  image: {
    marginTop: 0,
    marginLeft: 20,
  },
  imageLogo: {
    // marginBottom: 8,
  },
  container: {
    position: 'absolute',
    top: 10 + getStatusBarHeight(),
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 0,
  },
});

function Background({children}) {
  return (
    <ImageBackground resizeMode="repeat" style={backgroundStyles.background}>
      <KeyboardAvoidingView
        style={backgroundStyles.container}
        behavior="padding"
      >
        {children}
      </KeyboardAvoidingView>
    </ImageBackground>
  );
}
const backgroundStyles = StyleSheet.create({
  background: {
    flex: 1,
    width: '100%',
    backgroundColor: theme.colors.surface,
  },
  container: {
    // flex: 1,
    // padding: 20,
    width: '100%',
    // maxWidth: 340,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Gaming;
