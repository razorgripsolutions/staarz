import React, {useState, useEffect} from 'react';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';

// import all the components we are going to use

// import SearchableDropdown component
import {
  StyleSheet,
  SafeAreaView,
  Text,
  View,
  TouchableOpacity,
  Image,
  FlatList,
  Dimensions,
  Platform,
} from 'react-native';
import Moment from 'moment';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {StackActions} from '@react-navigation/native';
import {url, backendBaseUrl} from '../const/urlDirectory';
import {theme} from '../core/theme';
import {checkConnection} from '../components/checkConnection';

import {string} from '../const/string';

const lastMessage = [];
// import AsyncStorage from '@react-native-async-storage/async-storage';
function Messages(props) {
  // alert(JSON.stringify(props.userData.userID))
  const {navigation} = props;
  const bottomRoute = props.route;
  // Data Source for the SearchableDropdown
  const [userId, setuserId] = useState();

  const [suserData, setuserData] = useState();

  const [rchatdata, setrchatdata] = useState();
  const [userData, setUserdata] = useState([]); // route.params.userData);
  const [data, setData] = useState([]);
  const [net, setnet] = React.useState(true);

  const windowWidth = Dimensions.get('window').width;
  const windowHeight = Dimensions.get('window').height;
  useEffect(() => {
    getdata();
  }, [data]);

  checkConnection().then((res) => {
    if (res === false) {
      // dropDownAlertRef.alertWithType('error', 'Error', string.network);
      setnet(res);
    } else {
      setnet(res);
      // getasync1()
    }
  });

  // const backAction = () => {
  //   navigation.navigate("BottomTabs")
  //   return true;
  // };
  const getdata = async () => {
    const user = await AsyncStorage.getItem('user');

    const user1 = JSON.parse(user);

    // console.log("user===============----user-------------->>>>>>>>>>>user>>>>>>>>>-------------", JSON.stringify(user1.userID))
    const newUserId = user1.userID;
    setUserdata(user1);

    let rChatData = await AsyncStorage.getItem('rChatData');
    rChatData = JSON.parse(rChatData);
    // alert(JSON.stringify(rChatData))
    //  setuserData(user)
    // console.log("data new consloe data - adddesdd---------", rChatData)
    setuserId(newUserId);
    setrchatdata(rChatData);
    apicall(newUserId);
  };

  const msgFirstView = async (channelID, userID) => {
    // alert()

    const TSDAte = Date.now();
    // alert(TSDAte)
    // console.log("datata heeee-------------", details = {
    //   channelId: channelID,
    //   recUserID: userID,
    //   msg: "",
    //   ts: ""
    // })

    const details = {
      channelId: channelID,
      recUserID: userID,
      msg: ' ',
      ts: TSDAte,
    };
    console.log('datatae-------------', JSON.stringify(details));
    await fetch(url.sendMsg, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(details),
    })
      .then((response) => response.json())
      .then((responseData) => {
        // alert(JSON.stringify(responseData))
        if (responseData.status === 200) {
          // ???
        } else {
          // ???
        }
      })
      .catch((error) => {
        // alert(error)
      });
  };

  const apicall = (newUserId) => {
    // alert(userData)
    fetch(url.channellist, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        userId: newUserId,
        // authToken: rchatdata.authToken
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        //  alert(JSON.stringify(responseJson.result))
        // console.log("datamesagechangelist--------------", JSON.stringify(responseJson.result))
        if (responseJson.status === 200) {
          const MEssage_Collection = responseJson.result;
          const daat = MEssage_Collection.sort(
            (a, b) => a.lastMsgTS - b.lastMsgTS,
          );
          // alert(JSON.stringify(daat))
          setData(daat.reverse());
        }
      })
      .catch((error) => {
        // alert(error)
        // ß setIsloading(false)
        // alert('Oops error!!')
        // console.error(error);
      });
  };

  // eslint-disable-next-line react/no-unstable-nested-components
  function SquareView() {
    return <View style={styles.pic} />;
  }

  const renderItem = ({item, index}) => {
    // var ts = Math.round(new Date().getTime() / 1000);
    // var tsYesterday = ts - (24 * 3600);
    // alert(item[0].lastMsg)
    const LastM_Length = item.lastMsg;

    const count = data.length;
    // alert(count)
    // console.log("----count", count)
    // console.log("----index", index)
    const date = new Date();

    const year1 = date.getFullYear();
    const month1 = date.getMonth() + 1;
    const day1 = date.getDate();

    const time = new Date(Number(item.lastMsgTS));
    const year = time.getFullYear();
    const month = time.getMonth() + 1;
    const day = time.getDate();

    const Current_date = `${day1}/${month1}/${year1} `;
    const get_date = `${day}/${month}/${year} `;
    let d;
    if (Current_date === get_date) {
      d = Moment(Number(item.lastMsgTS)).format('hh:mma');
    } else {
      d = Moment(Number(item.lastMsgTS)).format('DD-MM-YYYY');
    }

    const today = new Date();
    const yesterday = new Date(today);

    yesterday.setDate(yesterday.getDate() - 1);
    const yesterday1 = Moment(yesterday).format('DD-MM-YYYY');

    // console.log("yesterday--", yesterday1)
    // console.log("d--", d)
    let d1;
    if (d === yesterday1) {
      d1 = 'Yesterday';
    } else {
      d1 = d;
    }

    return item.lastMsgTS === '0' ? null : (
      <TouchableOpacity
        style={{marginBottom: count === index + 1 ? 120 : null}}
        onPress={() =>
          checkConnection().then((res) => {
            if (res === false) {
              // dropDownAlertRef.alertWithType('error', 'Error', string.network);
              setnet(res);
            } else {
              setnet(res);
              navigation.dispatch(
                StackActions.push('PrivateMessage', {
                  chnnelId: item.channelID, // Agora channel id ......get om message screen
                  profilePicture: item.profilePicture,
                  username: item.fullName,
                  reciverId: item.userID,
                }),
              );
            }
          })
        }
      >
        {/* <TouchableOpacity  onPress ={()=> alert('In Progress')}> */}
        <View style={styles.row}>
          <View
            style={{
              width: '15%',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <Image
              source={
                item.profilePicture === ''
                  ? require('../assets/userpro.jpeg')
                  : {uri: `${backendBaseUrl}data/${item.profilePicture}`}
              }
              //  resizeMode="stretch"
              style={styles.pic}
            />
          </View>
          <View style={{width: '60%'}}>
            <View style={styles.nameContainer}>
              <Text numberOfLines={1} style={styles.nameTxt}>
                {item.fullName}
              </Text>
              {/* <Text style={{ color: 'gray' }}>{item.date} {Moment(item.lastMsgTS).format('hh:mm')}</Text> */}
              {/* <Text numberOfLines={2} style={styles.time}>{item.date} hello</Text> */}
              <Text
                numberOfLines={2}
                style={{
                  // fontWeight: '700',
                  color: '#77838F',
                  // fontSize: 15,
                  fontWeight: item.readReceipt === 0 ? null : 'bold',
                  fontFamily:
                    item.readReceipt === 0
                      ? 'SFUIText-Semibold'
                      : 'MPLUS1p-ExtraBold',
                  fontSize: item.readReceipt === 0 ? 12 : 13,
                  lineHeight: 22,
                  letterSpacing: 1,
                  // marginRight:70,
                  // marginLeft:10
                }}
              >
                {item.lastMsg}
              </Text>
              {/* <Text style={styles.time}>{item.date} {item.lastMsg}</Text> */}
            </View>
          </View>
          <View style={{width: '25%'}}>
            <Text
              style={{
                color: '#77838F',
                lineHeight: 14,
                fontSize: 12,
                fontFamily: 'SFUIText-Semibold',
                textAlign: 'center',
              }}
            >
              {d1 === '01-01-1970' ? 'Say Hi...' : d1}
            </Text>
            {/* {item.lastMsg !== null ?
              <> */}
            {item.readReceipt === 0 ? null : (
              <View
                style={{
                  height: hp(2.5),
                  marginTop: 2,
                  width: hp(2.5),
                  borderRadius: hp(2.5) / 2,
                  alignItems: 'center',
                  backgroundColor: '#000',
                  justifyContent: 'center',
                  alignSelf: 'center',
                }}
              >
                <Text
                  style={{
                    color: '#fff',
                    lineHeight: 14,
                    fontSize: 12,
                    fontFamily: 'SFUIText-Semibold',
                    textAlign: 'center',
                  }}
                >
                  {item.readReceipt}
                </Text>
              </View>
            )}
            {/* </> : console.log("item.lastMsg---------------", item.lastMsg)} */}
          </View>
        </View>
      </TouchableOpacity>
    );
  };
  // hello

  return (
    <SafeAreaView style={styles.container}>
      <View
        style={{
          width: '90%',
          alignItems: 'center',
          justifyContent: 'center',
          alignSelf: 'center',
        }}
      >
        <Text
          style={{
            color: '#243443',
            fontSize: 24,
            letterSpacing: 1,
            lineHeight: 36,
            fontWeight: '800',
            fontFamily: 'MPLUS1p-Bold',
          }}
        >
          Chat
        </Text>
      </View>
      {!net ? (
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            flex: 1,
            alignContent: 'center',
          }}
        >
          <Image
            source={require('../assets/internat.png')}
            style={{height: 50, width: 50}}
          />
          <Text style={{fontSize: 17, fontWeight: '700', marginTop: 5}}>
            {string.network}
          </Text>
          <TouchableOpacity
            color="white"
            mode="contained"
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              top: windowHeight * 0.3,
              bottom: 20,
            }}
            onPress={() => getdata()}
          >
            <View
              style={{
                backgroundColor: '#7643F8',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 50,
                paddingHorizontal: 30,
                paddingVertical: 20,
                marginTop: 10,
              }}
            >
              {
                Platform.OS === 'ios' ? (
                  // <Ionicons name="reload" color="black" size={30} />
                  <Text style={{fontSize: hp('2'), color: '#fff'}}>
                    {string.Try_Again}
                  </Text>
                ) : (
                  <Text style={{fontSize: hp('2'), color: '#fff'}}>
                    {string.Try_Again}
                  </Text>
                )
                // <MaterialCommunityIcons name="reload" color="black" size={30} />
              }
            </View>
          </TouchableOpacity>
        </View>
      ) : (
        <View>
          {/* {!data === "" ? */}
          <FlatList
            style={{marginTop: 10}}
            horizontal
            showsHorizontalScrollIndicator={false}
            // extraData={this.state}
            data={data}
            // keyExtractor = {(item) => {
            //   return item.id;
            // }}
            renderItem={({item, index}) =>
              item.lastMsgTS === 0 ? (
                <TouchableOpacity
                  style={{
                    alignItems: 'center',
                    height: item.online_status === 1 ? 70 : null,
                    paddingBottom: 12,
                  }}
                  //  onPress ={() => alert('ok')
                  //  }
                  onPress={() => {
                    msgFirstView(item.channelID, item.userID);

                    checkConnection().then((res) => {
                      if (res === false) {
                        // dropDownAlertRef.alertWithType('error', 'Error', string.network);
                        setnet(res);
                      } else {
                        setnet(res);

                        navigation.dispatch(
                          StackActions.push('PrivateMessage', {
                            chnnelId: item.channelID, // Agora channel id ......get om message screen
                            profilePicture: item.profilePicture,
                            username: item.fullName,
                            // userId :userId,
                            reciverId: item.userID,
                          }),
                        );
                        // getasync1()
                      }
                    });
                  }}
                >
                  {/* {item.online_status === 1 ? */}
                  {/* // console.log("11111", item.profilePicture) */}
                  <Image
                    source={
                      item.profilePicture === ''
                        ? require('../assets/userpro.jpeg')
                        : {uri: `${backendBaseUrl}data/${item.profilePicture}`}
                    }
                    style={{
                      width: 50,
                      height: 50,
                      marginLeft: 20,
                      borderRadius: 50 / 2,
                      resizeMode: 'cover',
                      alignItems: 'center',
                    }}
                  />
                  {/* : null} */}
                  <View
                    style={{
                      borderRadius: 12 / 2,
                      width: 12,
                      height: 12,
                      backgroundColor: '#7643F8',
                      position: 'absolute',
                      top: 35,
                      right: 0,
                      borderColor: '#FFFFFF',
                      borderWidth: 1,
                    }}
                  />
                </TouchableOpacity>
              ) : null
            }
          />
          {/* : null} */}

          <FlatList
            // style={{ marginTop: 10 }}
            // extraData={this.state}
            nestedScrollEnabled
            style={{backgroundColor: '#fff', marginTop: 0}}
            data={data}
            // keyExtractor = {(item) => {
            //   return item.id;
            // }}
            renderItem={renderItem}
          />
          {data.length === 0 ? (
            <View
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                height: '100%',
              }}
            >
              <Text style={{fontWeight: '600', color: '#000', fontSize: 20}}>
                No match found
              </Text>
            </View>
          ) : null}
        </View>
      )}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.colors.surface,
    flex: 1,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#dcdcdc',
    backgroundColor: '#FFFFFF',
    // borderBottomWidth: 1,
    padding: 10,
    justifyContent: 'space-between',
    // marginTop:10
  },
  pic: {
    borderRadius: hp('6.5') / 2,
    width: hp('6.5'),
    height: hp('6.5'),
    // backgroundColor:"green",
    // resizeMode:""
  },
  nameContainer: {
    paddingLeft: 8,
    paddingRight: 5,
    // flexDirection: 'row',
    // justifyContent: 'space-between',
    // width: 310,
  },
  nameTxt: {
    // marginLeft: 15,
    // fontWeight: '800', // DUP
    color: '#222',
    fontSize: 14,
    lineHeight: 17,
    fontWeight: '500',
    fontFamily: 'SFUIText-Semibold',
  },
  mblTxt: {
    fontWeight: '200',
    color: '#777',
    fontSize: 13,
  },
  end: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  time: {
    // fontWeight: '700',
    color: '#77838F',
    // fontSize: 15,
    fontFamily: 'SFUIText-Semibold',
    fontSize: 12,
    lineHeight: 22,
    letterSpacing: 1,
    // marginRight:70,
    // marginLeft:10
  },
  icon: {
    height: 28,
    width: 28,
  },
  // text: {
  //   // width: '100%',
  //   // height: 50,
  //   // textAlign: 'center',
  //   // marginBottom: 10,
  //   // fontWeight: 'bold',
  //   // fontSize: 30,
  //   // lineHeight: 74.97,
  //   color: "#000",
  //   //backgroundColor:'red',
  //   alignSelf: 'center',
  //   fontWeight: 'bold',
  //   //  paddingBottom:10
  // },
});
export default Messages;
