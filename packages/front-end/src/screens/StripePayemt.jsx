import React, {useState, useEffect} from 'react';
import {
  Dimensions,
  Image,
  View,
  StyleSheet,
  StatusBar,
  Text,
  TouchableOpacity,
  ImageBackground,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import {Button} from 'react-native-elements';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useStripe, StripeProvider} from '@stripe/stripe-react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
// import Moment from 'react-moment';
import moment from 'moment';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
// import { CardField, useStripe } from '@stripe/stripe-react-native';
import {StackActions} from '@react-navigation/native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {url} from '../const/urlDirectory';
import {string} from '../const/string';
import {theme} from '../core/theme';
import {checkConnection} from '../components/checkConnection';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const {width} = Dimensions.get('window');
const {height} = Dimensions.get('window');
// import { PaymentCardTextField } from 'tipsi-stripe'
const publishableKey =
  'pk_test_51J52DrSJfvAgy2GwIg08bYI44iodIHa6HzIeV7mjODrNMf5EHDk6puV51ImbnJTFQGtSU2OyDt07NuA0YrehTqzI00Qz2s2jLO';
let card_token;
let clientSecret;
function StripePayemt(props) {
  const [Details, setamount] = useState(props.route.params.Details);
  const {initPaymentSheet, presentPaymentSheet} = useStripe();
  const [loading, setLoading] = useState(false);
  const [userId, setuserId] = useState();
  const [userData, setUserdata] = useState();
  const [date, setdate] = useState(props.route.params.date);
  const [lastDate, setlastDate] = useState('');
  const [update, setupdate] = useState(props.route.params.status);
  const [net, setnet] = React.useState(true);
  const {navigation} = props;
  const {route} = props;
  const popAction = StackActions.pop(1);
  const fetchPaymentSheetParams = async () => {
    // alert(date)
    const response = await fetch(url.Payment, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        // /
        product: {
          price: Details.pakagePrice,
        },
      }),
    });

    const {paymentIntent, ephemeralKey, customer} = await response.json();
    console.log(' stripe--', paymentIntent, ephemeralKey, customer);
    return {
      paymentIntent,
      ephemeralKey,
      customer,
    };
    // console.log()
  };

  const initializePaymentSheet = async () => {
    const {paymentIntent, ephemeralKey, customer} =
      await fetchPaymentSheetParams();

    console.log(paymentIntent);
    console.log(ephemeralKey);
    console.log('customer-----', customer);

    const {error} = await initPaymentSheet({
      customerId: customer,
      customerEphemeralKeySecret: ephemeralKey,
      paymentIntentClientSecret: paymentIntent,
    });
    clientSecret = paymentIntent;
    // alert(JSON.stringify(error))
    if (!error) {
      console.log('error----', error);
      setLoading(true);
    }
  };

  const openPaymentSheet = async () => {
    checkConnection().then(async (res) => {
      if (res === false) {
        // dropDownAlertRef.alertWithType('error', 'Error', string.network);
        setnet(res);
      } else {
        setnet(res);
        console.log('clientSecret----', clientSecret);
        const {error} = await presentPaymentSheet({clientSecret});
        console.log('error', error);
        if (error) {
          alert(`Error code: ${error.code}`, error.message);
        } else {
          alert(`succeessfully bought this ${Details.pakageName}`);
          if (update === 1) {
            updatesubsciption();
          } else {
            subsciption();
          }
        }
      }
    });
  };

  useEffect(() => {
    ApiCAlling();
  }, []);

  const ApiCAlling = async () => {
    initializePaymentSheet();
    getid();
    getdate();

    checkConnection().then((res) => {
      if (res === false) {
        // dropDownAlertRef.alertWithType('error', 'Error', string.network);
        setnet(res);
      } else {
        setnet(res);
        // getasync1()
      }
    });
  };
  const getdate = async () => {
    const newDate = moment().add(Details.month, 'M').format('YYYY-MM-DD');
    // moment().format('L');//Current Date

    // date.setMonth(date.getMonth()+Details.month)

    setlastDate(newDate);
    // console.log("format-----------",date)
  };
  const getid = async () => {
    const user = await AsyncStorage.getItem('user');
    const user1 = JSON.parse(user);
    console.log(
      'user===============----user-------------->>>>>>>>>>>user>>>>>>>>>-------------',
      user1.userID,
    );
    const newUserId = user1.userID;

    setuserId(newUserId);

    console.log('userData------', newUserId);
  };

  const subsciption = () => {
    // alert(userId)
    fetch(url.addUserSubscribtion, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        userID: userId,
        pakageID: Details.pakageID,
        pakageStart: date,
        pakageEnd: lastDate,
        status: 1,
      }),
    })
      .then((response) => response.json())
      .then(async (responseJson) => {
        console.log('2000 without----', responseJson);
        if (responseJson.status === 200) {
          console.log('responseJson----', responseJson);

          await AsyncStorage.setItem('subToken', '1');
          navigation.replace('Setting_Profile', {
            userID: userId,
          });
        } else {
          console.log('error----', responseJson);
        }
      })
      .catch((error) => {
        // setIsloading(false)
        alert('Oops error!!');
        console.error('error------', error);
      });
  };

  const updatesubsciption = () => {
    // alert(userId)
    fetch(url.updateUserSubscribtion, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        userID: userId,
        pakageID: Details.pakageID,
        pakageStart: date,
        pakageEnd: lastDate,
        status: 1,
      }),
    })
      .then((response) => response.json())
      .then(async (responseJson) => {
        console.log('2000 without----', responseJson);
        if (responseJson.status === 200) {
          console.log('update user response----', responseJson);
          // navigation.popToTop()
          // navigation.dispatch(popAction);
          await AsyncStorage.setItem('subToken', '1');
          navigation.replace('Setting_Profile', {
            userID: userId,
          });
        } else {
          console.log('error----', responseJson);
        }
      })
      .catch((error) => {
        // setIsloading(false)
        // alert('Oops error!!')
        console.error('error------', error);
      });
  };

  return (
    <SafeAreaView
      style={{flex: 1, backgroundColor: '#fff', alignItems: 'center'}}
    >
      {/* <View style={{ flex: 1, alignItems: "center", justifyContent: "center", }}> */}
      <StatusBar barStyle="dark-content" backgroundColor="white" />

      <View
        style={{
          flexDirection: 'row',
          top: 0,
          alignItems: 'center',
          justifyContent: 'center',
          width: '100%',
          backgroundColor: '#fff',
          zIndex: 10,
        }}
      >
        <View style={{width: '20%'}}>
          <TouchableOpacity onPress={() => navigation.goBack()} style={{}}>
            <FontAwesome
              name="angle-left"
              size={25}
              color="#000"
              style={{left: 20}}
            />
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: '60%',
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'center',
          }}
        >
          <Image
            source={require('../assets/starzlogo.png')}
            style={sideLogoStyles.imageLogo}
          />
          {/* <Image source={require('../assets/STAARZ_109X19_black.png')} style={sideLogoStyles.image} /> */}
          <Text
            style={{
              fontSize: wp(6),
              fontFamily: 'MPLUS1p-Bold',
              color: '#243443',
              left: -2,
              lineHeight: 33,
              textAlign: 'center',
            }}
          >
            STAARZ
          </Text>
        </View>
        <View style={{backgroundColor: 'red', width: '20%'}} />
      </View>

      {!net ? (
        <View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
          <Image
            source={require('../assets/internat.png')}
            style={{height: 50, width: 50}}
          />
          <Text style={{fontSize: 17, fontWeight: '700', marginTop: 5}}>
            {string.network}
          </Text>
          <TouchableOpacity
            color="white"
            mode="contained"
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              top: windowHeight * 0.3,
            }}
            onPress={() => ApiCAlling()}
          >
            <View
              style={{
                backgroundColor: '#7643F8',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 50,
                paddingHorizontal: 30,
                paddingVertical: 20,
                marginTop: 10,
              }}
            >
              {
                Platform.OS === 'ios' ? (
                  // <Ionicons name="reload" color="black" size={30} />
                  <Text style={{fontSize: hp('2'), color: '#fff'}}>
                    {string.Try_Again}
                  </Text>
                ) : (
                  <Text style={{fontSize: hp('2'), color: '#fff'}}>
                    {string.Try_Again}
                  </Text>
                )
                // <MaterialCommunityIcons name="reload" color="black" size={30} />
              }
            </View>
          </TouchableOpacity>
        </View>
      ) : (
        <>
          <View style={{width: '100%', padding: 20, alignItems: 'center'}}>
            <Text style={{fontWeight: '600', fontSize: 22, color: '#000'}}>
              Order Details
            </Text>
          </View>
          <StripeProvider
            publishableKey={publishableKey}
            merchantIdentifier="merchant.identifier"
          >
            <View
              style={{
                height: '60%',
                backgroundColor: theme.colors.surface,
                marginHorizontal: 20,
                width: '90%',
                padding: 30,
                borderRadius: 10,
                marginTop: 30,
                shadowColor: '#ccc',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.5,
                shadowRadius: 3.84,

                elevation: 5,

                justifyContent: 'space-evenly',
              }}
            >
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                }}
              >
                <Text
                  style={{
                    color: '#8A98AC',
                    fontWeight: 'bold',
                    fontSize: 18,
                    paddingHorizontal: 10,
                  }}
                >
                  Package Name
                </Text>
                <Text
                  style={{
                    color: '#8A98AC',
                    fontWeight: 'bold',
                    fontSize: 18,
                    paddingHorizontal: 10,
                  }}
                >
                  {Details.pakageName}
                </Text>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  marginTop: 10,
                }}
              >
                <Text
                  style={{
                    color: '#8A98AC',
                    fontWeight: 'bold',
                    fontSize: 18,
                    paddingHorizontal: 10,
                  }}
                >
                  Amount
                </Text>

                <Text style={{color: '#000', fontWeight: 'bold', fontSize: 25}}>
                  ₪{Details.pakagePrice}
                </Text>
              </View>
              {/* <View style={{ flexDirection: "row", alignItems: "center", marginTop: 5, paddingHorizontal: 10,}}>

    <Text style={{ color: "#000", fontWeight: "bold", fontSize: 25 }}>₪{Details.pakagePrice}</Text>
    <Text style={{ color: "#7A7A7A", fontWeight: "bold", fontSize: 16, marginHorizontal: 8, marginTop: 6 }}>/</Text>
    <Text style={{ color: "#7A7A7A", fontWeight: "300", fontSize: 16, marginTop: 6 }}>Month</Text>
</View> */}
              {/* <View style={{ height: .5, width: "95%", marginTop: 20, backgroundColor: "#000" }} /> */}
              {/* <View style={{ height: 1, width: "100%",  backgroundColor: "red" ,}} /> */}
              {/* <View style={{ height:1,width:"100%",backgroundColor:"green",borderWidth:2}}/> */}

              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  paddingHorizontal: 10,
                  marginTop: 30,
                  alignSelf: 'center',
                }}
              >
                {/* <Text style={{ fontSize: 5,color:'#7A7A7A'}}>⬤</Text> */}
                <Text
                  style={{
                    marginLeft: 10,
                    color: '#8A98AC',
                    fontSize: 16,
                    fontWeight: '700',
                  }}
                >
                  {Details.unlimitedLikes} Unlimiteds like{' '}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  paddingHorizontal: 10,
                  marginTop: 10,
                  alignSelf: 'center',
                }}
              >
                {/* <Text style={{ fontSize: 5, color:'#7A7A7A'}}>⬤</Text> */}
                <Text
                  style={{
                    marginLeft: 10,
                    color: '#8A98AC',
                    fontSize: 16,
                    fontWeight: '700',
                  }}
                >
                  {Details.rewind} Rewind{' '}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  paddingHorizontal: 10,
                  marginTop: 10,
                  alignSelf: 'center',
                }}
              >
                {/* <Text style={{ fontSize: 5,color:'#7A7A7A'}}>⬤</Text> */}
                <Text
                  style={{
                    marginLeft: 10,
                    color: '#8A98AC',
                    fontSize: 16,
                    fontWeight: '700',
                  }}
                >
                  {Details.superLikesPerDay} Super Likes a day{' '}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  paddingHorizontal: 10,
                  marginTop: 10,
                  alignSelf: 'center',
                }}
              >
                {/* <Text style={{ fontSize: 5,color:'#7A7A7A'}}>⬤</Text> */}
                <Text
                  style={{
                    marginLeft: 10,
                    color: '#8A98AC',
                    fontSize: 16,
                    fontWeight: '700',
                  }}
                >
                  {Details.passport} Passport{' '}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  paddingHorizontal: 10,
                  marginTop: 10,
                  alignSelf: 'center',
                }}
              >
                {/* <Text style={{ fontSize: 5,color:'#7A7A7A'}}>⬤</Text> */}
                <Text
                  style={{
                    marginLeft: 10,
                    color: '#8A98AC',
                    fontSize: 16,
                    fontWeight: '700',
                  }}
                >
                  {Details.isAds} No ads
                </Text>
              </View>
            </View>
          </StripeProvider>
          <View style={{marginTop: 20}} />
          <View style={{width: '90%'}}>
            {/* {update !== 1 ? */}
            <Button
              // buttonStyle={ba}
              variant="primary"
              disabled={!loading}
              title="Checkout"
              onPress={openPaymentSheet}
            />
            {/* :
        <Button
        // buttonStyle={ba}
        variant="primary"
        disabled={!loading}
        title="Checkout"

        onPress={openPaymentSheet1}
      />} */}
          </View>
        </>
      )}
      {/* </View> */}
    </SafeAreaView>
  );
}

function StaarzLogo() {
  return (
    <View style={sideLogoStyles.container}>
      <Image
        source={require('../assets/starzlogo_23X26.png')}
        style={sideLogoStyles.imageLogo}
      />
      <Image
        source={require('../assets/STAARZ_109X19_black.png')}
        style={sideLogoStyles.image}
      />
    </View>
  );
}
const sideLogoStyles = StyleSheet.create({
  image: {
    marginTop: 0,
    marginLeft: 20,
  },
  imageLogo: {
    width: wp(7.5),
    height: wp(6.5),
    resizeMode: 'cover',
    left: -4,
  },
  container: {
    position: 'absolute',
    top: Platform.OS === 'ios' ? 10 + getStatusBarHeight() : 15,
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 0,
  },
});
function Background({children}) {
  return (
    <ImageBackground resizeMode="repeat" style={backgroundStyles.background}>
      <KeyboardAvoidingView
        style={backgroundStyles.container}
        behavior="padding"
      >
        {children}
      </KeyboardAvoidingView>
    </ImageBackground>
  );
}
const backgroundStyles = StyleSheet.create({
  background: {
    flex: 1,
    width: '100%',
    backgroundColor: theme.colors.surface,
  },
  container: {
    // flex: 1,
    // padding: 20,
    width: '100%',
    // maxWidth: 340,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default StripePayemt;
