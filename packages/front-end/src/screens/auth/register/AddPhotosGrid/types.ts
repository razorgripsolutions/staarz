export type ImageGridCardComponentProp = {
  image: string;
  addImage: () => void;
  removeImage: () => void;
};
