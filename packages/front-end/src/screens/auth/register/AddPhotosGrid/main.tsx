import React, {useCallback, useEffect, useState} from 'react';
import {
  Image,
  View,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
  Platform,
  ScrollView,
  Alert,
} from 'react-native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Text} from 'react-native-paper';
import Button from '../../../../components/Button';
import {theme} from '../../../../core/theme';
import {checkConnection} from '../../../../components/checkConnection';
import {string} from '../../../../const/string';
import {ImageGridCard} from './imageCard';
import {Colors} from '../../../../constants/colors';
import {Asset} from 'react-native-image-picker';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import * as ImagePicker from 'react-native-image-picker';
import {getMessageFromCode} from '../../../uploadYourPicture';
import Toast from 'react-native-toast-message';
import ActionSheet from 'react-native-actionsheet';
import {LoadingModal} from '../../../../components/loading';
import RNPermissions, {
  Permission,
  PERMISSIONS,
  check,
  RESULTS,
  request,
  openSettings,
} from 'react-native-permissions';

const data: string[] = [];
const list = [1, 2, 3, 4, 5, 6, 7, 8, 9];

const AddPhotoVideo = ({navigation}: any) => {
  const [Active, setActive] = useState(true);
  const [net, setnet] = React.useState(true);
  const [gettingImages, setGettingImages] = React.useState(false);
  const [path, setPath] = React.useState<Asset[] | undefined>([]);
  const [images, setImages] = React.useState<string[]>(data);
  const [listState, setListState] = useState(list);
  const {width} = Dimensions.get('window');
  const {height} = Dimensions.get('window');

  const ActionSheetRef = React.useRef<any>();

  const getdata = () => {
    checkConnection().then((res) => {
      if (res === false) {
        setnet(res);
      } else {
        setnet(res as any);
      }
    });
  };

  const uploadImage = React.useCallback(() => {
    let imageList: string[] = [];
    path?.map((image: any) => {
      return imageList.push(image?.uri ?? '');
    });
    setImages(images.concat(imageList));
    listState.splice(0, path?.length);
  }, [setImages, listState, path]);

  const chooseImage = React.useCallback(
    (index: number) => {
      setGettingImages(true);
      if (index === 0) {
        const options: ImagePicker.ImageLibraryOptions = {
          mediaType: 'photo',
          includeBase64: true,
          selectionLimit: 5,
        };
        return ImagePicker.launchImageLibrary(options, async (response) => {
          if (response.errorCode) {
            Toast.show({
              text1: 'Application Error',
              text2: getMessageFromCode(response.errorCode),
              type: 'danger',
            });
          } else {
            console.log('responses image', response?.assets);
            setPath(response?.assets);
          }
        });
      } else {
        setGettingImages(false);
      }
    },
    [setPath, setGettingImages],
  );

  const handleSkip = useCallback(() => {
    navigation.navigate('UploadYourPicture');
  }, [navigation]);

  const removeImage = React.useCallback(
    (selected: string) => {
      const newImages = [...images];
      setImages(newImages.filter((item) => item !== selected));
      listState.push(listState.length + 1);
    },
    [images, listState, setImages],
  );

  const handleOpenSettings = useCallback(async () => {
    return openSettings()
      .then((res) => {
        console.log(res);
      })
      .catch((e) => {
        console.log(e);
      });
  }, [openSettings]);

  const handlePermission = useCallback(async () => {
    let status;
    if (Platform.OS === 'android') {
      status = check(PERMISSIONS.ANDROID.READ_MEDIA_IMAGES).then((result) => {
        console.log('result1', result);
        if (result === RESULTS.DENIED) {
          return request(PERMISSIONS.ANDROID.READ_MEDIA_IMAGES).then(
            (requestResult) => {
              console.log('requestResult', requestResult);
            },
          );
        } else if (result === RESULTS.BLOCKED) {
          return Alert.alert(
            'Open Settings',
            'You would need to  allow gallery permissions in your settings.',
            [
              {
                text: 'No',
                onPress: () => {},
                style: 'cancel',
              },
              {
                text: 'Yes',
                onPress: handleOpenSettings,
              },
            ],
          );
        }
      });
    } else if (Platform.OS === 'ios') {
      status = check(PERMISSIONS.IOS.MEDIA_LIBRARY).then((result) => {
        console.log('result1', result);
        if (result === RESULTS.DENIED) {
          return request(PERMISSIONS.IOS.MEDIA_LIBRARY).then(
            (requestResult) => {
              console.log('requestResult', requestResult);
            },
          );
        } else if (result === RESULTS.BLOCKED) {
          return Alert.alert(
            'Open Settings',
            'You would need to  allow gallery permissions in your settings.',
            [
              {
                text: 'No',
                onPress: () => {},
                style: 'cancel',
              },
              {
                text: 'Yes',
                onPress: handleOpenSettings,
              },
            ],
          );
        }
      });
    }
    return status;
  }, [handleOpenSettings]);

  useEffect(() => {
    handlePermission();
  }, []);

  React.useEffect(() => {
    if ((path?.length ?? 0) > 1) {
      uploadImage();
      setActive(false);
      setGettingImages(false);
    } else {
      setGettingImages(false);
    }
  }, [uploadImage, setActive, setGettingImages]);

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
      <View style={{flex: 1, alignItems: 'center'}}>
        <StaarzLogo />
        {!net ? (
          <View
            style={{alignItems: 'center', justifyContent: 'center', flex: 1}}
          >
            <Image
              source={require('../../../../assets/internat.png')}
              style={{height: 50, width: 50}}
            />
            <Text style={{fontSize: 17, fontWeight: '700', marginTop: 5}}>
              {string.network}
            </Text>
            <TouchableOpacity
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                top: height * 0.3,
              }}
              onPress={() => getdata()}
            >
              <View
                style={{
                  backgroundColor: '#7643F8',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 50,
                  paddingHorizontal: 30,
                  paddingVertical: 20,
                  marginTop: 10,
                }}
              >
                {Platform.OS === 'ios' ? (
                  <Text style={{fontSize: hp('2'), color: '#fff'}}>
                    {string.Try_Again}
                  </Text>
                ) : (
                  <Text style={{fontSize: hp('2'), color: '#fff'}}>
                    {string.Try_Again}
                  </Text>
                )}
              </View>
            </TouchableOpacity>
          </View>
        ) : (
          <>
            <Text
              style={{
                letterSpacing: 0.4,
                fontSize:
                  width === 320 || height === 610.6666666666666 ? 24 : 29,
                marginTop: 50,
                lineHeight: 42,
                fontWeight: 'bold',
              }}
            >
              Add photos
            </Text>
            <Text
              style={{
                fontWeight: '500',
                fontSize:
                  width === 320 || height === 610.6666666666666 ? 16 : 18,
                textAlign: 'center',
                color: '#919FB1',
                lineHeight: 27,
                marginBottom: 10,
              }}
            >
              You can change this later
            </Text>
            <ScrollView contentContainerStyle={styles.imageListContainer}>
              {images.map((item) => (
                <ImageGridCard
                  key={item}
                  image={item}
                  removeImage={() => removeImage(item)}
                  addImage={() => ActionSheetRef?.current?.show()}
                />
              ))}
              {listState.map((item) => (
                <TouchableOpacity
                  key={item}
                  activeOpacity={0.7}
                  onPress={() => {
                    ActionSheetRef?.current?.show();
                  }}
                  style={styles.addImageButtonContainer}
                >
                  <FontAwesome
                    name="plus-circle"
                    color={Colors.lightTintColor}
                    size={35}
                  />
                  <Text style={styles.addPhotoText}>Add photo</Text>
                </TouchableOpacity>
              ))}
            </ScrollView>
            <View style={bottomButton.bottomView}>
              <Button
                disabled={Active}
                style={bottomButton.login}
                color="#8897AA"
                mode="contained"
                onPress={handleSkip}
              >
                <Text
                  style={{
                    textTransform: 'capitalize',
                    fontWeight: 'bold',
                    fontSize: 18,
                    color: 'white',
                  }}
                >
                  Next
                </Text>
              </Button>
              <TouchableOpacity onPress={handleSkip}>
                <Text
                  style={{fontWeight: '500', fontSize: 12, color: '#B093FA'}}
                >
                  Skip this page
                </Text>
              </TouchableOpacity>
            </View>
          </>
        )}
      </View>
      <LoadingModal show={gettingImages} />
      <ActionSheet
        ref={ActionSheetRef}
        title={'How do you like to add an image ?'}
        options={['Choose from gallery', 'cancel']}
        cancelButtonIndex={1}
        onPress={chooseImage}
      />
    </SafeAreaView>
  );
};

const bottomButton = StyleSheet.create({
  bottomView: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 24,
  },
  login: {
    marginBottom: 14,
    borderRadius: 8,
    width: '90%',
  },
});

function StaarzLogo() {
  return (
    <View style={sideLogoStyles.container}>
      <Image
        source={require('../../../../assets/starzlogo.png')}
        style={sideLogoStyles.imageLogo}
      />
      <Text
        style={{
          fontSize: wp(6),
          fontFamily: 'MPLUS1p-Bold',
          color: '#243443',
          left: -2,
          lineHeight: 33,
          textAlign: 'center',
        }}
      >
        STAARY
      </Text>
    </View>
  );
}

const sideLogoStyles = StyleSheet.create({
  image: {
    marginTop: 0,
    marginLeft: 20,
  },
  imageLogo: {
    width: wp(7.5),
    height: wp(6.5),
    left: -4,
  },
  container: {
    position: 'absolute',
    top: 0,
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 0,
  },
});

const styles = StyleSheet.create({
  forgotPassword: {
    width: '100%',
    alignItems: 'flex-end',
    marginRight: 10,
    marginBottom: 24,
  },
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  forgot: {
    fontSize: 13,
    marginTop: 10,
    marginRight: -2,
    color: theme.colors.secondary,
  },
  rememberMe: {
    fontSize: 13,
    marginTop: 10,
    marginLeft: 10,
    color: theme.colors.secondary,
  },
  link: {
    fontWeight: '100',
    color: '#9872FA',
    lineHeight: 30,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'white',
    marginTop: 50,
    width: '100%',
  },
  imageThumbnail: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    flex: 1,
  },
  imageListContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  addImageButtonContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: wp(26),
    width: wp(26),
    borderRadius: 8,
    margin: 10,
    borderStyle: 'dashed',
    borderWidth: 1,
    borderColor: Colors.lightTintColor,
    backgroundColor: Colors.profile.addImageGray,
  },
  addPhotoText: {
    color: Colors.lightTintColor,
    fontWeight: '500',
    fontSize: 12,
    paddingTop: 5,
  },
});

export default AddPhotoVideo;
