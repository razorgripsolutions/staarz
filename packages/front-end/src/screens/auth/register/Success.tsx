import React, {FC, useCallback, useEffect, useRef} from 'react';
import {View, Image} from 'react-native';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Text} from 'react-native-paper';
import {ConfettiCanon} from '../../../components/Confetti';
import {useNavigation} from '@react-navigation/core';
import Button from '../../../components/Button';

const SuccessScreen: FC<any> = () => {
  const confettiRef = useRef<any>(null);
  const navigation: any = useNavigation();

  useEffect(() => {
    if (confettiRef) {
      confettiRef?.current?.startConfetti();
    }
  }, []);

  const handleSubmit = useCallback(() => {
    navigation.navigate('LoginScreen');
  }, [navigation]);

  return (
    <>
      <View
        style={{
          flexDirection: 'column',
          justifyContent: 'space-between',
          flex: 1,
          paddingHorizontal: 24,
          backgroundColor: 'white',
        }}
      >
        <View
          style={{
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Image
              source={require('../../../assets/starzlogo.png')}
              style={{width: 65, height: 65}}
            />
            <Text style={{width: 8}} />
            <Text
              style={{
                fontWeight: '700',
                fontSize: 40,
                lineHeight: 60,
                color: '#101010',
                fontFamily: 'MPLUS1p-Bold',
              }}
            >
              STAARZ
            </Text>
          </View>

          <Text
            style={{
              fontSize: 17,
              color: '#101010',
              lineHeight: 27,
              fontWeight: '600',
            }}
          >
            Account created successfully
          </Text>
          <Text style={{height: 20}} />
          <Image
            resizeMode="contain"
            style={{
              height: hp(20),
              width: hp(20),
            }}
            source={{
              uri: 'https://icon-library.com/images/confetti-icon/confetti-icon-29.jpg',
            }}
          />
        </View>

        <Button
          style={{
            borderRadius: 8,
          }}
          color="#8897AA"
          onPress={handleSubmit}
          title={'Login to get started'}
          mode={'contained'}
        >
          <Text
            style={{
              textTransform: 'capitalize',
              fontWeight: 'bold',
              fontSize: hp('2'),
              color: 'white',
            }}
          >
            login
          </Text>
        </Button>
      </View>

      <ConfettiCanon ref={confettiRef} />
    </>
  );
};

export default SuccessScreen;
