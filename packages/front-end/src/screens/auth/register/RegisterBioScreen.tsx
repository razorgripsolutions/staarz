import React, {useState} from 'react';
import {
  Image,
  View,
  StyleSheet,
  TouchableOpacity,
  StatusBar,
  SafeAreaView,
  ScrollView,
  Dimensions,
  ActivityIndicator,
  Platform,
} from 'react-native';
import {Text} from 'react-native-paper';
import {CircleFade} from 'react-native-animated-spinkit';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import DatePicker from 'react-native-date-picker';
import PlacesInput from 'react-native-places-input';
import Tabs from 'react-native-tabs';
import DropDownPicker from 'react-native-dropdown-picker';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {checkConnection} from '../../../components/checkConnection';
import {theme} from '../../../core/theme';
import TextInput from '../../../components/TextInput';
import Button from '../../../components/Button';
import {string} from '../../../const/string';

const {width} = Dimensions.get('window'); // full width
const {height} = Dimensions.get('window');

function RegisterUserBioScreen({navigation, route}: any) {
  const [fullName, setfullName] = useState({
    value: route?.params?.userData.fullName,
    error: '',
  });
  const [accessToken, setAccessToken] = useState(route?.params?.accessToken);

  const [dateBirthday, setDateBirthday] = useState(null);
  const [dateBirthdayText, setDateBirthdayText] = useState('Date');

  const [location, setLocation] = useState({add: '', lat: 0.0, lon: 0.0});
  const [gender, setGender] = useState('');
  const [religion, setReligion] = useState('');
  const [interestedIn, setInterestedIn] = useState(0);
  const [lookingFor, setLookingFor] = useState();
  const [religionOptionsList, setReligionOptionsList] = useState([]); // route?.params?.religionOptionsList);
  const [relationshipTypeList, setRelationshipTypeList] = useState([]); // route?.params?.relationshipTypeList);

  const [isLoading, setIsloading] = useState(false);
  const [isPageLoading, setIsPageLoading] = useState(false);
  const [bottomHeight, setBottomHeight] = useState(10);

  const [net, setnet] = React.useState(true);
  const [date, setDate] = useState(new Date());
  const [open, setOpen] = useState(false);

  const getfuntion = () => {
    checkConnection().then((res) => {
      if (res === false) {
        setnet(res);
      } else {
        setnet(res as any);
      }
    });
  };

  const registerUserBioScreen = () => {
    checkConnection().then((res) => {
      if (res === false) {
        setnet(res);
      } else {
        setnet(res as any);
        registerUserBioScreen1();
      }
    });
  };
  function difference(date1: Date, date2: Date) {
    const date1utc = Date.UTC(
      date1.getFullYear(),
      date1.getMonth(),
      date1.getDate(),
    );
    const date2utc = Date.UTC(
      date2.getFullYear(),
      date2.getMonth(),
      date2.getDate(),
    );
    const day = 1000 * 60 * 60 * 24;
    return (date2utc - date1utc) / day;
  }

  const registerUserBioScreen1 = () => {
    const today = new Date();
  };

  const [showDateTimePicker, setShowDateTimePicker] = useState(false);

  return (
    <SafeAreaView style={styles.container}>
      <View
        style={{
          flex: 1,
          width: '90%',
          alignItems: 'center',
          justifyContent: 'center',
          alignSelf: 'center',
        }}
      >
        <StatusBar barStyle="dark-content" backgroundColor="white" />

        <View
          style={{
            flexDirection: 'row',
            position: 'absolute',
            top: 0,
            zIndex: 10,
            alignItems: 'center',
            justifyContent: 'center',
            width: '100%',
            backgroundColor: '#fff',
          }}
        >
          <Image
            source={require('../../../assets/starzlogo.png')}
            style={sideLogoStyles.imageLogo}
          />
          <Text
            style={{
              fontSize: wp(6),
              fontFamily: 'MPLUS1p-Bold',
              color: '#243443',
              left: -2,
              lineHeight: 33,
              textAlign: 'center',
            }}
          >
            STAARZ
          </Text>
        </View>

        {!net ? (
          <View
            style={{alignItems: 'center', justifyContent: 'center', flex: 1}}
          >
            <Image
              source={require('../../../assets/internat.png')}
              style={{height: 50, width: 50}}
            />
            <Text style={{fontSize: 17, fontWeight: '700', marginTop: 5}}>
              {string.network}
            </Text>
            <TouchableOpacity
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                top: height * 0.3,
              }}
              onPress={() => getfuntion()}
            >
              <View
                style={{
                  backgroundColor: '#7643F8',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 50,
                  paddingHorizontal: 30,
                  paddingVertical: 20,
                  marginTop: 10,
                }}
              >
                {Platform.OS === 'ios' ? (
                  <Text style={{fontSize: hp('2'), color: '#fff'}}>
                    {string.Try_Again}
                  </Text>
                ) : (
                  <Text style={{fontSize: hp('2'), color: '#fff'}}>
                    {string.Try_Again}
                  </Text>
                )}
              </View>
            </TouchableOpacity>
          </View>
        ) : (
          <>
            <View style={{flex: 9}}>
              <ScrollView
                showsVerticalScrollIndicator={false}
                keyboardShouldPersistTaps="always"
              >
                <Text
                  style={{
                    fontWeight: Platform.OS === 'ios' ? 'bold' : 'bold',
                    fontSize: wp('7'),
                    marginTop: 40,
                    justifyContent: 'center',
                    alignContent: 'center',
                    alignSelf: 'center',
                    lineHeight: 42,
                    fontFamily: 'SFUIText-Bold',
                    letterSpacing: 0.5,
                  }}
                >
                  Let&apos;s Get Started
                </Text>
                <Text
                  style={{
                    fontWeight: Platform.OS === 'ios' ? '600' : 'bold',
                    fontSize: hp(1.9),
                    textAlign: 'center',
                    letterSpacing: 1,
                    color: '#101010',
                    marginBottom: 15,
                    fontFamily: 'SFUIText-Bold',
                  }}
                >
                  Lets get to know each other
                </Text>
                {isPageLoading ? (
                  <View
                    style={{justifyContent: 'center', alignItems: 'center'}}
                  >
                    <CircleFade size={110} color="#5E2EBA" />
                  </View>
                ) : (
                  <View style={{width: '100%'}}>
                    <View style={{width: '100%'}}>
                      <TextInput
                        label="Full name"
                        placeholder="Enter your Full name"
                        value={fullName.value}
                        multiline={false}
                        onChangeText={(text: any) =>
                          setfullName({value: text, error: ''})
                        }
                        style={{width: wp(90), backgroundColor: '#fff'}}
                        theme={{
                          colors: {
                            placeholder: '#B9BAC8',
                            text: '#B9BAC8',
                            primary: '#B9BAC8',
                            underlineColor: 'transparent',
                            backgroundColor: '#fff',
                          },
                        }}
                        errorText={undefined}
                        description={undefined}
                        icon={undefined}
                        onPress={undefined}
                        colorIcon={undefined}
                        url={undefined}
                      />
                    </View>
                    <View style={{alignContent: 'flex-start', width: '100%'}}>
                      <Text
                        style={{
                          textAlign: 'left',
                          marginTop: 18,
                          marginBottom: 0,
                          alignContent: 'flex-start',
                          color: '#1E263C',
                          lineHeight: 22,
                          fontFamily: 'SFUIText-Semibold',
                        }}
                      >
                        Birthdate
                      </Text>

                      <View
                        style={{
                          flexDirection: 'row',
                          width: '100%',
                          alignItems: 'center',
                        }}
                      >
                        <View style={{width: '100%'}}>
                          <TextInput
                            editable={false}
                            value={dateBirthdayText}
                            style={{width: wp(90)}}
                            theme={{
                              colors: {
                                placeholder: '#B9BAC8',
                                text: '#B9BAC8',
                                primary: '#B9BAC8',
                                underlineColor: 'transparent',
                                backgroundColor: '#fff',
                              },
                            }}
                            errorText={undefined}
                            description={undefined}
                            icon={undefined}
                            onPress={undefined}
                            colorIcon={undefined}
                            url={undefined}
                          />
                        </View>
                        <TouchableOpacity
                          onPress={() => {
                            setOpen(true);
                          }}
                          style={{position: 'absolute', right: wp(2)}}
                        >
                          <Image
                            source={require('../../../assets/calendar-day.png')}
                            style={{width: 45, height: 45, marginTop: 20}}
                          />
                        </TouchableOpacity>
                      </View>

                      <DatePicker
                        modal
                        open={open}
                        date={date}
                        onConfirm={(date) => {
                          setOpen(false);
                          setDate(date);
                        }}
                        onCancel={() => {
                          setOpen(false);
                        }}
                        mode="date"
                        androidVariant="iosClone"
                      />
                    </View>
                    <View
                      style={{
                        alignContent: 'flex-start',
                        width: '100%',
                        zIndex: 10,
                      }}
                    >
                      <Text
                        style={{
                          textAlign: 'left',
                          marginTop: 18,
                          marginBottom: 10,
                          alignContent: 'flex-start',
                          color: '#1E263C',
                          lineHeight: 22,
                          fontFamily: 'SFUIText-Semibold',
                        }}
                      >
                        Location
                      </Text>
                      <View
                        style={{
                          width: '100%',
                          alignItems: 'center',
                          flexDirection: 'row',
                        }}
                      >
                        <PlacesInput
                          googleApiKey="AIzaSyBRMDOmQy7wF_f4ECcnYYGHW163EV2yUQc"
                          placeHolder="Select Location"
                          language="en-US"
                          onSelect={(place: {
                            result: {
                              formatted_address: any;
                              geometry: {location: {lat: any; lng: any}};
                            };
                          }) => {
                            setLocation({
                              add: place.result.formatted_address,
                              lat: place.result.geometry.location.lat,
                              lon: place.result.geometry.location.lng,
                            });
                          }}
                          stylesInput={{
                            height: wp(13),
                            borderRadius: 8,
                            borderBottomLeftRadius: 8,
                            borderTopLeftRadius: 8,
                            color: '#8A98AC',
                            width: wp(90),
                            backgroundColor: '#EDEEF7',
                          }}
                          stylesContainer={{
                            position: 'relative',
                            alignSelf: 'stretch',
                            margin: 0,
                            top: 0,
                            left: 0,
                            right: 0,
                            bottom: 0,
                            shadowOpacity: 0,
                          }}
                          stylesList={{
                            borderColor: '#0000',
                            borderLeftWidth: 1,
                            borderRightWidth: 1,
                            borderBottomWidth: 1,
                            left: -1,
                            right: -1,
                          }}
                        />
                      </View>
                    </View>
                    <View style={{width: '100%', alignSelf: 'center'}}>
                      <Text
                        style={{
                          textAlign: 'left',
                          marginTop: 18,
                          marginBottom: 8,
                          alignContent: 'flex-start',
                          color: '#1E263C',
                          lineHeight: 22,
                          fontFamily: 'SFUIText-Semibold',
                        }}
                      >
                        Gender
                      </Text>
                      <View
                        style={{
                          width: '70%',
                          paddingVertical:
                            width === 320 || height === 610.6666666666666
                              ? 20
                              : 26,
                          flexDirection: 'row',
                          justifyContent: 'center',
                          alignSelf: 'flex-start',
                          alignItems: 'center',
                        }}
                      >
                        <Tabs
                          selected={gender}
                          style={{
                            height: wp(13),
                            backgroundColor: '#EDEEF7',
                            width: '100%',
                            borderRadius: 8,
                          }}
                          selectedStyle={{
                            color: gender === '' ? '#8A98AC' : 'white',
                          }}
                          onSelect={(el: {
                            props: {name: React.SetStateAction<string>};
                          }) => setGender(el.props.name)}
                        >
                          <Text
                            style={{
                              color: '#8A98AC',
                              fontFamily:
                                gender === '0'
                                  ? 'MPLUS1p-Bold'
                                  : 'MPLUS1p-Medium',
                            }}
                          >
                            Male
                          </Text>

                          <Text
                            style={{
                              color: '#8A98AC',
                              fontFamily:
                                gender === '1'
                                  ? 'MPLUS1p-Bold'
                                  : 'MPLUS1p-Medium',
                            }}
                          >
                            Female
                          </Text>
                        </Tabs>
                      </View>
                    </View>
                    <View
                      style={{
                        alignContent: 'flex-start',
                        width: '100%',
                        ...(Platform.OS === 'ios' && {
                          zIndex: 9,
                        }),
                      }}
                    >
                      <Text
                        style={{
                          textAlign: 'left',
                          marginTop: 18,
                          marginBottom: 10,
                          alignContent: 'flex-start',
                          color: '#1E263C',
                          lineHeight: 22,
                          fontFamily: 'SFUIText-Semibold',
                        }}
                      >
                        What is your religion?
                      </Text>
                      <DropDownPicker
                        items={religionOptionsList}
                        dropDownMaxHeight={150}
                        labelStyle={{
                          fontSize: 14,
                          textAlign: 'left',
                          color: '#C2C3CF',
                        }}
                        defaultValue={religion}
                        containerStyle={{height: 50}}
                        style={{
                          width: '100%',
                          height: wp(13),
                          backgroundColor: '#fff',
                        }}
                        itemStyle={{
                          justifyContent: 'flex-start',
                        }}
                        dropDownStyle={{backgroundColor: '#fff'}}
                        onChangeItem={(item) => setReligion(item.value)}
                      />
                    </View>

                    <View
                      style={{
                        alignContent: 'flex-start',
                        width: '100%',
                        zIndex: 8,
                      }}
                    >
                      <Text
                        style={{
                          textAlign: 'left',
                          marginTop: 18,
                          marginBottom: 10,
                          alignContent: 'flex-start',
                          color: '#1E263C',
                          lineHeight: 22,
                          fontFamily: 'SFUIText-Semibold',
                        }}
                      >
                        Who are you interested in?
                      </Text>
                      <View
                        style={{
                          paddingVertical: 28,
                          width: '100%',
                          flexDirection: 'row',
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}
                      >
                        <Tabs
                          selected={interestedIn}
                          style={{
                            backgroundColor: '#EDEEF7',
                            width: '100%',
                            borderWidth: 1,
                            borderRadius: 8,
                            borderColor: 'white',
                          }}
                          selectedStyle={{color: 'white'}}
                          onSelect={(el: {
                            props: {name: React.SetStateAction<number>};
                          }) => setInterestedIn(el.props.name)}
                        >
                          <Text
                            style={{
                              color: '#8A98AC',
                              fontFamily:
                                interestedIn === 0
                                  ? 'MPLUS1p-Bold'
                                  : 'MPLUS1p-Medium',
                            }}
                          >
                            Male
                          </Text>
                          <Text
                            style={{
                              color: '#8A98AC',
                              fontFamily:
                                interestedIn === 1
                                  ? 'MPLUS1p-Bold'
                                  : 'MPLUS1p-Medium',
                            }}
                          >
                            Female
                          </Text>
                          <Text
                            style={{
                              color: '#8A98AC',
                              fontFamily:
                                interestedIn === 2
                                  ? 'MPLUS1p-Bold'
                                  : 'MPLUS1p-Medium',
                            }}
                          >
                            Both
                          </Text>
                        </Tabs>
                      </View>
                    </View>
                    <View
                      style={{
                        alignContent: 'flex-start',
                        width: '100%',
                        paddingBottom: 150,
                        ...(Platform.OS === 'ios' && {
                          zIndex: 5,
                        }),
                      }}
                    >
                      <Text
                        style={{
                          textAlign: 'left',
                          marginTop: 18,
                          marginBottom: 10,
                          alignContent: 'flex-start',
                          color: '#1E263C',
                          lineHeight: 22,
                          fontFamily: 'SFUIText-Semibold',
                        }}
                      >
                        What type of relationship are {'\n'}you looking for?
                      </Text>

                      <DropDownPicker
                        items={relationshipTypeList}
                        labelStyle={{
                          fontSize: 14,
                          textAlign: 'left',
                          color: '#C2C3CF',
                        }}
                        onOpen={() => {
                          setBottomHeight(180);
                        }}
                        onClose={() => setBottomHeight(10)}
                        defaultValue={lookingFor}
                        containerStyle={{height: 50}}
                        style={{width: '100%', backgroundColor: '#fff'}}
                        itemStyle={{
                          justifyContent: 'flex-start',
                        }}
                        dropDownStyle={{backgroundColor: '#fff'}}
                        onChangeItem={(item) => setLookingFor(item.value)}
                      />
                    </View>
                  </View>
                )}
              </ScrollView>
            </View>
            <View style={{flex: 1}}>
              <View style={bottomButton.bottomView}>
                {isLoading ? (
                  <ActivityIndicator
                    style={bottomButton.login}
                    size="large"
                    color="#8897AA"
                  />
                ) : (
                  <Button
                    style={bottomButton.login}
                    disabled={!(dateBirthday && fullName && gender)}
                    color="#8897AA"
                    mode="contained"
                    onPress={registerUserBioScreen}
                  >
                    <Text
                      style={{
                        textTransform: 'capitalize',
                        fontWeight: 'bold',
                        fontSize: 18,
                        color: 'white',
                      }}
                    >
                      Next
                    </Text>
                  </Button>
                )}
              </View>
            </View>
          </>
        )}
      </View>
    </SafeAreaView>
  );
}

const bottomButton = StyleSheet.create({
  bottomView: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    zIndex: 3,
    alignSelf: 'center',
    bottom: 0,
  },
  login: {
    marginBottom: hp('2'),
    borderRadius: 8,
    width: '100%',
  },
});

const sideLogoStyles = StyleSheet.create({
  image: {
    marginTop: 0,
    marginLeft: 20,
  },
  imageLogo: {
    width: wp(7.5),
    height: wp(6.5),
    left: -4,
  },
  container: {
    zIndex: 10,
    width: '100%',
    position: 'absolute',
    top: 0 + getStatusBarHeight(),
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    marginTop: 0,
    backgroundColor: 'white',
  },
});

const styles = StyleSheet.create({
  forgotPassword: {
    width: '100%',
    alignItems: 'flex-end',
    marginRight: 10,
    marginBottom: 24,
  },
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  forgot: {
    fontSize: 13,
    marginTop: 10,
    marginRight: -2,
    color: theme.colors.secondary,
  },
  rememberMe: {
    fontSize: 13,
    marginTop: 10,
    marginLeft: 10,
    color: theme.colors.secondary,
  },
  link: {
    fontWeight: '100',
    color: '#9872FA', // theme.colors.primary,
    lineHeight: 30,
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    width: '100%',
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'center',
  },
});

export default RegisterUserBioScreen;
