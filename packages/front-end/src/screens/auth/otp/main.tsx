import * as React from 'react';
import {
  Platform,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  BackHandler,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import {PinView} from '../../../components/PinView';
import {Colors} from '../../../constants/colors';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Toast from 'react-native-toast-message';
import {
  useVerifyEmail,
  VerifyEmailResponse,
} from '../../../api/hooks/email_login';
import {AxiosResponse} from 'axios';

const MainComponent = ({route, navigation}: any) => {
  const pinView = React.useRef<any>(null);
  const [showRemoveButton, setShowRemoveButton] = React.useState(false);
  const [enteredPin, setEnteredPin] = React.useState('');
  const [showCompletedButton, setShowCompletedButton] = React.useState(false);
  const [countDown, setCountdown] = React.useState(59);

  const {mutateAsync, isLoading} = useVerifyEmail();

  let resendLoad;
  //listen to backhandler
  const backAction = React.useCallback(() => {
    Toast.show({
      text1: 'Application Info',
      text2: "Oops, you can't go back",
      type: 'error',
    });
    return true;
  }, []);

  React.useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );
    return () => backHandler.remove();
  }, [backAction]);

  React.useEffect(() => {
    let timer: any;
    if (countDown > 0) {
      timer = setTimeout(() => setCountdown(countDown - 1), 1000);
    }
    return () => clearTimeout(timer);
  }, [countDown]);

  React.useEffect(() => {
    if (enteredPin.length > 0) {
      setShowRemoveButton(true);
    } else {
      setShowRemoveButton(false);
    }
    if (enteredPin.length === 6) {
      setShowCompletedButton(true);
    } else {
      setShowCompletedButton(false);
    }
  }, [enteredPin]);

  const handleSubmit = React.useCallback(async () => {
    try {
      const response: AxiosResponse<any> = await mutateAsync({
        emailAddress: route?.params?.emailAddress,
        code: enteredPin,
      });
      console.log('response', response.data);
      Toast.show({
        type: 'success',
        text1: 'Success',
        text2: `Email verified`,
      });
      navigation.navigate('LoginScreen');
    } catch (error) {
      Toast.show({
        type: 'error',
        text1: 'Application error',
        text2: 'Could not verify email',
      });
    }
  }, [mutateAsync, enteredPin]);

  const handleResendCode = React.useCallback(() => {}, []);

  return (
    <React.Fragment>
      <SafeAreaView style={styles.outerContainer}>
        <View style={styles.container}>
          <View style={{marginBottom: RFValue(10)}}>
            <Text style={styles.title}>Verify email address</Text>
            <Text style={styles.description}>
              Kindly enter the 6-digit code sent to this email address:{' '}
              <Text style={{color: Colors.buttonColor}}>
                {route?.params?.emailAddress}-{route?.params?.otp}
              </Text>
            </Text>
          </View>
          <View style={styles.pinviewContainer}>
            <PinView
              showInputText
              inputSize={RFValue(36)}
              ref={pinView}
              pinLength={6}
              buttonSize={RFValue(55)}
              onValueChange={(value: string) => setEnteredPin(value)}
              buttonAreaStyle={{
                ...Platform.select({
                  ios: {
                    marginTop: RFValue(10),
                  },
                }),
              }}
              inputAreaStyle={{
                marginBottom: RFValue(20),
              }}
              inputViewEmptyStyle={{
                backgroundColor: Colors.gray['100'],
                borderWidth: 1,
                borderColor: Colors.gray['100'],
              }}
              inputViewFilledStyle={{
                backgroundColor: Colors.buttonColor,
              }}
              buttonViewStyle={{
                borderWidth: 1,
                backgroundColor: Colors.white,
                borderColor: Colors.primary['500'],
              }}
              buttonTextStyle={{
                color: Colors.buttonColor,
              }}
              onButtonPress={(key: string) => {
                if (key === 'custom_left') {
                  pinView?.current?.clear();
                }
                if (key === 'custom_right') {
                  handleSubmit();
                }
              }}
              customLeftButton={
                showRemoveButton ? (
                  <Ionicons
                    name={'ios-backspace-outline'}
                    size={RFValue(30)}
                    color={Colors.red['600']}
                  />
                ) : undefined
              }
              customRightButton={
                showCompletedButton ? (
                  <>
                    {isLoading ? (
                      <ActivityIndicator color={Colors.buttonColor} />
                    ) : (
                      <Ionicons
                        name={'ios-checkmark'}
                        size={36}
                        color={Colors.buttonColor}
                      />
                    )}
                  </>
                ) : undefined
              }
            />
          </View>

          <View>
            <View style={styles.regiterContainer}>
              <Text
                style={{
                  ...Platform.select({
                    ios: {
                      marginBottom: RFValue(5),
                    },
                  }),
                }}
              >
                Didin't receive code?
              </Text>
              {resendLoad ? (
                <Text style={{color: Colors.yellow}}>Sending...</Text>
              ) : countDown !== 0 ? (
                <Text style={{color: Colors.yellow}}>
                  0:{countDown < 10 ? `0${countDown}` : countDown}
                </Text>
              ) : (
                <TouchableOpacity onPress={handleResendCode}>
                  <Text style={{color: Colors.buttonColor}}>Resend</Text>
                </TouchableOpacity>
              )}
            </View>
          </View>
        </View>
      </SafeAreaView>
    </React.Fragment>
  );
};

const styles = StyleSheet.create({
  outerContainer: {flex: 1, backgroundColor: Colors.white},
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: RFValue(10),
  },
  title: {
    fontSize: RFValue(23),
    textAlign: 'center',
    fontFamily: 'SFUIText-Semibold',
  },
  description: {
    fontSize: RFValue(13),
    color: Colors.gray['500'],
    marginTop: RFValue(5),
    textAlign: 'center',
  },
  regiterContainer: {
    alignItems: 'center',
    ...Platform.select({
      ios: {
        marginTop: RFValue(20),
      },
    }),
    marginBottom: RFValue(100),
  },
  pinviewContainer: {},
});

export default MainComponent;
