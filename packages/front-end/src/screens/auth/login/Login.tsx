import * as React from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  StatusBar,
  View,
  Image,
  KeyboardAvoidingView,
  ActivityIndicator,
  Platform,
  Dimensions,
  Alert,
} from 'react-native';
import {Text} from 'react-native-paper';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {LoginBackground, StaarzLogo} from './components';
import {CheckBox} from 'react-native-elements';
import {
  AccessToken,
  GraphRequest,
  GraphRequestManager,
  LoginManager,
} from 'react-native-fbsdk';
import {
  FacebookLoginResponse,
  FacebookUserDetails,
  useLoginWithFacebook,
} from '../../../api/hooks/login';
import {useNetInfo} from '@react-native-community/netinfo';
import {useAuthProvider} from '../../../../App';
import {GoogleSignin} from '@react-native-google-signin/google-signin';
import Zocial from 'react-native-vector-icons/Zocial';
import TextInput from '../../../components/TextInput';
import DropdownAlert from 'react-native-dropdownalert';
import Toast from 'react-native-toast-message';
import Button from '../../../components/Button';
import {AxiosResponse} from 'axios';
import auth from '@react-native-firebase/auth';
import {
  EmailLoginResponse,
  useBasicEmailLogin,
  getUserProfile,
  useSendEmailVerification,
  SendEmailVerificationCodeResponse,
} from '../../../api/hooks/email_login';
import {emailValidator} from '../../../helpers/emailValidator';

const {width} = Dimensions.get('window'); // full width
const {height} = Dimensions.get('window');

GoogleSignin.configure({
  webClientId:
    '323997151563-qt7jj78gn7ule62erhs7d72l06tq8kn6.apps.googleusercontent.com',
});

function Login(props: {navigation: any}) {
  const [checked, setChecked] = React.useState(false);
  const [email, setEmail] = React.useState({value: '', error: ''});
  const [password, setPassword] = React.useState({value: '', error: ''});

  const [passtrue, setpasstrue] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [userInfo, setUserInfo] = React.useState<any>();
  const [isProfileDetailsLoading, setIsProfileDetailsLoading] =
    React.useState(false);
  const [token, setToken] = React.useState<any>();
  const {navigation} = props;
  const {signIn} = useAuthProvider();
  const {isInternetReachable} = useNetInfo();

  const {mutateAsync, isLoading} = useLoginWithFacebook();
  const {mutateAsync: basicEmailLoginInvoker, isLoading: isBasicLoginLoading} =
    useBasicEmailLogin();
  const {mutateAsync: sendEmailInvoker, isLoading: isEmailVerificationLoading} =
    useSendEmailVerification();

  const dropDownAlertRef = React.useRef<any>();

  const handleSendEmailVerification = React.useCallback(
    async (emailAddress: string) => {
      try {
        const response: AxiosResponse<SendEmailVerificationCodeResponse> =
          await sendEmailInvoker({
            emailAddress,
          });
        if (response.data?.otp) {
          Toast.show({
            type: 'success',
            text1: 'Success',
            text2: `Verify account`,
          });
          return navigation.push('OTPScreen', {
            emailAddress: emailAddress,
            otp: response.data?.otp,
          });
        } else {
          Toast.show({
            type: 'error',
            text1: 'Application error',
            text2: "user with given email doesn't exist",
          });
        }
      } catch (error) {
        Toast.show({
          type: 'error',
          text1: 'Application error',
          text2: "user with given email doesn't exist",
        });
      }
    },
    [sendEmailInvoker],
  );

  const handleBasicEmailLogiin = React.useCallback(async () => {
    if (!isInternetReachable) {
      return Toast.show({
        text1: 'Oops, an error occured',
        text2: 'Internet is not reachable',
        type: 'error',
      });
    }
    const emailError = emailValidator(email.value);
    if (email.value === '' || password.value === '') {
      return Toast.show({
        type: 'error',
        text1: 'Validation error',
        text2: 'All fields are required',
      });
    }
    if (emailError) {
      return Toast.show({
        type: 'error',
        text1: 'Validation error',
        text2: 'Enter a valid email address',
      });
    }
    try {
      const loginResponse: AxiosResponse<EmailLoginResponse> =
        await basicEmailLoginInvoker({
          emailAddress: email.value,
          password: password.value,
        });
      const token = loginResponse.data.access_token;
      if (token) {
        setIsProfileDetailsLoading(true);
        const profileResponse = await getUserProfile(token);
        console.log('profile res --->', profileResponse);
        if (profileResponse?.emailAddress) {
          await signIn(token, {
            ...profileResponse,
          });
          Toast.show({
            type: 'success',
            text1: 'Success',
            text2: `Welcome back ${profileResponse.fullName}`,
          });
          setIsProfileDetailsLoading(false);
          return navigation.navigate('BottomTabs');
        }
      } else {
        setIsProfileDetailsLoading(false);
        Toast.show({
          type: 'error',
          text1: 'Application error',
          text2: 'Wrong email or password',
        });
      }
    } catch (error: any) {
      handleSendEmailVerification(email.value);
    }
  }, [
    setIsProfileDetailsLoading,
    basicEmailLoginInvoker,
    getUserProfile,
    handleSendEmailVerification,
    isInternetReachable,
    email,
    password,
  ]);

  const handleFacebookLogin = async (
    userInfo: FacebookUserDetails,
    token: string,
  ) => {
    try {
      const response: AxiosResponse<FacebookLoginResponse> = await mutateAsync({
        access_token: token,
      });
      // await signIn(response?.data?.access_token, userInfo);
      Toast.show({
        type: 'success',
        text1: 'Success',
        text2: `Welcome back ${userInfo.first_name}`,
      });
      return;
      navigation.navigate('BottomTabs');
    } catch (error: any) {
      console.log('error occured', error);
      Toast.show({
        type: 'error',
        text1: 'Application error',
        text2: error.response?.data.message ?? 'Oops, an error occured',
      });
    }
  };

  const getInfoFromToken = (token: any) => {
    const PROFILE_REQUEST_PARAMS = {
      fields: {
        string: 'id, name, first_name, last_name, email, picture',
      },
    };
    const profileRequest = new GraphRequest(
      '/me',
      {accessToken: token, parameters: PROFILE_REQUEST_PARAMS},
      (error, aResult: any) => {
        if (error) {
          console.log(`login info has error: ${error}`);
        } else {
          handleFacebookLogin(aResult, token);
        }
      },
    );
    new GraphRequestManager().addRequest(profileRequest).start();
  };

  const handleFacebookTrigger = async () => {
    try {
      LoginManager.setLoginBehavior('web_only');
      await LoginManager.logInWithPermissions(['public_profile', 'email']).then(
        (login) => {
          if (login.isCancelled) {
            console.log('Login cancelled');
          } else {
            AccessToken.getCurrentAccessToken().then((data) => {
              const newAccessToken = data?.accessToken.toString();
              getInfoFromToken(newAccessToken);
            });
          }
        },
        (error) => {
          console.log(`Login fail with error000: ${error}`);
        },
      );
    } catch ({message}) {
      console.log('User have facebook app');
      if (Platform.OS === 'android') {
        LoginManager.setLoginBehavior('web_only');
      }
      await LoginManager.logInWithPermissions([
        'public_profile',
        'email',
        'mobile',
      ]).then(
        (login) => {
          if (login.isCancelled) {
            console.log('Login cancelled');
          } else {
            AccessToken.getCurrentAccessToken().then((data) => {
              const newAccessToken = data?.accessToken.toString();
              getInfoFromToken(newAccessToken);
            });
          }
        },
        (error) => {
          console.log(`Login fail with error----: ${error}`);
        },
      );
    }
  };

  const loginWithFacebook = React.useCallback(() => {
    if (!isInternetReachable) {
      return Toast.show({
        text1: 'Oops, an error occured',
        text2: 'Internet is not reachable',
        type: 'error',
      });
    }
    handleFacebookTrigger();
  }, [handleFacebookTrigger]);

  const googleSignIn = async () => {
    setLoading(true);
    const {idToken}: any = await GoogleSignin.signIn().catch((e) => {
      Alert.alert(e.message);
      setLoading(false);
    });
    // Create a Google credential with the token
    const googleCredential = await auth.GoogleAuthProvider.credential(idToken);
    const accessToken = await (await GoogleSignin.getTokens()).accessToken;

    // Sign-in the user with the credential
    await auth()
      .signInWithCredential(googleCredential)
      .then(async (res: any) => {
        setUserInfo(res);
        setToken(accessToken);
        // await signIn(accessToken, {
        //   email: res.user?.email,
        //   picture: {
        //     data: {
        //       url: res?.user?.photoURL,
        //     },
        //   },
        //   last_name: res?.additionalUserInfo?.profile?.family_name,
        //   name: res?.user?.displayName,
        //   first_name: res?.additionalUserInfo?.profile?.given_name,
        //   id: 0,
        // });
        // Toast.show({
        //   type: 'success',
        //   text1: 'Success',
        //   text2: `Welcome back ${res?.additionalUserInfo?.profile?.given_name}`,
        // });
        return;
        navigation.navigate('BottomTabs');
      })
      .catch((e) => {
        Alert.alert(e.message);
      });
    // console.log(res);
    setLoading(false);
  };

  const handleGoResetPassword = React.useCallback(() => {
    navigation.navigate('ResetPasswordScreen');
  }, []);

  return (
    <LoginBackground>
      <KeyboardAvoidingView
        style={{
          alignItems: 'center',
          width: '100%',
          flex: 8,

          backgroundColor: 'rgba(0, 0, 0, 0.69)',
        }}
        behavior={Platform.OS === 'ios' ? 'padding' : 'padding'}
      >
        <>
          <View
            style={{
              height: '40%',
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
              width: '100%',
            }}
            testID="login-page"
          >
            <StatusBar
              barStyle="light-content"
              backgroundColor="rgba(0, 0, 0, 0.7)"
            />
            <StaarzLogo />
          </View>
          <View style={{height: '60%', alignItems: 'center', width: '100%'}}>
            <View style={{flex: 1, alignItems: 'center'}}>
              <View
                style={{
                  position: 'absolute',
                  bottom: 160,
                  alignItems: 'center',
                }}
              >
                <Button
                  style={{
                    borderRadius: 45,
                    width: wp(90),
                    bottom:
                      width === 320 || height === 610.6666666666666 ? -25 : 8,
                  }}
                  color="#2673CB"
                  mode="contained"
                  onPress={loginWithFacebook}
                >
                  <Zocial name="facebook" color="white" size={hp('2.3')} />
                  <Text
                    style={{
                      textTransform: 'none',
                      fontWeight: '500',
                      fontSize: hp('2.1'),
                      color: 'white',
                    }}
                  >
                    &nbsp;&nbsp;Connect with Facebook
                  </Text>
                </Button>
                <Button
                  style={{
                    borderRadius: 45,
                    width: wp(90),
                    bottom:
                      width === 320 || height === 610.6666666666666 ? -25 : 12,
                  }}
                  color="#FC3850"
                  mode="contained"
                  onPress={googleSignIn}
                >
                  <Zocial
                    name="googleplus"
                    color="white"
                    size={hp('2.3')}
                    style={{}}
                  />
                  <Text
                    style={{
                      textTransform: 'none',
                      fontWeight: '500',
                      fontSize: hp('2.1'),
                      color: 'white',
                    }}
                  >
                    &nbsp;&nbsp;Connect with Google
                  </Text>
                </Button>
                <View
                  style={{
                    height: 30,
                    width: width / 1.01,
                    alignItems: 'center',
                    bottom:
                      width === 320 || height === 610.6666666666666 ? -40 : 0,
                  }}
                >
                  <Image
                    source={require('../../../assets/or.png')}
                    style={styles.dividerLine}
                  />
                </View>
              </View>

              <View style={{position: 'absolute', bottom: 0}}>
                <TextInput
                  style={{backgroundColor: '#3F2A30'}}
                  label="Email"
                  returnKeyType="next"
                  value={email.value}
                  onChangeText={(text: string) =>
                    setEmail({value: text, error: ''})
                  }
                  error={!!email.error}
                  errorText={email.error}
                  autoCapitalize="none"
                  autoCompleteType="email"
                  textContentType="emailAddress"
                  keyboardType="email-address"
                  placeholder="Enter your email"
                  theme={{
                    borderRadius: 25,
                    colors: {
                      placeholder: 'white',
                      text: 'white',
                      primary: '#FFFFFF',
                      underlineColor: 'transprent',
                    },
                  }}
                  description={''}
                  icon={''}
                  onPress={() => null}
                  colorIcon={''}
                  url={''}
                />

                <TextInput
                  style={{
                    backgroundColor: '#3F2A30',
                  }}
                  label="Password"
                  returnKeyType="done"
                  value={password.value}
                  onChangeText={(text: string) =>
                    setPassword({value: text, error: ''})
                  }
                  error={!!password.error}
                  errorText={password.error}
                  url={
                    passtrue
                      ? require('../../../assets/eyeIcon.png')
                      : require('../../../assets/eyeIcon.png')
                  }
                  onPress={() => setpasstrue(!passtrue)}
                  colorIcon="#fff"
                  secureTextEntry={passtrue}
                  placeholder="Enter your password"
                  theme={{
                    colors: {
                      placeholder: '#fff',
                      text: '#fff',
                      primary: '#FFFFFF',
                      borderWidth: 0.5,
                      underlineColor: 'transparent',
                    },
                  }}
                  description={''}
                  icon={''}
                />
              </View>
            </View>
          </View>
        </>
      </KeyboardAvoidingView>
      <View
        style={{
          alignItems: 'center',
          width: '100%',
          height: '20%',
          flex: 2,
          backgroundColor: 'rgba(0, 0, 0, 0.69)',
        }}
      >
        <View
          style={{
            flexDirection: 'row',
            width: '100%',
            alignItems: 'center',
            height: '29%',
          }}
        >
          <View style={{width: '50%', zIndex: 10}}>
            <CheckBox
              disabled={false}
              checked={checked}
              size={18}
              containerStyle={{
                backgroundColor: 'transparent',
                borderWidth: 0,
                borderRadius: 8,
                paddingVertical: 5,
              }}
              onPress={() => {
                setChecked(!checked);
              }}
              textStyle={{
                color: 'white',
                fontSize: hp('1.7'),
                alignItems: 'center',
                textAlign: 'center',
                marginLeft: 4,
                lineHeight: 20,
              }}
              title="Remember me"
            />
          </View>
          <View
            style={{
              width: '50%',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <TouchableOpacity onPress={handleGoResetPassword}>
              <Text
                style={{
                  color: '#CBCEE8',
                  textAlign: 'right',
                  fontSize: hp('1.7'),
                  right: -20,
                }}
              >
                Forgot password?{' '}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.bottomButtonView}>
          {isLoading ||
          isBasicLoginLoading ||
          isProfileDetailsLoading ||
          isEmailVerificationLoading ? (
            <ActivityIndicator
              style={styles.bottomButtonLogin}
              size="large"
              color="#8897AA"
            />
          ) : (
            <Button
              style={styles.bottomButtonLogin}
              color="#8897AA"
              mode="contained"
              disabled={
                isLoading ||
                isBasicLoginLoading ||
                isProfileDetailsLoading ||
                isEmailVerificationLoading
              }
              onPress={handleBasicEmailLogiin}
            >
              <Text
                style={{
                  textTransform: 'capitalize',
                  fontWeight: 'bold',
                  fontSize: hp('2.3'),
                  color: 'white',
                }}
              >
                Login
              </Text>
            </Button>
          )}
          <View>
            <Text
              style={{color: 'white', letterSpacing: 0.5, fontSize: hp('1.8')}}
            >
              Still doesnt have an account?{' '}
            </Text>
          </View>
          <TouchableOpacity
            style={{}}
            onPress={() => navigation.navigate('RegisterScreen')}
          >
            <Text
              style={{
                fontWeight: 'bold',
                color: '#8A98AC',
                lineHeight: 21,
                fontSize: hp('1.8'),
              }}
            >
              Sign up
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      <DropdownAlert
        ref={dropDownAlertRef}
        imageSrc={require('../../../assets/ForgetMEssage.png')}
        imageStyle={{width: wp(7), height: wp(7), alignSelf: 'center'}}
        containerStyle={{
          padding: 15,
          backgroundColor: '#7643F6',
          borderTopRightRadius: 20,
          borderBottomLeftRadius: 20,
          borderBottomRightRadius: 20,
          top: Platform.OS === 'ios' ? '10%' : '2%',
          width: '90%',
          alignSelf: 'center',
        }}
        closeInterval={10000}
        messageStyle={{
          fontSize: wp(2.8),
          color: '#FFFFFF',
          top: -3,
          fontFamily: 'MPLUS1p-Bold',
        }}
        titleStyle={{
          fontSize: wp(2.8),
          color: '#FFFFFF',
          fontFamily: 'MPLUS1p-Bold',
        }}
        defaultTextContainer={{fontSize: wp(1), left: 10}}
      />
    </LoginBackground>
  );
}

const styles = StyleSheet.create({
  dividerLine: {
    width: wp(90),
  },
  bottomButtonView: {
    width: '90%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: Platform.OS === 'ios' ? hp(1.5) : hp(1),
  },
  bottomButtonLogin: {
    marginBottom: 10,
    paddingHorizontal: 20,
    borderRadius: 8,
  },
});

export default Login;
