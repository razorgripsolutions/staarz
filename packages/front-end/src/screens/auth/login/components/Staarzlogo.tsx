import * as React from 'react';
import {View, Image, StyleSheet} from 'react-native';

function StaarzLogo() {
  return (
    <View style={styles.container}>
      <Image source={require('../../../../assets/logo_41x48.png')} />
      <Image
        source={require('../../../../assets/staarz_180X33_white.png')}
        style={styles.staarzLogo}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginTop: '15%',
  },
  staarzLogo: {
    marginTop: 8,
    marginLeft: 20,
  },
});

export {StaarzLogo};
