import React from 'react';

export interface LoginBackgroundComponentProp {
  children: React.ReactNode;
}
