import * as React from 'react';
import {ImageBackground, StyleSheet} from 'react-native';
import {LoginBackgroundComponentProp} from './types';

function LoginBackground({children}: LoginBackgroundComponentProp) {
  return (
    <ImageBackground
      source={require('../../../../assets/loginscreenbg.png')}
      resizeMode="stretch"
      style={styles.container}
    >
      {children}
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',

    flex: 1,
  },
});

export {LoginBackground};
