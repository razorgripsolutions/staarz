import * as React from 'react';
import {ResetPasswordNavigatorStack} from './types';
import {createStackNavigator} from '@react-navigation/stack';
import {ResetPasswordScreen} from './password';

const Stack = createStackNavigator<ResetPasswordNavigatorStack>();

const defaultHeaderOptions = {
  headerShown: false,
};

function ResetPasswordComponent() {
  return (
    <Stack.Navigator initialRouteName="password">
      <Stack.Screen
        name="password"
        component={ResetPasswordScreen}
        options={{...defaultHeaderOptions}}
      />
    </Stack.Navigator>
  );
}

export default ResetPasswordComponent;
