import {
  Text,
  KeyboardAvoidingView,
  StyleSheet,
  Alert,
  View,
  TouchableOpacity,
  Image,
  Platform,
} from 'react-native';
import Button from '../../../../components/Button';
import React, {useCallback, useState} from 'react';
import {LoginBackground} from '../../../auth/login/components';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import TextInput from '../../../../components/TextInput';
import {FontStyles} from '../../../../constants/font';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import Ionicons from 'react-native-vector-icons/Ionicons';

const ResetPasswordScreen = ({navigation}: any) => {
  const [password, setPassword] = useState({value: '', error: ''});
  const [confirmPassword, setConfirmPassword] = useState({
    value: '',
    error: '',
  });
  const [secureTextEntry, setSecureTextEntry] = useState(false);
  const [confirmSecureTextEntry, setConfirmSecureTextEntry] = useState(false);
  const eyeIcon = require('../../../../assets/eyeIcon.png');

  const resetPassword = useCallback(() => {
    if (!password.value || !confirmPassword.value) {
      Alert.alert('Please fill all the fields');
      return;
    }
    if (password.value !== confirmPassword.value) {
      Alert.alert('The password confirmation does not match');
      return;
    }
  }, [password, confirmPassword]);

  return (
    <LoginBackground>
      <KeyboardAvoidingView
        style={styles.keyboardAvoidingView}
        behavior={'padding'}
      >
        <StaarzLogo goBack={navigation.goBack} />

        <Text style={styles.title}>Create new Password</Text>
        <Text style={styles.text}>
          The new password must be different{'\n'}from your previous password
        </Text>

        <TextInput
          style={styles.passwordStyle}
          label="Password"
          returnKeyType="done"
          value={password.value}
          onChangeText={(text: string) => setPassword({value: text, error: ''})}
          error={!!password.error}
          errorText={password.error}
          url={eyeIcon}
          onPress={() => setSecureTextEntry(!secureTextEntry)}
          colorIcon="#fff"
          secureTextEntry={secureTextEntry}
          placeholder="Enter your password"
          theme={inputTheme}
          description={undefined}
          icon={undefined}
        />

        <TextInput
          style={styles.passwordStyle}
          label="Confirm Password"
          returnKeyType="done"
          value={confirmPassword.value}
          onChangeText={(text: string) =>
            setConfirmPassword({value: text, error: ''})
          }
          error={!!confirmPassword.error}
          errorText={confirmPassword.error}
          url={eyeIcon}
          onPress={() => setConfirmSecureTextEntry(!confirmSecureTextEntry)}
          colorIcon="#fff"
          secureTextEntry={confirmSecureTextEntry}
          placeholder="Enter your password"
          theme={inputTheme}
          description={undefined}
          icon={undefined}
        />

        <Button
          style={styles.bottomButtonResetPassword}
          mode="contained"
          onPress={() => resetPassword()}
        >
          <Text style={styles.buttonStyle}>Reset Password</Text>
        </Button>
      </KeyboardAvoidingView>
    </LoginBackground>
  );
};

export {ResetPasswordScreen};

export function StaarzLogo({goBack}: {goBack: () => void}) {
  return (
    <View style={sideLogoStyles.container}>
      <View style={{width: '20%'}}>
        <TouchableOpacity
          onPress={goBack}
          style={{
            paddingLeft: 8,
          }}
        >
          <Ionicons name="arrow-back-outline" size={24} color={'#fff'} />
        </TouchableOpacity>
      </View>
      <View
        style={{
          width: '60%',
          alignItems: 'center',
          flexDirection: 'row',
          justifyContent: 'center',
        }}
      >
        <Image
          source={require('../../../../assets/starzlogo.png')}
          style={sideLogoStyles.imageLogo}
        />
        <Text
          style={{
            fontSize: wp(6),
            fontFamily: 'MPLUS1p-Bold',
            color: '#fff',
            left: -2,
            lineHeight: 33,
            textAlign: 'center',
          }}
        >
          STAARZ
        </Text>
      </View>
      <View style={{width: '20%'}} />
    </View>
  );
}

const sideLogoStyles = StyleSheet.create({
  image: {
    marginLeft: 20,
  },
  imageLogo: {
    width: wp(7.5),
    height: wp(6.5),
    left: -4,
  },
  container: {
    position: 'absolute',
    top: Platform.OS !== 'ios' ? 0 : 0 + getStatusBarHeight(),
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    zIndex: 10,
    width: '100%',
  },
});

const styles = StyleSheet.create({
  title: {
    letterSpacing: 0.4,
    fontSize: 26,
    lineHeight: 36,
    fontFamily: FontStyles.semiBold,
    color: '#CBCEE8',
    marginTop: hp(15),
  },

  text: {
    textAlign: 'left',
    letterSpacing: 1,
    color: '#8A98AC',
    lineHeight: 24,
    fontSize: 14,
    fontWeight: '300',
    marginTop: hp(2),
    marginBottom: hp(4),
  },

  keyboardAvoidingView: {
    alignItems: 'center',
    width: '100%',
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.69)',
  },

  passwordStyle: {
    backgroundColor: '#3F2A30',
  },

  bottomButtonResetPassword: {
    marginBottom: 10,
    borderRadius: 8,
    width: '90%',
    backgroundColor: '#8897AA',
    marginTop: hp(4),
  },

  buttonStyle: {
    textTransform: 'none',
    fontWeight: 'bold',
    fontSize: hp('2.3'),
    color: 'white',
  },
});

const inputTheme = {
  colors: {
    placeholder: '#fff',
    text: '#fff',
    primary: '#FFFFFF',
    borderWidth: 0.5,
    underlineColor: 'transparent',
  },
};
