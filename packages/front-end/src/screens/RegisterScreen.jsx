import React, {useState} from 'react';
import {
  Image,
  View,
  StyleSheet,
  TouchableOpacity,
  StatusBar,
  TextInput,
  Dimensions,
  ActivityIndicator,
  Platform,
} from 'react-native';
import {Text} from 'react-native-paper';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {CheckBox} from 'react-native-elements';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {
  AccessToken,
  GraphRequest,
  GraphRequestManager,
  LoginManager,
} from 'react-native-fbsdk';
import {GoogleSignin} from '@react-native-google-signin/google-signin';
import Zocial from 'react-native-vector-icons/Zocial';
import {passwordValidator} from '../helpers/passwordValidator';
import {emailValidator} from '../helpers/emailValidator';
import {theme} from '../core/theme';
import Button from '../components/Button';
import {checkConnection} from '../components/checkConnection';
import {string} from '../const/string';
import {useNetInfo} from '@react-native-community/netinfo';
import Toast from 'react-native-toast-message';
import {useLoginWithFacebook} from '../api/hooks/login';
import {useAuthProvider} from '../../App';
import auth from '@react-native-firebase/auth';
import {Colors} from '../constants/colors';
import {getUserProfile} from '../api/hooks/email_login';
import {useBasicEmailSignup} from '../api/hooks/email_signup';

GoogleSignin.configure({
  webClientId:
    '323997151563-qt7jj78gn7ule62erhs7d72l06tq8kn6.apps.googleusercontent.com',
});

function RegisterScreen({navigation, route}) {
  const [signUpemail, setEmail] = useState({value: '', error: ''});

  const [checked, setChecked] = React.useState(false);
  const [signUppassword, setPassword] = useState({value: '', error: ''});
  const [signUppassword2, setPassword2] = useState({value: '', error: ''});
  const [passtrue, setpasstrue] = useState(true);
  const [passtrue1, setpasstrue1] = useState(true);
  const [isFBLoading, setIsFBloading] = useState(false);
  const [isGoogleLoading, setIsGoogleloading] = useState(false);

  const [loading, setLoading] = React.useState(false);
  const [net, setnet] = React.useState(true);

  const [isDecodingToken, setIsDecodingToken] = useState(false);
  const windowHeight = Dimensions.get('window').height;
  const {width} = Dimensions.get('window');
  const {height} = Dimensions.get('window');

  const {signIn, userState, register} = useAuthProvider();
  const {isInternetReachable} = useNetInfo();

  const {mutateAsync, isLoading} = useLoginWithFacebook();
  const {mutateAsync: emailInvoker, isLoading: emailLoading} =
    useBasicEmailSignup();

  const getfuntion = () => {
    checkConnection().then((res) => {
      if (res === false) {
        setnet(res);
      } else {
        setnet(res);
      }
    });
  };

  const onSignUpPressed = () => {
    checkConnection().then((res) => {
      if (res === false) {
        setnet(res);
      } else {
        setnet(res);
        onSignUpPressed1();
      }
    });
  };

  const onSignUpPressed1 = async () => {
    const reg = /^(?=.*?[A-Z])(?=.*?[0-9]).{8,}$/;
    const emailError = emailValidator(signUpemail.value);
    const passwordError = passwordValidator(signUppassword.value);
    const password2Error = passwordValidator(signUppassword2.value);
    if (emailError || passwordError || password2Error) {
      setEmail({...signUpemail, error: emailError});
      setPassword({...signUppassword, error: passwordError});
      setPassword2({...signUppassword2, error: password2Error});
      return;
    }
    if (reg.test(signUppassword.value) === false) {
      setPassword({
        ...signUppassword,
        error:
          'Password must contain at least 8 characters, 1 capital letter, 1 number',
      });
    } else {
      if (signUppassword.value !== signUppassword2.value) {
        setPassword2({...signUppassword2, error: 'Passwords does not match'});
        return;
      }
      try {
        const response = await emailInvoker({
          emailAddress: signUpemail.value,
          password: signUppassword.value,
        });
        await register({
          ...userState?.registrationData,
          emailAddress: signUpemail.value,
          password: signUppassword.value,
          id: response?.data?.id,
        });
        Toast.show({
          type: 'success',
          text1: 'Success',
          text2: 'Complete your profile',
        });
        return onSuccessValidation_Pass();
      } catch (error) {
        Toast.show({
          type: 'error',
          text1: 'Application error',
          text2: error.response?.data.message ?? 'Oops, an error occured',
        });
      }
    }
  };

  const onSuccessValidation_Pass = () => {
    navigation.navigate('RegisterUserBioScreen');
  };

  const handleFacebookLogin = async (userInfo, token) => {
    try {
      const response = await mutateAsync({
        access_token: token,
      });
      const newToken = response?.data?.access_token;
      if (newToken) {
        setIsDecodingToken(true);
        const profileResponse = await getUserProfile(newToken);
        if (profileResponse?.emailAddress) {
          await register({
            ...userState?.registrationData,
            emailAddress: profileResponse?.emailAddress,
            fullName: profileResponse?.fullName,
            id: profileResponse?.id,
          });
          setIsDecodingToken(false);
          Toast.show({
            type: 'success',
            text1: 'Success',
            text2: 'Complete your profile',
          });
          return onSuccessValidation_Pass();
        }
      }
    } catch (error) {
      console.log('error occured', error);
      Toast.show({
        type: 'error',
        text1: 'Application error',
        text2: error.response?.data.message ?? 'Oops, an error occured',
      });
    }
  };

  const getInfoFromToken = (token) => {
    const PROFILE_REQUEST_PARAMS = {
      fields: {
        string: 'id, name, first_name, last_name, email, picture',
      },
    };
    const profileRequest = new GraphRequest(
      '/me',
      {accessToken: token, parameters: PROFILE_REQUEST_PARAMS},
      (error, aResult) => {
        if (error) {
          console.log(`login info has error: ${error}`);
        } else {
          handleFacebookLogin(aResult, token);
        }
      },
    );
    new GraphRequestManager().addRequest(profileRequest).start();
  };

  const loginWithFacebook = React.useCallback(() => {
    // if (!isInternetReachable) {
    //   return Toast.show({
    //     text1: 'Oops, an error occured',
    //     text2: 'Internet is not reachable',
    //     type: 'error',
    //   });
    // }
    handleFacebookTrigger();
  }, [handleFacebookTrigger]);

  const handleFacebookTrigger = () => {
    try {
      LoginManager.setLoginBehavior('web_only');
      LoginManager.logInWithPermissions(['public_profile', 'email']).then(
        (login) => {
          if (login.isCancelled) {
            console.log('Login cancelled');
          } else {
            AccessToken.getCurrentAccessToken().then((data) => {
              const newAccessToken = data?.accessToken.toString();
              getInfoFromToken(newAccessToken);
            });
          }
        },
        (error) => {
          console.log(`Login fail with error000: ${error}`);
        },
      );
    } catch ({message}) {
      console.log('User have facebook app');
      if (Platform.OS === 'android') {
        LoginManager.setLoginBehavior('web_only');
      }
      LoginManager.logInWithPermissions([
        'public_profile',
        'email',
        'mobile',
      ]).then(
        (login) => {
          if (login.isCancelled) {
            console.log('Login cancelled');
          } else {
            AccessToken.getCurrentAccessToken().then((data) => {
              const newAccessToken = data?.accessToken.toString();
              getInfoFromToken(newAccessToken);
            });
          }
        },
        (error) => {
          console.log(`Login fail with error----: ${error}`);
        },
      );
    }
  };

  const googleSignIn = async () => {
    setLoading(true);
    const {idToken} = await GoogleSignin.signIn().catch((e) => {
      Alert.alert(e.message);
      setLoading(false);
    });
    const googleCredential = await auth.GoogleAuthProvider.credential(idToken);
    const accessToken = await (await GoogleSignin.getTokens()).accessToken;

    return console.log('tokennnn', accessToken);
    await auth()
      .signInWithCredential(googleCredential)
      .then(async (res) => {
        await signIn(accessToken, {
          email: res.user?.email,
          picture: {
            data: {
              url: res?.user?.photoURL,
            },
          },
          last_name: res?.additionalUserInfo?.profile?.family_name,
          name: res?.user?.displayName,
          first_name: res?.additionalUserInfo?.profile?.given_name,
          id: '',
        });
        Toast.show({
          type: 'success',
          text1: 'Success',
          text2: `Good to see you ${res?.additionalUserInfo?.profile?.given_name}`,
        });
        return navigation.navigate('BottomTabs');
      })
      .catch((e) => {
        Alert.alert(e.message);
      });
    setLoading(false);
  };

  return (
    <View
      style={{
        flex: 1,
        padding: 20,
        width: '100%',
        height: hp('100'),
        backgroundColor: theme.colors.surface,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <StatusBar barStyle="dark-content" backgroundColor="white" />
      <StaarzLogo goBack={navigation.goBack} />

      {!net ? (
        <View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
          <Image
            source={require('../assets/internat.png')}
            style={{height: 50, width: 50}}
          />
          <Text
            style={{
              fontSize: 17,
              fontWeight: '700',
              marginTop: 5,
              textAlign: 'center',
            }}
          >
            {string.network}
          </Text>
          <TouchableOpacity
            color="white"
            mode="contained"
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              top: windowHeight * 0.3,
            }}
            onPress={() => getfuntion()}
          >
            <View
              style={{
                backgroundColor: '#7643F8',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 50,
                paddingHorizontal: 30,
                paddingVertical: 20,
                marginTop: 10,
              }}
            >
              {Platform.OS === 'ios' ? (
                <Text style={{fontSize: hp('2'), color: '#fff'}}>
                  {string.Try_Again}
                </Text>
              ) : (
                <Text
                  style={{
                    fontSize: hp('2'),
                    color: '#fff',
                    textAlign: 'center',
                  }}
                >
                  {string.Try_Again}
                </Text>
              )}
            </View>
          </TouchableOpacity>
        </View>
      ) : (
        <KeyboardAwareScrollView>
          <View
            style={{flex: 8, alignItems: 'center', justifyContent: 'center'}}
          >
            <View style={styles.container}>
              <Text
                style={{
                  fontWeight: Platform.OS === 'ios' ? 'bold' : 'bold',
                  letterSpacing: 2,
                  fontSize: hp('3.5'),
                  fontFamily: 'MPLUS1p-Bold',
                  lineHeight: 42,
                  marginTop: 12,
                }}
              >
                Sign up
              </Text>
              <Text
                style={{
                  textAlign: 'center',
                  letterSpacing: 1,
                  lineHeight: 20,
                  color: 'grey',
                  fontSize: 17,
                  marginTop: 8,
                }}
              >
                We make it easy for everyone {'\n'} to get along
              </Text>
              <Text />

              {isFBLoading ? (
                <>
                  <Button
                    disabled
                    style={{borderRadius: 40, width: '85%'}}
                    color="#2673CB"
                    mode="contained"
                    onPress={loginWithFacebook}
                  >
                    <Zocial name="facebook" color="white" size={hp('2.3')} />
                    <Text
                      style={{
                        textTransform: 'none',
                        fontWeight: '500',
                        fontSize: hp('2.1'),
                        color: 'white',
                      }}
                    >
                      &nbsp;&nbsp;Connect with Facebook
                    </Text>
                  </Button>
                </>
              ) : (
                <Button
                  style={{borderRadius: 40, width: wp('90%')}}
                  color="#2673CB"
                  mode="contained"
                  onPress={loginWithFacebook}
                >
                  <Zocial name="facebook" color="white" size={hp('2.3')} />
                  <Text
                    style={{
                      textTransform: 'none',
                      fontWeight: '500',
                      fontSize: hp('2.1'),
                      color: 'white',
                    }}
                  >
                    &nbsp;&nbsp;Connect with Facebook
                  </Text>
                </Button>
              )}
              {isGoogleLoading ? (
                <>
                  <Button
                    disabled
                    style={{borderRadius: 40, width: wp('90%')}}
                    color="#FC3850"
                    mode="contained"
                    onPress={googleSignIn}
                  >
                    <Text style={{marginLeft: 100}}>
                      <Zocial
                        name="googleplus"
                        color="white"
                        size={hp('2.3')}
                      />
                    </Text>
                    <Text
                      style={{
                        textTransform: 'none',
                        fontWeight: 'bold',
                        fontSize: hp('2.1'),
                        color: 'white',
                      }}
                    >
                      &nbsp;&nbsp;Connect with Google
                    </Text>
                  </Button>
                </>
              ) : (
                <Button
                  style={{borderRadius: 40, width: wp('90%')}}
                  color="#FC3850"
                  mode="contained"
                  onPress={googleSignIn}
                >
                  <Text style={{marginLeft: 100}}>
                    <Zocial name="googleplus" color="white" size={hp('2.3')} />
                  </Text>
                  <Text
                    style={{
                      textTransform: 'none',
                      fontWeight: '500',
                      fontSize: hp('2.1'),
                      color: 'white',
                    }}
                  >
                    &nbsp;&nbsp;Connect with Google
                  </Text>
                </Button>
              )}
              <OrDividerLine />
              <View style={{width: wp('90%'), marginTop: 15}}>
                <Text
                  style={{
                    color: '#1E263C',
                    position: 'absolute',
                    zIndex: 10,
                    marginHorizontal: 12,
                    backgroundColor: '#fff',
                    fontSize: wp(4),
                    letterSpacing: 0.8,
                    top: -11,
                    paddingHorizontal: 1,
                  }}
                >
                  {' '}
                  Email{' '}
                </Text>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignSelf: 'center',
                  }}
                >
                  <View style={{width: '100%'}}>
                    <TextInput
                      style={{
                        backgroundColor: '#fff',
                        padding:
                          width === 320 || height === 610.6666666666666
                            ? 8
                            : 15,
                        borderWidth: 0.5,
                        borderColor: '#C3C7E5',
                        borderRadius: 5,
                        fontSize: 16,
                        color: Colors.gray['900'],
                      }}
                      onChangeText={(text) => {
                        setEmail({value: text, error: ''});
                      }}
                      placeholder="Enter your email"
                      autoCapitalize="none"
                      keyboardType="email-address"
                      placeholderTextColor={Colors.gray['500']}
                      defaultValue={
                        userState?.registrationData?.emailAddress ?? ''
                      }
                    />
                  </View>
                  <View
                    style={{
                      width: '10%',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                  />
                </View>

                <Text style={{color: 'red'}}>{signUpemail.error}</Text>
              </View>

              <View style={{width: wp('90%'), marginTop: 15}}>
                <Text
                  style={{
                    color: '#1E263C',
                    position: 'absolute',
                    zIndex: 10,
                    marginHorizontal: 12,
                    backgroundColor: '#fff',
                    fontSize: wp(4),
                    letterSpacing: 0.8,
                    top: -11,
                    paddingHorizontal: 1,
                  }}
                >
                  {' '}
                  Password{' '}
                </Text>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignSelf: 'center',
                  }}
                >
                  <View style={{width: '100%'}}>
                    <TextInput
                      style={{
                        backgroundColor: '#fff',
                        padding:
                          width === 320 || height === 610.6666666666666
                            ? 8
                            : 15,
                        borderWidth: 0.5,
                        borderColor: '#C3C7E5',
                        borderRadius: 5,
                        fontSize: 16,
                        color: Colors.gray['900'],
                      }}
                      onChangeText={(text) =>
                        setPassword({value: text, error: ''})
                      }
                      placeholder="Enter your password"
                      placeholderTextColor={Colors.gray['500']}
                      secureTextEntry={passtrue1}
                    />
                  </View>
                  <View
                    style={{
                      width: '10%',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                  >
                    <TouchableOpacity
                      onPress={() => setpasstrue1(!passtrue1)}
                      style={{zIndex: 110, right: 40}}
                    >
                      <Image
                        source={
                          passtrue1
                            ? require('../assets/eyeIcon.png')
                            : require('../assets/eyeIcon.png')
                        }
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <Text style={{color: 'red'}}>{signUppassword.error}</Text>
              </View>
              <View style={{width: wp('90%'), marginTop: 15}}>
                <Text
                  style={{
                    color: '#1E263C',
                    position: 'absolute',
                    zIndex: 10,
                    marginHorizontal: 12,
                    backgroundColor: '#fff',
                    fontSize: wp(4),
                    letterSpacing: 0.8,
                    top: -11,
                    paddingHorizontal: 1,
                  }}
                >
                  {' '}
                  Repeat password{' '}
                </Text>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignSelf: 'center',
                  }}
                >
                  <View style={{width: '100%'}}>
                    <TextInput
                      style={{
                        backgroundColor: '#fff',
                        padding:
                          width === 320 || height === 610.6666666666666
                            ? 8
                            : 15,
                        borderWidth: 0.5,
                        borderColor: '#C3C7E5',
                        borderRadius: 5,
                        fontSize: 16,
                        color: Colors.gray['900'],
                      }}
                      onChangeText={(text) =>
                        setPassword2({value: text, error: ''})
                      }
                      placeholder="Enter your password"
                      placeholderTextColor={Colors.gray['500']}
                      secureTextEntry={passtrue}
                    />
                  </View>
                  <View
                    style={{
                      width: '10%',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                  >
                    <TouchableOpacity
                      onPress={() => setpasstrue(!passtrue)}
                      style={{zIndex: 110, right: 40}}
                    >
                      <Image
                        source={
                          passtrue
                            ? require('../assets/eyeIcon.png')
                            : require('../assets/eyeIcon.png')
                        }
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <Text style={{color: 'red'}}>{signUppassword2.error}</Text>
              </View>
            </View>
          </View>
          <View
            style={{flex: 2, alignItems: 'center', justifyContent: 'center'}}
          >
            <View style={{marginVertical: 35}} />
            <View style={bottomButton.bottomView}>
              {isLoading || isDecodingToken || emailLoading ? (
                <ActivityIndicator
                  style={bottomButton.login}
                  size="large"
                  color="#8897AA"
                />
              ) : (
                <Button
                  style={bottomButton.login}
                  color="#8897AA"
                  disabled={
                    !checked || emailLoading || isDecodingToken || isLoading
                  }
                  mode="contained"
                  onPress={onSignUpPressed}
                >
                  <Text
                    style={{
                      textTransform: 'capitalize',
                      fontWeight: 'bold',
                      fontSize: hp('2.3'),
                      color: 'white',
                    }}
                  >
                    Sign up
                  </Text>
                </Button>
              )}
              <View>
                <Text
                  style={{
                    color: '#BFC7D3',
                    letterSpacing: 0.5,
                    fontSize: hp('1.7'),
                  }}
                >
                  By creating an account, you agree to our
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              >
                <CheckBox
                  disabled={false}
                  value={checked}
                  checked={checked}
                  checkedColor="#A17EFA"
                  size={hp('2')}
                  containerStyle={{
                    backgroundColor: 'transparent',
                    borderWidth: 0,
                    borderRadius: 8,
                  }}
                  onPress={() => {
                    setChecked(!checked);
                  }}
                  textStyle={{
                    color: 'white',
                    fontWeight: 'normal',
                    fontSize: hp('2'),
                  }}
                />
                <TouchableOpacity
                  onPress={() => navigation.navigate('PrivacyPolicy')}
                >
                  <Text
                    style={{
                      fontWeight: 'bold',
                      color: '#A17EFA',
                      fontSize: hp('1.7'),
                    }}
                  >
                    Terms and Conditions
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </KeyboardAwareScrollView>
      )}
    </View>
  );
}

function StaarzLogo({goBack}) {
  return (
    <View style={sideLogoStyles.container}>
      <View style={{width: '20%'}}>
        <TouchableOpacity onPress={goBack} style={{width: 15, height: 12}}>
          <Image
            style={{width: 30, height: 15}}
            source={require('../assets/arrow_back.png')}
          />
        </TouchableOpacity>
      </View>
      <View
        style={{
          width: '60%',
          alignItems: 'center',
          flexDirection: 'row',
          justifyContent: 'center',
        }}
      >
        <Image
          source={require('../assets/starzlogo.png')}
          style={sideLogoStyles.imageLogo}
        />
        <Text
          style={{
            fontSize: wp(6),
            fontFamily: 'MPLUS1p-Bold',
            color: '#243443',
            left: -2,
            lineHeight: 33,
            textAlign: 'center',
          }}
        >
          STAARY
        </Text>
      </View>
      <View style={{width: '20%'}} />
    </View>
  );
}

const bottomButton = StyleSheet.create({
  bottomView: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    bottom: hp('2'),
  },
  login: {
    marginBottom: 15,
    borderRadius: 8,
    width: wp('90%'),
  },
});

const sideLogoStyles = StyleSheet.create({
  image: {
    marginLeft: 20,
  },
  imageLogo: {
    width: wp(7.5),
    height: wp(6.5),
    left: -4,
  },
  container: {
    position: 'absolute',
    top: Platform.OS !== 'ios' ? 0 : 0 + getStatusBarHeight(),
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    backgroundColor: '#fff',
    zIndex: 10,
    width: '100%',
  },
});

function OrDividerLine() {
  return (
    <View style={{}}>
      <Image
        source={require('../assets/or.png')}
        style={orDividerLineStyle.or}
      />
    </View>
  );
}

const orDividerLineStyle = StyleSheet.create({
  or: {
    marginTop: hp('1'),
    marginBottom: hp('1'),
    alignSelf: 'center',
    width: wp('90%'),
    height: 15,
  },
});

const styles = StyleSheet.create({
  forgotPassword: {
    width: '100%',
    alignItems: 'flex-end',
    marginRight: 10,
    marginBottom: 24,
  },
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  forgot: {
    fontSize: 13,
    marginTop: 10,
    marginRight: -2,
    color: theme.colors.secondary,
  },
  rememberMe: {
    fontSize: 13,
    marginTop: 10,
    marginLeft: 10,
    color: theme.colors.secondary,
  },
  link: {
    color: '#A17EFA', // theme.colors.primary,
    lineHeight: 30,
    fontSize: 13,
  },
});

export default RegisterScreen;
