import React, {useState} from 'react';
import {
  Image,
  View,
  StyleSheet,
  KeyboardAvoidingView,
  Dimensions,
  TouchableOpacity,
  StatusBar,
  ActivityIndicator,
  SafeAreaView,
  Platform,
  ImageBackground,
} from 'react-native';
import {Text} from 'react-native-paper';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Button from '../components/Button';
import TextInput from '../components/TextInput';
import {checkConnection} from '../components/checkConnection';
import {theme} from '../core/theme';
import {url} from '../const/urlDirectory';
import {string} from '../const/string';

const socialProfile = ({navigation, route}) => {
  const [userData, setUserData] = useState(route.params.userData);
  // const [userData, setUserData] = useState();
  // console.log("userdata----", route.params.userData)
  // const [accessToken, setAccessToken] = useState();
  const [accessToken, setAccessToken] = useState(route.params.accessToken);
  const [Active, setActive] = useState(true);
  const [instaProfileLink, setInstaProfileLink] = useState('');
  const [fbProfileLink, setFbProfileLink] = useState('');
  const [isLoading, setIsloading] = useState(false);
  const [fbCheck, setFbCheck] = useState('');
  const [net, setnet] = React.useState(true);
  const windowWidth = Dimensions.get('window').width;
  const windowHeight = Dimensions.get('window').height;
  const {width} = Dimensions.get('window');
  const {height} = Dimensions.get('window');
  const getasync = (async) => {
    checkConnection().then((res) => {
      if (res === false) {
        setnet(res);
      } else {
        setnet(res);
        // registerUserBioScreen2()
      }
    });
  };
  const onSocialProfileSubmit = () => {
    checkConnection().then((res) => {
      if (res === false) {
        setnet(res);
      } else {
        setnet(res);
        registerUserBioScreen2();
      }
    });
  };

  const registerUserBioScreen2 = () => {
    const fb_url =
      // eslint-disable-next-line no-useless-escape
      /(?:https?:\/\/)?(?:www\.)?facebook\.com\/.(?:(?:\w)*#!\/)?(?:pages\/)?(?:[\w\-]*\/)*([\w\-\.]*)/;
    // var pattern = /^(?:(?:http|https):\/\/)?(?:www.)?facebook.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[?\w\-]*\/)?(?:profile.php\?id=(?=\d.*))?([\w\-]*)?$/;

    if (fb_url.test(fbProfileLink) === false) {
      alert('your facebook profile is not valid ');
    } else {
      onSocialProfileSubmit1();
    }
  };

  const onSocialProfileSubmit1 = () => {
    const someProperty = userData;
    someProperty.fbProfileLink = fbProfileLink;
    someProperty.instaProfileLink = instaProfileLink;
    if (!fbProfileLink || fbProfileLink.length <= 0) {
      return false;
    }
    setIsloading(true);
    fetch(url.socialProfile, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        fbProfileLink,
        instaProfileLink,
        status: 3,
        userID: userData.userID,
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.status === 200) {
          setIsloading(false);
          navigation.replace('uploadYourPicture', {
            userData,
            accessToken,
          });
        }
        if (responseJson.status === 404) {
          // alert("Oops Error!!")
          setIsloading(false);
          // navigation.push('socialProfile', {
          //   userData: userData,
          //   accessToken: accessToken
          // })
        }
        if (responseJson.status === 400) {
          setIsloading(false);
          // alert("Oops!! Something went wrong.")
        }
      })
      .catch((error) => {
        setIsloading(false);
        // alert('Oops error!!')
        console.error(error);
      });
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          alignSelf: 'center',
        }}
      >
        {/* <StatusBar  backgroundColor="light-content" /> */}
        <StatusBar barStyle="dark-content" backgroundColor="white" />

        <View
          style={{
            flexDirection: 'row',
            position: 'absolute',
            top: 10,
            alignItems: 'center',
          }}
        >
          <Image
            source={require('../assets/starzlogo.png')}
            style={sideLogoStyles.imageLogo}
          />
          {/* <Image source={require('../assets/STAARZ_109X19_black.png')} style={sideLogoStyles.image} /> */}
          <Text
            style={{
              fontSize: wp(6),
              fontFamily: 'MPLUS1p-Bold',
              color: '#243443',
              left: -2,
              lineHeight: 33,
              textAlign: 'center',
            }}
          >
            STAARZ
          </Text>
        </View>
        {/* <KeyboardAvoidingView style={loginBackgroundStyles.container} > */}
        {!net ? (
          <View
            style={{alignItems: 'center', justifyContent: 'center', flex: 1}}
          >
            <Image
              source={require('../assets/internat.png')}
              style={{height: 50, width: 50}}
            />
            <Text style={{fontSize: 17, fontWeight: '700', marginTop: 5}}>
              {string.network}
            </Text>
            <TouchableOpacity
              color="white"
              mode="contained"
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                top: windowHeight * 0.3,
              }}
              onPress={() => getasync()}
            >
              <View
                style={{
                  backgroundColor: '#7643F8',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 50,
                  paddingHorizontal: 30,
                  paddingVertical: 20,
                  marginTop: 10,
                }}
              >
                {
                  Platform.OS === 'ios' ? (
                    // <Ionicons name="reload" color="black" size={30} />
                    <Text style={{fontSize: hp('2'), color: '#fff'}}>
                      {string.Try_Again}
                    </Text>
                  ) : (
                    <Text style={{fontSize: hp('2'), color: '#fff'}}>
                      {string.Try_Again}
                    </Text>
                  )
                  // <MaterialCommunityIcons name="reload" color="black" size={30} />
                }
              </View>
            </TouchableOpacity>
          </View>
        ) : (
          <>
            <View style={{flex: 7}}>
              <Text
                style={{
                  color: '#000000',
                  fontWeight: Platform.OS === 'ios' ? 'bold' : 'bold',
                  fontSize: wp('7'),
                  marginTop: '20%',
                  textAlign: 'center',
                  lineHeight: 36,
                  fontFamily: 'MPLUS1p-ExtraBold',
                }}
              >
                Social Profiles
              </Text>
              <Text
                style={{
                  fontWeight: '500',
                  fontSize:
                    width === 320 || height === 610.6666666666666 ? 14 : 18,
                  textAlign: 'center',
                  letterSpacing: 0.5,
                  color: '#8A98AC',
                  marginBottom: 15,
                  lineHeight: 27,
                  fontFamily: 'SFUIText-Semibold',
                }}
              >
                Please link your social profiles
              </Text>
              <View
                style={{
                  alignContent: 'flex-start',
                  width: '100%',
                  position: 'relative',
                  marginTop: 30,
                }}
              >
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Image
                    source={require('../assets/facebook.png')}
                    style={{
                      height: 20,
                      width: 20,
                      marginTop: 2,
                      resizeMode: 'cover',
                    }}
                  />
                  <Text
                    style={{
                      textAlign: 'left',
                      alignContent: 'flex-start',
                      fontWeight: '600',
                      fontSize:
                        width === 320 || height === 610.6666666666666 ? 18 : 21,
                      paddingLeft: 10,
                      lineHeight: 30,
                      fontFamily: 'MPLUS1p-Bold',
                      marginTop: 3,
                    }}
                  >
                    {/* <Icon name="facebook-square" size={22} color="#39559F" />  */}
                    Facebook
                  </Text>
                </View>
                <Text
                  style={{
                    fontSize: 14,
                    color: '#7643F8',
                    marginTop: 10,
                    lineHeight: 20,
                    fontFamily: 'MPLUS1p-Regular',
                  }}
                >
                  *This field is required
                </Text>
                <TextInput
                  // label="Full name"
                  returnKeyType="next"
                  placeholder="Facebook profile link"
                  value={fbProfileLink}
                  // error={!!fbCheck}
                  // errorText={fbCheck}
                  onChangeText={(text) => setFbProfileLink(text)}
                  style={{
                    color: 'white',
                    backgroundColor: 'white',
                    borderRadius: 5,
                    width: '100%',
                    marginTop: -10,
                  }}
                  theme={{
                    colors: {text: 'black', primary: 'rgb(33, 151, 186)'},
                  }}
                />
              </View>
              <View style={{height: 20}} />
              <View
                style={{
                  alignContent: 'flex-start',
                  width: '100%',
                  position: 'relative',
                }}
              >
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Image
                    source={require('../assets/instagram.png')}
                    style={{height: 20, width: 20, marginTop: 2}}
                  />
                  <Text
                    style={{
                      textAlign: 'left',
                      alignContent: 'flex-start',
                      fontWeight: '600',
                      fontSize:
                        width === 320 || height === 610.6666666666666 ? 18 : 21,
                      paddingLeft: 10,
                      lineHeight: 30,
                      fontFamily: 'MPLUS1p-Bold',
                      marginTop: 3,
                    }}
                  >
                    {/* <Icon name="instagram" size={22} color="#FFC852" />  */}
                    Instagram
                  </Text>
                </View>
                <TextInput
                  // label="Full name"
                  returnKeyType="next"
                  placeholder="Instagram profile link"
                  value={instaProfileLink}
                  onChangeText={(text) => setInstaProfileLink(text)}
                  style={{
                    color: 'white',
                    backgroundColor: 'white',
                    borderRadius: 5,
                    width: '100%',
                  }}
                  theme={{
                    colors: {text: 'black', primary: 'rgb(33, 151, 186)'},
                  }}
                />
              </View>
            </View>
            {/* </KeyboardAvoidingView> */}
            {/* <Button color='#8897AA' marginTop={355} mode="contained" onPress={onSocialProfileSubmit}>
        <Text style={{color:"white", fontWeight:"bold"}}>Next</Text>
      </Button> */}
            {/* <View style={{ marginBottom: 400 }}></View> */}
            <View
              style={{flex: 3, justifyContent: 'center', alignItems: 'center'}}
            >
              <View style={bottomButton.bottomView}>
                {isLoading ? (
                  <ActivityIndicator
                    style={bottomButton.login}
                    size="large"
                    color="#8897AA"
                  />
                ) : (
                  <Button
                    disabled={fbProfileLink === ''}
                    style={bottomButton.login}
                    color="#8897AA"
                    mode="contained"
                    onPress={onSocialProfileSubmit}
                  >
                    <Text
                      style={{
                        textTransform: 'capitalize',
                        fontWeight: 'bold',
                        fontSize: hp('2.5'),
                        color: 'white',
                      }}
                    >
                      Next
                    </Text>
                  </Button>
                )}
              </View>
            </View>
          </>
        )}
      </View>
      {/* <DropdownAlert ref={ref => dropDownAlertRef = ref} /> */}
    </SafeAreaView>
  );
};

const bottomButton = StyleSheet.create({
  bottomView: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
  },
  login: {
    marginBottom: 30,
    borderRadius: 8,
    width: '90%',
  },
});

function StaarzLogo() {
  return (
    <View style={sideLogoStyles.container}>
      <Image
        source={require('../assets/starzlogo_23X26.png')}
        style={sideLogoStyles.imageLogo}
      />
      <Image
        source={require('../assets/STAARZ_109X19_black.png')}
        style={sideLogoStyles.image}
      />
    </View>
  );
}

const sideLogoStyles = StyleSheet.create({
  image: {
    // marginTop: 0,
    marginLeft: 20,
  },
  imageLogo: {
    // left: 10,
    width: wp(7.5),
    height: wp(6.5),
    left: -4,
  },
  container: {
    // zIndex: 10,
    width: '100%',
    // position: 'absolute',
    // top: hp('2'),

    // top: 0 + getStatusBarHeight(),
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    marginTop: 0,
    // backgroundColor: "white"
  },
  // image: {
  //   width: 24,
  //   height: 24,
  // }
});

function LoginBackground({children}) {
  return (
    <ImageBackground
      source={require('../assets/loginscreenbg.png')}
      resizeMode="stretch"
      style={loginBackgroundStyles.background}
    >
      <KeyboardAvoidingView
        style={loginBackgroundStyles.container}
        behavior="padding"
      >
        {children}
      </KeyboardAvoidingView>
    </ImageBackground>
  );
}

const loginBackgroundStyles = StyleSheet.create({
  background: {
    width: '100%',
    height: '100%',
    // position: 'absolute',
    // top: '0%',
    flex: 1,
  },
  container: {
    // flex: 1,

    width: '100%',
    height: '100%',
    // alignSelf: 'center',
    // alignItems: 'center',
    // justifyContent: 'center',
    // backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
});
function SocialMediaLoginButton() {
  return (
    <View>
      <Image
        source={require('../assets/facebook_login.png')}
        style={socialMediaLoginButtonStyle.facebook}
      />
      <Image
        source={require('../assets/Google_login.png')}
        style={socialMediaLoginButtonStyle.google}
      />
      <Image
        source={require('../assets/or.png')}
        style={socialMediaLoginButtonStyle.or}
      />
    </View>
  );
}

const socialMediaLoginButtonStyle = StyleSheet.create({
  facebook: {
    marginBottom: 8,
  },
  google: {
    marginTop: 10,
  },
  or: {
    marginTop: 25,
    marginBottom: 10,
    alignSelf: 'center',
  },
});

const styles = StyleSheet.create({
  forgotPassword: {
    width: '100%',
    alignItems: 'flex-end',
    marginRight: 10,
    marginBottom: 24,
  },
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  forgot: {
    fontSize: 13,
    marginTop: 10,
    marginRight: -2,
    color: theme.colors.secondary,
  },
  rememberMe: {
    fontSize: 13,
    marginTop: 10,
    marginLeft: 10,
    color: theme.colors.secondary,
  },
  link: {
    fontWeight: '100',
    color: '#9872FA',
    lineHeight: 30,
  },
});

export default socialProfile;
