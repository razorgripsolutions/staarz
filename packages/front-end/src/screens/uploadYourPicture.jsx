import React, {useRef, useState, useCallback, useEffect} from 'react';
import {
  Image,
  View,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  StatusBar,
  ImageBackground,
  Dimensions,
  Platform,
  ActivityIndicator,
  Alert,
} from 'react-native';
import {Text} from 'react-native-paper';
import * as ImagePicker from 'react-native-image-picker';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {string} from '../const/string';
import {checkConnection} from '../components/checkConnection';
import Button from '../components/Button';
import Toast from 'react-native-toast-message';
import ActionSheet from 'react-native-actionsheet';
import {useAuthProvider} from '../../App';
import RNPermissions, {
  Permission,
  PERMISSIONS,
  check,
  RESULTS,
  request,
  openSettings,
} from 'react-native-permissions';

export const getMessageFromCode = (code) => {
  let response = 'Error Occured';
  switch (code) {
    case 'camera_unavailable':
      response = 'Camera is unavailable';
      break;
    case 'permission':
      response = 'You dont have permissions for this action';
      break;
    default:
      break;
  }
  return response;
};

const uploadYourPicture = ({navigation}) => {
  const [Active, setActive] = useState(true);
  const [isLoading, setIsloading] = useState(false);
  const [net, setnet] = React.useState(true);

  const [path, setPath] = useState(null);
  const [file, setFile] = useState(null);

  const {userState, register} = useAuthProvider();

  const ActionSheetRef = useRef();

  const windowHeight = Dimensions.get('window').height;
  const {width} = Dimensions.get('window');
  const {height} = Dimensions.get('window');

  const handleOpenSettings = useCallback(async () => {
    return openSettings()
      .then((res) => {
        console.log(res);
      })
      .catch((e) => {
        console.log(e);
      });
  }, [openSettings]);

  const handlePermission = useCallback(async () => {
    let status;
    if (Platform.OS === 'android') {
      status = check(PERMISSIONS.ANDROID.READ_MEDIA_IMAGES).then((result) => {
        console.log('result1', result);
        if (result === RESULTS.DENIED) {
          return request(PERMISSIONS.ANDROID.READ_MEDIA_IMAGES).then(
            (requestResult) => {
              console.log('requestResult', requestResult);
            },
          );
        } else if (result === RESULTS.BLOCKED) {
          return Alert.alert(
            'Open Settings',
            'You would need to  allow gallery permissions in your settings.',
            [
              {
                text: 'No',
                onPress: () => {},
                style: 'cancel',
              },
              {
                text: 'Yes',
                onPress: handleOpenSettings,
              },
            ],
          );
        }
      });
    } else if (Platform.OS === 'ios') {
      status = check(PERMISSIONS.IOS.MEDIA_LIBRARY).then((result) => {
        console.log('result1', result);
        if (result === RESULTS.DENIED) {
          return request(PERMISSIONS.IOS.MEDIA_LIBRARY).then(
            (requestResult) => {
              console.log('requestResult', requestResult);
            },
          );
        } else if (result === RESULTS.BLOCKED) {
          return Alert.alert(
            'Open Settings',
            'You would need to  allow gallery permissions in your settings.',
            [
              {
                text: 'No',
                onPress: () => {},
                style: 'cancel',
              },
              {
                text: 'Yes',
                onPress: handleOpenSettings,
              },
            ],
          );
        }
      });
    }
    return status;
  }, [handleOpenSettings]);

  useEffect(() => {
    handlePermission();
  }, []);

  const getfuntion = () => {
    checkConnection().then((res) => {
      if (res === false) {
        setnet(res);
      } else {
        setnet(res);
      }
    });
  };
  const onUploadYourPictureSubmit = () => {
    checkConnection().then((res) => {
      if (res === false) {
        setnet(res);
      } else {
        setnet(res);
        registerUserBioScreen2();
      }
    });
  };

  const registerUserBioScreen2 = async () => {
    await register({
      ...userState.registrationData,
      galleryImage: file,
    });
    navigation.navigate('VerifyYourPicture');
  };

  const uploadImage = useCallback(() => {
    setActive(false);
  }, [setActive]);

  const askToUpload = () => {
    Alert.alert(
      'Confirm',
      'Are you sure you want to upload this image?',
      [
        {
          text: 'No',
          onPress: () => {},
          style: 'cancel',
        },
        {
          text: 'Yes!',
          onPress: uploadImage,
        },
      ],
      {cancelable: false},
    );
  };

  const chooseImage = useCallback(
    (index) => {
      if (index === 0) {
        const options = {
          mediaType: 'photo',
          includeBase64: true,
        };
        return ImagePicker.launchImageLibrary(options, async (response) => {
          if (response.errorCode) {
            Toast.show({
              text1: 'Application Error',
              text2: getMessageFromCode(response.errorCode),
              type: 'danger',
            });
          } else {
            setPath(response?.assets?.[0]?.uri);
            setFile(response?.assets?.[0]);
            askToUpload();
          }
        });
      }
    },
    [askToUpload, setFile],
  );

  return (
    <>
      <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
        <View style={{flex: 1}}>
          <StatusBar barStyle="dark-content" backgroundColor="white" />
          <View
            style={{
              flexDirection: 'row',
              alignSelf: 'center',
              flex: 0.5,
              zIndex: 10,
              top: 0,
              alignItems: 'center',
            }}
          >
            <Image
              source={require('../assets/starzlogo.png')}
              style={sideLogoStyles.imageLogo}
            />
            <Text
              style={{
                fontSize: wp(6),
                fontFamily: 'MPLUS1p-Bold',
                color: '#243443',
                left: -2,
                lineHeight: 33,
                textAlign: 'center',
              }}
            >
              STAARY
            </Text>
          </View>
          {!net ? (
            <View
              style={{alignItems: 'center', justifyContent: 'center', flex: 1}}
            >
              <Image
                source={require('../assets/internat.png')}
                style={{height: 50, width: 50}}
              />
              <Text style={{fontSize: 17, fontWeight: '700', marginTop: 5}}>
                {string.network}
              </Text>
              <TouchableOpacity
                color="white"
                mode="contained"
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  top: windowHeight * 0.3,
                }}
                onPress={() => getfuntion()}
              >
                <View
                  style={{
                    backgroundColor: '#7643F8',
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 50,
                    paddingHorizontal: 30,
                    paddingVertical: 20,
                    marginTop: 10,
                  }}
                >
                  {Platform.OS === 'ios' ? (
                    <Text style={{fontSize: hp('2'), color: '#fff'}}>
                      {string.Try_Again}
                    </Text>
                  ) : (
                    <Text style={{fontSize: hp('2'), color: '#fff'}}>
                      {string.Try_Again}
                    </Text>
                  )}
                </View>
              </TouchableOpacity>
            </View>
          ) : (
            <>
              <View style={{flex: 5.5}}>
                <ImageBackground
                  source={
                    !path
                      ? require('../assets/pexels-photo-227294.jpeg')
                      : {uri: path}
                  }
                  resizeMode="cover"
                  style={{
                    width: '100%',
                    height: '100%',
                    flex: 5,
                  }}
                />
              </View>

              <View
                style={{
                  flex: 4,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              >
                <View
                  style={{
                    height: '30%',
                    width: '100%',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                >
                  <TouchableOpacity
                    onPress={() => {
                      ActionSheetRef?.current?.show();
                    }}
                    style={{marginTop: -hp(10)}}
                  >
                    <Image
                      source={require('../assets/uploadYourPictureButton.png')}
                      style={uploadButton.button}
                    />
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    height: '70%',
                    width: '100%',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                >
                  <Text
                    style={{
                      letterSpacing: 0.4,
                      fontSize:
                        width === 320 || height === 610.6666666666666 ? 22 : 26,
                      lineHeight: 36,
                      fontFamily: 'SFUIText-Semibold',
                      fontWeight: 'bold',
                    }}
                  >
                    Upload your picture
                  </Text>
                  <Text
                    style={{
                      textAlign: 'center',
                      letterSpacing: 1,
                      color: '#000000',
                      lineHeight: 24,
                      fontSize:
                        width === 320 || height === 610.6666666666666 ? 12 : 14,
                      fontWeight: '300',
                    }}
                  >
                    Please upload a picture of yourself{'\n'}from your media
                  </Text>
                  <Image
                    source={require('../assets/uploadYourPictureNavigationImage.png')}
                    style={uploadButton.navigation}
                  />
                  <View style={bottomButton.bottomView}>
                    {isLoading ? (
                      <ActivityIndicator
                        style={bottomButton.login}
                        size="large"
                        color="#8897AA"
                      />
                    ) : (
                      <Button
                        disabled={Active}
                        style={bottomButton.login}
                        color="#8897AA"
                        mode="contained"
                        onPress={onUploadYourPictureSubmit}
                      >
                        <Text
                          style={{
                            textTransform: 'capitalize',
                            fontWeight: 'bold',
                            fontSize: 18,
                            color: 'white',
                          }}
                        >
                          Next
                        </Text>
                      </Button>
                    )}
                  </View>
                </View>
              </View>
            </>
          )}
        </View>
      </SafeAreaView>
      <ActionSheet
        ref={ActionSheetRef}
        title={'How do you like to add an image ?'}
        options={['Choose from gallery', 'Cancel']}
        cancelButtonIndex={2}
        onPress={chooseImage}
      />
    </>
  );
};

const bottomButton = StyleSheet.create({
  bottomView: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    bottom: Platform.OS === 'ios' ? 20 : 25,
    paddingLeft: 20,
    paddingRight: 20,
  },
  login: {
    marginBottom: 30,
    top: '20%',
    borderRadius: 8,
  },
});

const sideLogoStyles = StyleSheet.create({
  image: {
    marginLeft: 20,
  },
  imageLogo: {
    width: wp(7.5),
    height: wp(6.5),
    left: -4,
  },
  container: {
    position: 'absolute',
    top: Platform.OS === 'ios' ? hp('4') : hp('3'),
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 0,
    zIndex: 10,
  },
});

const uploadButton = StyleSheet.create({
  button: {},
  navigation: {
    marginVertical: hp('5'),
  },
});

export default uploadYourPicture;
