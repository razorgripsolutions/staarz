import React, {useState, useEffect} from 'react';
import {
  FlatList,
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Image,
  Dimensions,
  Platform,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  heightPercentageToDP,
} from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {CircleFade} from 'react-native-animated-spinkit';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {url, backendBaseUrl} from '../const/urlDirectory';
import {checkConnection} from '../components/checkConnection';
import {string} from '../const/string';

const {height} = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    flex: 1,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    marginTop: 30,
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
});

const sideLogoStyles = StyleSheet.create({
  image: {
    marginLeft: 20,
  },
  imageLogo: {
    width: wp(7.5),
    height: wp(6.5),
    left: -4,
  },
  container: {
    position: 'absolute',
    top: Platform.OS === 'ios' ? 0 + getStatusBarHeight() : 10,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    zIndex: 10,
    width: '100%',
  },
});

function UnblockuserScreen(props) {
  const [isLoading, setIsloading] = useState(false);
  const [Data, setData] = useState([]);

  const [net, setnet] = React.useState(true);

  const [userID, setuserID] = useState(props.route.params.userID);

  const {navigation} = props;

  const windowHeight = Dimensions.get('window').height;

  const UnBlock = async (blockedUser) => {};

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
      <View
        style={{
          flexDirection: 'row',
          top: Platform.OS === 'ios' ? 0 : 0,
          alignItems: 'center',
          justifyContent: 'center',
          width: '100%',
          backgroundColor: '#fff',
          zIndex: 10,
        }}
      >
        <View style={{width: '20%'}}>
          <TouchableOpacity onPress={() => navigation.goBack()} style={{}}>
            <FontAwesome
              name="angle-left"
              size={25}
              color="#000"
              style={{left: 20}}
            />
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: '60%',
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'center',
          }}
        >
          <Image
            source={require('../assets/starzlogo.png')}
            style={sideLogoStyles.imageLogo}
          />
          <Text
            style={{
              fontSize: wp(6),
              fontFamily: 'MPLUS1p-Bold',
              color: '#243443',
              left: -2,
              lineHeight: 33,
              textAlign: 'center',
            }}
          >
            STAARY
          </Text>
          {/* <Image source={require('../assets/STAARZ_109X19_black.png')} style={sideLogoStyles.image} /> */}
        </View>
        <View style={{backgroundColor: 'red', width: '20%'}} />
      </View>

      {!net ? (
        <View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
          <Image
            source={require('../assets/internat.png')}
            style={{height: 50, width: 50}}
          />
          <Text style={{fontSize: 17, fontWeight: '700', marginTop: 5}}>
            {string.network}
          </Text>
          <TouchableOpacity
            color="white"
            mode="contained"
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              top: windowHeight * 0.3,
            }}
            onPress={() => apicall1()}
          >
            <View
              style={{
                backgroundColor: '#7643F8',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 50,
                paddingHorizontal: 30,
                paddingVertical: 20,
                marginTop: 10,
              }}
            >
              {
                Platform.OS === 'ios' ? (
                  // <Ionicons name="reload" color="black" size={30} />
                  <Text style={{fontSize: hp('2'), color: '#fff'}}>
                    {string.Try_Again}
                  </Text>
                ) : (
                  <Text style={{fontSize: hp('2'), color: '#fff'}}>
                    {string.Try_Again}
                  </Text>
                )
                // <MaterialCommunityIcons name="reload" color="black" size={30} />
              }
            </View>
          </TouchableOpacity>
        </View>
      ) : (
        <>
          {isLoading ? (
            <CircleFade
              size={110}
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                top: height * 0.4,
                alignSelf: 'center',
              }}
              color="#5E2EBA"
            />
          ) : (
            <View style={styles.container}>
              {Data.length === 0 ? (
                <View
                  style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: '100%',
                  }}
                >
                  <Text
                    style={{
                      fontSize: 20,
                      fontWeight: 'bold',
                      alignSelf: 'center',
                      justifyContent: 'center',
                      alignContent: 'center',
                    }}
                  >
                    You have not blocked anyone{' '}
                  </Text>
                </View>
              ) : null}
              <FlatList
                style={{
                  height: '100%',
                  width: '100%',
                  flex: 1,
                  alignSelf: 'center',
                }}
                data={Data}
                renderItem={({item}) => (
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <View style={{marginVertical: 20, width: '15%'}}>
                      <Image
                        style={{width: 40, height: 40, borderRadius: 40 / 2}}
                        //  source={require('./../assets/uploadYourPicture.png')}
                        // source={require('./../assets/userpro.jpeg')}
                        // url={userData.profilePicture === "" ? require('./../assets/userpro.jpeg') : { uri: backendBaseUrl + 'data/' + userData.profilePicture }}

                        source={
                          item.profilePicture === ''
                            ? require('../assets/userpro.jpeg')
                            : {
                                uri: `${backendBaseUrl}data/${item.profilePicture}`,
                              }
                        }
                      />
                    </View>
                    <Text
                      style={{
                        width: '65%',
                        // fontWeight: '800',
                        color: '#222',
                        fontSize: 14,
                        lineHeight: 17,
                        fontWeight: '500',
                        fontFamily: 'SFUIText-Semibold',
                      }}
                    >
                      {item.fullName}
                    </Text>
                    <TouchableOpacity
                      onPress={() => UnBlock(item.blockedUser)}
                      style={{
                        width: '20%',
                        borderRadius: 10,
                        backgroundColor: '#ccc',
                        padding: 10,
                        alignItems: 'center',
                      }}
                    >
                      <Text style={{fontSize: heightPercentageToDP('1.5')}}>
                        Unblock
                      </Text>
                    </TouchableOpacity>
                  </View>
                )}
              />
            </View>
          )}
        </>
      )}
    </SafeAreaView>
  );
}

export default UnblockuserScreen;
