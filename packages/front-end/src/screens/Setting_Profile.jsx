import React, {useEffect, useState} from 'react';
import {
  Text,
  TouchableOpacity,
  StyleSheet,
  Alert,
  View,
  SafeAreaView,
  Dimensions,
  Platform,
  ScrollView,
  Switch,
} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import Tabs from 'react-native-tabs';
import {theme} from '../core/theme';
import UserHeaderSetting from '../components/UserHeaderSetting';
import Button from '../components/Button';
import {useAuthProvider} from '../../App';

const {width} = Dimensions.get('window');
const {height} = Dimensions.get('window');

function Setting_Profile(props) {
  const {signOut, userState} = useAuthProvider();
  const [sliderOneValue, setSliderOneValue] = React.useState(49);

  const [isEnabled, setIsEnabled] = useState(true);
  const [smokerSwitch, setsmokerSwitch] = useState(false);
  const [NewMatchSwitch, setNewMatchSwitch] = useState(false);
  const [NewmessageSwitch, setNewmessageSwitch] = useState(false);

  const [nonCollidingMultiSliderValue, setNonCollidingMultiSliderValue] =
    React.useState([18, 100]);
  const [interestedIn, setInterestedIn] = useState();
  const {navigation} = props;

  useEffect(() => {
    if (userState?.userData) {
      const value = userState?.userData?.intrestedIn === 'Male' ? '0' : '1';
      setInterestedIn?.(value);
    }
  }, [setInterestedIn, userState]);

  const onCollidingMultiSliderValuesChange = (values) =>
    setNonCollidingMultiSliderValue(values);

  const sliderOneValuesChange = (values) => {
    if (isEnabled === true) {
      setSliderOneValue(values);
    } else {
      setSliderOneValue(values * (0.621371).toFixed(2));
    }
  };
  const toggleSwitch = () => {
    if (isEnabled !== true) {
      setSliderOneValue(sliderOneValue * (1.6).toFixed(1));
    } else {
      setSliderOneValue(sliderOneValue * (0.6).toFixed(1));
    }
    setIsEnabled((previousState) => !previousState);
  };
  const smokerSwitchbutton = () =>
    setsmokerSwitch((previousState) => !previousState);
  const NewMatchhbutton = () =>
    setNewMatchSwitch((previousState) => !previousState);
  const Newmessagebutton = () =>
    setNewmessageSwitch((previousState) => !previousState);

  const handleLogout = async () => {
    Alert.alert('Logout', 'Are you sure want to logout ?', [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {text: 'OK', onPress: () => logoutFuntion()},
    ]);
  };

  const logoutFuntion = async () => {
    await signOut();
    navigation.replace('LoginScreen');
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: theme.colors.surface}}>
      <UserHeaderSetting goBack={navigation.goBack} userName="Settings" />
      <ScrollView showsVerticalScrollIndicator={false} style={{flex: 1}}>
        <View
          style={{
            width: '100%',
            padding: 20,
            justifyContent: 'space-between',
            flexDirection: 'row',
            marginTop: 30,
          }}
        >
          <Text
            style={{
              color: '#000',
              marginBottom: 5,
              fontWeight: '600',
              fontSize: width === 320 || height === 610.6666666666666 ? 14 : 16,
              lineHeight: 21,
              fontFamily: 'MPLUS1p-Bold',
            }}
          >
            Age-range
          </Text>
          <Text
            style={{
              marginBottom: 5,
              marginLeft: 20,
              fontWeight: '600',
              fontSize: width === 320 || height === 610.6666666666666 ? 14 : 16,
              lineHeight: 21,
              fontFamily: 'MPLUS1p-Bold',
              color: '#98A5B7',
            }}
          >
            {nonCollidingMultiSliderValue[0]}-{nonCollidingMultiSliderValue[1]}
          </Text>
        </View>
        <View
          style={{
            width: '100%',
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <MultiSlider
            values={[
              nonCollidingMultiSliderValue[0],
              nonCollidingMultiSliderValue[1],
            ]}
            sliderLength={width / 1.2}
            onValuesChange={onCollidingMultiSliderValuesChange}
            min={0}
            max={100}
            step={1}
            allowOverlap
            snapped
            markerStyle={{
              height: 20,
              width: 20,
              borderRadius: 100,
              backgroundColor: '#fff',
              marginTop: 5,
              elevation: 10,
            }}
            unselectedStyle={{backgroundColor: '#EFEFEF', height: 5}}
            pressedMarkerStyle={{backgroundColor: '#EFEFEF', height: 5}}
            selectedStyle={{backgroundColor: '#560CCE', height: 5}}
            trackStyle={{borderRadius: 2, height: 5}}
          />
        </View>
        <View
          style={{
            width: '100%',
            padding: 20,
            justifyContent: 'space-between',
            flexDirection: 'row',
          }}
        >
          <Text
            style={{
              color: '#000',
              marginBottom: 5,
              fontWeight: '600',
              fontSize: width === 320 || height === 610.6666666666666 ? 14 : 16,
              lineHeight: 21,
              fontFamily: 'MPLUS1p-Bold',
            }}
          >
            Distance
          </Text>
          <Text
            style={{
              marginBottom: 5,
              fontWeight: '600',
              fontSize: width === 320 || height === 610.6666666666666 ? 14 : 16,
              lineHeight: 21,
              fontFamily: 'MPLUS1p-Bold',
              color: '#98A5B7',
            }}
          >
            {sliderOneValue} {!isEnabled ? 'Mile' : 'Km'}
          </Text>
        </View>
        <View
          style={{
            width: '100%',
            paddingHorizontal: 20,
            justifyContent: 'space-between',
            flexDirection: 'row',
            alignItems: 'center',
          }}
        >
          <Text
            style={{
              marginBottom: 5,
              fontWeight: '600',
              fontSize: width === 320 || height === 610.6666666666666 ? 14 : 16,
              lineHeight: 21,
              fontFamily: 'MPLUS1p-Bold',
              color: '#98A5B7',
            }}
          >
            {isEnabled ? 'kilometers' : 'Miles'}
          </Text>
          <Switch
            trackColor={{false: '#d8e6f8', true: '#81b0ff'}}
            thumbColor={isEnabled ? '#fff' : '#fff'}
            ios_backgroundColor="#ECF3FB"
            onValueChange={toggleSwitch}
            value={isEnabled}
          />
        </View>
        <View style={{width: '100%', marginTop: 20, alignItems: 'center'}}>
          <MultiSlider
            values={[sliderOneValue[0]]}
            sliderLength={width / 1.2}
            onValuesChange={sliderOneValuesChange}
            min={0}
            max={isEnabled ? 50 : 31.0686}
            step={1}
            allowOverlap
            snapped
            markerStyle={{
              height: 20,
              width: 20,
              borderRadius: 100,
              backgroundColor: '#fff',
              marginTop: 5,
              elevation: 10,
            }}
            unselectedStyle={{backgroundColor: '#EFEFEF', height: 5}}
            pressedMarkerStyle={{backgroundColor: '#EFEFEF', height: 5}}
            selectedStyle={{backgroundColor: '#560CCE', height: 5}}
            trackStyle={{borderRadius: 2, height: 5}}
          />
        </View>
        <View
          style={{
            width: '100%',
            padding: 20,
            justifyContent: 'space-between',
            flexDirection: 'row',
          }}
        >
          <Text
            style={{
              color: '#000',
              fontWeight: '600',
              fontSize: width === 320 || height === 610.6666666666666 ? 14 : 16,
              lineHeight: 21,
              fontFamily: 'MPLUS1p-Bold',
            }}
          >
            Show me
          </Text>
        </View>
        <View
          style={{
            marginTop: 45,
            width: '90%',
            flexDirection: 'row',
            justifyContent: 'center',
            alignSelf: 'center',
            alignItems: 'center',
            marginBottom: 10,
          }}
        >
          <Tabs
            selected={interestedIn}
            style={{
              backgroundColor: '#EDEEF7',
              width: '100%',
              borderRadius: 8,
              borderColor: 'white',
            }}
            selectedStyle={{color: 'white'}}
            onSelect={(el) => setInterestedIn(el.props.name)}
          >
            <Text
              name="0"
              style={{
                color: '#8A98AC',
                fontFamily:
                  interestedIn === '0' ? 'MPLUS1p-Bold' : 'MPLUS1p-Medium',
              }}
              selectedIconStyle={{
                backgroundColor: '#8A98AC',
                borderTopColor: '#8A98AC',
                color: '#8a98ac',
                borderTopLeftRadius: 8,
                borderBottomLeftRadius: 8,
              }}
            >
              Male
            </Text>
            <Text
              name="1"
              style={{
                color: '#8A98AC',
                fontFamily:
                  interestedIn === '1' ? 'MPLUS1p-Bold' : 'MPLUS1p-Medium',
              }}
              selectedIconStyle={{
                backgroundColor: '#8A98AC',
                borderTopColor: '#8A98AC',
                color: '#8a98ac',
              }}
            >
              Female
            </Text>
            <Text
              name="2"
              style={{
                color: '#8A98AC',
                fontFamily:
                  interestedIn === '2' ? 'MPLUS1p-Bold' : 'MPLUS1p-Medium',
              }}
              selectedIconStyle={{
                backgroundColor: '#8A98AC',
                borderTopColor: '#8A98AC',
                color: '#8a98ac',
                borderTopRightRadius: 8,
                borderBottomRightRadius: 8,
              }}
            >
              Both
            </Text>
          </Tabs>
        </View>
        <View
          style={{
            width: '100%',
            paddingHorizontal: 20,
            justifyContent: 'space-between',
            flexDirection: 'row',
            alignItems: 'center',
            marginVertical: 20,
          }}
        >
          <Text
            style={{
              color: '#000',
              fontWeight: '600',
              fontSize: width === 320 || height === 610.6666666666666 ? 14 : 16,
              lineHeight: 21,
              fontFamily: 'MPLUS1p-Bold',
            }}
          >
            Allow smokers
          </Text>
          <Switch
            trackColor={{false: '#d8e6f8', true: '#81b0ff'}}
            thumbColor={smokerSwitch ? '#fff' : '#fff'}
            ios_backgroundColor="#ECF3FB"
            onValueChange={smokerSwitchbutton}
            value={smokerSwitch}
          />
        </View>
        <View
          style={{
            width: '100%',
            paddingHorizontal: 20,
            justifyContent: 'space-between',
            flexDirection: 'row',
            alignItems: 'center',
            marginVertical: 20,
          }}
        >
          <Text
            style={{
              fontWeight: '800',
              color: '#000',
              fontSize: 24,
              lineHeight: 36,
              fontFamily: 'MPLUS1p-Bold',
            }}
          >
            Notifications
          </Text>
        </View>
        <View
          style={{
            width: '100%',
            paddingHorizontal: 20,
            justifyContent: 'space-between',
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: 5,
          }}
        >
          <Text
            style={{
              color: '#000',
              fontWeight: '600',
              fontSize: width === 320 || height === 610.6666666666666 ? 14 : 16,
              lineHeight: 21,
              fontFamily: 'MPLUS1p-Bold',
            }}
          >
            New Match
          </Text>
          <Switch
            trackColor={{false: '#d8e6f8', true: '#81b0ff'}}
            thumbColor={NewMatchSwitch ? '#fff' : '#fff'}
            ios_backgroundColor="#ECF3FB"
            onValueChange={NewMatchhbutton}
            value={NewMatchSwitch}
            style={{}}
          />
        </View>
        <View
          style={{
            width: '100%',
            paddingHorizontal: 20,
            justifyContent: 'space-between',
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: 15,
          }}
        >
          <Text
            style={{
              color: '#000',
              fontWeight: '600',
              fontSize: width === 320 || height === 610.6666666666666 ? 14 : 16,
              lineHeight: 21,
              fontFamily: 'MPLUS1p-Bold',
            }}
          >
            New Message
          </Text>
          <Switch
            trackColor={{false: '#d8e6f8', true: '#81b0ff'}}
            thumbColor={NewmessageSwitch ? '#fff' : '#fff'}
            ios_backgroundColor="#ECF3FB"
            onValueChange={Newmessagebutton}
            value={NewmessageSwitch}
            style={{}}
          />
        </View>

        <View
          style={{
            width: '100%',
            paddingHorizontal: 20,
            justifyContent: 'space-between',
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: 15,
            marginBottom: 30,
          }}
        >
          <Text
            style={{
              color: 'red',
              fontWeight: '600',
              fontSize: width === 320 || height === 610.6666666666666 ? 14 : 16,
              lineHeight: 21,
              fontFamily: 'MPLUS1p-Bold',
            }}
          >
            Blocked Users
          </Text>
          <View
            style={{
              backgroundColor: '#ECF3FB',
              borderRadius: 100,
              paddingHorizontal: Platform.OS === 'ios' ? 5 : 2,
              paddingVertical: Platform.OS === 'ios' ? 5 : null,
            }}
          >
            <TouchableOpacity
              onPress={() => navigation.navigate('UnblockuserScreen')}
              style={{
                width: 100,
                height: 30,
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <Text
                style={{
                  letterSpacing: 1,
                  fontWeight: '600',
                  fontSize: 16,
                }}
              >
                Show
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={{
            width: '100%',
            paddingHorizontal: 20,
            justifyContent: 'space-between',
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: 15,
          }}
        >
          <Button style={bottomButton.login} color="#8897AA" mode="contained">
            <Text
              style={{
                textTransform: 'capitalize',
                fontWeight: 'bold',
                fontSize: 18,
                color: 'white',
              }}
            >
              Done
            </Text>
          </Button>
        </View>

        <View
          style={{
            width: '100%',
            paddingHorizontal: 20,
            flexDirection: 'row',
            alignItems: 'center',
            marginBottom: 30,
            justifyContent: 'center',
            marginTop: Platform.OS === 'ios' ? 25 : -10,
          }}
        >
          <Text
            style={{
              marginHorizontal: 5,
              fontWeight: '600',
              fontSize: 16,
            }}
          >
            Want to
          </Text>
          <TouchableOpacity onPress={handleLogout}>
            <Text style={{color: '#560CCE', fontWeight: '600', fontSize: 16}}>
              Logout ?
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const bottomButton = StyleSheet.create({
  bottomView: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 35,
  },
  login: {
    marginBottom: height >= 844 ? 10 : 30,
    borderRadius: 8,
  },
});

export default Setting_Profile;
