import React, {useState} from 'react';
import {Text} from 'react-native-paper';
import {LoginButton} from 'react-native-fbsdk';
import {GoogleSignin} from '@react-native-google-signin/google-signin';
import {View, TouchableOpacity} from 'react-native';
import Background from '../components/Background';
import Logo from '../components/Logo';
import Header from '../components/Header';
import Button from '../components/Button';

function Dashboard({navigation}) {
  const [userData, setUserData] = useState();
  const _signOut = async () => {
    // Remove user session from the device.
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      // Removing user Info
      navigation.reset({
        index: 0,
        routes: [{name: 'StartScreen'}],
      });
    } catch (error) {
      console.error(error);
    }
  };
  return (
    <Background>
      <Logo />
      <Header>Let’s start</Header>

      {userData?.fbAuthStatus ? (
        <LoginButton
          publishPermissions={['email']}
          onLoginFinished={(error, result) => {
            if (error) {
              alert(`Login failed with error: ${error.message}`);
            } else if (result.isCancelled) {
              alert('Login was cancelled');
            } else {
              alert(
                `Login was successful with permissions: ${JSON.stringify(
                  result,
                )}`,
              );
            }
          }}
          onLogoutFinished={() => {
            alert('User logged out');
            navigation.reset({
              index: 0,
              routes: [{name: 'StartScreen'}],
            });
          }}
        />
      ) : (
        <Text />
      )}
      {userData?.googleAuthStatus ? (
        <TouchableOpacity onPress={_signOut}>
          <Text>Logout</Text>
        </TouchableOpacity>
      ) : (
        <Text />
      )}

      <View>
        {userData?.fbAuthStatus || userData?.googleAuthStatus ? (
          <Text />
        ) : (
          <>
            <Button
              mode="outlined"
              onPress={() =>
                navigation.reset({
                  index: 0,
                  routes: [{name: 'StartScreen'}],
                })
              }
            >
              <Text>Logout</Text>
            </Button>
          </>
        )}
      </View>
    </Background>
  );
}

export default Dashboard;
