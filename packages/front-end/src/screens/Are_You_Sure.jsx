import React, {useState, useEffect} from 'react';
import {
  Image,
  View,
  StyleSheet,
  Keyboard,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
  TextInput,
  Platform,
} from 'react-native';
import {Text} from 'react-native-paper';
// import TextInput from '../components/TextInput'

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {getStatusBarHeight} from 'react-native-status-bar-height';

import {StackActions} from '@react-navigation/native';

import {SafeAreaView} from 'react-native-safe-area-context';
import {theme} from '../core/theme';
import {string} from '../const/string';
import {checkConnection} from '../components/checkConnection';
import Button from '../components/Button';
// import { block } from 'react-native-reanimated'

function Are_You_Sure({navigation, route}) {
  const [keyboardStatus, setKeyboardStatus] = useState(undefined);

  const [signUpemail, setEmail] = useState({value: '', error: ''});
  const [userLoginId, setuserLoginId] = useState(route.params.userLoginId);
  // const [rchatdata, setrchatdata] =useState(route.params.rchatdata)
  const [message, setmessage] = useState('');
  const [onatherUserId, setonatherUserId] = useState(
    route.params.onatherUserId,
  );
  const [reasonId, setselcted] = useState(route.params.selcted);

  // const [userData, setUserData] = useState(props);
  // console.log("userData-  About -----------------",userLoginId ,"onatherUserId- about ------",onatherUserId ,"selcted  about----------",reasonId,"rchatdata-------",rchatdata)

  const [isGoogleLoading, setIsGoogleloading] = useState(false);
  const [isLoading, setisLoading] = useState(false);
  // const [religionOptionsList, setReligionOptionsList] = useState([]);
  // const [relationshipTypeList, setRelationshipTypeList] = useState([]);
  const [net, setnet] = React.useState(true);

  const windowWidth = Dimensions.get('window').width;
  const windowHeight = Dimensions.get('window').height;
  const {width} = Dimensions.get('window');
  const {height} = Dimensions.get('window');
  useEffect(() => {
    const showSubscription = Keyboard.addListener('keyboardDidShow', () => {
      setKeyboardStatus('Keyboard Shown');
    });
    const hideSubscription = Keyboard.addListener('keyboardDidHide', () => {
      setKeyboardStatus('Keyboard Hidden');
    });
    ApiCAlling();
    return () => {
      showSubscription.remove();
      hideSubscription.remove();
    };
  }, []);

  const ApiCAlling = async () => {
    checkConnection().then((res) => {
      if (res === false) {
        // dropDownAlertRef.alertWithType('error', 'Error', string.network);
        setnet(res);
      } else {
        setnet(res);
        // creatChannel();
      }
    });
  };

  const block = () => {
    checkConnection().then((res) => {
      if (res === false) {
        // dropDownAlertRef.alertWithType('error', 'Error', string.network);
        setnet(res);
      } else {
        setnet(res);
        block1();
      }
    });
  };

  const block1 = () => {
    let data = '';
    if (message === '') {
      // alert("blank")
      data = 'null';
    } else {
      // alert("full")
      data = message;
    }
    console.log('newdat-----', userLoginId, onatherUserId, reasonId, data);
    setisLoading(true);
    fetch('http://52.3.61.123:7070/auth/addToBlock', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        userID: userLoginId,
        blockedUser: onatherUserId,
        reasonId,
        repotReason: data,
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(
          'block========================================================',
          JSON.stringify(responseJson),
        );

        if (responseJson.status === 200) {
          console.log(
            'block========================================================',
            JSON.stringify(responseJson),
          );
          // console.log(JSON.stringify(userData));
          // navigation.navigate("Messages",{userID:userID})
          // alert(responseJson)

          setisLoading(false);
          navigation.dispatch(StackActions.popToTop());
          // navigation.push("BottomTabs")

          if (responseJson.status === 404) {
            // alert("Oops!! Something went wrong. 404")
            setisLoading(false);
            // navigation.reset({
            //   index: 0,
            //   routes: [{ name: 'LoginScreen' }],
            // })
          }
          if (responseJson.status === 400) {
            // alert("Oops!! Something went wrong. 404")
            setisLoading(false);
            // navigation.reset({
            //   index: 0,
            //   routes: [{ name: 'LoginScreen' }],
            // })
          }
        }
      })
      .catch((error) => {
        setisLoading(false);
        alert('Oops error!!');
        console.error(error);
      });
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
      <View style={{flex: 1, marginHorizontal: 20, alignItems: 'center'}}>
        <View
          style={{
            flexDirection: 'row',
            top: Platform.OS === 'ios' ? 0 : 0,
            alignItems: 'center',
            justifyContent: 'center',
            width: '100%',
            backgroundColor: '#fff',
            zIndex: 10,
          }}
        >
          <View style={{width: '20%'}}>
            <TouchableOpacity onPress={() => navigation.goBack()} style={{}}>
              <FontAwesome
                name="angle-left"
                size={25}
                color="#000"
                style={{}}
              />
            </TouchableOpacity>
          </View>
          <View
            style={{
              width: '60%',
              alignItems: 'center',
              flexDirection: 'row',
              justifyContent: 'center',
            }}
          >
            <Image
              source={require('../assets/starzlogo.png')}
              style={sideLogoStyles.imageLogo}
            />
            {/* <Image source={require('../assets/STAARZ_109X19_black.png')} style={sideLogoStyles.image} /> */}
            <Text
              style={{
                fontSize: wp(6),
                fontFamily: 'MPLUS1p-Bold',
                color: '#243443',
                left: -2,
                lineHeight: 33,
                textAlign: 'center',
              }}
            >
              STAARZ
            </Text>
          </View>
          <View style={{backgroundColor: 'red', width: '20%'}} />
        </View>

        {!net ? (
          <View
            style={{alignItems: 'center', justifyContent: 'center', flex: 1}}
          >
            <Image
              source={require('../assets/internat.png')}
              style={{height: 50, width: 50}}
            />
            <Text style={{fontSize: 17, fontWeight: '700', marginTop: 5}}>
              {string.network}
            </Text>
            <TouchableOpacity
              color="white"
              mode="contained"
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                top: windowHeight * 0.3,
              }}
              onPress={() => ApiCAlling()}
            >
              <View
                style={{
                  backgroundColor: '#7643F8',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 50,
                  paddingHorizontal: 30,
                  paddingVertical: 20,
                  marginTop: 10,
                }}
              >
                {
                  Platform.OS === 'ios' ? (
                    // <Ionicons name="reload" color="black" size={30} />
                    <Text style={{fontSize: hp('2'), color: '#fff'}}>
                      {string.Try_Again}
                    </Text>
                  ) : (
                    <Text style={{fontSize: hp('2'), color: '#fff'}}>
                      {string.Try_Again}
                    </Text>
                  )
                  // <MaterialCommunityIcons name="reload" color="black" size={30} />
                }
              </View>
            </TouchableOpacity>
          </View>
        ) : (
          <>
            <StaarzLogoText />

            <View
              style={{
                backgroundColor: '#fff',
                width: '100%',
                borderRadius: 5,
                position: 'absolute',
                top: '23%',
              }}
            >
              <Text
                style={{
                  color: '#101010',
                  marginBottom: 0,
                  marginTop: 15,
                  alignContent: 'flex-start',
                  fontWeight: 'bold',
                  fontSize:
                    width === 320 || height === 610.6666666666666 ? 16 : 19,
                  lineHeight: 21,
                  fontFamily: 'SFUIText-Semibold',
                }}
              >
                Tell us what happenned?{' '}
              </Text>
              <View
                style={{
                  backgroundColor: '#fff',
                  width: '100%',
                  borderRadius: 5,
                  borderColor: '#ccc',
                  borderWidth: 1,
                  marginTop: 10,
                  paddingHorizontal: 5,
                  height: 150,
                }}
              >
                <TextInput
                  multiline
                  placeholderTextColor="#B9BAC8"
                  // returnKeyType="next"
                  style={{
                    fontSize: 15,
                    lineHeight: 20,
                    fontFamily: 'MPLUS1p-Medium',
                    textAlignVertical: 'top',
                  }}
                  numberOfLines={6}
                  keyboardType="email-address"
                  returnKeyType="next"
                  value={message.value}
                  onSubmitEditing={Keyboard.dismiss}
                  onChangeText={(text) => setmessage(text)}
                  placeholder="Tell us more...."
                />
              </View>
            </View>
            <View style={bottomButton.bottomView}>
              {isLoading ? (
                <ActivityIndicator
                  style={bottomButton.login}
                  size="large"
                  color="#8897AA"
                />
              ) : (
                <Button
                  style={bottomButton.login}
                  color="#8897AA"
                  mode="contained"
                  onPress={() => block()}
                >
                  <Text
                    style={{
                      textTransform: 'capitalize',
                      fontWeight: 'bold',
                      fontSize: 18,
                      color: 'white',
                    }}
                  >
                    Block
                  </Text>
                </Button>
              )}
              <View>
                <Text
                  style={{
                    color: '#BFC7D3',
                    fontSize: 12,
                    lineHeight: 20,
                    fontWeight: '500',
                    fontFamily: 'MPLUS1p-Medium',
                  }}
                >
                  By creating an account , you agree to our
                </Text>
              </View>
              <TouchableOpacity>
                <Text
                  style={{
                    // fontWeight: 'bold', // DUP
                    fontSize: 13,
                    lineHeight: 20,
                    fontWeight: '500',
                    fontFamily: 'MPLUS1p-Medium',
                    color: '#7643F8',
                  }}
                >
                  Terms and Conditions
                </Text>
              </TouchableOpacity>
            </View>
          </>
        )}
      </View>
    </SafeAreaView>
  );
}

function StaarzLogo() {
  return (
    <View style={sideLogoStyles.container}>
      <Image
        source={require('../assets/starzlogo_23X26.png')}
        style={sideLogoStyles.imageLogo}
      />
      <Image
        source={require('../assets/STAARZ_109X19_black.png')}
        style={sideLogoStyles.image}
      />
    </View>
  );
}

function StaarzLogoText() {
  return (
    <View style={{position: 'absolute', top: '10%'}}>
      <Text
        style={{
          fontWeight: '900',
          letterSpacing: 0.4,
          fontSize: 24,
          alignItems: 'center',
          alignSelf: 'center',
          lineHeight: 36,
          fontFamily: 'MPLUS1p-Bold',
        }}
      >
        Are you sure?
      </Text>
      <Text
        style={{
          textAlign: 'center',
          letterSpacing: 1,
          fontSize: 17,
          marginTop: 5,
          color: 'grey',
          fontWeight: '600',
          lineHeight: 22,
          fontFamily: 'MPLUS1p-Medium',
        }}
      >
        We will rich to you 24 hours
      </Text>
    </View>
  );
}

const bottomButton = StyleSheet.create({
  bottomView: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
  },
  login: {
    marginBottom: 15,
    borderRadius: 8,
  },
});

const sideLogoStyles = StyleSheet.create({
  image: {
    marginTop: 5,
    marginLeft: 20,
  },
  imageLogo: {
    width: wp(7.5),
    height: wp(6.5),
    resizeMode: 'cover',
    left: -4,
    // marginTop: 8,
    // marginBottom: 8,
  },
  container: {
    position: 'absolute',
    top: 0 + getStatusBarHeight(),
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    backgroundColor: 'white',
    zIndex: 10,
    width: '100%',
  },
});

const orDividerLineStyle = StyleSheet.create({
  or: {
    marginTop: 25,
    marginBottom: 10,
    alignSelf: 'center',
  },
});

const socialMediaLoginButtonStyle = StyleSheet.create({
  facebook: {
    // marginBottom: 8,
  },
  google: {
    // marginLeft:20
  },
});

const styles = StyleSheet.create({
  forgotPassword: {
    width: '100%',
    alignItems: 'flex-end',
    marginRight: 10,
    marginBottom: 24,
  },
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  forgot: {
    fontSize: 13,
    marginTop: 10,
    marginRight: -2,
    color: theme.colors.secondary,
  },
  rememberMe: {
    fontSize: 13,
    marginTop: 10,
    marginLeft: 10,
    color: theme.colors.secondary,
  },
  link: {
    // fontWeight: '100',
    color: '#A17EFA', // theme.colors.primary,
    lineHeight: 30,
    fontSize: 13,
  },
  otionContent: {
    width: '100%',
    backgroundColor: '#f0f8ff',
    justifyContent: 'space-between',
    alignItems: 'center',
    //   padd
    marginVertical: 12,
    flexDirection: 'row',
  },
  selctoptionTExt: {
    fontWeight: 'bold',
    width: '90%',
    //   backgroundColor:"red",
    fontSize: 13,
  },
  NullChekbox: {
    height: 13,
    width: 13,
    borderWidth: 2,
    borderColor: '#ccc',
    borderRadius: 2,
  },
  chekborright: {
    height: 13,
    width: 13,
    borderWidth: 2,
    borderColor: '#8a2be2',
    backgroundColor: '#8a2be2',
    borderRadius: 2,
  },
});

export default Are_You_Sure;
