import * as React from 'react';
import {Image} from 'react-native';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import LikesComponent from './main/likes';
import ProfileSwipe from './ProfileSwipe';
import SettingsComponent from './main/settings';
import {MessagingMainStack} from './main/messaging';

const Tab = createMaterialBottomTabNavigator();

function BottomTabs({navigation, route}) {
  return (
    <Tab.Navigator
      initialRouteName="ProfileSwipe"
      inactiveColor="#8A98AC"
      activeColor="#7643F8"
      showLable
      shifting={false}
      barStyle={{backgroundColor: 'white', padding: 5}}
      tabBarOptions={{
        showLable: true,
        // fontSize:50,
        activeTintColor: '#e91e63',
        innerWidth: '50%',
        outerWidth: '50%',
      }}
    >
      <Tab.Screen
        name="Likes"
        component={LikesComponent}
        options={{
          tabBarLabel: 'Likes',
          tabBarIcon: ({color, focused}) => (
            <Image
              source={
                focused
                  ? require('../assets/heart-A.png')
                  : require('../assets/hearticon.png')
              }
              style={{height: wp('7'), width: wp('7'), resizeMode: 'contain'}}
            />
          ),
        }}
      />

      <Tab.Screen
        name="Messages"
        component={MessagingMainStack}
        options={{
          tabBarLabel: 'Messages',
          tabBarIcon: ({color, focused}) => (
            <Image
              source={
                focused
                  ? require('../assets/messages-A.png')
                  : require('../assets/messagesicon.png')
              }
              style={{height: wp('7'), width: wp('7'), resizeMode: 'contain'}}
            />
          ),
        }}
      />
      <Tab.Screen
        name="ProfileSwipe"
        component={ProfileSwipe}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({color, focused}) => (
            <Image
              source={
                focused
                  ? require('../assets/Frame-B.png')
                  : require('../assets/Frame161089.png')
              }
              style={{height: wp('7'), width: wp('7'), resizeMode: 'contain'}}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={SettingsComponent}
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: ({color, focused}) => (
            <Image
              source={
                focused
                  ? require('../assets/Frame-A.png')
                  : require('../assets/Frame161090.png')
              }
              style={{height: wp('7'), width: wp('7'), resizeMode: 'contain'}}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
}
export default BottomTabs;
