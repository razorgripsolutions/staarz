import React, {useState, useEffect} from 'react';
import {
  Dimensions,
  Image,
  View,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  Platform,
} from 'react-native';
import {Text} from 'react-native-paper';
// import Background from '../components/Background'
import {getStatusBarHeight} from 'react-native-status-bar-height';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
// import Background from '../components/Background'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {backendBaseUrl} from '../const/urlDirectory';
import {theme} from '../core/theme';

function FullImageScreen(props) {
  // alert(JSON.stringify(props.route.params.matchUserData));
  const windowWidth = Dimensions.get('window').width;
  const windowHeight = Dimensions.get('window').height;
  // const temp = {"userID":403,"userEmail":"ghdfgdf@dfsgdf.dfg","userPassword":"","username":"","facebookToken":"","googleToken":"","fbAuthStatus":0,"googleAuthStatus":0,"fullName":" Fhfgh gf","dob":"31-03-2021","locationText":"6-E, Nungambakkam High Road 1A, Dr, Thirumurthy Nagar, 1st Street, Nungambakkam, Chennai, Tamil Nadu 600034, India","lat":13.055441,"lon":80.2479924,"gender":"1","religionID":2,"introVideoID":0,"profilePicture":"","height":0,"weight":0,"alternateLocation":"","haveCar":0,"doesSmoke":0,"aboutMe":"","relationshipType":2,"packageID":0,"identifyID":0,"emailVerified":1,"isImageVerified":0,"isVideoVerified":0,"isProfileVerified":0,"facebookID":"","googleID":"","uploadedImage":"","clickedImage":"","fbProfileLink":"Dghgh","instaProfileLink":"Fghhjjjk"}
  const [userData, setUserData] = useState(props.route.params.url);
  //  const [matchUserData, setmatchUserData] = useState(props.route.params.matchUserData);//route.params.userData);
  const [accessToken, setAccessToken] = useState(
    props.route.params.accessToken,
  ); // route.params.accessToken);
  const [instaProfileLink, setInstaProfileLink] = useState('');
  const [fbProfileLink, setFbProfileLink] = useState('');
  const [isloading, setIsloading] = useState(false);
  const [isDetailsloading, setIsDetailsloading] = useState(false);
  const [usersProfile, setUsersProfile] = useState(
    props.route.params.matchUserData,
  );

  const {navigation} = props;
  const {route} = props;

  useEffect(() => {}, []);

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
      <View
        style={{
          paddingBottom: 5,
          flexDirection: 'row',
          top: Platform.OS === 'ios' ? 0 : 0,
          alignItems: 'center',
          justifyContent: 'center',
          width: '100%',
          backgroundColor: '#fff',
        }}
      >
        <View style={{width: '20%'}}>
          <TouchableOpacity onPress={() => navigation.goBack()} style={{}}>
            <FontAwesome
              name="angle-left"
              size={25}
              color="#000"
              style={{left: 20}}
            />
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: '60%',
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'center',
          }}
        >
          <Image
            source={require('../assets/starzlogo.png')}
            style={sideLogoStyles.imageLogo}
          />
          <Text
            style={{
              fontSize: wp(6),
              fontFamily: 'MPLUS1p-Bold',
              color: '#243443',
              left: -2,
              lineHeight: 33,
              textAlign: 'center',
            }}
          >
            STAARZ
          </Text>
          {/* <Image source={require('../assets/STAARZ_109X19_black.png')} style={sideLogoStyles.image} /> */}
        </View>
        <View style={{width: '20%'}} />
      </View>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          width: '98%',

          justifyContent: 'center',
          alignContent: 'center',
          alignSelf: 'center',
          // borderRadius: 20
        }}
      >
        <Image
          resizeMode="cover"
          // style={backgroundStyles.background} DUP
          source={{uri: `${backendBaseUrl}data/${userData}`}}
          style={{
            height: '99%',
            width: '100%',
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'center',
            borderRadius: 20,
          }}
        />
      </View>
    </SafeAreaView>
  );
}

const swipeCardStyles = StyleSheet.create({
  container: {
    // top:0,
    // width:"100%",
    // backgroundColor:"red",
    top: 10 + getStatusBarHeight(),
  },
  card: {
    flex: 1,
    borderRadius: 4,
    borderWidth: 2,
    width: '100%',
    borderColor: '#E8E8E8',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  text: {
    textAlign: 'center',
    fontSize: 50,
    // backgroundColor: "transparent"
  },
});

const sideLogoStyles = StyleSheet.create({
  image: {
    marginTop: 0,
    marginLeft: 20,
  },
  imageLogo: {
    // left: 10,
    width: wp(7.5),
    height: wp(6.5),
    left: -4,
    // marginBottom: 8,
  },
  container: {
    // position: 'absolute',
    top: Platform.OS === 'ios' ? hp('5') : hp('2'),
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    backgroundColor: 'white',
    zIndex: 10,
    width: '100%',
    // marginTop: 0
  },
  // image: {
  //   width: 24,
  //   height: 24,
  // }
});

const backgroundStyles = StyleSheet.create({
  background: {
    flex: 1,
    width: '100%',
    top: -10,
    backgroundColor: theme.colors.surface,
  },
  container: {
    flex: 1,
    // padding: 20,
    width: '100%',
    // maxWidth: 340,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default FullImageScreen;
