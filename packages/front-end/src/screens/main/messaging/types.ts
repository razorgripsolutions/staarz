export type MessagingTopNavigatorStack = {
  messaging: undefined;
  match: undefined;
};

export type MessagingStackNavigator = {
  messages: undefined;
};

export type MatchStackNavigator = {
  matchList: undefined;
};
