import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Messaging} from './messagesList/allMessages';

export type MessagingNavigatorStack = {
  allMessages: undefined;
};

const Stack = createStackNavigator<MessagingNavigatorStack>();

const defaultHeaderOptions = {
  headerShown: false,
};

const MessagingStack = () => {
  return (
    <Stack.Navigator initialRouteName="allMessages">
      <Stack.Screen
        name="allMessages"
        component={Messaging}
        options={{...defaultHeaderOptions}}
      />
    </Stack.Navigator>
  );
};

export {MessagingStack};
