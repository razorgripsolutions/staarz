import React from 'react';
import {View} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import Ionicons from 'react-native-vector-icons/Ionicons';

export const ChatEmpty = () => {
  return (
    <View
      style={{
        marginVertical: RFValue(40),
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <Ionicons name="ios-home" />
    </View>
  );
};
