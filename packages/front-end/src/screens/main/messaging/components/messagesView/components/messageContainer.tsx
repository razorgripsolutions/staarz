import React from 'react';
import {Bubble} from 'react-native-gifted-chat';
import {Colors} from '../../../../../../constants/colors';

export const renderBubble = (props: any) => (
  <Bubble
    {...props}
    wrapperStyle={{
      right: {backgroundColor: Colors.primary},
    }}
  />
);
