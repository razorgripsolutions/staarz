import React from 'react';
import {ActivityIndicator, View} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export const LoadEarlier = () => {
  return (
    <View style={{marginVertical: RFValue(40)}}>
      <ActivityIndicator />
    </View>
  );
};
