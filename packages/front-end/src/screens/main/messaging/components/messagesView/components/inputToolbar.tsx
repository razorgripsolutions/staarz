import React from 'react';
import {InputToolbar, Actions, Composer, Send} from 'react-native-gifted-chat';
import {RFValue} from 'react-native-responsive-fontsize';
import {Colors} from '../../../../../../constants/colors';
import Ionicons from 'react-native-vector-icons/Ionicons';

export const renderInputToolbar = (props: any) => (
  <InputToolbar {...props} primaryStyle={{alignItems: 'flex-end'}} />
);

export const renderActions = (props: any) => (
  <Actions
    {...props}
    containerStyle={{
      width: RFValue(30),
      height: RFValue(30),
      alignItems: 'center',
      justifyContent: 'center',
      marginBottom: RFValue(3),
      marginRight: RFValue(0),
      marginLeft: RFValue(7),
      backgroundColor: Colors.gray['100'],
      borderRadius: RFValue(5),
    }}
    icon={() => (
      <Ionicons
        name={'ios-add'}
        color={Colors.primary['900']}
        size={RFValue(18)}
      />
    )}
    options={{
      Camera: () => {
        console.log('Camera');
      },
      'Photo & Video Library': () => {
        console.log('Photo & Video Library');
      },
      Document: () => {
        console.log('Photo & Video Library');
      },
      Cancel: () => {
        console.log('Cancel');
      },
    }}
    optionTintColor={Colors.primary['900']}
  />
);

export const renderComposer = (props: any) => (
  <Composer
    {...props}
    textInputStyle={{
      color: '#222B45',
      backgroundColor: Colors.gray['100'],
      borderRadius: 10,
      paddingTop: RFValue(10),
      paddingHorizontal: 12,
      marginRight: RFValue(10),
      marginLeft: RFValue(10),
      marginTop: 2,
      marginBottom: 10,
    }}
    placeholderTextColor={Colors.gray['400']}
  />
);

export const renderSend = (props: any) => (
  <Send
    {...props}
    disabled={!props.text}
    containerStyle={{
      width: 44,
      height: 44,
      alignItems: 'center',
      justifyContent: 'center',
      marginHorizontal: 4,
    }}
  >
    <Ionicons
      name={'ios-arrow-forward-circle'}
      color={Colors.primary}
      size={RFValue(25)}
    />
  </Send>
);
