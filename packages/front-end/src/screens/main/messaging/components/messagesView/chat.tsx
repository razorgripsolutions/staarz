import React, {useCallback, useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {GiftedChat} from 'react-native-gifted-chat';
import {
  renderInputToolbar,
  // renderActions,
  renderComposer,
  renderSend,
} from './components/inputToolbar';
import {getBottomSpace} from 'react-native-iphone-x-helper';
import {RFValue} from 'react-native-responsive-fontsize';
import {renderBubble} from './components/messageContainer';
import {LoadEarlier} from './components/loadEarlier';
import {Colors} from '../../../../../constants/colors';
import Entypo from 'react-native-vector-icons/Entypo';
import {usePubNub} from 'pubnub-react';
import {useAuthProvider} from '../../../../../../App';
import {useBlockUser} from '../../../../../api/hooks/chat';
import Toast from 'react-native-toast-message';

const ChatView = ({route, navigation}: any) => {
  const pubnub = usePubNub();
  const {userState} = useAuthProvider();
  const channel = route.params.data;
  const [messages, setMessages] = useState<any>([]);

  const {mutateAsync, isLoading} = useBlockUser();

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {});
    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    pubnub.addListener({message: handleMessage});
    pubnub.subscribe({channels: [channel._id]});
  }, [pubnub, channel]);

  const handleMessage = (event: any) => {
    const message = event.message;
    const text = JSON.stringify(message);
    const parsedMessage = JSON.parse(text) as any;
    if (parsedMessage?.user?._id !== userState?.userData?.id?.toString()) {
      setMessages((prev: any) => GiftedChat.append(prev, [JSON.parse(text)]));
    }
  };

  const handleBlockUser = useCallback(async () => {
    try {
      const response = await mutateAsync({
        userId: channel._id,
      });
      Toast.show({
        type: 'success',
        text1: 'Success',
        text2: `${channel.name} blocked successfully`,
      });
    } catch (error) {
      return Toast.show({
        type: 'error',
        text1: 'Block error',
        text2: 'An error occured',
      });
    }
  }, [mutateAsync]);

  const handleShowBlockAlert = useCallback(() => {
    Alert.alert(
      `Block ${channel.name}`,
      'Are you sure you want to block user?',
      [
        {
          text: 'No',
          onPress: () => {},
          style: 'cancel',
        },
        {
          text: 'Yes',
          onPress: handleBlockUser,
        },
      ],
    );
  }, [handleBlockUser]);

  const onSend = useCallback(
    (messages = []) => {
      //save in chat
      setMessages((previousMessages: any) =>
        GiftedChat.append(previousMessages, messages),
      );

      messages.map((message: any) => {
        pubnub
          .publish({channel: channel._id, message: message})
          .then(() => {})
          .catch();
      });
    },
    [setMessages],
  );

  return (
    <View style={styles.container}>
      <View style={styles.userContainer}>
        {/* profile container */}
        <View style={styles.profileContainer}>
          <View style={{marginRight: RFValue(15)}}>
            <Image
              source={{
                uri:
                  channel?.profilePicture ??
                  'https://images.unsplash.com/photo-1519699047748-de8e457a634e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8OHx8ZmFjZXxlbnwwfDJ8MHx8&auto=format&fit=crop&w=500&q=60',
              }}
              style={{
                width: RFValue(45),
                height: RFValue(45),
                borderRadius: RFValue(25),
              }}
            />
          </View>
          <View>
            <Text style={{fontSize: RFValue(13), color: Colors.gray['900']}}>
              {channel?.name}
            </Text>
            <Text
              style={{color: Colors.gray['400'], fontSize: 12, paddingTop: 3}}
            >
              October 28, 2022 - 10:50pm
            </Text>
          </View>
        </View>
        {/* block container */}
        <TouchableOpacity
          onPress={handleShowBlockAlert}
          activeOpacity={0.9}
          style={styles.blockContainer}
        >
          <Entypo name="block" size={20} color={Colors.red['600']} />
        </TouchableOpacity>
      </View>
      <GiftedChat
        loadEarlier={false}
        messages={messages}
        onSend={(messages: any) => onSend(messages)}
        user={{
          _id: userState?.userData?.id.toString() ?? '',
          name: userState?.userData?.fullName,
          avatar:
            userState?.userData?.profilePicture ??
            'https://images.unsplash.com/photo-1526509569184-2fe126e71cd3?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mjl8fHdvbWFuJTIwZGF0aW5nfGVufDB8MXwwfHw%3D&auto=format&fit=crop&w=500&q=60',
        }}
        renderLoadEarlier={({}) => <LoadEarlier />}
        renderInputToolbar={renderInputToolbar}
        // renderActions={renderActions}
        renderComposer={renderComposer}
        renderSend={renderSend}
        bottomOffset={getBottomSpace() + RFValue(-5)}
        renderBubble={renderBubble}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  userContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomColor: Colors.gray['200'],
    borderBottomWidth: StyleSheet.hairlineWidth,
    padding: RFValue(10),
  },
  profileContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  blockContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    width: 40,
    borderRadius: 20,
    backgroundColor: Colors.white,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,
    elevation: 8,
  },
});

export default ChatView;
