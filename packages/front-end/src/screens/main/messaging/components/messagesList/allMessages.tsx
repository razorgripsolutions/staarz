import * as React from 'react';
import {
  Dimensions,
  FlatList,
  Platform,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import {Colors} from '../../../../../constants/colors';
import {ChatListItem} from './chatListItem';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {useQuery} from 'react-query';
import {AxiosResponse} from 'axios';
import {get} from '../../../../../api/helpers';
import {API_ENDPOINTS} from '../../../../../api/routes';
import {CircleFade} from 'react-native-animated-spinkit';

const {height} = Dimensions.get('screen');

const Messaging = ({navigation}: any) => {
  const {data, isLoading, refetch} = useQuery<any, any, AxiosResponse<any[]>>(
    'messages',
    () => get(API_ENDPOINTS.getChat),
  );
  return (
    <React.Fragment>
      <View style={styles.container}>
        {isLoading ? (
          <CircleFade
            size={70}
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              top: height * 0.2,
              alignSelf: 'center',
            }}
            color="#5E2EBA"
          />
        ) : (
          <>
            {data?.data?.length === 0 ? (
              <View style={styles.innerContainer}>
                <Text style={styles.noMessagesText}>No messages found</Text>
                <View style={styles.referesButtonOuterContainer}>
                  <TouchableOpacity
                    activeOpacity={0.9}
                    style={styles.refereshButtonContainer}
                  >
                    <View style={styles.refereshButtonInnerContainer}>
                      {Platform.OS === 'ios' ? (
                        <Text style={styles.refereshText}>Refresh</Text>
                      ) : (
                        <Text style={styles.refereshText}>Refresh</Text>
                      )}
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            ) : (
              <SafeAreaView style={{flex: 1}}>
                <FlatList
                  style={{paddingTop: RFValue(10)}}
                  data={data?.data}
                  renderItem={({item}) => (
                    <ChatListItem
                      data={item}
                      onViewChat={() => {
                        navigation.push('chatView', {
                          data: item,
                        });
                      }}
                    />
                  )}
                  keyExtractor={(item) => item._id}
                />
              </SafeAreaView>
            )}
          </>
        )}
      </View>
    </React.Fragment>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    position: 'relative',
  },
  innerContainer: {
    flex: 0.3,
    marginTop: height * 0.2,
  },
  noMessagesText: {
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: hp('3'),
    fontWeight: '900',
    textAlign: 'center',
    color: 'black',
  },

  referesButtonOuterContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  refereshButtonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  refereshButtonInnerContainer: {
    backgroundColor: '#7643F8',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    paddingHorizontal: 30,
    paddingVertical: 20,
    marginTop: 16,
  },
  refereshText: {
    fontSize: hp('2'),
    color: '#fff',
  },
});

export {Messaging};
