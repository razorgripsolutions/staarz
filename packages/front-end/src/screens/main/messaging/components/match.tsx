import * as React from 'react';
import {
  Dimensions,
  FlatList,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {Colors} from '../../../../constants/colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {MatchListItem} from '../shared';
import {useAuthProvider} from '../../../../../App';
import {handlePickRandomImages} from '../../../../components/utils/random-images';
import {MatchResponse, useGetMatch} from '../../../../api/hooks/match';
import {AxiosError, AxiosResponse} from 'axios';
import {imagesData} from '../../../../components/data/images';
import {CircleFade} from 'react-native-animated-spinkit';

const {height} = Dimensions.get('screen');

const Match = ({navigation}: any) => {
  const {userState} = useAuthProvider();
  const {mutateAsync, isLoading, data} = useGetMatch();
  const [matchUsers, setMatchUsers] = React.useState<MatchResponse[]>([]);

  const handleGetUsers = React.useCallback(() => {
    mutateAsync({
      userId: userState?.userData?.id ?? 0,
    })
      .then(async (response: AxiosResponse<MatchResponse[]>) => {
        const {data: results} = response;
        const images = handlePickRandomImages(results?.length, imagesData);
        let newLists: MatchResponse[] = [];
        results.forEach((item, index) => {
          const itemImage = images?.[index];
          const newImage = {
            ...item,
            profilePicture: item?.profilePicture ?? itemImage,
          };
          newLists.push(newImage);
        });
        setMatchUsers(newLists);
      })
      .catch((e: AxiosError<unknown>) => {});
  }, [setMatchUsers, handlePickRandomImages, userState]);

  React.useEffect(() => {
    if (userState?.userData) {
      handleGetUsers();
    }
  }, [userState, handleGetUsers]);

  return (
    <View style={styles.container}>
      {isLoading ? (
        <CircleFade
          size={70}
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            top: height * 0.2,
            alignSelf: 'center',
          }}
          color="#5E2EBA"
        />
      ) : (
        <>
          {data && matchUsers?.length === 0 ? (
            <View style={styles.innerContainer}>
              <Text style={styles.noMessagesText}>No match yet :(</Text>
              <View style={styles.referesButtonOuterContainer}>
                <TouchableOpacity
                  activeOpacity={0.9}
                  style={styles.refereshButtonContainer}
                >
                  <View style={styles.refereshButtonInnerContainer}>
                    {Platform.OS === 'ios' ? (
                      <Text style={styles.refereshText}>Refresh</Text>
                    ) : (
                      <Text style={styles.refereshText}>Refresh</Text>
                    )}
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          ) : (
            <FlatList
              data={matchUsers}
              keyExtractor={(item) => item.id.toString()}
              showsVerticalScrollIndicator={false}
              renderItem={({item}) => (
                <MatchListItem
                  match={item}
                  onStartConversation={() => {
                    navigation.push('chatView', {
                      data: {
                        _id: item?.id,
                        name: item?.fullName,
                        profilePicture: item?.profilePicture,
                      },
                    });
                  }}
                />
              )}
            />
          )}
        </>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    backgroundColor: Colors.white,
  },
  innerContainer: {
    flex: 0.3,
    marginTop: height * 0.2,
  },
  noMessagesText: {
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: hp('3'),
    fontWeight: '900',
    textAlign: 'center',
    color: 'black',
  },

  referesButtonOuterContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  refereshButtonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  refereshButtonInnerContainer: {
    backgroundColor: '#7643F8',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    paddingHorizontal: 30,
    paddingVertical: 20,
    marginTop: 16,
  },
  refereshText: {
    fontSize: hp('2'),
    color: '#fff',
  },
});

export {Match};
