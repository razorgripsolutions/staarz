import * as React from 'react';
import {Image, SafeAreaView, StyleSheet, View} from 'react-native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {MessagingTopNavigatorStack} from './types';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {Colors} from '../../../constants/colors';
import {FontStyles} from '../../../constants/font';
import {Text} from 'react-native-paper';
import {useAuthProvider} from '../../../../App';
import {Match, MessagingStack} from './components';

const Tab = createMaterialTopTabNavigator<MessagingTopNavigatorStack>();
export const ProfilePlaceHolder =
  'https://e7.pngegg.com/pngimages/1000/665/png-clipart-computer-icons-profile-s-free-angle-sphere.png';

function MessagesTabComponent() {
  const {userState} = useAuthProvider();
  return (
    <>
      <SafeAreaView style={styles.container}>
        <View style={styles.pageHeaderContainer}>
          <View />
          <View style={styles.textContainer}>
            <Text style={styles.text}>Messaging</Text>
          </View>
          <Image
            style={styles.avatar}
            source={{
              uri: userState?.userData?.profilePicture ?? ProfilePlaceHolder,
            }}
          />
        </View>
        <Tab.Navigator
          screenOptions={{
            tabBarStyle: styles.tabBarMainStyle,
            tabBarActiveTintColor: Colors.tintColor,
            tabBarInactiveTintColor: Colors.gray['500'],
            tabBarIndicatorStyle: styles.tabBarIndicator,
            tabBarLabelStyle: styles.tabBarLabel,
            tabBarItemStyle: styles.tabBarItem,
            tabBarIndicatorContainerStyle: styles.tabBarContainer,
            lazy: true,
          }}
        >
          <Tab.Screen
            name="match"
            component={Match}
            options={{
              tabBarLabel: 'Match',
            }}
          />
          <Tab.Screen
            name="messaging"
            component={MessagingStack}
            options={{
              tabBarLabel: 'Messaging',
            }}
          />
        </Tab.Navigator>
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    flex: 1,
  },
  pageHeaderContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    display: 'flex',
    alignItems: 'center',
    paddingHorizontal: wp(3),
  },
  textContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontWeight: '700',
    fontSize: 15,
  },
  avatar: {
    width: wp(9),
    height: wp(9),
    resizeMode: 'cover',
    borderRadius: 25,
  },
  tabBarMainStyle: {
    backgroundColor: Colors.white,
    marginLeft: -8,
  },
  tabBarIndicator: {
    backgroundColor: Colors.white,
    shadowColor: 'none',
    borderBottomWidth: 0,
  },
  tabBarLabel: {
    fontSize: 12,
    textTransform: 'capitalize',
    marginBottom: -3,
    fontFamily: FontStyles.bold,
  },
  tabBarItem: {
    width: 105,
  },
  tabBarContainer: {
    width: '20%',
    marginLeft: 20,
  },
});
export {MessagesTabComponent};
