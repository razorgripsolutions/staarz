import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {MessagesTabComponent} from './tab';
import {StyleSheet, View, Text, Image, TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import Entypo from 'react-native-vector-icons/Entypo';
import ChatViewComponent from './components/messagesView';
import {Colors} from '../../../constants/colors';

type MainMessagingNavigatorStack = {
  messageTab: undefined;
  chatView: undefined;
};

const defaultHeaderOptions = {
  headerShown: false,
};

const Stack = createStackNavigator<MainMessagingNavigatorStack>();

const MessagingMainStack = () => {
  return (
    <Stack.Navigator initialRouteName="messageTab">
      <Stack.Screen
        name="messageTab"
        component={MessagesTabComponent}
        options={{...defaultHeaderOptions}}
      />
      <Stack.Screen
        name="chatView"
        component={ChatViewComponent}
        options={{
          header: ({navigation}) => {
            return (
              <View style={styles.pageHeaderContainer}>
                <TouchableOpacity
                  activeOpacity={0.9}
                  onPress={() => navigation.goBack()}
                >
                  <Entypo name="chevron-thin-left" size={wp(3.5)} />
                </TouchableOpacity>
                <View style={styles.logoContainer}>
                  <Image
                    source={require('../../../assets/starzlogo.png')}
                    style={styles.logo}
                  />
                  <Text
                    style={{
                      fontSize: wp(6),
                      fontFamily: 'MPLUS1p-Black',
                      color: '#243443',
                      paddingLeft: 7,
                      lineHeight: 33,
                      textAlign: 'center',
                      fontWeight: '900',
                    }}
                  >
                    STAARY
                  </Text>
                </View>
                <View />
              </View>
            );
          },
        }}
      />
    </Stack.Navigator>
  );
};

const styles = StyleSheet.create({
  pageHeaderContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    display: 'flex',
    alignItems: 'center',
    paddingHorizontal: wp(3),
    backgroundColor: Colors.white,
  },
  logoContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: -23,
  },
  logo: {
    width: wp(7.5),
    height: wp(6.5),
    resizeMode: 'cover',
  },
  logoText: {
    marginLeft: wp(3),
    resizeMode: 'contain',
  },
});

export {MessagingMainStack};
