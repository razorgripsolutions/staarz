import {MatchResponse} from '../../../../api/hooks/match';

export type MatchListItemComponentProp = {
  match: MatchResponse;
  onStartConversation: () => void;
};
