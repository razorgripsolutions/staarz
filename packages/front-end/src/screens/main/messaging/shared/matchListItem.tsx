import * as React from 'react';
import {Image, StyleSheet, TouchableOpacity, Text, View} from 'react-native';
import {Colors} from '../../../../constants/colors';
import {MatchListItemComponentProp} from './types';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {FontStyles} from '../../../../constants/font';
import {RFValue} from 'react-native-responsive-fontsize';

const MatchListItem = ({
  match,
  onStartConversation,
}: MatchListItemComponentProp) => {
  return (
    <React.Fragment>
      <View style={styles.container}>
        <View style={styles.innerContainer}>
          {/* match details */}
          <View style={styles.matchDetailsContainer}>
            <Image
              style={styles.profileImage}
              source={{
                uri: match.profilePicture,
              }}
            />
            <View style={styles.textContainer}>
              <Text style={styles.matchTextName}>{match.fullName}</Text>
              <View style={styles.matchOtherContainer}>
                <Text style={styles.matchOtherText}>{match.gender}</Text>
                <View style={styles.divider} />
                <Text style={styles.matchOtherText}>24</Text>
                <View style={styles.divider} />
                <Text style={styles.matchOtherText}>
                  {match.height ?? '173cm'}
                </Text>
              </View>
            </View>
          </View>
          {/* button */}
          <TouchableOpacity
            onPress={onStartConversation}
            style={styles.buttonContainer}
            activeOpacity={0.9}
          >
            <MaterialIcons name="message" size={20} color={Colors.primary} />
          </TouchableOpacity>
        </View>
      </View>
    </React.Fragment>
  );
};

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: 1,
    borderBottomColor: Colors.gray['200'],
  },
  innerContainer: {
    margin: 20,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  matchDetailsContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  profileImage: {
    width: RFValue(45),
    height: RFValue(45),
    borderRadius: RFValue(25),
    resizeMode: 'contain',
  },
  textContainer: {
    paddingLeft: 20,
    display: 'flex',
  },
  matchTextName: {
    fontSize: RFValue(13),
    color: Colors.gray['900'],
    fontFamily: FontStyles.bold,
  },
  matchOtherContainer: {
    display: 'flex',
    flexDirection: 'row',
    paddingTop: 5,
  },
  matchOtherText: {
    color: Colors.gray['500'],
    fontSize: 12,
    fontFamily: FontStyles.regular,
  },
  divider: {
    width: 2,
    backgroundColor: Colors.gray['100'],
    marginHorizontal: 8,
  },

  buttonContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    width: 40,
    borderRadius: 20,
    backgroundColor: Colors.white,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,
    elevation: 8,
  },
});

export {MatchListItem};
