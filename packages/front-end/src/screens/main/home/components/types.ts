import React from 'react';

export interface InterestComponentProp {
  interest: InterestProp;
  isInterestSelected?: InterestProp;
  handleSelectInterest: () => void;
  isButtonDisabled?: boolean;
}
export type InterestProp = {
  name: string;
  id: string;
};

export interface SwipeProfileComponentProp {
  isProfileModalShown: boolean;
  setIsProfileModalShown: React.Dispatch<React.SetStateAction<boolean>>;
}
