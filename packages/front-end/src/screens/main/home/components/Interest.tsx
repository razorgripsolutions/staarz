import * as React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import {Colors} from '../../../../constants/colors';
import {InterestComponentProp} from './types';

const Interests = ({
  interest,
  isInterestSelected,
  handleSelectInterest,
  isButtonDisabled,
}: InterestComponentProp) => {
  return (
    <React.Fragment>
      <TouchableOpacity
        onPress={handleSelectInterest}
        style={{
          height: 35,
          paddingHorizontal: 15,
          borderRadius: 30,
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          marginBottom: 15,
          marginRight: 10,
          backgroundColor: isInterestSelected ? Colors.primary : Colors.white,
          borderWidth: 1,
          borderColor: isInterestSelected ? 'none' : Colors.gray['500'],
        }}
        disabled={isButtonDisabled}
        activeOpacity={0.5}
      >
        <Text
          style={{
            color: isInterestSelected ? Colors.white : Colors.gray['500'],
            fontSize: 14,
          }}
        >
          {interest.name}
        </Text>
      </TouchableOpacity>
    </React.Fragment>
  );
};

Interests.defaultProps = {
  isButtonDisabled: false,
};
export {Interests};
