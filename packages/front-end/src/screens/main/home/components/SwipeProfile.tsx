import * as React from 'react';
import {StyleSheet, View, Text, Image, FlatList} from 'react-native';
import {Colors} from '../../../../constants/colors';
import {Interests} from './Interest';
import {InterestProp} from './types';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import {useAuthProvider} from '../../../../../App';

export const InterestList: InterestProp[] = [
  {
    id: '22',
    name: 'Active',
  },
  {
    id: '8',
    name: 'Adventure',
  },
  {
    id: '34',
    name: 'Animals',
  },
  {
    id: '12',
    name: 'Beach',
  },
  {
    id: '24',
    name: 'Beer',
  },
  {
    id: '6',
    name: 'Books',
  },
  {
    id: '11',
    name: 'Camping',
  },
  {
    id: '27',
    name: 'Coffee',
  },
  {
    id: '21',
    name: 'Concert',
  },
  {
    id: '14',
    name: 'Cook',
  },
  {
    id: '19',
    name: 'Dancing',
  },
  {
    id: '9',
    name: 'Fishing',
  },
  {
    id: '26',
    name: 'Food',
  },
  {
    id: '20',
    name: 'Football',
  },

  {
    id: '39',
    name: 'Gym',
  },
  {
    id: '7',
    name: 'Hiking',
  },
  {
    id: '10',
    name: 'Hunting',
  },
  {
    id: '31',
    name: 'Jokes',
  },
  {
    id: '32',
    name: 'Lough',
  },
  {
    id: '17',
    name: 'Movies',
  },
  {
    id: '5',
    name: 'Music',
  },
  {
    id: '16',
    name: 'Netflix',
  },
  {
    id: '2',
    name: 'Outdoors',
  },
];
const SwipeProfile = ({user, activeIndex}: any) => {
  const newProfile = user?.[activeIndex];
  const {userState} = useAuthProvider();
  const [selectedInterest, setSelectedInterest] = React.useState<
    InterestProp[]
  >([]);

  const handleSelectInterest = React.useCallback(
    (selectedItem: InterestProp) => {
      const isInterestSelected = selectedInterest.find(
        (list) => list === selectedItem,
      );
      if (isInterestSelected)
        setSelectedInterest(
          selectedInterest.filter((item) => item.id !== selectedItem.id),
        );
      else {
        let newSelectedInterest = [...selectedInterest];
        newSelectedInterest.push(selectedItem);
        setSelectedInterest(newSelectedInterest);
      }
    },
    [setSelectedInterest, selectedInterest],
  );

  return (
    <>
      <View style={styles.container}>
        <View style={styles.detailsContainer}>
          <Text style={styles.mainText}>{newProfile?.fullName}</Text>
          <View style={styles.basicProfileContainer}>
            <Text style={styles.subText}> {newProfile?.gender ?? 'Male'}</Text>
            <Image
              source={require('../../../../assets/Swipe_Profile_Bar.png')}
              style={styles.bar}
            />
            <Text style={styles.subText}>26</Text>
            <Image
              source={require('../../../../assets/Swipe_Profile_Bar.png')}
              style={styles.bar}
            />
            <Text style={styles.subText}> {newProfile?.height ?? '170cm'}</Text>
          </View>
          <View style={styles.locationContainer}>
            <EvilIcons color={Colors.gray['400']} size={20} name="location" />
            <View style={styles.locationTextContainer}>
              <Text style={[styles.subText]}>East Legon, Accra</Text>
              <Text style={[styles.subText]}>23km away</Text>
            </View>
          </View>
          <Text style={styles.mainText}>Interests</Text>
          <View style={styles.interestContainer}>
            <FlatList
              showsHorizontalScrollIndicator={false}
              horizontal
              data={InterestList}
              keyExtractor={(item) => item.id}
              renderItem={({item}) => {
                // common interests between user and logged in user
                const commonInterests = newProfile?.interests?.filter(
                  (item: string) =>
                    userState?.userData?.interests?.indexOf(item) !== -1,
                );
                const isInterestSelected = commonInterests?.find(
                  (list: any) => list === item.name,
                );
                return (
                  <Interests
                    handleSelectInterest={() => handleSelectInterest(item)}
                    isInterestSelected={isInterestSelected}
                    interest={item}
                    isButtonDisabled
                  />
                );
              }}
              contentContainerStyle={styles.interestListContainer}
            />
          </View>
          <View style={styles.aboutContainer}>
            <Text
              style={[
                styles.mainText,
                {paddingHorizontal: 0, paddingBottom: 8},
              ]}
            >
              About
            </Text>
            <Text style={styles.subText}>{newProfile?.aboutMe}</Text>
          </View>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 10,
    paddingBottom: 10,
    backgroundColor: Colors.white,
    display: 'flex',
    justifyContent: 'space-between',
    marginTop: -20,
  },
  detailsContainer: {
    height: '90%',
  },
  mainText: {
    fontWeight: 'bold',
    fontSize: 16,
    paddingHorizontal: 21,
    paddingTop: 14,
    lineHeight: 20,
  },
  basicProfileContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 14,
    paddingHorizontal: 21,
  },
  subText: {
    fontSize: 14,
    fontWeight: '400',
    lineHeight: 22,
  },
  bar: {
    height: 15,
    width: 10,
    resizeMode: 'contain',
    marginHorizontal: 10,
  },

  locationContainer: {
    display: 'flex',
    flexDirection: 'row',
    paddingHorizontal: 16,
  },
  locationTextContainer: {
    marginLeft: 8,
    alignItems: 'flex-start',
    marginBottom: 8,
    marginTop: -8,
  },
  aboutContainer: {
    marginVertical: 10,
    paddingHorizontal: 21,
  },
  interestContainer: {
    paddingTop: 20,
  },
  interestListContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: '95%',
    paddingLeft: 20,
  },
});

export {SwipeProfile};
