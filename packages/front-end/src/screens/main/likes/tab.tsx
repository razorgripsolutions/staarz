import * as React from 'react';
import {Image, SafeAreaView, StyleSheet, View, Text} from 'react-native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {LikedMeComponent} from './components/LikedMe';
import {LikedComponent} from './components/Liked';
import {LikesTopNavigatorStack} from './types';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {Colors} from '../../../constants/colors';
import {Searchbar} from 'react-native-paper';
import {FontStyles} from '../../../constants/font';
import {useAuthProvider} from '../../../../App';
import {ProfilePlaceHolder} from '../settings/tab';
import Entypo from 'react-native-vector-icons/Entypo';

const Tab = createMaterialTopTabNavigator<LikesTopNavigatorStack>();

function LikesTabComponent() {
  const {userState} = useAuthProvider();
  const [searchQuery, setSearchQuery] = React.useState('');

  const handleSearch = React.useCallback(
    (query: string) => {
      setSearchQuery(query);
    },
    [setSearchQuery],
  );
  return (
    <>
      <SafeAreaView style={styles.container}>
        <View style={styles.pageHeaderContainer}>
          <Entypo name="chevron-thin-left" size={wp(3.5)} />
          <View style={styles.logoContainer}>
            <Image
              source={require('../../../assets/starzlogo.png')}
              style={styles.logo}
            />
            <Text
              style={{
                fontSize: wp(6),
                fontFamily: 'MPLUS1p-Bold',
                color: '#243443',
                paddingLeft: 7,
                lineHeight: 33,
                textAlign: 'center',
                fontWeight: '900',
              }}
            >
              STAARY
            </Text>
          </View>
          <Image
            style={styles.avatar}
            source={{
              uri: userState?.userData?.profilePicture ?? ProfilePlaceHolder,
            }}
          />
        </View>
        <Searchbar
          placeholder="Search..."
          onChangeText={handleSearch}
          value={searchQuery}
          style={styles.searchBarStyle}
          inputStyle={styles.searchBarInputStyle}
        />

        <Tab.Navigator
          screenOptions={{
            tabBarStyle: styles.tabBarMainStyle,
            tabBarActiveTintColor: Colors.tintColor,
            tabBarInactiveTintColor: Colors.gray['500'],
            tabBarIndicatorStyle: styles.tabBarIndicator,
            tabBarLabelStyle: styles.tabBarLabel,
            tabBarItemStyle: styles.tabBarItem,
            tabBarIndicatorContainerStyle: styles.tabBarContainer,
            lazy: true,
          }}
        >
          <Tab.Screen
            name="liked"
            component={LikedComponent}
            options={{
              tabBarLabel: 'Liked',
            }}
          />
          <Tab.Screen
            name="likedMe"
            component={LikedMeComponent}
            options={{
              tabBarLabel: 'Liked by me',
            }}
          />
        </Tab.Navigator>
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    flex: 1,
  },
  pageHeaderContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    display: 'flex',
    alignItems: 'center',
    paddingHorizontal: wp(3),
  },
  logoContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    width: wp(7.5),
    height: wp(6.5),
    resizeMode: 'cover',
  },
  logoText: {
    marginLeft: wp(3),
    resizeMode: 'contain',
  },
  avatar: {
    width: wp(9),
    height: wp(9),
    resizeMode: 'cover',
    borderRadius: 25,
  },
  searchBarStyle: {
    marginHorizontal: wp(4),
    marginTop: wp(4),
    marginBottom: wp(2),
    shadowColor: 'none',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0,
    shadowRadius: 0,
    elevation: 0,
    backgroundColor: Colors.searchTextInputBackground,
    height: wp(9),
    borderRadius: 7,
  },
  searchBarInputStyle: {
    fontSize: 13,
    marginLeft: -11,
    fontFamily: FontStyles.bold,
  },
  tabBarMainStyle: {
    backgroundColor: Colors.white,
    marginLeft: -5,
  },
  tabBarIndicator: {
    backgroundColor: Colors.white,
    shadowColor: 'none',
    borderBottomWidth: 0,
  },
  tabBarLabel: {
    fontSize: 12,
    textTransform: 'capitalize',
    marginBottom: -3,
    fontFamily: FontStyles.bold,
  },
  tabBarItem: {
    width: 98,
  },
  tabBarContainer: {
    width: '20%',
    marginLeft: 20,
  },
});
export {LikesTabComponent};
