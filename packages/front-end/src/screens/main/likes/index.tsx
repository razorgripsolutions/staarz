import * as React from 'react';
import {LikesNavigatorStack} from './types';
import {createStackNavigator} from '@react-navigation/stack';
import {LikesTabComponent} from './tab';

const Stack = createStackNavigator<LikesNavigatorStack>();

function LikesComponent() {
  return (
    <Stack.Navigator initialRouteName="likes">
      <Stack.Screen
        name="likes"
        component={LikesTabComponent}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
}

export default LikesComponent;
