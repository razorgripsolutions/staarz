export interface ListemComponentProp {
  onPress: () => void;
  imageUrl: string;
  displayName: string;
}

export type ListItemProp = {
  imageUrl: string;
  displayName: string;
  id: string;
};
