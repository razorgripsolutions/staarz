import * as React from 'react';
import {
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  Dimensions,
  View,
} from 'react-native';
import {ListemComponentProp} from './types';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {Colors} from '../../../../constants/colors';
import {FontStyles} from '../../../../constants/font';

const {width} = Dimensions.get('window');

function ListItem({onPress, imageUrl, displayName}: ListemComponentProp) {
  return (
    <>
      <TouchableOpacity
        onPress={onPress}
        activeOpacity={0.9}
        style={styles.container}
      >
        <ImageBackground
          source={{
            uri: imageUrl,
          }}
          imageStyle={styles.imageStyle}
          resizeMode="cover"
        >
          <View style={styles.overlayView} />
          <View style={styles.textContainer}>
            <Text style={styles.text}>{displayName}</Text>
          </View>
        </ImageBackground>
      </TouchableOpacity>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    width: width / 2 - 25,
    height: width / 2 - 25,
    marginLeft: 10,
    marginTop: 10,
    borderRadius: 5,
  },
  overlayView: {
    width: width / 2 - 25,
    height: width / 2 - 25,
    position: 'absolute',
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
    borderRadius: 5,
  },
  imageStyle: {
    width: width / 2 - 25,
    height: width / 2 - 25,
    borderRadius: 5,
  },
  textContainer: {
    height: '100%',
    alignItems: 'flex-end',
    flexDirection: 'row',
    padding: wp(2),
  },
  text: {
    fontWeight: '700',
    color: Colors.white,
    fontSize: 12,
    fontFamily: FontStyles.semiBold,
  },
});

export {ListItem};
