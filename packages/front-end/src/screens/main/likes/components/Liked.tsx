import * as React from 'react';
import {Dimensions, FlatList, StyleSheet, View} from 'react-native';
import {Colors} from '../../../../constants/colors';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {ListItem} from '../shared';
import {AxiosError, AxiosResponse} from 'axios';
import {LikesResponse, useGetLikes} from '../../../../api/hooks/likes';
import {useAuthProvider} from '../../../../../App';
import {CircleFade} from 'react-native-animated-spinkit';
import {EmptyState} from '../../../../components/alerts';
import {handlePickRandomImages} from '../../../../components/utils/random-images';
import {imagesData} from '../../../../components/data/images';

const {height} = Dimensions.get('window');

function LikedComponent() {
  const {userState} = useAuthProvider();
  const {mutateAsync, isLoading, data} = useGetLikes();
  const [users, setUsers] = React.useState<LikesResponse[]>([]);

  const handleGetUsers = React.useCallback(() => {
    mutateAsync({
      userId: userState?.userData?.id ?? 0,
    })
      .then(async (response: AxiosResponse<LikesResponse[]>) => {
        const {data: results} = response;
        const images = handlePickRandomImages(results?.length, imagesData);
        let newLists: LikesResponse[] = [];
        results.forEach((item, index) => {
          const itemImage = images?.[index];
          const newImage = {
            ...item,
            profilePicture: item?.profilePicture ?? itemImage,
          };
          newLists.push(newImage);
        });
        setUsers(newLists);
      })
      .catch((e: AxiosError<unknown>) => {});
  }, [setUsers, handlePickRandomImages, userState]);

  React.useEffect(() => {
    if (userState?.userData) {
      handleGetUsers();
    }
  }, [userState]);

  return (
    <View style={styles.container}>
      {isLoading ? (
        <CircleFade
          size={70}
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            top: height * 0.2,
            alignSelf: 'center',
          }}
          color="#5E2EBA"
        />
      ) : (
        <>
          {data && users.length === 0 ? (
            <EmptyState model="record" />
          ) : (
            <FlatList
              data={users}
              showsVerticalScrollIndicator={false}
              keyExtractor={(item) => item.id.toString()}
              numColumns={2}
              renderItem={({item}) => (
                <View style={styles.scrollContainer}>
                  <ListItem
                    onPress={() => null}
                    imageUrl={item.profilePicture ?? ''}
                    displayName={item?.fullName}
                  />
                </View>
              )}
            />
          )}
        </>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    flex: 1,
  },
  scrollContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingHorizontal: wp(1.8),
  },
});
export {LikedComponent};
