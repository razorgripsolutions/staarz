import * as React from 'react';
import {Image, SafeAreaView, StyleSheet, View} from 'react-native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {PackagesComponent} from './components/Packages';
import {SettingsTopNavigatorStack} from './types';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {Colors} from '../../../constants/colors';
import {FontStyles} from '../../../constants/font';
import {Text} from 'react-native-paper';
import {useAuthProvider} from '../../../../App';
import ProfileComponent from './components/Profile';

const Tab = createMaterialTopTabNavigator<SettingsTopNavigatorStack>();
export const ProfilePlaceHolder =
  'https://e7.pngegg.com/pngimages/1000/665/png-clipart-computer-icons-profile-s-free-angle-sphere.png';

function SettingsTabComponent() {
  const {userState} = useAuthProvider();
  return (
    <>
      <SafeAreaView style={styles.container}>
        <View style={styles.pageHeaderContainer}>
          <View />
          <View style={styles.textContainer}>
            <Text style={styles.text}>Settings</Text>
          </View>
          <Image
            style={styles.avatar}
            source={{
              uri: userState?.userData?.profilePicture ?? ProfilePlaceHolder,
            }}
          />
        </View>

        <Tab.Navigator
          screenOptions={{
            tabBarStyle: styles.tabBarMainStyle,
            tabBarActiveTintColor: Colors.tintColor,
            tabBarInactiveTintColor: Colors.gray['500'],
            tabBarIndicatorStyle: styles.tabBarIndicator,
            tabBarLabelStyle: styles.tabBarLabel,
            tabBarItemStyle: styles.tabBarItem,
            tabBarIndicatorContainerStyle: styles.tabBarContainer,
            lazy: true,
          }}
        >
          <Tab.Screen
            name="profile"
            component={ProfileComponent}
            options={{
              tabBarLabel: 'Profile',
            }}
          />
          <Tab.Screen
            name="packages"
            component={PackagesComponent}
            options={{
              tabBarLabel: 'Packages',
            }}
          />
        </Tab.Navigator>
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    flex: 1,
  },
  pageHeaderContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    display: 'flex',
    alignItems: 'center',
    paddingHorizontal: wp(3),
  },
  textContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontWeight: '700',
    fontSize: 15,
  },
  avatar: {
    width: wp(9),
    height: wp(9),
    resizeMode: 'cover',
    borderRadius: 25,
  },
  tabBarMainStyle: {
    backgroundColor: Colors.white,
    marginLeft: -8,
  },
  tabBarIndicator: {
    backgroundColor: Colors.white,
    shadowColor: 'none',
    borderBottomWidth: 0,
  },
  tabBarLabel: {
    fontSize: 12,
    textTransform: 'capitalize',
    marginBottom: -3,
    fontFamily: FontStyles.bold,
  },
  tabBarItem: {
    width: 105,
  },
  tabBarContainer: {
    width: '20%',
    marginLeft: 20,
  },
});
export {SettingsTabComponent};
