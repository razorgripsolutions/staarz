import * as React from 'react';
import {
  Alert,
  Dimensions,
  FlatList,
  Image,
  Platform,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import {Colors} from '../../../../constants/colors';
import {BlockedUser, BlockListItem} from './block-list';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {useQuery} from 'react-query';
import {AxiosResponse} from 'axios';
import {get} from '../../../../api/helpers';
import {API_ENDPOINTS} from '../../../../api/routes';
import {CircleFade} from 'react-native-animated-spinkit';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {useUnblockUser} from '../../../../api/hooks/chat';
import Entypo from 'react-native-vector-icons/Entypo';
import Toast from 'react-native-toast-message';

const {height} = Dimensions.get('screen');

const BlockedUsers = ({navigation}: any) => {
  const {data, isLoading, refetch} = useQuery<
    any,
    any,
    AxiosResponse<BlockedUser[]>
  >('blocked-users', () => get(API_ENDPOINTS.getBlockedUsers));

  const {mutateAsync, isLoading: isUnblockLoading} = useUnblockUser();

  const handleUnblockUser = React.useCallback(
    async (selectedUser) => {
      try {
        const response = await mutateAsync({
          userId: selectedUser?.id,
        });
        refetch();
        Toast.show({
          type: 'success',
          text1: 'Success',
          text2: `${selectedUser?.fullName} unblocked successfully`,
        });
      } catch (error) {
        return Toast.show({
          type: 'error',
          text1: 'Unblock error',
          text2: 'An error occured',
        });
      }
    },
    [mutateAsync, refetch],
  );

  const handleShowUnblockAlert = React.useCallback(
    (user: BlockedUser) => {
      return Alert.alert(
        `Unblock ${user?.fullName}`,
        'Are you sure you want to unblock',
        [
          {
            text: 'No',
            onPress: () => {},
            style: 'cancel',
          },
          {
            text: 'Yes',
            onPress: () => handleUnblockUser(user),
          },
        ],
      );
    },
    [Alert, handleUnblockUser],
  );

  return (
    <React.Fragment>
      <View style={styles.pageHeaderContainer}>
        <TouchableOpacity
          activeOpacity={0.9}
          onPress={() => navigation.goBack()}
        >
          <Entypo name="chevron-thin-left" size={wp(3.5)} />
        </TouchableOpacity>
        <View style={styles.logoContainer}>
          <Image
            source={require('../../../../assets/starzlogo.png')}
            style={styles.logo}
          />
          <Text
            style={{
              fontSize: wp(6),
              fontFamily: 'MPLUS1p-Black',
              color: '#243443',
              paddingLeft: 7,
              lineHeight: 33,
              textAlign: 'center',
              fontWeight: '900',
            }}
          >
            STAARY
          </Text>
        </View>
        <View />
      </View>
      <View style={styles.container}>
        {isLoading ? (
          <CircleFade
            size={70}
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              top: height * 0.2,
              alignSelf: 'center',
            }}
            color="#5E2EBA"
          />
        ) : (
          <>
            {data?.data?.length === 0 ? (
              <View style={styles.innerContainer}>
                <Text style={styles.noMessagesText}>
                  No blocked users found
                </Text>
                <View style={styles.referesButtonOuterContainer}>
                  <TouchableOpacity
                    activeOpacity={0.9}
                    style={styles.refereshButtonContainer}
                  >
                    <View style={styles.refereshButtonInnerContainer}>
                      {Platform.OS === 'ios' ? (
                        <Text style={styles.refereshText}>Refresh</Text>
                      ) : (
                        <Text style={styles.refereshText}>Refresh</Text>
                      )}
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            ) : (
              <SafeAreaView style={{flex: 1}}>
                <FlatList
                  style={{paddingTop: RFValue(10)}}
                  data={data?.data}
                  renderItem={({item}) => (
                    <BlockListItem
                      data={item}
                      handleUnblock={() => handleShowUnblockAlert(item)}
                      isUnblockLoading={isUnblockLoading}
                    />
                  )}
                  keyExtractor={(item) => item.id.toString()}
                />
              </SafeAreaView>
            )}
          </>
        )}
      </View>
    </React.Fragment>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    position: 'relative',
  },
  innerContainer: {
    flex: 0.3,
    marginTop: height * 0.2,
  },
  noMessagesText: {
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: hp('3'),
    fontWeight: '900',
    textAlign: 'center',
    color: 'black',
  },

  referesButtonOuterContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  refereshButtonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  refereshButtonInnerContainer: {
    backgroundColor: '#7643F8',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    paddingHorizontal: 30,
    paddingVertical: 20,
    marginTop: 16,
  },
  refereshText: {
    fontSize: hp('2'),
    color: '#fff',
  },
  pageHeaderContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    display: 'flex',
    alignItems: 'center',
    paddingHorizontal: wp(3),
    backgroundColor: Colors.white,
  },
  logoContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: -23,
  },
  logo: {
    width: wp(7.5),
    height: wp(6.5),
    resizeMode: 'cover',
  },
  logoText: {
    marginLeft: wp(3),
    resizeMode: 'contain',
  },
});

export {BlockedUsers};
