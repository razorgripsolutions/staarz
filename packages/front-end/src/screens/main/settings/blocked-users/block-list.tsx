import React, {FC} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableHighlight,
  Image,
  TouchableOpacity,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import {Colors} from '../../../../constants/colors';
import _ from 'lodash';

export type BlockedUser = {
  emailAddress: string;
  id: number;
  profilePicture: string;
  fullName: string;
  gender: string;
};

interface Props {
  handleUnblock: () => void;
  data: BlockedUser;
  isUnblockLoading: boolean;
}

const BlockListItem: FC<Props> = ({handleUnblock, data, isUnblockLoading}) => {
  return (
    <>
      <View>
        <View style={styles.listItemContainer}>
          <View style={{marginRight: RFValue(15)}}>
            <Image
              source={{
                uri:
                  data?.profilePicture ??
                  'https://images.unsplash.com/photo-1519699047748-de8e457a634e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8OHx8ZmFjZXxlbnwwfDJ8MHx8&auto=format&fit=crop&w=500&q=60',
              }}
              style={{
                width: RFValue(45),
                height: RFValue(45),
                borderRadius: RFValue(25),
              }}
            />
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          >
            <View
              style={{
                paddingRight: RFValue(0),
              }}
            >
              <View>
                <Text
                  style={{fontSize: RFValue(13), color: Colors.gray['900']}}
                >
                  {_.truncate(`${data?.fullName}`, {
                    length: 20,
                  })}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  marginTop: RFValue(4),
                }}
              >
                <View style={{width: '78%'}}>
                  <Text
                    style={{fontSize: RFValue(11), color: Colors.gray['600']}}
                  >
                    {_.truncate(`${data?.emailAddress}`, {
                      length: 55,
                    })}
                  </Text>
                </View>
              </View>
            </View>
            <TouchableOpacity
              activeOpacity={0.9}
              onPress={handleUnblock}
              disabled={isUnblockLoading}
              style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: Colors.gray['900'],
                padding: RFValue(8),
                borderRadius: RFValue(15),
              }}
            >
              <Text style={{color: Colors.gray['100'], fontSize: 10}}>
                Unblock
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {},
  rowBetween: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  centerRow: {
    alignItems: 'center',
  },
  listItemContainer: {
    paddingVertical: RFValue(10),
    paddingHorizontal: RFValue(10),
    borderBottomWidth: 1,
    borderBottomColor: Colors.gray['100'],
    height: RFValue(70),
    flexDirection: 'row',
    alignItems: 'center',
  },
  rowContainer: {
    flexDirection: 'row',
    flex: 1,
  },
  indicatorView: {
    justifyContent: 'center',
    alignItems: 'center',
    height: RFValue(20),
    width: RFValue(20),
    backgroundColor: Colors.gray['900'],
    borderRadius: RFValue(10),
    position: 'absolute',
    top: 22,
    right: 10,
    display: 'flex',
  },
});

export {BlockListItem};
