import * as React from 'react';
import {SettingsNavigatorStack} from './types';
import {createStackNavigator} from '@react-navigation/stack';
import {SettingsTabComponent} from './tab';
import {PhotoGrid} from './components/Photogrid';

const Stack = createStackNavigator<SettingsNavigatorStack>();

const defaultHeaderOptions = {
  headerShown: false,
};
function SettingsComponent() {
  return (
    <Stack.Navigator initialRouteName="settings">
      <Stack.Screen
        name="settings"
        component={SettingsTabComponent}
        options={{...defaultHeaderOptions}}
      />
      <Stack.Screen
        name="photoGrid"
        component={PhotoGrid}
        options={{...defaultHeaderOptions}}
      />
    </Stack.Navigator>
  );
}

export default SettingsComponent;
