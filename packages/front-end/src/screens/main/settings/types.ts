export type SettingsTopNavigatorStack = {
  profile: undefined;
  packages: undefined;
};

export type SettingsNavigatorStack = {
  settings: undefined;
  photoGrid: undefined;
};

export namespace Settings_Packages {
  export type PackagesCardComponentProp = {
    package: PackageList;
  };
  export type PackageList = {
    type: 'plus' | 'gold' | 'platinum';
    amount: string;
    features: PackageFeature[];
    name: string;
  };
  export type PackageFeature = {
    name: string;
  };
  export type PhotoGridCardComponnentProp = {
    image: string;
    removeImage: () => void;
  };
}
