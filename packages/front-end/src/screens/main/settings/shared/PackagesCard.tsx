import * as React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {Text} from 'react-native-paper';
import {Colors} from '../../../../constants/colors';
import {Settings_Packages} from '../types';

function PackagesCard({
  package: listItem,
}: Settings_Packages.PackagesCardComponentProp) {
  const color =
    listItem.type === 'plus'
      ? Colors.packages.textGray
      : listItem.type === 'gold'
      ? Colors.packages.goldLight
      : Colors.packages.platinumLight;
  return (
    <View style={styles.container}>
      <View style={styles.priceOuterContainer}>
        <View style={styles.priceInnerContainer}>
          <Text
            style={[
              styles.packageName,
              {
                color: color,
              },
            ]}
          >
            {listItem.name}
          </Text>
          <View style={styles.packageRateContainer}>
            <Text style={styles.packageRateAmount}>₪{listItem.amount}</Text>
            <Text style={styles.packageRateOccurence}>/Month</Text>
          </View>
        </View>
      </View>
      <View style={styles.packageListOuterContainer}>
        {listItem.features.map((item) => (
          <View key={item.name} style={styles.packageListInnerContainer}>
            <Text
              style={[
                styles.packageListBullet,
                {
                  color,
                },
              ]}
            >
              {'\u2022'}
            </Text>
            <Text style={styles.packageListText}>{item.name}</Text>
          </View>
        ))}
      </View>
      <TouchableOpacity style={styles.buttonContainer}>
        <Text style={styles.buttonText}>Buy Package</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    padding: 16,
    width: 290,
    borderRadius: 15,
    shadowOpacity: 0.1,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowRadius: 3,
    elevation: 3,
    margin: 10,
  },
  priceOuterContainer: {
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: Colors.packages.stroke,
  },
  priceInnerContainer: {
    padding: 16,
  },
  packageName: {
    fontWeight: 'bold',
  },
  packageRateContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 10,
  },
  packageRateAmount: {
    fontWeight: 'bold',
    fontSize: 29,
    lineHeight: 28,
  },
  packageRateOccurence: {
    fontSize: 11,
    lineHeight: 39,
  },
  packageListOuterContainer: {
    marginVertical: 16,
    paddingHorizontal: 16,
  },
  packageListInnerContainer: {
    paddingVertical: 8,
    flexDirection: 'row',
  },
  packageListText: {
    color: Colors.packages.textGray,
    fontSize: 14,
    flex: 1,
    paddingLeft: 5,
    marginTop: -10,
  },
  packageListBullet: {
    color: Colors.packages.packageListBullet,
    fontSize: 35,
    marginTop: -25,
  },
  buttonContainer: {
    marginHorizontal: 16,
    marginBottom: 16,
    backgroundColor: Colors.packages.buttonBackground,
    height: 45,
    width: '60%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  buttonText: {
    color: Colors.packages.buttonText,
    fontSize: 14,
  },
});

export {PackagesCard};
