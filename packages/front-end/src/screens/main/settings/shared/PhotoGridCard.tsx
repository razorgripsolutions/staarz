import * as React from 'react';
import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import {Settings_Packages} from '../types';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {Colors} from '../../../../constants/colors';

function ImageGridCard({
  image: url,
  removeImage,
}: Settings_Packages.PhotoGridCardComponnentProp) {
  return (
    <React.Fragment>
      <View style={styles.container}>
        <Image
          source={{
            uri: url,
          }}
          style={styles.image}
          resizeMode="contain"
        />
        <TouchableOpacity
          onPress={removeImage}
          style={styles.deleteContainer}
          activeOpacity={0.9}
        >
          <FontAwesome5 name="trash" color={'red'} size={13} />
        </TouchableOpacity>
      </View>
    </React.Fragment>
  );
}

const styles = StyleSheet.create({
  container: {
    margin: 10,
    position: 'relative',
  },
  image: {
    height: wp(26),
    width: wp(26),
    borderRadius: 8,
  },
  deleteContainer: {
    height: 28,
    width: 28,
    borderRadius: 20,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.profile.addImageGray,
    position: 'absolute',
    top: -10,
    right: -10,
  },
});

export {ImageGridCard};
