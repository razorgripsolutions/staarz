import * as React from 'react';
import {ScrollView, FlatList, StyleSheet, View} from 'react-native';
import {Colors} from '../../../../constants/colors';
import {PackagesCard} from '../shared';
import {Settings_Packages} from '../types';

const datalist: Settings_Packages.PackageList[] = [
  {
    type: 'plus',
    name: 'Plus',
    amount: '25',
    features: [
      {
        name: 'Unlimited likes',
      },
      {
        name: 'Rewind',
      },
      {
        name: '5 Super likes a day',
      },
      {
        name: 'Passport',
      },
      {
        name: 'No ads',
      },
    ],
  },
  {
    type: 'gold',
    name: 'Gold',
    amount: '25',
    features: [
      {
        name: 'Unlimited likes',
      },
      {
        name: 'Rewind',
      },
      {
        name: '5 Super likes a day',
      },
      {
        name: 'Passport',
      },
      {
        name: 'No ads',
      },
    ],
  },
  {
    type: 'platinum',
    name: 'Platinum',
    amount: '25',
    features: [
      {
        name: 'Unlimited likes',
      },
      {
        name: 'Rewind',
      },
      {
        name: '5 Super likes a day',
      },
      {
        name: 'Passport',
      },
      {
        name: 'No ads',
      },
    ],
  },
];

function PackagesComponent() {
  const [ageRangeSliderValue, setAgeRangeSliderValue] = React.useState([
    18, 38,
  ]);
  const [distanceSliderValue, setDistanceSliderValue] = React.useState([
    50, 500,
  ]);
  const [isDistanceEnabled, setIsDistanceEnabled] = React.useState(false);
  const [isAllowSmokersEnabled, setIsAllowSmokersEnabled] =
    React.useState(false);

  const handleToggleDistanceSwitch = React.useCallback(
    () => setIsDistanceEnabled((previousState) => !previousState),
    [setIsDistanceEnabled],
  );

  const handleToggleAllowSmokersSwitch = React.useCallback(
    () => setIsAllowSmokersEnabled((previousState) => !previousState),
    [setIsAllowSmokersEnabled],
  );
  const handleAgeRanderSliderValuesChange = React.useCallback(
    (values: React.SetStateAction<number[]>) => setAgeRangeSliderValue(values),
    [setAgeRangeSliderValue],
  );

  const handleDistanceSliderValuesChange = React.useCallback(
    (values: React.SetStateAction<number[]>) => setDistanceSliderValue(values),
    [setDistanceSliderValue],
  );

  return (
    <ScrollView style={styles.container}>
      <View
        style={{
          flex: 0.8,
        }}
      >
        <FlatList
          horizontal
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item) => item.name}
          data={datalist}
          renderItem={({item}) => <PackagesCard package={item} />}
        />
      </View>
      {/* <View style={styles.controlsContainer}>
        <View style={styles.labelContainer}>
          <Text style={styles.mainLabel}>Age range</Text>
          <Text style={styles.smallLabel}>18-50</Text>
        </View>
        <View
          style={[
            styles.sliderContainer,
            {
              paddingVertical: 4,
            },
          ]}
        >
          <MultiSlider
            values={[ageRangeSliderValue[0], ageRangeSliderValue[1]]}
            sliderLength={width / 1.14}
            onValuesChange={handleAgeRanderSliderValuesChange}
            min={18}
            max={50}
            step={1}
            allowOverlap
            snapped
            customLabel={CustomLabel as any}
            unselectedStyle={styles.unselectedSliderStyle}
            selectedStyle={styles.selectedSliderStyle}
            markerStyle={styles.sliderMarkerStyle}
          />
        </View>
        <View style={styles.labelContainer}>
          <Text style={styles.mainLabel}>Distance</Text>
          <Text style={styles.smallLabel}>50KM</Text>
        </View>
        <View style={[styles.labelContainer, {paddingTop: 5}]}>
          <Text style={styles.smallLabel}>Millimeters</Text>
          <Switch
            trackColor={{false: '#ECF3FB', true: Colors.primary}}
            thumbColor={isDistanceEnabled ? Colors.white : Colors.white}
            // ios_backgroundColor="#ECF3FB"
            onValueChange={handleToggleDistanceSwitch}
            value={isDistanceEnabled}
            style={{transform: [{scaleX: 0.6}, {scaleY: 0.6}]}}
          />
        </View>
        <View style={styles.sliderContainer}>
          <MultiSlider
            values={[distanceSliderValue[0], distanceSliderValue[1]]}
            sliderLength={width / 1.14}
            onValuesChange={handleDistanceSliderValuesChange}
            min={50}
            max={1000}
            step={1}
            allowOverlap
            snapped
            customLabel={CustomLabel as any}
            unselectedStyle={styles.unselectedSliderStyle}
            selectedStyle={styles.selectedSliderStyle}
            markerStyle={styles.sliderMarkerStyle}
          />
        </View>
        <View style={[styles.labelContainer, {paddingTop: 5}]}>
          <Text style={styles.mainLabel}>Allow smokers</Text>
          <Switch
            trackColor={{false: '#ECF3FB', true: Colors.primary}}
            thumbColor={isAllowSmokersEnabled ? '#fff' : '#fff'}
            // ios_backgroundColor="#ECF3FB"
            onValueChange={handleToggleAllowSmokersSwitch}
            value={isAllowSmokersEnabled}
            style={{transform: [{scaleX: 0.6}, {scaleY: 0.6}]}}
          />
        </View>
        <Text
          style={[
            styles.mainLabel,
            {
              fontSize: 21,
              paddingVertical: 15,
              fontWeight: 'bold',
            },
          ]}
        >
          Notifications
        </Text>
        <View style={styles.labelContainer}>
          <Text style={styles.mainLabel}>New Match</Text>
          <Switch
            trackColor={{false: '#ECF3FB', true: Colors.primary}}
            thumbColor={isAllowSmokersEnabled ? '#fff' : '#fff'}
            // ios_backgroundColor="#ECF3FB"
            onValueChange={handleToggleAllowSmokersSwitch}
            value={isAllowSmokersEnabled}
            style={{transform: [{scaleX: 0.6}, {scaleY: 0.6}]}}
          />
        </View>
        <View style={[styles.labelContainer, {paddingTop: 8}]}>
          <Text style={styles.mainLabel}>New Message</Text>
          <Switch
            trackColor={{false: '#ECF3FB', true: Colors.primary}}
            thumbColor={isAllowSmokersEnabled ? '#fff' : '#fff'}
            // ios_backgroundColor="#ECF3FB"
            onValueChange={handleToggleAllowSmokersSwitch}
            value={isAllowSmokersEnabled}
            style={{transform: [{scaleX: 0.6}, {scaleY: 0.6}]}}
          />
        </View>
        <View style={[styles.labelContainer, {paddingTop: 8}]}>
          <Text style={styles.mainLabel}>Message like</Text>
          <Switch
            trackColor={{false: '#ECF3FB', true: Colors.primary}}
            thumbColor={isAllowSmokersEnabled ? '#fff' : '#fff'}
            // ios_backgroundColor="#ECF3FB"
            onValueChange={handleToggleAllowSmokersSwitch}
            value={isAllowSmokersEnabled}
            style={{transform: [{scaleX: 0.6}, {scaleY: 0.6}]}}
          />
        </View>
        <View style={[styles.labelContainer, {paddingTop: 8}]}>
          <Text style={styles.mainLabel}>Super like</Text>
          <Switch
            trackColor={{false: '#ECF3FB', true: Colors.primary}}
            thumbColor={isAllowSmokersEnabled ? '#fff' : '#fff'}
            // ios_backgroundColor="#ECF3FB"
            onValueChange={handleToggleAllowSmokersSwitch}
            value={isAllowSmokersEnabled}
            style={{transform: [{scaleX: 0.6}, {scaleY: 0.6}]}}
          />
        </View>
      </View> */}
    </ScrollView>
  );
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    paddingHorizontal: 5,
    paddingVertical: 25,
    flex: 1,
  },
  controlsContainer: {
    padding: 15,
    paddingBottom: 30,
  },
  labelContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  mainLabel: {
    fontWeight: '600',
    color: Colors.tintColor,
    fontSize: 14,
  },
  smallLabel: {
    fontWeight: '400',
    color: Colors.lightTintColor,
    fontSize: 14,
  },
  sliderContainer: {
    paddingHorizontal: 10,
  },
  selectedSliderStyle: {
    backgroundColor: Colors.primary,
    height: 5,
  },
  unselectedSliderStyle: {
    backgroundColor: Colors.sliderUnselected,
    height: 5,
  },
  sliderMarkerStyle: {
    backgroundColor: Colors.sliderUnselected,
    height: 23,
    width: 23,
  },
});
export {PackagesComponent};
