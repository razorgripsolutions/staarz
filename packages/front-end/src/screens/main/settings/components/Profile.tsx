import React from 'react';
import {
  Dimensions,
  Image,
  View,
  StatusBar,
  TouchableOpacity,
  ImageBackground,
  Platform,
} from 'react-native';
import {Text} from 'react-native-paper';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useAuthProvider} from '../../../../../App';
import {ProfilePlaceHolder} from '../tab';

function Profile(props: any) {
  const windowHeight = Dimensions.get('window').height;
  const {navigation} = props;
  const {userState} = useAuthProvider();

  const handleGoToPhotoGrid = React.useCallback(() => {
    navigation.navigate('photoGrid');
  }, [navigation]);

  return (
    <SafeAreaView
      style={{
        flex: 1,
        paddingTop: Platform.OS === 'android' ? windowHeight / 8 : 0,
        backgroundColor: '#fff',
      }}
    >
      <View style={{flex: 1, alignItems: 'center'}}>
        <StatusBar barStyle="dark-content" backgroundColor="white" />
        <View>
          <Image
            source={{
              uri: userState?.userData?.profilePicture ?? ProfilePlaceHolder,
            }}
            style={{
              width: 100,
              height: 100,
              borderRadius: 50,
            }}
          />
        </View>
        <View style={{marginTop: 20, justifyContent: 'center'}}>
          <Text
            numberOfLines={1}
            style={{
              textAlign: 'center',
              color: '#000000',
              lineHeight: 27,
              fontSize: 19,
              fontWeight: '600',
            }}
          >
            {userState?.userData?.fullName}
          </Text>
          <Text
            style={{
              textAlign: 'center',
              fontSize: 16,
              color: '#8A98AC',
              lineHeight: 20,
              fontFamily: 'SFUIText-Semibold',
              paddingHorizontal: wp(22),
            }}
          >
            {userState?.userData?.occupation
              ? `Works as ${userState?.userData?.occupation}`
              : 'Manager at Walmart, Shenkar College'}
          </Text>

          <View
            style={{
              justifyContent: 'center',
              flexDirection: 'row',
              marginTop: 15,
            }}
          >
            <TouchableOpacity
              style={{padding: 10}}
              onPress={() => navigation.navigate('Setting_Profile')}
            >
              <View
                style={{
                  backgroundColor: 'white',
                  justifyContent: 'center',
                  alignItems: 'center',
                  height: 60,
                  width: 60,
                  borderRadius: 60 / 2,
                  shadowOpacity: 0.1,
                  shadowRadius: 5,
                  elevation: 3,
                  shadowOffset: {
                    width: 0,
                    height: 2,
                  },
                }}
              >
                <Image
                  source={require('../../../../assets/Vector.png')}
                  style={{
                    height: wp(7),
                    width: wp(7),
                    resizeMode: 'contain',
                  }}
                />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={handleGoToPhotoGrid}
              style={{padding: 10}}
            >
              <View
                style={{
                  backgroundColor: 'white',
                  justifyContent: 'center',
                  alignItems: 'center',
                  height: 60,
                  width: 60,
                  borderRadius: 50,
                  shadowOpacity: 0.1,
                  shadowRadius: 5,
                  elevation: 3,
                  shadowOffset: {
                    width: 0,
                    height: 2,
                  },
                }}
              >
                <ImageBackground
                  source={require('../../../../assets/Path.png')}
                  style={{
                    height: wp(6.2),
                    width: wp(7.1),
                    alignSelf: 'center',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                >
                  <Image
                    source={require('../../../../assets/Path1.png')}
                    style={{
                      height: wp(2.2),
                      width: wp(2.2),
                      resizeMode: 'contain',
                    }}
                  />
                </ImageBackground>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={{padding: 10}}
              onPress={() => navigation.navigate('EditProfileScreen')}
            >
              <View
                style={{
                  backgroundColor: 'white',
                  justifyContent: 'center',
                  alignItems: 'center',
                  height: 60,
                  width: 60,
                  borderRadius: 50,
                  shadowOpacity: 0.1,
                  shadowRadius: 5,
                  elevation: 3,
                  shadowOffset: {
                    width: 0,
                    height: 2,
                  },
                }}
              >
                <Image
                  source={require('../../../../assets/Icon.png')}
                  style={{
                    height: wp(6.3),
                    width: wp(6.3),
                    resizeMode: 'contain',
                  }}
                />
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
}

export default Profile;
