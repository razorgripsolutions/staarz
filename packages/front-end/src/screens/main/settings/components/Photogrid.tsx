import * as React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {ImageGridCard} from '../shared/PhotoGridCard';
import {Text} from 'react-native-paper';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {Colors} from '../../../../constants/colors';
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {getMessageFromCode} from '../../../uploadYourPicture';
import Toast from 'react-native-toast-message';
import * as ImagePicker from 'react-native-image-picker';
import ActionSheet from 'react-native-actionsheet';
import {useAuthProvider} from '../../../../../App';
import {ProfilePlaceHolder} from '../tab';
import {Asset} from 'react-native-image-picker';
import {LoadingModal} from '../../../../components/loading';

const data: string[] = [
  // 'https://images.unsplash.com/photo-1561828995-aa79a2db86dd?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mjh8fGZhc2hpb258ZW58MHwyfDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60',
  // 'https://images.unsplash.com/photo-1590159983013-d4ff5fc71c1d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MzN8fGZhc2hpb258ZW58MHwyfDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60',
  // 'https://images.unsplash.com/photo-1568166460861-fcaf9bb7c1a0?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mjl8fGZhc2hpb258ZW58MHwyfDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60',
  // 'https://images.unsplash.com/photo-1562273180-90004dbb33fb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MjB8fGZhc2hpb258ZW58MHwyfDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60',
  // 'https://images.unsplash.com/photo-1484186694682-a940e4b1a9f7?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTd8fGZhc2hpb258ZW58MHwyfDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60',
];

function PhotoGrid() {
  const navigation = useNavigation();
  const [images, setImages] = React.useState<string[]>(data);
  const [path, setPath] = React.useState<Asset[] | undefined>([]);
  const {userState} = useAuthProvider();

  const [gettingImages, setGettingImages] = React.useState(false);

  const ActionSheetRef = React.useRef<any>();

  const handleGoBack = React.useCallback(() => {
    navigation.goBack();
  }, [navigation]);

  const uploadImage = React.useCallback(() => {
    let imageList: string[] = [];
    path?.map((image: any) => {
      return imageList.push(image?.uri ?? '');
    });
    setImages(images.concat(imageList));
  }, [setImages, path]);

  const chooseImage = React.useCallback(
    (index: number) => {
      setGettingImages(true);
      if (index === 0) {
        const options: ImagePicker.ImageLibraryOptions = {
          mediaType: 'photo',
          includeBase64: true,
          selectionLimit: 5,
        };
        return ImagePicker.launchImageLibrary(options, async (response) => {
          if (response.errorCode) {
            Toast.show({
              text1: 'Application Error',
              text2: getMessageFromCode(response.errorCode),
              type: 'danger',
            });
          } else {
            console.log('responses image', response?.assets);
            setPath(response?.assets);
          }
        });
      } else {
        setGettingImages(false);
      }
    },
    [setPath, setGettingImages],
  );

  const removeImage = React.useCallback(
    (selected: string) => {
      const newImages = [...images];
      setImages(newImages.filter((item) => item !== selected));
    },
    [images, setImages],
  );

  React.useEffect(() => {
    if ((path?.length as number) > 1) {
      uploadImage();
      setGettingImages(false);
    } else {
      setGettingImages(false);
    }
  }, [uploadImage, setGettingImages]);

  return (
    <React.Fragment>
      <SafeAreaView style={styles.pageHeaderContainer}>
        <TouchableOpacity onPress={handleGoBack} activeOpacity={0.8}>
          <Entypo
            style={{paddingLeft: 14}}
            name="chevron-thin-left"
            size={wp(3.5)}
          />
        </TouchableOpacity>

        <View style={styles.logoContainer}>
          <Image
            source={require('../../../../assets/starzlogo.png')}
            style={styles.logo}
          />
          <Image
            source={require('../../../../assets/STAARZ_109X19_black.png')}
            style={styles.logoText}
          />
        </View>
        <Image
          style={styles.avatar}
          source={{
            uri: userState.userData?.profilePicture ?? ProfilePlaceHolder,
          }}
        />
      </SafeAreaView>
      <SafeAreaView style={styles.container}>
        <View style={styles.innerContainer}>
          <ScrollView contentContainerStyle={styles.imageListContainer}>
            {images.map((item) => (
              <ImageGridCard
                image={item}
                removeImage={() => removeImage(item)}
              />
            ))}
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => {
                ActionSheetRef?.current?.show();
              }}
              style={styles.addImageButtonContainer}
            >
              <FontAwesome
                name="plus-circle"
                color={Colors.lightTintColor}
                size={42}
              />
            </TouchableOpacity>
          </ScrollView>
          <View style={styles.bottomContainer}>
            <TouchableOpacity style={styles.doneButton} activeOpacity={0.9}>
              <Text style={styles.doneText}>Done</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.cancelButton}>
              <Text style={styles.cancelChanges}>Cancel Changes</Text>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
      <LoadingModal show={gettingImages} />
      <ActionSheet
        ref={ActionSheetRef}
        title={'How do you like to add an image ?'}
        options={['Choose from gallery', 'cancel']}
        cancelButtonIndex={1}
        onPress={chooseImage}
      />
    </React.Fragment>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.white,
    width: '100%',
  },
  innerContainer: {
    paddingTop: wp(15),
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  pageHeaderContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    display: 'flex',
    alignItems: 'center',

    backgroundColor: Colors.white,
  },
  logoContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  logo: {
    width: wp(7.5),
    height: wp(6.5),
    resizeMode: 'cover',
  },
  logoText: {
    marginLeft: wp(3),
    resizeMode: 'contain',
  },
  avatar: {
    width: wp(9),
    height: wp(9),
    resizeMode: 'cover',
    borderRadius: 25,
    marginRight: 13,
  },
  imageListContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  addImageButtonContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: wp(26),
    width: wp(26),
    borderRadius: 8,
    margin: 10,
    borderStyle: 'dashed',
    borderWidth: 1,
    borderColor: Colors.lightTintColor,
    backgroundColor: Colors.profile.addImageGray,
  },
  bottomContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  doneButton: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.lightTintColor,
    height: 50,
    width: '100%',
    borderRadius: 10,
  },
  doneText: {
    color: Colors.white,
    fontWeight: 'bold',
  },
  cancelButton: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 18,
  },
  cancelChanges: {
    color: Colors.primary,
    fontWeight: '500',
  },
});

export {PhotoGrid};
