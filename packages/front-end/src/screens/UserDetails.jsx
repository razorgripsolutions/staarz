/* eslint-disable no-nested-ternary */
/* eslint-disable no-unused-expressions */
import React, {useState, useEffect} from 'react';
import {
  Dimensions,
  Image,
  View,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  ImageBackground,
  KeyboardAvoidingView,
  SafeAreaView,
  ScrollView,
  Platform,
} from 'react-native';
import {Text, Chip} from 'react-native-paper';
// import Background from '../components/Background'
import {getStatusBarHeight} from 'react-native-status-bar-height';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {CircleFade} from 'react-native-animated-spinkit';
import Ionicons from 'react-native-vector-icons/Ionicons';
// import Background from '../components/Background'
import {
  heightPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {url, backendBaseUrl} from '../const/urlDirectory';
import {theme} from '../core/theme';

function UserDetails(props) {
  // alert(JSON.stringify(props.route.params.matchUserData));
  const windowWidth = Dimensions.get('window').width;
  const windowHeight = Dimensions.get('window').height;
  // const temp = {"userID":403,"userEmail":"ghdfgdf@dfsgdf.dfg","userPassword":"","username":"","facebookToken":"","googleToken":"","fbAuthStatus":0,"googleAuthStatus":0,"fullName":" Fhfgh gf","dob":"31-03-2021","locationText":"6-E, Nungambakkam High Road 1A, Dr, Thirumurthy Nagar, 1st Street, Nungambakkam, Chennai, Tamil Nadu 600034, India","lat":13.055441,"lon":80.2479924,"gender":"1","religionID":2,"introVideoID":0,"profilePicture":"","height":0,"weight":0,"alternateLocation":"","haveCar":0,"doesSmoke":0,"aboutMe":"","relationshipType":2,"packageID":0,"identifyID":0,"emailVerified":1,"isImageVerified":0,"isVideoVerified":0,"isProfileVerified":0,"facebookID":"","googleID":"","uploadedImage":"","clickedImage":"","fbProfileLink":"Dghgh","instaProfileLink":"Fghhjjjk"}
  const [userData, setUserData] = useState(props.route.params.userData);
  //  const [matchUserData, setmatchUserData] = useState(props.route.params.matchUserData);//route.params.userData);
  const [accessToken, setAccessToken] = useState(
    props.route.params.accessToken,
  ); // route.params.accessToken);
  const [instaProfileLink, setInstaProfileLink] = useState('');
  const [fbProfileLink, setFbProfileLink] = useState('');
  const [isloading, setIsloading] = useState(false);
  const [isDetailsloading, setIsDetailsloading] = useState(false);
  const [usersProfile, setUsersProfile] = useState(
    props.route.params.matchUserData,
  );
  const [direction, setDirection] = useState('ltr');
  const [cardImage, setCardImage] = useState('');
  const [cardIndex, setCardIndex] = useState(0);
  const [cardImageStack, setCardImageStack] = useState([]);
  const {navigation} = props;
  const {route} = props;
  const [userInterests, setUserInterests] = useState([]);
  const [userInterestsList, setUserInterestsList] = useState([]);
  const [userInterestsListObj, setUserInterestsListObj] = useState([]);
  // ["Travel","Outdoors","Smoke","Tattoo","Music","Books","Hiking","Adventure","Fishing","Hunting","Camping","Beach","Eat","Cook","Video Games","Netflix","Movies","Write","Dancing","Football","Concert","Active","Work","Beer","Drink","Food","Coffee","Wine","Pizza","Family","Jokes","Lough","God","Animals","Dog","Pet","Cat","Sports","Gym","Walks","Nerd","Studying"]);
  // alert(JSON.stringify(props))
  // console.log(JSON.stringify(userData));

  const apicall = async () => {
    setIsDetailsloading(true);
    fetch(url.fetchInterestList, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        userID: userData.userID,
        token: accessToken,
      }),
    })
      .then((response) => response.json())
      .then(async (responseJson) => {
        if (responseJson.status === 200) {
          let temp = [];
          console.log('response-----new', JSON.stringify(responseJson));
          await setUserInterestsListObj(responseJson.result);
          const userInterestsListObj1 = responseJson.result;

          // 27, 8, 25, 17, 10
          // alert(JSON.stringify(userInterestsListObj1))
          console.log(
            ` UserInterestsListObj 1===>${JSON.stringify(
              userInterestsListObj1,
            )}`,
          );
          console.log(` UserInterestsListObj 1===|>${userData.interest1}`);
          (await userData.interest1) > 0
            ? temp.push(
                getKeyByValue(userInterestsListObj1, userData.interest1),
              )
            : '';
          (await userData.interest2) > 0
            ? temp.push(
                getKeyByValue(userInterestsListObj1, userData.interest2),
              )
            : '';
          (await userData.interest3) > 0
            ? temp.push(
                getKeyByValue(userInterestsListObj1, userData.interest3),
              )
            : '';
          (await userData.interest4) > 0
            ? temp.push(
                getKeyByValue(userInterestsListObj1, userData.interest4),
              )
            : '';
          (await userData.interest5) > 0
            ? temp.push(
                getKeyByValue(userInterestsListObj1, userData.interest5),
              )
            : '';
          // console.log(temp.push(getKeyByValue(userInterestsListObj1, userData.interest1)));
          // console.log(temp.push(getKeyByValue(userInterestsListObj1, userData.interest2)));
          // console.log(temp)
          // await userData.interest1 > 0 ? temp.push(getKeyByValue(userInterestsListObj, userData.interest1)) : "";
          // await userData.interest2 > 0 ? temp.push(getKeyByValue(userInterestsListObj, userData.interest2)) : "";
          // await userData.interest3 > 0 ? temp.push(getKeyByValue(userInterestsListObj, userData.interest3)) : "";
          // await userData.interest4 > 0 ? temp.push(getKeyByValue(userInterestsListObj, userData.interest4)) : "";
          // await userData.interest5 > 0 ? temp.push(getKeyByValue(userInterestsListObj, userData.interest5)) : "";
          // console.log(temp.push(getKeyByValue(userInterestsListObj, userData.interest1)));
          // setTimeout(function () {
          setUserInterests(temp);
          // }, 500)
          // await setUserInterestsList(temp)
          // console.log(' setUserInterestsList 1==|>' + JSON.stringify(userInterestsList))
          temp = [];
          (await usersProfile.interest1) > 0
            ? temp.push(
                getKeyByValue(userInterestsListObj1, usersProfile.interest1),
              )
            : '';
          (await usersProfile.interest2) > 0
            ? temp.push(
                getKeyByValue(userInterestsListObj1, usersProfile.interest2),
              )
            : '';
          (await usersProfile.interest3) > 0
            ? temp.push(
                getKeyByValue(userInterestsListObj1, usersProfile.interest3),
              )
            : '';
          (await usersProfile.interest4) > 0
            ? temp.push(
                getKeyByValue(userInterestsListObj1, usersProfile.interest4),
              )
            : '';
          (await usersProfile.interest5) > 0
            ? temp.push(
                getKeyByValue(userInterestsListObj1, usersProfile.interest5),
              )
            : '';
          setUserInterestsList(temp);
          // // setTimeout(function () {
          // //   setIsDetailsloading(false)
          // // }, 500)
          // // setTimeout(function () {
          // await setUserInterestsList(temp)
          console.log(
            ` setUserInterestsList 2===|>${JSON.stringify(userInterestsList)}`,
          );
          // // setIsDetailsloading(false)
          // // setTimeout(function () {
          // //   setIsDetailsloading(false)
          // // }, .5)
          // // }, 500)
        }
        if (responseJson.status === 404) {
          // alert("Error in fetching religion list.")/
        }
        if (responseJson.status === 400) {
          // alert("Oops!! Something went wrong.")
        }
      })
      .catch((error) => {
        // alert('Oops error!!')
        console.error(error);
      });
  };

  useEffect(() => {
    setIsDetailsloading(true);
    setUsersProfile(props.route.params.matchUserData);
    swipePhoto(
      props.route.params.matchUserData,
      -1,
      props.route.params.matchUserData.profilePicture,
    );

    apicall();

    setIsDetailsloading(false);
  }, []);

  function getKeyByValue(object, value) {
    return Object.keys(object).find((key) => object[key] === value);
  }

  const fetchProfileForMatch = () => {
    setIsloading(true);
    fetch(url.fetchProfileForMatch, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        genderInterest: 1,
        userID: userData.userID,
        lat: userData.lat,
        lon: userData.lon,
        // genderInterest :1,
        // userID : 499,
        // lat : 32.1133141,
        // lon : 34,
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.status === 200) {
          // alert(JSON.stringify(responseJson));
          console.log(` profile data===== ${JSON.stringify(responseJson)}`);
          // console.log("========================================================");
          // console.log(JSON.stringify(userData));
          setUsersProfile(responseJson.user);
          setIsloading(false);
        }
        if (responseJson.status === 404) {
          // alert("Oops Error!!")
          setIsloading(false);
        }
        if (responseJson.status === 400) {
          setIsloading(false);
          // alert("Oops!! Something went wrong.")
        }
      })
      .catch((error) => {
        setIsloading(false);
        alert(error);
        console.error(error);
      });
  };

  // eslint-disable-next-line react/no-unstable-nested-components
  function SwipeButtons(user) {
    return (
      <View style={swipeButtonsStyles.container}>
        <TouchableOpacity
          color="white"
          mode="contained"
          style={{padding: 10}}
          onPress={() => {
            alert('dislikeButton method is undefined!!!!'); // dislikeButton is the method we got it with
            // dislikeButton(user);
          }}
        >
          <View
            style={{
              backgroundColor: 'white',
              justifyContent: 'center',
              alignItems: 'center',
              height: 60,
              width: 60,
              borderRadius: 50,
            }}
          >
            <FontAwesome name="remove" color="#B9BAC8" size={40} />
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          color="white"
          mode="contained"
          style={{padding: 10}}
          onPress={() => {
            alert('rewindButton method is undefined!!!!'); // rewindButton is the method we got it with
            // rewindButton(user)
          }}
        >
          <View
            style={{
              backgroundColor: 'white',
              justifyContent: 'center',
              alignItems: 'center',
              height: 60,
              width: 60,
              borderRadius: 50,
            }}
          >
            <FontAwesome5 name="undo" color="black" size={30} />
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          color="white"
          mode="contained"
          style={{padding: 10}}
          onPress={() => {
            alert('superLikeButton method is undefined!!!!'); // superLikeButton is the method we got it with
            // superLikeButton(user)
          }}
        >
          <View
            style={{
              backgroundColor: 'white',
              justifyContent: 'center',
              alignItems: 'center',
              height: 60,
              width: 60,
              borderRadius: 50,
            }}
          >
            <MaterialCommunityIcons
              name="lightning-bolt"
              color="#7643F8"
              size={35}
            />
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          color="white"
          mode="contained"
          style={{padding: 10}}
          // eslint-disable-next-line no-undef
          onPress={() => likeButton(user)}
        >
          <View
            style={{
              backgroundColor: 'white',
              justifyContent: 'center',
              alignItems: 'center',
              height: 60,
              width: 60,
              borderRadius: 50,
            }}
          >
            <Entypo name="heart" color="#F84747" size={35} />
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          color="white"
          mode="contained"
          style={{padding: 10}}
          // eslint-disable-next-line no-undef
          onPress={() => boostButton(user)}
        >
          <View
            style={{
              backgroundColor: 'white',
              justifyContent: 'center',
              alignItems: 'center',
              height: 60,
              width: 60,
              borderRadius: 50,
            }}
          >
            <MaterialCommunityIcons
              name="rocket-launch"
              color="#422886"
              size={35}
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
  const swipeButtonsStyles = StyleSheet.create({
    container: {
      position: 'absolute',
      height: 80,
      width: '100%',
      bottom: 30,
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'row',
    },
  });

  // eslint-disable-next-line react/no-unstable-nested-components
  function ProfilesBasicInfo(userProfile) {
    const userArray = userProfile.user;
    let userAge = 0;
    if (userArray.dob) {
      const birthDate = new Date(userArray.dob);
      const ageDifMs = Date.now() - birthDate.getTime();
      const ageDate = new Date(ageDifMs); // miliseconds from epoch
      userAge = Math.abs(ageDate.getUTCFullYear() - 1970);
    }

    function distance(lat1, lon1, lat2, lon2) {
      const p = 0.017453292519943295; // Math.PI / 180
      const c = Math.cos;
      const a =
        0.5 -
        c((lat2 - lat1) * p) / 2 +
        (c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p))) / 2;

      return (12742 * Math.asin(Math.sqrt(a))).toFixed(2); // 2 * R; R = 6371 km
    }

    const dist =
      userArray.lat && userArray.lon && userData.lat && userData.lon
        ? distance(userArray.lat, userArray.lon, userData.lat, userData.lon)
        : 0;
    return (
      <View style={profilesBasicInfoStyle.container}>
        {userArray ? (
          <>
            <View style={profilesBasicInfoStyle.row}>
              <Text style={profilesBasicInfoStyle.fullName}>
                {userArray.fullName ? userArray.fullName : ''}
              </Text>
            </View>
            <View style={profilesBasicInfoStyle.row}>
              <Text style={profilesBasicInfoStyle.genderAgeHeight}>
                {userArray.gender === 0
                  ? 'Male'
                  : userArray.gender === 1
                  ? 'Female'
                  : userArray.gender === 2
                  ? 'Other'
                  : ''}
                {userAge ? (
                  <Text style={profilesBasicInfoStyle.genderAgeHeight1}>
                    {' '}
                    |{' '}
                  </Text>
                ) : (
                  <></>
                )}
                {userAge || ''}
                {userArray.height ? (
                  <Text style={profilesBasicInfoStyle.genderAgeHeight1}>
                    {' '}
                    |{' '}
                  </Text>
                ) : (
                  <></>
                )}
                {userArray.height ? `${userArray.height} cm` : ''}
              </Text>
            </View>
            <View style={profilesBasicInfoStyle.row}>
              <Image
                source={require('../assets/lock-u.png')}
                style={{width: 12, height: 18, resizeMode: 'contain'}}
              />
              <Text style={profilesBasicInfoStyle.address}>
                {userArray.locationText ? userArray.locationText : ''}
              </Text>
            </View>
            <View style={profilesBasicInfoStyle.row}>
              <Text style={profilesBasicInfoStyle.dist}>
                {dist ? `${dist} KM` : ''}
              </Text>
            </View>
            <Text />
          </>
        ) : (
          <></>
        )}
      </View>
    );
  }
  const profilesBasicInfoStyle = StyleSheet.create({
    container: {
      // flexDirection:"column",
      color: 'black',
      // position: 'absolute',
      // height:120,
      width: '100%',
      // bottom:100,
      paddingLeft: 30,
      justifyContent: 'flex-start',
      alignItems: 'flex-start',
      marginTop: 0,
      // backgroundColor: "red"
    },
    fullName: {
      color: 'black',
      lineHeight: 23,
      fontFamily: 'MPLUS1p-Bold',
      fontSize: 16,
      letterSpacing: 1.09,
      fontWeight: '800',
    },
    genderAgeHeight: {
      color: 'black',
      lineHeight: 24,
      fontFamily: 'MPLUS1p-Bold',
      fontSize: 13,
      letterSpacing: 0.7,
      fontWeight: '500',
    },
    genderAgeHeight1: {
      color: '#B9BAC8',
      lineHeight: 30,
      fontSize: 15,
      letterSpacing: 0.7,
      // width:4,
      fontWeight: '400',
    },
    address: {
      color: 'black',
      fontSize: 13,
      lineHeight: 19,
      left: 5,
      letterSpacing: 1,
    },
    dist: {
      color: 'black',
      fontSize: 13,
      lineHeight: 19,
      left: 15,
    },
    row: {
      flexDirection: 'row',
      width: '90%',
    },
  });

  const styles = StyleSheet.create({
    container: {},
    leftBox: {
      width: windowWidth * 0.35,
      height: windowHeight * 0.5,
    },
    rightBox: {
      width: windowWidth * 0.35,
      height: windowHeight * 0.5,
    },
    centerBox: {
      width: windowWidth * 0.25,
      height: windowHeight * 0.5,
    },
  });

  const swipePhoto = async (newUserData, swipeDirection, currentCardImage) => {
    if (swipeDirection === -1) {
      setIsloading(true);
      const temp = [];
      if (newUserData.profilePicture && newUserData.profilePicture !== '') {
        temp.push(newUserData.profilePicture);
      }
      if (newUserData.galleryPhotos1 && newUserData.galleryPhotos1 !== '') {
        temp.push(newUserData.galleryPhotos1);
      }
      if (newUserData.galleryPhotos2 && newUserData.galleryPhotos2 !== '') {
        temp.push(newUserData.galleryPhotos2);
      }
      if (newUserData.galleryPhotos3 && newUserData.galleryPhotos3 !== '') {
        temp.push(newUserData.galleryPhotos3);
      }
      if (newUserData.galleryPhotos4 && newUserData.galleryPhotos4 !== '') {
        temp.push(newUserData.galleryPhotos4);
      }
      if (newUserData.galleryPhotos5 && newUserData.galleryPhotos5 !== '') {
        temp.push(newUserData.galleryPhotos5);
      }
      if (newUserData.galleryPhotos6 && newUserData.galleryPhotos6 !== '') {
        temp.push(newUserData.galleryPhotos6);
      }
      await setCardImageStack(temp);
      // alert(temp)
      setCardIndex(0);
      setCardImage(temp[0]);
      // setTimeout(function(){
      setIsloading(false);
      // }, .05)
    } else {
      if (swipeDirection === 1 && cardIndex <= cardImageStack.length - 1) {
        // right
        setIsloading(true);
        let tempIndex = cardIndex;
        cardImageStack.length - 1 > tempIndex ? tempIndex++ : tempIndex;
        setCardIndex(tempIndex);
        setCardImage(cardImageStack[tempIndex]);
        setTimeout(() => {
          setIsloading(false);
          console.log(
            'swiperrrrrrrrrr',
            'swipe right',
            tempIndex,
            cardImageStack.length,
          );
        }, 0.05);
      }
      if (swipeDirection === 0 && cardIndex >= 0) {
        // left
        setIsloading(true);
        let tempIndex = cardIndex;
        tempIndex > 1 ? tempIndex-- : (tempIndex = 0);
        setCardIndex(tempIndex);
        setCardImage(cardImageStack[tempIndex]);
        setTimeout(() => {
          setIsloading(false);
          console.log(
            'swiperrrrrrrrrr',
            'swipe left',
            tempIndex,
            cardImageStack.length,
          );
        }, 0.05);
      }
    }
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
      {/* <BackButton goBack={navigation.goBack} />
      <StaarzLogo /> */}
      <View
        style={{
          paddingBottom: 5,
          flexDirection: 'row',
          top: Platform.OS === 'ios' ? 0 : 0,
          alignItems: 'center',
          justifyContent: 'center',
          width: '100%',
          backgroundColor: '#fff',
        }}
      >
        <View style={{width: '20%'}}>
          <TouchableOpacity onPress={() => navigation.goBack()} style={{}}>
            <FontAwesome
              name="angle-left"
              size={25}
              color="#000"
              style={{left: 20}}
            />
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: '60%',
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'center',
          }}
        >
          <Image
            source={require('../assets/starzlogo.png')}
            style={sideLogoStyles.imageLogo}
          />
          <Text
            style={{
              fontSize: wp(6),
              fontFamily: 'MPLUS1p-Bold',
              color: '#243443',
              left: -2,
              lineHeight: 33,
              textAlign: 'center',
            }}
          >
            STAARZ
          </Text>
          {/* <Image source={require('../assets/STAARZ_109X19_black.png')} style={sideLogoStyles.image} /> */}
        </View>
        <View style={{width: '20%'}} />
      </View>
      <View
        style={{
          flex: 1,
          width: '100%',
          justifyContent: 'center',
          alignContent: 'center',
          alignSelf: 'center',
        }}
      >
        <ScrollView
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="handled"
          showsHorizontalScrollIndicator={false}
        >
          {isloading || isDetailsloading ? (
            <CircleFade
              size={110}
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                alignContent: 'center',
                alignSelf: 'center',
                top: windowHeight * 0.4,
              }}
              color="#5E2EBA"
            />
          ) : (
            <View>
              <View style={swipeCardStyles.container}>
                {usersProfile ? (
                  <View>
                    {/* //{alert(JSON.stringify(userInterestsList))} */}
                    {/* {alert(JSON.stringify(userInterestsListObj))} */}
                    {/* {alert(JSON.stringify(usersProfile))} */}
                    <View
                      style={
                        (swipeCardStyles.card,
                        {height: windowHeight * 0.86, top: -30})
                      }
                    >
                      <ImageBackground
                        resizeMode="cover"
                        style={backgroundStyles.background}
                        source={{
                          uri:
                            cardImage && cardImage !== ''
                              ? `${backendBaseUrl}data/${cardImage}`
                              : usersProfile.profilePicture &&
                                usersProfile.profilePicture !== ''
                              ? `${backendBaseUrl}data/${usersProfile.profilePicture}`
                              : `${backendBaseUrl}data/tempImage.jpeg`,
                        }}
                      />
                    </View>

                    <View>
                      <ProfilesBasicInfo user={usersProfile} />
                    </View>

                    {userInterestsList.length > 0 ? (
                      <View style={userDetailsStyle.interestStyleContainer}>
                        <Text
                          style={{
                            color: 'black',
                            lineHeight: 23,
                            fontFamily: 'MPLUS1p-Bold',
                            fontSize: 16,
                            letterSpacing: 1.09,
                            fontWeight: '800',
                          }}
                        >
                          {' '}
                          Interests{' '}
                        </Text>
                        {/* <SelectableChips chipStyle={{ height: 30 }} chipStyleSelected={{ backgroundColor: "#7643F8" }} valueStyleSelected={{ color: "white" }} valueStyle={{ color: '#B6BFCB', fontSize: 12, fontWeight: "700" }} initialSelectedChips={userInterests} initialChips={userInterestsList} onChangeChips={(chip) => setUserInterests(chip)} alertRequired={false} /> */}
                        <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
                          {userInterestsList[0] ? (
                            <View style={{padding: 2}}>
                              <Chip
                                mode="outlined"
                                textStyle={{
                                  color:
                                    userInterests.indexOf(
                                      userInterestsList[0],
                                    ) !== -1
                                      ? 'white'
                                      : '#8A98AC',
                                }}
                                style={{
                                  backgroundColor:
                                    userInterests.indexOf(
                                      userInterestsList[0],
                                    ) !== -1
                                      ? '#7643F8'
                                      : 'white',
                                }}
                                disabled
                              >
                                {userInterestsList[0]}
                              </Chip>
                            </View>
                          ) : (
                            <></>
                          )}
                          {userInterestsList[1] ? (
                            <View style={{padding: 2}}>
                              <Chip
                                mode="outlined"
                                textStyle={{
                                  color:
                                    userInterests.indexOf(
                                      userInterestsList[1],
                                    ) !== -1
                                      ? 'white'
                                      : '#8A98AC',
                                }}
                                style={{
                                  backgroundColor:
                                    userInterests.indexOf(
                                      userInterestsList[1],
                                    ) !== -1
                                      ? '#7643F8'
                                      : 'white',
                                }}
                                disabled
                              >
                                {userInterestsList[1]}
                              </Chip>
                            </View>
                          ) : (
                            <></>
                          )}
                          {userInterestsList[2] ? (
                            <View style={{padding: 2}}>
                              <Chip
                                mode="outlined"
                                textStyle={{
                                  color:
                                    userInterests.indexOf(
                                      userInterestsList[2],
                                    ) !== -1
                                      ? 'white'
                                      : '#8A98AC',
                                }}
                                style={{
                                  backgroundColor:
                                    userInterests.indexOf(
                                      userInterestsList[2],
                                    ) !== -1
                                      ? '#7643F8'
                                      : 'white',
                                }}
                                disabled
                              >
                                {userInterestsList[2]}
                              </Chip>
                            </View>
                          ) : (
                            <></>
                          )}
                          {userInterestsList[3] ? (
                            <View style={{padding: 2}}>
                              <Chip
                                mode="outlined"
                                textStyle={{
                                  color:
                                    userInterests.indexOf(
                                      userInterestsList[3],
                                    ) !== -1
                                      ? 'white'
                                      : '#8A98AC',
                                }}
                                style={{
                                  backgroundColor:
                                    userInterests.indexOf(
                                      userInterestsList[3],
                                    ) !== -1
                                      ? '#7643F8'
                                      : 'white',
                                }}
                                disabled
                              >
                                {userInterestsList[3]}
                              </Chip>
                            </View>
                          ) : (
                            <></>
                          )}
                          {userInterestsList[4] ? (
                            <View style={{padding: 2}}>
                              <Chip
                                mode="outlined"
                                textStyle={{
                                  color:
                                    userInterests.indexOf(
                                      userInterestsList[4],
                                    ) !== -1
                                      ? 'white'
                                      : '#8A98AC',
                                }}
                                style={{
                                  backgroundColor:
                                    userInterests.indexOf(
                                      userInterestsList[4],
                                    ) !== -1
                                      ? '#7643F8'
                                      : 'white',
                                }}
                                disabled
                              >
                                {userInterestsList[4]}
                              </Chip>
                            </View>
                          ) : (
                            <></>
                          )}
                        </View>
                      </View>
                    ) : (
                      <></>
                    )}

                    {usersProfile.aboutMe ? (
                      <View style={userDetailsStyle.interestStyleContainer}>
                        <Text
                          style={{
                            marginTop: 20,
                            color: 'black',
                            lineHeight: 23,
                            fontFamily: 'MPLUS1p-Bold',
                            fontSize: 16,
                            letterSpacing: 1.09,
                            fontWeight: '800',
                          }}
                        >
                          {' '}
                          About{' '}
                        </Text>
                        {/* <SelectableChips chipStyle={{height:30}} chipStyleSelected={{backgroundColor:"#7643F8"}} valueStyleSelected={{color:"white"}} valueStyle={{color:'#B6BFCB', fontSize:12, fontWeight:"700"}} initialSelectedChips={userInterests} initialChips={userInterestsList}  onChangeChips={(chip) => setUserInterests(chip)} alertRequired={false}/> */}
                        <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
                          <Text
                            style={{
                              paddingLeft: 10,
                              lineHeight: 19,
                              fontSize: 13,
                              fontFamily: 'MPLUS1p-Medium',
                            }}
                          >
                            {usersProfile.aboutMe}{' '}
                          </Text>
                          {/* <Text>{cardImageStack}</Text> */}
                        </View>
                      </View>
                    ) : (
                      <></>
                    )}

                    <View style={{width: '100%'}}>
                      <FlatList
                        data={cardImageStack}
                        numColumns={3}
                        style={{marginTop: 10, marginHorizontal: 20}}
                        columnWrapperStyle={{
                          justifyContent: 'space-around',
                          paddingVertical: 10,
                        }}
                        renderItem={({item}) => (
                          // <Text>{item}</Text>
                          <TouchableOpacity
                            style={{borderRadius: 20}}
                            onPress={() =>
                              navigation.navigate('FullImageScreen', {
                                url: item,
                              })
                            }
                          >
                            <Image
                              source={{uri: `${backendBaseUrl}data/${item}`}}
                              style={{
                                width: heightPercentageToDP('13'),
                                height: heightPercentageToDP('15'),
                                borderRadius: 8,
                              }}
                            />
                          </TouchableOpacity>
                        )}
                      />
                    </View>
                  </View>
                ) : (
                  <>
                    <Text
                      style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        top: windowHeight * 0.4,
                        fontSize: 30,
                        fontWeight: '800',
                      }}
                    >
                      No user found
                    </Text>
                    <TouchableOpacity
                      color="white"
                      mode="contained"
                      style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        top: windowHeight * 0.4,
                      }}
                      onPress={() => fetchProfileForMatch()}
                    >
                      <View
                        style={{
                          backgroundColor: 'white',
                          justifyContent: 'center',
                          alignItems: 'center',
                          height: 60,
                          width: 60,
                          borderRadius: 50,
                        }}
                      >
                        <Ionicons name="reload" color="black" size={30} />
                      </View>
                    </TouchableOpacity>
                  </>
                )}
              </View>

              <View style={{paddingBottom: 100}}>
                {/* <Text>hrlooodmsfjksdjfdsjkfjksk</Text> */}
              </View>
            </View>
          )}
        </ScrollView>
      </View>
    </SafeAreaView>
  );
}

const swipeCardStyles = StyleSheet.create({
  container: {
    // top:0,
    // width:"100%",
    // backgroundColor:"red",
    top: 10 + getStatusBarHeight(),
  },
  card: {
    flex: 1,
    borderRadius: 4,
    borderWidth: 2,
    width: '100%',
    borderColor: '#E8E8E8',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  text: {
    textAlign: 'center',
    fontSize: 50,
    // backgroundColor: "transparent"
  },
});

function StaarzLogo() {
  return (
    <View style={sideLogoStyles.container}>
      <Image
        source={require('../assets/starzlogo_23X26.png')}
        style={sideLogoStyles.imageLogo}
      />
      <Image
        source={require('../assets/STAARZ_109X19_black.png')}
        style={sideLogoStyles.image}
      />
    </View>
  );
}
const sideLogoStyles = StyleSheet.create({
  image: {
    marginTop: 0,
    marginLeft: 20,
  },
  imageLogo: {
    // left: 10,
    width: wp(7.5),
    height: wp(6.5),
    left: -4,
    // marginBottom: 8,
  },
  container: {
    // position: 'absolute',
    top:
      Platform.OS === 'ios'
        ? heightPercentageToDP('5')
        : heightPercentageToDP('2'),
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    backgroundColor: 'white',
    zIndex: 10,
    width: '100%',
    // marginTop: 0
  },
  // image: {
  //   width: 24,
  //   height: 24,
  // }
});

function Background({children}) {
  return (
    <ImageBackground resizeMode="repeat" style={backgroundStyles.background}>
      <KeyboardAvoidingView
        style={backgroundStyles.container}
        behavior="padding"
      >
        {children}
      </KeyboardAvoidingView>
    </ImageBackground>
  );
}
const backgroundStyles = StyleSheet.create({
  background: {
    flex: 1,
    width: '100%',
    top: -10,
    backgroundColor: theme.colors.surface,
  },
  container: {
    flex: 1,
    // padding: 20,
    width: '100%',
    // maxWidth: 340,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const userDetailsStyle = StyleSheet.create({
  background: {},
  interestStyleContainer: {
    flex: 1,
    padding: 0,
    paddingHorizontal: 20,
    width: '100%',
    // maxWidth: 340,
    // alignSelf: 'center',
    // alignItems: 'center',
    // justifyContent: 'center',
  },
  fadedStyleContainer: {
    flex: 1,
    paddingLeft: 30,
    width: '100%',
  },
});

export default UserDetails;
