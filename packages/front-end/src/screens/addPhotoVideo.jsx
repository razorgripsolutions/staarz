import React, {useState} from 'react';
import {
  Image,
  View,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  FlatList,
  SafeAreaView,
  Platform,
} from 'react-native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  heightPercentageToDP,
} from 'react-native-responsive-screen';
import {Text} from 'react-native-paper';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import ImagePicker from 'react-native-image-picker';
import {CircleFade} from 'react-native-animated-spinkit';
import Button from '../components/Button';
import {theme} from '../core/theme';
import {url} from '../const/urlDirectory';
import {checkConnection} from '../components/checkConnection';
import {string} from '../const/string';
import {API_ENDPOINTS} from '../api/routes';

const addPhotoVideo = ({navigation, route}) => {
  const [userData, setUserData] = useState();
  const [accessToken, setAccessToken] = useState();

  const [Active, setActive] = useState(true);
  const [net, setnet] = React.useState(true);
  const windowWidth = Dimensions.get('window').width;
  const windowHeight = Dimensions.get('window').height;
  const {width} = Dimensions.get('window');
  const {height} = Dimensions.get('window');
  const [dataSource, setDataSource] = useState([
    {
      id: 0,
      src: '',
      title: 'Add photo',
      icon: 'plus-circle',
      Imagetype: 1,
      mediaName: '',
    },
    {
      id: 1,
      src: '',
      title: 'Add photo',
      icon: 'plus-circle',
      Imagetype: 1,
      mediaName: '',
    },
    {
      id: 2,
      src: '',
      title: 'Add photo',
      icon: 'plus-circle',
      Imagetype: 1,
      mediaName: '',
    },
    {
      id: 3,
      src: '',
      title: 'Add photo',
      icon: 'plus-circle',
      Imagetype: 1,
      mediaName: '',
    },
    {
      id: 4,
      src: '',
      title: 'Add photo',
      icon: 'plus-circle',
      Imagetype: 1,
      mediaName: '',
    },
    {
      id: 5,
      src: '',
      title: 'Add photo',
      icon: 'plus-circle',
      Imagetype: 1,
      mediaName: '',
    },
    {
      id: 6,
      src: '',
      title: 'Add video',
      icon: 'video-camera',
      Imagetype: 0,
      mediaName: '',
    },
    {
      id: 7,
      src: '',
      title: 'Add video',
      icon: 'video-camera',
      Imagetype: 0,
      mediaName: '',
    },
    {
      id: 8,
      src: '',
      title: 'Add video',
      icon: 'video-camera',
      Imagetype: 0,
      mediaName: '',
    },
  ]);
  const [isLoading, setIsLoading] = useState(false);

  const getdata = () => {
    checkConnection().then((res) => {
      if (res === false) {
        setnet(res);
      } else {
        setnet(res);
      }
    });
  };

  const onAddPhotoVideoSubmit = () => {
    checkConnection().then((res) => {
      if (res === false) {
        setnet(res);
        // dropDownAlertRef.alertWithType('error', 'Error', string.network);
      } else {
        setnet(res);
        onAddPhotoVideoSubmit1();
      }
    });
  };

  const onAddPhotoVideoSubmit1 = () => {
    setIsLoading(true);

    fetch(url.addPhotoVideoSubmit, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        galleryPhotos1: dataSource[0].mediaName,
        galleryPhotos2: dataSource[1].mediaName,
        galleryPhotos3: dataSource[2].mediaName,
        galleryPhotos4: dataSource[3].mediaName,
        galleryPhotos5: dataSource[4].mediaName,
        galleryPhotos6: dataSource[5].mediaName,
        galleryVideo1: dataSource[6].mediaName,
        galleryVideo2: dataSource[7].mediaName,
        galleryVideo3: dataSource[8].mediaName,
        status: 5,
        userID: userData.userID,
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        // alert(JSON.stringify(responseJson))
        if (responseJson.status === 200) {
          navigation.replace('TellUsMoreAboutYou1', {
            userData,
            accessToken,
          });
          setIsLoading(false);
        }
        if (responseJson.status === 404) {
          alert(responseJson.message);
          setIsLoading(false);
        }
        if (responseJson.status === 400) {
          alert(responseJson.message);
          // alert("Oops!! Something went wrong.")
          setIsLoading(false);
        }
      })
      .catch((error) => {
        // alert(error)
        console.error(error);
        setIsLoading(false);
      });
  };

  const onCardButtonPress = async (item) => {
    setIsLoading(true);
    const options = {
      noData: true,
      mediaType: item.Imagetype !== 0 ? 'photo' : 'video',
      // mediaType: 'video',
      maxWidth: 500,
      maxHeight: 500,
      quality: 0.5,
    };
    await ImagePicker.launchImageLibrary(options, (response) => {
      // alert(JSON.stringify(response))
      // alert("response------",JSON.stringify(response.path.slice(response.path.length - 4, response.path.length)))
      if (response.uri) {
        // imageType = response.type

        console.log('type------', response.type ? response.type : 'video/mp4');
        const data = new FormData();
        const tempURI = response.uri;
        data.append('userID', userData.userID);
        data.append('galleryImage', {
          uri: response.uri,
          type: response.type ? response.type : 'video/mp4',
          //  type:"video/mp4",
          name: response.type ? response.type : 'video.mp4',
        });
        const config = {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
          },
          body: data,
        };

        fetch(API_ENDPOINTS.uploadGalleryMedia, config)
          .then((res) => res.json())
          .then((res) => {
            if (res.status === 200) {
              console.log('200---------', JSON.stringify(res));
              //  alert(JSON.stringify(res))
              const qty = dataSource; // make a copy
              const ids = item.id;
              qty[item.id].src = tempURI;
              qty[item.id].mediaName = res.res.filename;
              setDataSource(qty);
              setActive(false);
              //   alert(JSON.stringify(qty))
              setIsLoading(false);
            } else {
              console.log('else---------', JSON.stringify(res));

              setIsLoading(false);
            }
          })
          .catch((err) => {
            console.log('err---------', JSON.stringify(err));

            setIsLoading(false);
            // alert(err)
          });
      } else {
        setIsLoading(false);
      }
    });
  };

  const renderItem = ({item}) => (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      {item.src ? (
        <>
          {item.title === 'Add photo' ? (
            <TouchableOpacity
              style={
                (cardButton.card,
                {
                  borderRadius: 5,
                  borderColor: '#8A98AC',
                  borderWidth: 1.2,
                  borderStyle: 'dashed',
                  width: heightPercentageToDP('13'),
                  height: heightPercentageToDP('14'),
                })
              }
              color="#F8FCFF"
              mode="contained"
              onPress={() => onCardButtonPress(item)}
            >
              <View style={styles.imageThumbnail}>
                <Image
                  source={{uri: item.src}}
                  style={{
                    width: heightPercentageToDP('13'),
                    height: heightPercentageToDP('14'),
                    justifyContent: 'center',
                    alignItems: 'center',
                    resizeMode: 'cover',
                    borderColor: '#8A98AC',
                    borderWidth: 1.2,
                  }}
                />
              </View>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              style={
                (cardButton.card,
                {
                  borderRadius: 5,
                  borderColor: '#8A98AC',
                  borderWidth: 1.2,
                  borderStyle: 'dashed',
                  width: heightPercentageToDP('13'),
                  height: heightPercentageToDP('14'),
                })
              }
              color="#F8FCFF"
              mode="contained"
              onPress={() => onCardButtonPress(item)}
            >
              {/* <View style={styles.imageThumbnail}>
            <Entypo style={{ width: heightPercentageToDP('13'), height: heightPercentageToDP('15'), justifyContent: 'center', alignItems: 'center', resizeMode: "cover", borderColor: "#8A98AC", borderWidth: 1.2 }} name="video" size={20} color="#8A98AC" />
          </View> */}

              <View
                style={{
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderWidth: 1.2,
                  borderColor: '#8A98AC',
                }}
              >
                <Entypo style={{}} name="video" size={20} color="#8A98AC" />
                {/* <Text style={{ color: "#8A98AC", fontSize: heightPercentageToDP('1.2'), textAlign: "center" }}>{item.title}</Text> */}
              </View>
            </TouchableOpacity>
          )}
        </>
      ) : (
        <TouchableOpacity
          style={
            (cardButton.card,
            {
              borderRadius: 5,
              borderColor: '#8A98AC',
              borderWidth: 1.2,
              borderStyle: 'dashed',
              width: heightPercentageToDP('13'),
              height: heightPercentageToDP('14'),
              marginVertical: 10,
            })
          }
          color="#F8FCFF"
          mode="contained"
          onPress={() => onCardButtonPress(item)}
        >
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: '#e5eef6',
            }}
          >
            <FontAwesome name={item.icon} size={wp(6)} color="#8A98AC" />
            <Text
              style={{
                color: '#8A98AC',
                fontSize: wp(3),
                textAlign: 'center',
              }}
            >
              {item.title}
            </Text>
          </View>
        </TouchableOpacity>
      )}

      {/* <TouchableOpacity style={cardButton.card,{borderColor:"#8A98AC", borderWidth:1.2, borderStyle: 'dashed', width:heightPercentageToDP('13'),height:heightPercentageToDP('15'),marginVertical:10}}  color='#F8FCFF'  mode="contained" onPress={ () =>onCardButtonPress(item)}>
  <View style={{flex:1,alignItems:"center",justifyContent:"center"}}>
              <FontAwesome  name={item.icon} size={20} color="#8A98AC" />
              <Text style={{color:"#8A98AC",fontSize:heightPercentageToDP('1.2'),textAlign:"center"}}>{item.title}</Text>
            </View>

            </TouchableOpacity> */}

      {/* {item.src ?<>{(item.title=="Add photo")?<TouchableOpacity style={cardButton.card,{marginTop:10, borderColor:"#8A98AC", borderWidth:1.2, borderStyle: 'dashed', width:"33%", height:"50%"}}  color='#F8FCFF'  mode="contained" onPress={ () =>onCardButtonPress(item)}><View style={styles.imageThumbnail}><Image
                  source={{ uri: item.src }}
                  style={{ marginTop:17,width: "33%", height: "50%", justifyContent: 'center', alignItems: 'center',resizeMode: "cover", borderColor:"#8A98AC", borderWidth:1.2}}
                /></View></TouchableOpacity>:<TouchableOpacity style={cardButton.card,{ borderColor:"#8A98AC", borderWidth:1.2, borderStyle: 'dashed', width:"33%", height:"50%"}}  color='#F8FCFF'  mode="contained" onPress={ () =>onCardButtonPress(item)}><View style={styles.imageThumbnail}><Entypo style={{ marginTop:17,width: "33%", height: "50%", justifyContent: 'center', alignItems: 'center',resizeMode: "cover", borderColor:"#8A98AC", borderWidth:1.2}} name="video" size={28} color="#8A98AC" /></View></TouchableOpacity>}
          </>
          :<><Button style={cardButton.card,{borderColor:"#8A98AC", borderWidth:1.2, borderStyle: 'dashed', width:"33%", height:"50%"}}  color='#F8FCFF'  mode="contained" onPress={ () =>onCardButtonPress(item)}><View style={styles.imageThumbnail}>
              <Icon  name={item.icon} size={28} color="#8A98AC" />
              <Text style={{color:"#8A98AC", fontSize:13}}>{item.title}</Text>
            </View></Button>
          </>
        } */}
    </View>
  );

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
      <View style={{flex: 1, alignItems: 'center'}}>
        <StaarzLogo />
        {!net ? (
          <View
            style={{alignItems: 'center', justifyContent: 'center', flex: 1}}
          >
            <Image
              source={require('../assets/internat.png')}
              style={{height: 50, width: 50}}
            />
            <Text style={{fontSize: 17, fontWeight: '700', marginTop: 5}}>
              {string.network}
            </Text>
            <TouchableOpacity
              color="white"
              mode="contained"
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                top: windowHeight * 0.3,
              }}
              onPress={() => getdata()}
            >
              <View
                style={{
                  backgroundColor: '#7643F8',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 50,
                  paddingHorizontal: 30,
                  paddingVertical: 20,
                  marginTop: 10,
                }}
              >
                {
                  Platform.OS === 'ios' ? (
                    // <Ionicons name="reload" color="black" size={30} />
                    <Text style={{fontSize: hp('2'), color: '#fff'}}>
                      {string.Try_Again}
                    </Text>
                  ) : (
                    <Text style={{fontSize: hp('2'), color: '#fff'}}>
                      {string.Try_Again}
                    </Text>
                  )
                  // <MaterialCommunityIcons name="reload" color="black" size={30} />
                }
              </View>
            </TouchableOpacity>
          </View>
        ) : (
          <>
            <Text
              style={{
                letterSpacing: 0.4,
                fontSize:
                  width === 320 || height === 610.6666666666666 ? 24 : 29,
                marginTop: '25%',
                lineHeight: 42,
                fontWeight: 'bold',
              }}
            >
              Add photos/video
            </Text>
            <Text
              style={{
                fontWeight: '500',
                fontSize:
                  width === 320 || height === 610.6666666666666 ? 16 : 18,
                textAlign: 'center',
                color: '#919FB1',
                lineHeight: 27,
              }}
            >
              You can change this later
            </Text>

            <View style={styles.container}>
              {!isLoading ? (
                <FlatList
                  data={dataSource}
                  renderItem={renderItem}
                  numColumns={3}
                  // updateCellsBatchingPeriod={1}
                />
              ) : (
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <CircleFade size={110} color="#5E2EBA" />
                </View>
              )}
            </View>
            <View style={bottomButton.bottomView}>
              <Button
                disabled={Active}
                style={bottomButton.login}
                color="#8897AA"
                mode="contained"
                onPress={onAddPhotoVideoSubmit}
              >
                <Text
                  style={{
                    textTransform: 'capitalize',
                    fontWeight: 'bold',
                    fontSize: 18,
                    color: 'white',
                  }}
                >
                  Next
                </Text>
              </Button>
            </View>
          </>
        )}
      </View>

      {/* <DropdownAlert ref={ref => dropDownAlertRef = ref} /> */}
    </SafeAreaView>
  );
};

const bottomButton = StyleSheet.create({
  bottomView: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
  },
  login: {
    marginBottom: 30,
    borderRadius: 8,
    width: '90%',
  },
});

const cardButton = StyleSheet.create({
  card: {
    borderRadius: 8,
    backgroundColor: 'green',
    flexDirection: 'column',
  },
});

function StaarzLogo() {
  return (
    <View style={sideLogoStyles.container}>
      <Image
        source={require('../assets/starzlogo.png')}
        style={sideLogoStyles.imageLogo}
      />
      {/* <Image source={require('../assets/STAARZ_109X19_black.png')} style={sideLogoStyles.image} /> */}
      <Text
        style={{
          fontSize: wp(6),
          fontFamily: 'MPLUS1p-Bold',
          color: '#243443',
          left: -2,
          lineHeight: 33,
          textAlign: 'center',
        }}
      >
        STAARZ
      </Text>
    </View>
  );
}

const sideLogoStyles = StyleSheet.create({
  image: {
    marginTop: 0,
    marginLeft: 20,
  },
  imageLogo: {
    // left: 10,
    width: wp(7.5),
    height: wp(6.5),
    left: -4,
    // marginBottom: 8,
  },
  container: {
    // backgroundColor:"red",
    position: 'absolute',
    top: 0,
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 0,
  },
});

const styles = StyleSheet.create({
  forgotPassword: {
    width: '100%',
    alignItems: 'flex-end',
    marginRight: 10,
    marginBottom: 24,
  },
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  forgot: {
    fontSize: 13,
    marginTop: 10,
    marginRight: -2,
    color: theme.colors.secondary,
  },
  rememberMe: {
    fontSize: 13,
    marginTop: 10,
    marginLeft: 10,
    color: theme.colors.secondary,
  },
  link: {
    fontWeight: '100',
    color: '#9872FA',
    lineHeight: 30,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'white',
    // backgroundColor:"red",
    marginTop: 50,
    width: '100%',
  },
  imageThumbnail: {
    justifyContent: 'center',
    alignItems: 'center',
    // width:"100%",
    alignSelf: 'center',

    flex: 1,

    // height: "100%",
    // backgroundColor:"red"
  },
});

export default addPhotoVideo;
