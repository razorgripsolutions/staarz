import React, {useEffect} from 'react';
import {View, Text} from 'react-native';
import Logo from '../components/Logo';
import Background from '../components/Background';

function StartScreen({navigation}) {
  useEffect(() => {
    setTimeout(() => {
      navigation.navigate('LoginScreen');
      // }
    }, 1000);
  }, []);

  return (
    <Background>
      <Logo />
      <View
        style={{
          width: '90%',
          alignItems: 'center',
          justifyContent: 'center',
          top: -20,
        }}
      >
        <Text
          style={{
            color: '#7643F8',
            fontSize: 45,
            letterSpacing: 1,
            lineHeight: 74,
            fontFamily: 'MPLUS1p-Bold',
          }}
        >
          STAARZ
        </Text>
      </View>
    </Background>
  );
}

export default StartScreen;
