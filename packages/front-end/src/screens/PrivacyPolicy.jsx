import React, {useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  Platform,
} from 'react-native';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {CircleFade} from 'react-native-animated-spinkit';
import {WebView} from 'react-native-webview';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
function PrivacyPolicy({navigation}) {
  // Data Source for the SearchableDropdown
  const [serverData, setServerData] = useState([]);
  const [data, setData] = useState([]);
  const [isloading, setisloading] = useState(true);

  return (
    <SafeAreaView style={styles.container}>
      <View style={{flex: 1}}>
        <View
          style={{
            flexDirection: 'row',
            top: Platform.OS === 'ios' ? 0 : 0,
            alignItems: 'center',
            justifyContent: 'center',
            width: '100%',
            backgroundColor: '#fff',
            zIndex: 10,
          }}
        >
          <View style={{width: '20%'}}>
            <TouchableOpacity onPress={() => navigation.goBack()} style={{}}>
              <FontAwesome
                name="angle-left"
                size={25}
                color="#000"
                style={{left: 20}}
              />
            </TouchableOpacity>
          </View>
          <View
            style={{
              width: '60%',
              alignItems: 'center',
              flexDirection: 'row',
              justifyContent: 'center',
            }}
          >
            <Image
              source={require('../assets/starzlogo.png')}
              style={sideLogoStyles.imageLogo}
            />
            {/* <Image source={require('../assets/STAARZ_109X19_black.png')} style={sideLogoStyles.image} /> */}
            <Text
              style={{
                fontSize: 22,
                fontWeight: Platform.OS === 'ios' ? '800' : 'bold',
                color: '#243443',
                left: 5,
              }}
            >
              STAARZ
            </Text>
          </View>
          <View style={{backgroundColor: 'red', width: '20%'}} />
        </View>
        <WebView
          source={{uri: 'http://www.cavagroup.eu/privacy-policy/'}}
          javaScriptEnabled
          // For the Cache
          domStorageEnabled
          // View to show while loading the webpage
          renderLoading={LoadingIndicatorView}
          // Want to show the view or not
          startInLoadingState
        />
      </View>
    </SafeAreaView>
  );
}

function LoadingIndicatorView() {
  return (
    <View style={{flex: 1}}>
      <CircleFade
        size={110}
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          alignSelf: 'center',
          top: -40,
        }}
        color="#5E2EBA"
      />
    </View>
  );
}
function StaarzLogo() {
  return (
    <View style={sideLogoStyles.container}>
      <Image
        source={require('../assets/starzlogo_23X26.png')}
        style={sideLogoStyles.imageLogo}
      />
      <Image
        source={require('../assets/STAARZ_109X19_black.png')}
        style={sideLogoStyles.image}
      />
    </View>
  );
}

const sideLogoStyles = StyleSheet.create({
  image: {
    marginTop: 5,
    marginLeft: 20,
  },
  imageLogo: {
    height: hp(3.5),
    width: hp(3.5),
    // marginTop: 8,
    // marginBottom: 8,
  },
  container: {
    // position: 'absolute',
    // top:Platform.OS !== "ios"?5 : 0+getStatusBarHeight(),
    // alignItems:'center',
    // flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    // backgroundColor:"white",
    // zIndex:10,
    width: '100%',
  },
});

export default PrivacyPolicy;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    padding: 10,
  },
  titleText: {
    padding: 8,
    fontSize: 16,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  headingText: {
    padding: 8,
  },
});
