/* eslint-disable react/no-unused-class-component-methods */
/* eslint-disable no-prototype-builtins */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-unused-state */
/* eslint-disable react/no-access-state-in-setstate */
import React, {Component} from 'react';
import {Platform, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import RtcEngine, {
  RtcLocalView,
  RtcRemoteView,
  VideoRenderMode,
} from 'react-native-agora';
import RtmEngine from 'agora-react-native-rtm';
import {
  authenticateWithToken,
  loadHistory,
  messageSubscription,
  onMessage,
} from '../components/chat';
import requestCameraAndAudioPermission from '../components/permissions';
import styles from '../components/Style2';
// import Callkeepscreen  from '../components/callkeepscreen';
import {url} from '../const/urlDirectory';
// import RNCallKeep from 'react-native-callkeep';
const Sound = require('react-native-sound');
// interface Props {}
const URL = 'http://52.3.61.123:8080'; // enter your server url here
const UID = 0;
/**
 * @property peerIds Array for storing connected peers
 * @property appId
 * @property channelName Channel Name for the current session
 * @property joinSucceed State variable for storing success
 */
// interface State {
//   appId: string;
//   token: string;
//   channelName: string;
//   joinSucceed: boolean;
//   peerIds: number[];
// }
const axios = require('axios').default;

class voiceCall extends Component {
  // _rtcEngine = RtcEngine;
  // _rtmEngine = RtmEngine;
  // _engine?: RtcEngine;
  // _rtmEngine = '';
  constructor(props) {
    console.log(JSON.stringify(props.route.params));
    super(props);

    this.state = {
      appId: '89aedac6feff41f3b7f592bf8de82fd0',
      token: '',
      channelName: props.route.params.channelId,
      joinSucceed: false,
      userid: props.route.params.uId,
      receiverId: props.route.params.recevierId,
      peerIds: [],
      callpopScreen: false,
      inCommingScreen: false,
    };

    if (Platform.OS === 'android') {
      // Request required permissions from Android
      requestCameraAndAudioPermission().then(() => {
        console.log('requested!');
      });
    }

    // RNCallKeep.setup({
    //   ios: {
    //     appName: 'Staarz',
    //   },
    //   android: {
    //      alertTitle: 'Permissions required',
    //     alertDescription: 'This application needs to access your phone accounts',
    //     cancelButton: 'Cancel',
    //     okButton: 'ok',
    //   },
    // });
  }

  async componentDidMount() {
    const token = await this.fetchToken(
      this.state.userid,
      this.state.channelName,
      1,
    );
    // alert("ok")

    console.log(
      `ChannelName in FetchToken componentDidMount==>${this.state.channelName}user Id in FetchToken of componentDidMount===>${this.state.userid}`,
    );
    // console.log("token in in FetchToken componentDidMount==>"+token)
    console.log(
      `token in in FetchToken componentDidMount--------==>${token.rtcToken}`,
    );
    await this.setState({token});

    this.newFuntion();

    // await this.initRTM()
    //  console.log('ChannelName in FetchToken componentDidMount==>'+this.state.channelName+'user Id in FetchToken of componentDidMount===>'+this.state.userid)
    //  //alert('ChannelName in FetchToken componentDidMount==>'+this.state.channelName+'user Id in FetchToken of componentDidMount===>'+this.state.userid)

    //  alert("token in in FetchToken componentDidMount==>"+token.rtcToken)

    //  RNCallKeep.addEventListener('answerCall', answerCall);
    //  RNCallKeep.addEventListener('didPerformDTMFAction', didPerformDTMFAction);
    //  RNCallKeep.addEventListener('didReceiveStartCallAction', didReceiveStartCallAction);
    //  RNCallKeep.addEventListener('didPerformSetMutedCallAction', didPerformSetMutedCallAction);
    //  RNCallKeep.addEventListener('didToggleHoldCallAction', didToggleHoldCallAction);
    //  RNCallKeep.addEventListener('endCall', endCall);

    //  return () => {
    //    RNCallKeep.removeEventListener('answerCall', answerCall);
    //    RNCallKeep.removeEventListener('didPerformDTMFAction', didPerformDTMFAction);
    //    RNCallKeep.removeEventListener('didReceiveStartCallAction', didReceiveStartCallAction);
    //    RNCallKeep.removeEventListener('didPerformSetMutedCallAction', didPerformSetMutedCallAction);
    //    RNCallKeep.removeEventListener('didToggleHoldCallAction', didToggleHoldCallAction);
    //    RNCallKeep.removeEventListener('endCall', endCall);
    //  }
  }

  newFuntion = async () => {
    // alert("0k")
    await this.initRTC();
    await this.getChatMassage();
  };

  componentWillUnmount() {
    if (this.state.subscription) {
      this.state.subscription.unsubscribe();
      // alert("yes");
    }
    // this._rtmEngine?.destroyClient();
    this._rtcEngine?.destroy();
  }

  getChatMassage = async () => {
    console.log(
      `${this.props.route.params.authtiken}===${this.props.route.params.rchat}===${this.props.route.params.channelId}`,
    );

    const chatToken = this.props.route.params.authtiken;
    const userId = this.props.route.params.rchat;

    authenticateWithToken(chatToken, userId);

    let lastTime = null;
    const {channelId} = this.props.route.params;

    loadHistory(channelId, lastTime, (message) => {
      if (message.error) {
        // error
        console.log(`app chat => ${JSON.stringify(message.error)}`);
      } else if (message.result.messages.length > 0) {
        // console.log("loadHistory msg", JSON.stringify(message));
        lastTime = message.result.messages[0].ts;
        this.setState({
          messages: message.result.messages
            .filter((key) => !key.hasOwnProperty('t'))
            .reverse(),
          isLoading: false,
        });
      } else {
        this.setState({isLoading: false});
      }
    });

    const subscription = messageSubscription(channelId, (message) => {
      //  console.log("getSubscription msg", message);
      // console.log("subscription agr", message.fields.args.filter((key) => !key.hasOwnProperty('t')))
      // add message to local state if not empty
      this.setState({
        messages: this.state.messages.concat(
          message.fields.args.filter((key) => !key.hasOwnProperty('t')),
        ),
      });
    });
    // console.log("subscription API => "+ subscription);
    this.setState({subscription});
    onMessage(channelId, (message) => {
      if (message.fields !== undefined) {
        if (message.fields.args !== undefined) {
          if (message.fields.args.length !== 0) {
            if (message.fields.args[0].u !== undefined) {
              const channelIdOnMessage = message.fields.args[0].rid;
              const userIdOnMessage = message.fields.args[0].u._id;
              const mainNameOnMessage = message.fields.args[0].u.name;
              const userNameOnMessage = message.fields.args[0].u.username;
              const incomingOnMessage = message.fields.args[0].msg;
            }
          }
        }
      }

      //  alert(JSON.stringify(message.result.messages))
    });
  };

  async chatSendMessage() {
    // let channelId = channelId;

    const details = {
      channelId: this.props.route.params.channelId,
      msg: `starzz0bdaf1377adee7f38d63866ebd15${this.state.userid}`,
      userId: this.props.route.params.rchat,
      authToken: this.props.route.params.authtiken,
    };
    console.log(`user details=>${JSON.stringify(details)}`);
    // this.setState({ isLoading: true })
    // return false;
    this.setState({msg: ''});
    await fetch(url.sendMsg, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(details),
    })
      .then((response) => response.json())
      .then((responseData) => {
        console.log(JSON.stringify(responseData));
        if (responseData.status === 200) {
          // this.setState({ isLoading: false, msg:"" });
        } else {
          //   setTimeout(() => {
          //     Alert.alert(
          //         'Alert',
          //         error,
          //         [
          //             // {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
          //             //  {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          //             { text: 'OK', onPress: () => this.setState({ isLoading: false }) },
          //         ],
          //         { cancelable: false }
          //     )
          // }, 100);
        }
      })
      .catch((error) => {
        setTimeout(() => {
          alert(
            'Alert',
            error,
            [
              // {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
              //  {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
              {text: 'OK', onPress: () => this.setState({isLoading: false})},
            ],
            {cancelable: false},
          );
        }, 100);
      });
  }

  fetchToken = (uid, channelName, tokenRole) =>
    // console.log('1. Wait for promise fetchToken...')
    new Promise(function (resolve) {
      alert('yes');
      //  console.log('2. Wait for promise fetchToken...')
      /*
        axios.post(URL+'/fetch_rtc_token', {
            uid: uid,
            channelName: channelName,
            role: tokenRole
        }, {
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
            }
        })
            .then(function (response) {
            //  console.log('3. Wait for promise fetchToken...')
             // console.log(JSON.stringify(response))
                const token = response.data.token;
                resolve(token);
            })
            .catch(function (error) {
             // console.log('4.Error for promise fetchToken...')
                console.log(error);
            });
// })
            */
      const that = this;
      console.log(
        `${URL}/rte/${channelName}/${tokenRole}/` +
          `uid` +
          `/${uid}?expireTime=60000`,
      );
      // return false
      fetch(
        `${URL}/rte/${channelName}/${tokenRole}/` +
          `uid` +
          `/${uid}?expireTime=60000`,
      )
        .then((response) => {
          response.json().then(async (data) => {
            console.log(
              `RTM both token => ${JSON.stringify(data)}  ${data.rtcToken}`,
            );
            // that._engine?.renewToken(data.rtcToken);
            const tokendata = data;
            resolve(tokendata);
          });
        })
        .catch((err) => {
          console.log('Fetch Error', err);
        });
    });

  initRTM = async () => {
    const {appId} = this.state;
    console.log(`appid==>${appId}`);
    this._rtmEngine = await new RtmEngine();

    // await this._rtmEngine.createClient(appId).catch((e) => console.log("creatclient error==>"+e));

    this._rtmEngine.on('error', (evt) => {
      console.log(`_rtmEngine error => ${evt}`);
    });

    this._rtmEngine.on('connectionStateChanged', (evt) => {
      // alert("connectionStateChanged"+evt)
      console.log(`connectionStateChanged${JSON.stringify(evt)}`);
      if (evt.state === 3) {
        // CallKeep;
      }
      // this._rtmEngine.sendLocalInvitation(this.state.receiverId,this.state.channelName);
    });

    this._rtmEngine.on('localInvitationReceivedByPeer', (evt) => {
      // alert("localInvitationReceivedByPeer"+evt)
      console.log(`localInvitationReceivedByPeer${JSON.stringify(evt)}`);
      // this._rtmEngine.acceptRemoteInvitation(this.state.receiverId,this.state.channelName);
    });

    this._rtmEngine.on('remoteInvitationReceived', async (evt) => {
      // alert("localInvitationReceivedByPeer"+evt)
      console.log(`remoteInvitationReceived ${JSON.stringify(evt)}`);

      const acceptRemoteInvitation =
        await this._rtmEngine.acceptRemoteInvitation({
          uid: this.state.receiverId.toString(),
          channelId: this.state.channelName,
          response: 'testing accept',
        });
      //  console.log("acceptRemoteInvitation API => "+acceptRemoteInvitation);
      // this._rtmEngine.acceptRemoteInvitation(this.state.receiverId,this.state.channelName);
    });

    this._rtmEngine.on('remoteInvitationAccepted', (evt) => {
      // alert("localInvitationReceivedByPeer"+evt)
      console.log(`remoteInvitationAccepted => ${JSON.stringify(evt)}`);
      // this._rtmEngine.acceptRemoteInvitation(this.state.receiverId,this.state.channelName);
    });

    this._rtmEngine.on('remoteInvitationCanceled', (evt) => {
      // alert("localInvitationReceivedByPeer"+evt)
      console.log(`remoteInvitationCanceled => ${JSON.stringify(evt)}`);
      // this._rtmEngine.acceptRemoteInvitation(this.state.receiverId,this.state.channelName);
    });

    this._rtmEngine.on('remoteInvitationFailure', (evt) => {
      // alert("localInvitationReceivedByPeer"+evt)
      console.log(`remoteInvitationFailure => ${JSON.stringify(evt)}`);
      // this._rtmEngine.acceptRemoteInvitation(this.state.receiverId,this.state.channelName);
    });

    this._rtmEngine.on('remoteInvitationRefused', (evt) => {
      // alert("localInvitationReceivedByPeer"+evt)
      console.log(`remoteInvitationRefused => ${JSON.stringify(evt)}`);
      // this._rtmEngine.acceptRemoteInvitation(this.state.receiverId,this.state.channelName);
    });

    this._rtmEngine.on('localInvitationAccepted', (evt) => {
      // alert("localInvitationReceivedByPeer"+evt)
      console.log(`localInvitationAccepted => ${JSON.stringify(evt)}`);
      // this._rtmEngine.acceptRemoteInvitation(this.state.receiverId,this.state.channelName);
    });
    this._rtmEngine.on('localInvitationReceivedByPeer', (evt) => {
      // alert("localInvitationReceivedByPeer"+evt)
      console.log(`localInvitationReceivedByPeer => ${JSON.stringify(evt)}`);
      // this._rtmEngine.acceptRemoteInvitation(this.state.receiverId,this.state.channelName);
    });
    // await this._rtmEngine.

    // rtmMannager = this._rtmEngine.createLocalInvitation
    // this._rtmEngine.sendLocalInvitation(this.state.userid,this.state.channelName)

    // console.log(Cient)
    // console.log(this.state.userid.toString());
    // //  console.log(typeof (this.state.userid.toString()))

    //  await    this._rtmEngine.createClient(appId);
    //  //this.uid = uid;
    //  this._rtmEngine.login({
    //    uid: this.state.uid,
    //    token: this.state.token,
    //  });
    await this._rtmEngine
      .createClient(appId)
      .catch((e) => console.log(`createClient => ${e}`));
  };

  /**
   * @name init
   * @description Function to initialize the Rtc Engine, attach event listeners and actions
   */
  initRTC = async () => {
    const {appId} = this.state;
    this._engine = await RtcEngine.create(appId);

    // this._engine.setClientRole('host')
    // RtmCallManager = await this._rtmEngine.createClient(appId).catch((e) => console.log(e));
    // this._rtmEngine.on('error', (evt) => {
    //   console.log(evt);
    // });
    await this._engine.enableVideo();

    this._engine.addListener('Warning', (warn) => {
      console.log('Warning', warn);
    });

    this._engine.addListener('Error', (err) => {
      console.log('Error', err);
    });

    this._engine.addListener(
      'TokenPrivilegeWillExpire',
      // - WHAT DO THEY MEAN
      // eslint-disable-next-line no-undef
      (token = async () => {
        console.log('trigger');
        const token1 = await this.fetchToken(
          this.state.userid,
          this.state.channelName,
          1,
        );
        this._engine.renewToken(token1);
        console.log(`Token Renewd==>${JSON.stringify(token1)}`);
      }),
    );

    this._engine.addListener('UserJoined', (uid, elapsed) => {
      console.log('UserJoined==>', uid, elapsed);
      // Get current peer IDs
      const {peerIds} = this.state;
      // If new user
      if (peerIds.indexOf(uid) === -1) {
        this.setState({
          // Add peer ID to state array
          peerIds: [...peerIds, uid],
        });
      }
    });

    this._engine.addListener('UserOffline', (uid, reason) => {
      console.log('UserOffline ==>', uid, reason);
      const {peerIds} = this.state;
      this.setState({
        // Remove peer ID from state array
        peerIds: peerIds.filter((id) => id !== uid),
      });
    });

    // If Local user joins RTC channel
    this._engine.addListener('JoinChannelSuccess', (channel, uid, elapsed) => {
      console.log(
        'JoinChannelSuccess channel name and uid provided by agora server',
        channel,
        uid,
        elapsed,
      );
      this.setState({
        joinSucceed: true,
        callpopScreen: true,
      });

      //  this.chatSendMessage();
      // this.PlayRemoteURLSoundFile();
      // alert('JoinChannelSuccess channel name and uid provided by agora server ==>', channel, uid, elapsed);
      // console.log('ChannelName in  JoinChannelSuccess ==>'+this.state.channelName+'user Id in JoinChannelSuccess===>'+this.state.userid)
      // alert('ChannelName in  JoinChannelSuccess==>'+this.state.channelName+'user Id in  JoinChannelSuccess===>'+this.state.userid)
      // Set state variable to true

      // this._rtmEngine.sendLocalInvitation(this.state.userid,this.state.channelName,'Hiii');
      // this.inviteCall(this.state.receiverId,this.state.channelName)
      // this.inviteCall(this.state.userid,this.state.channelName)
    });

    // this._engine.onTokenPrivilegeWillExpire(  )
    // let to =   await this._engine.renewToken(token);
    // console.log(to)
    //   this._engine.on("token-privilege-will-expire", async function () {
    //     let token = await fetchToken(uid, options.channel, 1);

    // });
  };

  PlayRemoteURLSoundFile = () => {
    Sound.setCategory('Playback');
    const myRemoteSound = new Sound(
      'https://www.soundjay.com/ambient/sounds/boarding-accouncement-1.mp3',
      null,
      (error) => {
        if (error) {
          console.log(error);
        } else {
          myRemoteSound.play((success) => {
            if (success) {
              console.log('Sound playing');
              this.endCall();
              this.setState({ActiveCall: 0});
            } else {
              console.log('Issue playing file');
            }
          });
        }
      },
    );
    myRemoteSound.setVolume(0.9);
    myRemoteSound.release();
  };

  /**
   *
   * @name startCall
   * @description Function to start the call
   */
  startCall = async () => {
    // console.log('ChannelName2==>'+this.state.channelName)
    // let token = await this.fetchToken(0, this.state.channelName, 1);

    //  console.log("token==>"+token)
    // await client.join(options.appId, options.channel, token, uid);
    //   // Join Channel using null token and channel name
    //     ///   inviteCall()

    /// console.log(token)

    console.log(
      `ChannelName in  joinChannel ==>${this.state.channelName}user Id in joinChannel===>${this.state.userid}===${this.state.token.rtcToken}`,
    );
    //  alert('ChannelName in  joinChannel==>'+this.state.channelName+'user Id in FetchToken of joinChannel===>'+this.state.userid)
    await this._engine?.joinChannel(
      this.state.token.rtcToken,
      this.state.channelName,
      null,
      this.state.userid,
    );
    // await this._rtmEngine?.joinChannel(this.state.channelName)
    //   .catch((e) => console.log(e));

    // let login_rtmEngine = await this._rtmEngine?.login({uid:this.state.userid.toString(),token: this.state.token.rtmToken}).catch((e) => console.log("Login error==>"+JSON.stringify(e)));
    // console.log("login_rtmEngine API => "+login_rtmEngine)
    // // console.log("_rtmEngine init => "+this._rtmEngine);

    // // await this._rtmEngine
    // //     ?.login({ uid: myUsername })
    // //     .catch((e) => console.log(e));

    // let joinChannel_rtmEngine = await this._rtmEngine?.joinChannel(this.state.channelName)
    //   .catch((e) => console.log("joinChannel_rtmEngine error==>"+e));
    //   console.log("joinChannel_rtmEngine API => "+joinChannel_rtmEngine)

    // console.log("ReceverId===>"+this.state.receiverId.toString()+"Channel Name==>"+this.state.channelName)

    //   await this._rtmEngine.sendLocalInvitation(this.state.receiverId,this.state.channelName);
    //  await this._rtmEngine.acceptRemoteInvitation(this.state.receiverId,this.state.channelName);
    //
    //  let sendLocalInvitation = await this._rtmEngine.sendLocalInvitation({uid:this.state.receiverId.toString(),channelId: this.state.channelName, content : "hiiiiiiiiiii"}).catch((e) => console.log("sendLocalInvitation error==>"+JSON.stringify(e)));
    //  console.log("sendLocalInvitation API => "+sendLocalInvitation)

    //   let {channelName} = this.state;
    //   //console.log(channelName+"=="+this.state.channelName)let that = this;

    // fetch(URL + '/rtc/' + channelName + '/publisher/uid/' + UID)
    //   .then(function (response) {
    //     response.json().then(async function (data) {
    //       console.log(  data.rtcToken)
    //     //   await this._engine?.joinChannel(
    //     //     this.state.token,
    //     //     this.state.channelName,
    //     //     null,
    //     //     0,
    //     //   );
    //      });
    //   })
    //   .catch(function (err) {
    //     console.log('Fetch Error', err);
    //   });

    //   await this._engine?.joinChannel(
    //     this.state.token,
    //     this.state.channelName,
    //     null,
    //     0,
    //   );

    // await this._engine?.joinChannel(
    //   // '00689aedac6feff41f3b7f592bf8de82fd0IAD/47j9E+k7jI7O8HpTg/7jp2pVHBF++6GK2uM+yeCP6o8OExoAAAAAEABc42rkrpCvYAEAAQCukK9g',
    //     data.rtcToken,
    //    this.state.channelName,
    //    null,
    //    0,
    //  );
  };

  /**
   * @name endCall
   * @description Function to end the call
   */
  endCall = async () => {
    await this._engine?.leaveChannel();
    this.setState({peerIds: [], joinSucceed: false});
  };

  render() {
    return (
      //  this.state.callpopScreen === true?
      //  <View style={styles.max}>
      <View style={styles.max}>
        <View style={styles.buttonHolder}>
          <TouchableOpacity onPress={this.startCall} style={styles.button}>
            <Text style={styles.buttonText}> Start Call </Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.endCall} style={styles.button}>
            <Text style={styles.buttonText}> End Call </Text>
          </TouchableOpacity>
        </View>
        {this._renderVideos()}
        {/* <CallKeep /> */}
      </View>
      // </View>:<View style={styles.max}><Callkeepscreen/></View>
    );
  }

  _renderVideos = () => {
    const {joinSucceed} = this.state;
    return joinSucceed ? (
      <View style={styles.fullView}>
        <RtcLocalView.SurfaceView
          style={styles.max}
          channelId={this.state.channelName}
          renderMode={VideoRenderMode.Hidden}
        />
        {this._renderRemoteVideos()}
      </View>
    ) : null;
  };

  _renderRemoteVideos = () => {
    const {peerIds} = this.state;
    return (
      <ScrollView
        style={styles.remoteContainer}
        contentContainerStyle={{paddingHorizontal: 2.5}}
        horizontal
      >
        {peerIds.map((value) => (
          <RtcRemoteView.SurfaceView
            style={styles.remote}
            uid={value}
            channelId={this.state.channelName}
            renderMode={VideoRenderMode.Hidden}
            zOrderMediaOverlay
          />
        ))}
      </ScrollView>
    );
  };
}
export default voiceCall;
