import React, {Fragment} from 'react';
import {View, Text} from 'react-native';
import Background from '../../components/Background';
import Logo from '../../components/Logo';

const Loader = () => {
  return (
    <Fragment>
      <Background>
        <Logo />
        <View
          style={{
            width: '90%',
            alignItems: 'center',
            justifyContent: 'center',
            top: -20,
          }}
        >
          <Text
            style={{
              color: '#7643F8',
              fontSize: 45,
              letterSpacing: 1,
              lineHeight: 74,
              fontFamily: 'MPLUS1p-Bold',
            }}
          >
            STAARY
          </Text>
        </View>
      </Background>
    </Fragment>
  );
};

export default Loader;
