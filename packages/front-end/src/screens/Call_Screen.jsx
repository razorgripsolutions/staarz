/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/no-unused-class-component-methods */
/* eslint-disable react/no-deprecated */
/* eslint-disable react/no-unused-state */
import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  BackHandler,
  Platform,
  Dimensions,
  AppState,
  SafeAreaView,
} from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import AntDesign from 'react-native-vector-icons/AntDesign';
// import { authenticateWithToken, loadHistory, messageSubscription, clear, onMessage, getObservable } from '../components/chat';
// import { authenticateWithToken, loadHistory, messageSubscription, clear, sendMessage, connectToServer, setStatus, chatkeepAlive, onMessage } from '../components/chat';
import moment from 'moment';
import BackgroundTimer from 'react-native-background-timer';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import RtcEngine, {
  RtcLocalView,
  RtcRemoteView,
  VideoRenderMode,
} from 'react-native-agora';
import AsyncStorage from '@react-native-async-storage/async-storage';
import api_service from '../const/api_service';
import {url, backendBaseUrl} from '../const/urlDirectory';
// import useActiveCall from './use-active-call';
import requestCameraAndAudioPermission from '../components/permissions';
import styles2 from '../components/Style2';
import CallerScreen from '../components/CallerScreen';
import CallPopupScreen from '../components/CallPopupScreen';
import OutGoingVideo from '../components/OutGoingVideo';
// import RNCallKeep from 'react-native-callkeep';
import IncomingCallScreen from '../components/IncomingCallScreen';
import VideoCallIncoming from '../components/VideoCallIncoming';

const FirebseServer_key =
  'AAAAyi-nYt8:APA91bFuAwBsarOi6lFzlEfpnaTE-7o-6AyAOqfEArBFRZcObXOmDwAGTDkJrAX8EQSAlcxyYGZRdHX9duhljyTDQZoCch3mTXdGZiiruXKByMnKFiR1HpPgCxjWPL8OdEzJg3GYWMMm';
const Sound = require('react-native-sound');

const URL = 'http://52.3.61.123:8080'; // enter your server url here
const axios = require('axios').default;

let whoosh = '';
let outgoingRing = '';
const {width} = Dimensions.get('window'); // full width
const {height} = Dimensions.get('window'); // full height
BackgroundTimer.start();
const hitSlop = {top: 10, left: 10, right: 10, bottom: 10};

class Call_Screen extends Component {
  constructor(props) {
    super(props);
    const channelId = props.route.params.chnnelId;
    const {username} = props.route.params;
    const {reciverId} = props.route.params;
    // this.setActiveCall = React.useContext(Store);
    this.state = {
      userid: '',
      user: [],
      AudioCall: 0,
      modelVisiviblcallfuntion: false,
      muteCall: false,
      speakerCall: false,
      videCAlltype: this.props.route.params.videCAlltype,
      calltype: this.props.route.params.calltype,
      call: this.props.route.params.call,
      userJoined: false,
      messages: [],
      peerIds: [],
      firebase_token: '',
      msg: '',
      hideVideoFuntion: true,
      callStart: 0,
      subscription: '',
      profile_pic: '',
      profileData: '',
      msgPlaceholder: '',
      isLoading: false,
      heldCalls: '',
      setLog: '',
      calls: '',
      appState: AppState.currentState,
      setCallMuted: '',
      activevideoCall: 0,
      outGoinVideo: 0,
      filerdMessages: [],
      ActiveCall: 0,
      ActiveCallstate: 0,
      soundActive: 0,
      incomingactivevideoCall: 0,
      showOutGoingCSren: 0,
      VideoModelScreen: false,
      modelVisiviblBlock: false,
      showScreen: 0,
      ShowVideoCAllScreen: 0,
      appId: '89aedac6feff41f3b7f592bf8de82fd0',
      token: '',
      channelName: this.props.route.params.chnnelId,
      receiverId: props.route.params.recevierId,
      authtiken: '',
      rchat: '',
      joinSucceed: false,
      onatherUserId: '',
      keepAliveSubscription: '',
    };
    if (Platform.OS === 'android') {
      requestCameraAndAudioPermission().then(() => {
        console.log('requested!');
      });
    }
  }

  PlayoutgoingBusyRing = async () => {
    Sound.setCategory('Playback');
    outgoingRing = new Sound(
      Platform.OS === 'ios' ? 'numberbusy.mp3' : 'numberbusy1.mp3',
      Sound.MAIN_BUNDLE,
      (error) => {
        if (error) {
          console.log('failed to load the sound-----------------', error);
          // alert(JSON.stringify(error))
          return;
        }

        // loaded successfully
        console.log(
          `duration in seconds: ${outgoingRing.getDuration()}number of channels: ${outgoingRing.getNumberOfChannels()}`,
        );

        // Play the sound with an onEnd callback
        outgoingRing.play(async (success) => {
          if (success) {
            console.log('successfully finished playing');

            this.updateCallingStatus();
            this.endCall();
            // alert("outgoingRing.play")
            this.setState({ActiveCall: 0, activevideoCall: 0});
          } else {
            console.log('playback failed due to audio decoding errors');
          }
        });
      },
    );

    // Reduce the volume by half
    outgoingRing.setVolume(0.5);

    // Position the sound to the full right in a stereo field
    outgoingRing.setPan(1);

    // Loop indefinitely until stop() is called
    outgoingRing.setNumberOfLoops(-1);

    outgoingRing.setCurrentTime(2.5);
    outgoingRing.release();
  };

  // ComponentDidmount
  async componentDidMount() {
    // alert(this.props.route.params.profilePicture)
    // console.log("this.props.route.params.chnnelId---------------------------", this.props.route.params.chnnelId)
    const user = await AsyncStorage.getItem('user');
    const user1 = JSON.parse(user);
    // alert(user1.fullName)
    // console.log("user===============----user-------------->>>>>>>>>>>user>>>>>>>>>-------------", user1.fullName, user1.username)
    const userId = user1.userID;
    let rChatData = await AsyncStorage.getItem('rChatData');
    rChatData = JSON.parse(rChatData);

    // alert(JSON.stringify(rChatData.userId))
    // console.log("data new consloe data - adddesdd---------", rChatData.authToken)
    this.setState({
      userid: userId,
      user: user1,
      authtiken: rChatData.authToken,
      rchat: rChatData.userId,
    });
    await this.getrtcrtmToken();
    // let chatToken = this.props.route.params.rchatauthToken;
    const otherUser = this.props.route.params.reciverId;
    // console.log("reciverId--------------------------------------", otherUser)
    // console.log("userLoginId--------------------------------------", this.state.userid)
    this.setState({
      onatherUserId: otherUser,
    });

    await this.getfcmtokenApi();
    await this.newFuntion();
    if (this.state.calltype === 20) {
      this.stopoutgoingRing();
      this.stopRemoteURLSoundFile();
      await this.endCall();
    } else if (this.state.calltype === 1) {
      if (this.state.ActiveCall === 0) {
        // this.PlayRemoteURLSoundFile()
        this.setState({
          ActiveCall: 1,
          showScreen: 1,
          showOutGoingCSren: 0,
          ShowVideoCAllScreen: 0,
          AudioCall: 0,
        });
      } else {
        // ???
      }
    } else if (this.state.calltype === 2) {
      if (this.props.route.params.status !== 'busy') {
        // alert('ok')

        if (this.state.ActiveCall === 0) {
          // this.PlayRemoteURLSoundFile()
          this.startCall();
          this.PlayRemoteURLSoundFile();
          this.firebseApi({calltype: 3, Audio: 'Incoming Audio Call'});
          this.setState({
            ActiveCall: 0,
            showScreen: 1,
            activevideoCall: 1,
            ShowVideoCAllScreen: 1,
            calltype: 2,
          });
        } else {
          /// ???
        }
      } else {
        // const data = {
        //   loggedUserID: this.state.userid,
        //   userID: this.props.route.params.reciverId,
        //   status: 1
        // };
        // const urlPath = url.callActiveStatus;
        // // alert(urlPath)
        // const result = await api_service.post(urlPath, data);
        this.PlayoutgoingBusyRing();

        this.setState({
          ActiveCall: 0,
          showScreen: 1,
          activevideoCall: 1,
          ShowVideoCAllScreen: 1,
          calltype: 2,
        });
      }
    } else if (this.state.calltype === 3) {
      this.PlayRemoteURLSoundFile();
      // alert(this.state.calltype)
      this.setState({ActiveCall: 0, ShowVideoCAllScreen: 1, calltype: 3});
    } else if (this.state.calltype === 4) {
      if (this.props.route.params.status !== 'busy') {
        this.startCall();
        this.PlayRemoteURLSoundFile();
        this.firebseApi({calltype: 5, Audio: 'Incoming Video Call'});
        this.setState({ActiveCall: 1, AudioCall: 0});
      } else {
        this.PlayoutgoingBusyRing();
        this.setState({ActiveCall: 1, AudioCall: 0});
      }
    } else if (this.state.calltype === 5) {
      this.PlayRemoteURLSoundFile();
      this.setState({ShowVideoCAllScreen: 0, calltype: 5});
    } else if (this.state.calltype === 6) {
      /// ???
    } else {
      // alert("non")
    }
  }

  // eslint-disable-next-line class-methods-use-this
  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', () => true);
  }

  //  newFuntion
  newFuntion = async () => {
    await this.initRTC();
  };

  initRTC = async () => {
    console.log(
      'upsated -------------------------initrtc---------------------- fierst ',
    );
    const {appId} = this.state;
    this._engine = await RtcEngine.create(appId);
    await this._engine.enableVideo();
    this._engine.addListener('Warning', (warn) => {
      console.log('Warning', warn);
    });
    this._engine.addListener('ConnectionStateChanged', async (state) => {
      // alert("State==>" + state)
      // if (state === 1) {
      //   this.firebseApi1({ calltype: 10, Audio: "end remote call" })
      // } else { }
      // endCall()
    });

    this._engine.addListener('Error', (err) => {
      console.log('Error---------', err);
    });
    // this._engine.setLocalVoicePitch
    this._engine.addListener('TokenPrivilegeWillExpire', async () => {
      console.log('trigger');
      const token1 = await this.fetchToken(
        this.state.userid,
        this.state.channelName,
        1,
      );
      this._engine.renewToken(token1);
      console.log(`Token Renewd==>${JSON.stringify(token1)}`);
    });

    this._engine.addListener('UserJoined', async (uid, elapsed) => {
      console.log('UserJoined==>', uid, elapsed);

      const {peerIds} = this.state;
      this.stopRemoteURLSoundFile();
      this.stopoutgoingRing();
      // alert
      this.setState({
        userJoined: true,
        ShowVideoCAllScreen: 0,
        calltype: 7,
      });
      // If new user
      if (peerIds.indexOf(uid) === -1) {
        this.setState({
          peerIds: [...peerIds, uid],
          outGoinVideo: 1,
          call: 1,
          callStart: 1,
          // calltype: 6,
        });
      }
    });

    this._engine.addListener('UserOffline', async (uid, reason) => {
      // alert(reason)
      // await AsyncStorage.removeItem('StateActiveCall');
      // await AsyncStorage.setItem('StateActiveCall', "")
      // var StateActiveCall111 = await AsyncStorage.getItem('StateActiveCall')
      // alert(StateActiveCall111)

      console.log('UserOffline ==>', uid, reason);
      const {peerIds} = this.state;
      this.endCall();
      this.setState({
        // Remove peer ID from state array
        callpopScreen: false,
        ActiveCall: 0,
        showScreen: 0,
        activevideoCall: 0,
        outGoinVideo: 0,
        peerIds: peerIds.filter((id) => id !== uid),
      });
    });
    // If Local user joins RTC channel
    this._engine.addListener('JoinChannelSuccess', (channel, uid, elapsed) => {
      // alert("okl")
      // alert("ok")
      console.log(
        'JoinChannelSuccess channel name and uid provided by agora server',
        channel,
        uid,
        elapsed,
      );
      this.setState({
        joinSucceed: true,
        // calltype: 6,
        callpopScreen: true,
        //  ActiveCall:1,
        call: 1,
        callStart: 1,
      });
    });
  };

  getfcmtokenApi = async () => {
    const data = this.props.route.params.reciverId;
    const Token = await AsyncStorage.getItem('Token');
    // navigation.navigate("LoginScreen")
    const Token1 = JSON.parse(Token);
    console.log(
      'Token-callscreenn-dattatatattatatatat-----',
      JSON.stringify({
        token: Token1,
        userID: data,
        device_type: Platform.OS === 'ios' ? 'ios' : 'android',
      }),
    );

    return fetch(url.getFirebaseTokenByUserId, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        token: Token1,
        userID: data,
        device_type: Platform.OS === 'ios' ? 'ios' : 'androis',
      }),
    })
      .then((response) => response.json())
      .then((response) => {
        console.log(
          'fcmGet----------------fcmtoken  Api response-------------------',
          JSON.stringify(response.result[0].firebase_token),
        );
        this.setState({firebase_token: response.result[0].firebase_token});
      })
      .catch((error) => {
        console.log(error);
      });
  };

  firebseApi = (data) => {
    // alert(this.state.firebase_token)
    // console.warn(this.state.firebase_token)
    // console.warn("-----00000000---------------------", this.state.firebase_token)
    console.log(
      'Firebase Api video CAll Json ---- ',
      JSON.stringify({
        Authorization: FirebseServer_key,
        to: this.state.firebase_token,
        notification: {
          content_available: true,
          title: this.state.user.fullName,
          body: data.Audio,
          icon: 'ic_launcher',
          actions: '["Accept", "Reject"]',
          sound: 'default',
        },
        data: {
          title: 'ok',
          calltype: data.calltype,
          actions: '["Accept", "Reject"]',
          ChanelID: this.props.route.params.chnnelId,
          fullName: this.state.user.fullName,
          profilePicture:
            this.state.user.profilePicture === ''
              ? require('../assets/userpro.jpeg')
              : this.state.user.profilePicture,

          userIDOther: this.state.userid,
          DeletId: '1',
          content_available: true,
          priority: 'high',
          sound: 'file_example_mp3_1mg.mp3',

          icon: 'ic_launcher',
          time: 'ok',
        },
        android: {
          priority: 'high',
        },
        aps: {
          content_available: true,
        },
      }),
    );
    const dateAndTime = moment().format('hh:mm:ss');
    // console.log(`${hours}:${minutes}:${seconds}`)
    fetch('https://fcm.googleapis.com/fcm/send', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `key=${FirebseServer_key}`,
      },
      body: JSON.stringify({
        to: this.state.firebase_token,
        notification: {
          content_available: true,
          title: this.state.user.fullName,
          body: data.Audio,
          icon: 'ic_launcher',
          actions: '["Accept", "Reject"]',
          sound: 'deafult',
        },
        data: {
          title: 'React native App',
          calltype: data.calltype,
          actions: '["Accept", "Reject"]',
          ChanelID: this.props.route.params.chnnelId,
          fullName: this.state.user.fullName,
          profilePicture:
            this.state.user.profilePicture === ''
              ? require('../assets/userpro.jpeg')
              : this.state.user.profilePicture,

          userIDOther: this.state.userid,
          content_available: true,
          priority: 'high',
          sound: 'file_example_mp3_1mg.mp3',
          time: dateAndTime,
          icon: 'ic_launcher',
        },
        android: {
          priority: 'high',
        },
        aps: {
          content_available: true,
        },
      }),
    })
      .then((response) => response.json())
      .then((response) => {
        console.log(
          '----------firebase--------firebase-datadatdatdatdatdadatdatadtadatadt=================',
          response,
        );
      })
      .catch((error) => {
        console.log('----------firebase------error---', error);
      });
  };

  stopRemoteURLSoundFile = async () => {
    whoosh.stop(() => {});
    whoosh.release();
  };

  // stop_Outgoing_Sound
  stopoutgoingRing = async () => {
    outgoingRing.stop(() => {});
    outgoingRing.release();
  };

  //   @Incoming Sound
  PlayRemoteURLSoundFile = async () => {
    Sound.setCategory('Playback');
    whoosh = new Sound(
      Platform.OS === 'ios'
        ? 'file_example_MP3_1MG.mp3'
        : 'file_example_mp3_1mg.mp3',
      Sound.MAIN_BUNDLE,
      (error) => {
        if (error) {
          console.log('failed to load the sound', error);
          return;
        }
        // loaded successfully
        // console.log('duration in seconds: ' + whoosh.getDuration() + 'number of channels: ' + whoosh.getNumberOfChannels());
        // Play the sound with an onEnd callback
        whoosh.play(async (success) => {
          if (success) {
            console.log('successfully finished playing');
            // await AsyncStorage.setItem('StateActiveCall', "")
            // await AsyncStorage.removeItem('StateActiveCall');
            this.firebseApi({calltype: 20, Audio: 'End call.', DeletId: '1'});
            this.endCall();
            // alert("whoosh.play")
            this.setState({ActiveCall: 0, activevideoCall: 0});
          } else {
            console.log('playback failed due to audio decoding errors');
          }
        });
      },
    );
    // Reduce the volume by half
    whoosh.setVolume(0.5);
    // Position the sound to the full right in a stereo field
    whoosh.setPan(1);
    // Loop indefinitely until stop() is called
    whoosh.setNumberOfLoops(-1);
    whoosh.setCurrentTime(2.5);
    // whoosh.pause();
    // whoosh.stop(() => {
    //   whoosh.play();
    // });
    whoosh.release();
  };

  // OutGoinf Sound
  PlayoutgoingRing = async () => {
    Sound.setCategory('Playback');
    outgoingRing = new Sound(
      Platform.OS === 'ios'
        ? 'file_example_MP3_1MG.mp3'
        : 'file_example_mp3_1mg.mp3',
      Sound.MAIN_BUNDLE,
      (error) => {
        if (error) {
          console.log('failed to load the sound-----------------', error);
          // alert(JSON.stringify(error))
          return;
        }
        // loaded successfully
        console.log(
          `duration in seconds: ${outgoingRing.getDuration()}number of channels: ${outgoingRing.getNumberOfChannels()}`,
        );

        // Play the sound with an onEnd callback
        outgoingRing.play(async (success) => {
          if (success) {
            console.log('successfully finished playing');
            // await AsyncStorage.setItem('StateActiveCall', "")
            // await AsyncStorage.removeItem('StateActiveCall');
            // this.firebseApi({ calltype: 20, Audio: "End call.", DeletId: "1" })
            this.endCall();
            // alert("outgoingRing.play")
            this.setState({ActiveCall: 0, activevideoCall: 0});
          } else {
            console.log('playback failed due to audio decoding errors');
          }
        });
      },
    );

    // Reduce the volume by half
    outgoingRing.setVolume(0.5);

    // Position the sound to the full right in a stereo field
    outgoingRing.setPan(1);

    // Loop indefinitely until stop() is called
    outgoingRing.setNumberOfLoops(-1);

    outgoingRing.setCurrentTime(2.5);
    outgoingRing.release();
  };

  // getfcmtokenApi
  getrtcrtmToken = async () => {
    const token = await this.fetchToken(
      this.state.userid,
      this.props.route.params.chnnelId,
      1,
    );
    await this.setState({token});
  };

  fetchToken = async (uid, channelName, tokenRole) =>
    new Promise(function (resolve) {
      const that = this;
      console.log(
        '-----------------------------',
        `${URL}/rte/${channelName}/${tokenRole}/` +
          `uid` +
          `/${uid}?expireTime=60000`,
      );
      // return false
      fetch(
        `${URL}/rte/${channelName}/${tokenRole}/` +
          `uid` +
          `/${uid}?expireTime=60000`,
      )
        .then((response) => {
          response.json().then(async (data) => {
            console.log(
              `RTM both token => ${JSON.stringify(data)}  ${data.rtcToken}`,
            );
            const tokendata = data;
            resolve(tokendata);
          });
        })
        .catch((err) => {
          console.log('Fetch Error', err);
        });
    });

  endCall = async () => {
    // alert("ok")
    if (this.props.route.params.status === 'busy') {
      this.stopRemoteURLSoundFile();
      this.stopoutgoingRing();
      // this.
      await this._engine?.leaveChannel();
      // this.props.navigation.dispatch(NavigationActions.back())
      this.setState({
        peerIds: [],
        calltype: 0,
        outGoinVideo: 0,
        joinSucceed: false,
        callpopScreen: false,
        ActiveCall: 0,
        showOutGoingCSren: 0,
        showScreen: 0,
        activevideoCall: 0,
        ShowVideoCAllScreen: 0,
        userJoined: false,
      });
      // this.setState({})
      this.props.navigation.goBack();
      // this.props.navigation.navigate('BottomTabs')
      this.componentDidMount();
    } else {
      // await AsyncStorage.setItem('StateActiveCall', "")
      // await AsyncStorage.removeItem('StateActiveCall')
      // const data = await AsyncStorage.getItem('StateActiveCall')
      // alert(data)
      await this.updateCallingStatus();
      this.stopRemoteURLSoundFile();
      this.stopoutgoingRing();
      // this.
      await this._engine?.leaveChannel();
      // this.props.navigation.dispatch(NavigationActions.back())
      this.setState({
        peerIds: [],
        calltype: 0,
        outGoinVideo: 0,
        joinSucceed: false,
        callpopScreen: false,
        ActiveCall: 0,
        showOutGoingCSren: 0,
        showScreen: 0,
        activevideoCall: 0,
        ShowVideoCAllScreen: 0,
        userJoined: false,
      });
      // this.setState({})
      this.props.navigation.goBack();
      // this.props.navigation.navigate('BottomTabs')
      this.componentDidMount();
    }
    console.log('cancelcal--------------------------------');
  };

  //
  startCall = async () => {
    console.log('token==------------------------------------------>');
    // alert(this.state.userid)
    console.log(
      `ChannelName in  joinChannel ==>${this.state.channelName}user Id in joinChannel===>${this.state.userid}===${this.state.token.rtcToken}`,
    );
    await this._engine?.joinChannel(
      this.state.token.rtcToken,
      this.state.channelName,
      null,
      this.state.userid,
    );
  };
  //  videoAccept

  videoAccept = async () => {
    this.startCall();
    this.stopRemoteURLSoundFile();
    await this.setState({ActiveCall: 1, ShowVideoCAllScreen: 0, calltype: 7});
  };

  incomingCAll = () => {
    // alert("ok")
    this.startCall();
  };

  endCall1 = async () => {
    if (this.props.route.params.status === 'busy') {
      // this.firebseApi({ calltype: 20, Audio: "End call.", DeletId: "1" })
      this.stopRemoteURLSoundFile();
      this.stopoutgoingRing();
      this.endCall();
      this.componentDidMount();
    } else {
      this.updateCallingStatus();
      this.firebseApi({calltype: 20, Audio: 'End call.', DeletId: '1'});
      this.stopRemoteURLSoundFile();
      this.stopoutgoingRing();
      this.endCall();
      this.componentDidMount();
    }
    // this.firebseApi({ calltype: 20, Audio: "End call.", DeletId: "1" })
    // this.stopRemoteURLSoundFile()
    // this.stopoutgoingRing()
    // this.endCall()
    // this.componentDidMount()
    // this.setState({ ShowVideoCAllScreen: 0, activevideoCall: 0, ActiveCall: 0 })
  };

  updateCallingStatus = async () => {
    const data = {
      loggedUserID: this.state.userid,
      userID: this.props.route.params.reciverId,
      status: 0,
    };
    // alert(JSON.stringify(data))
    const urlPath = url.callActiveStatus;
    // alert(urlPath)
    const result = await api_service.post(urlPath, data);
  };

  _renderVideos = () => {
    const {joinSucceed} = this.state;
    const {peerIds} = this.state;
    // alert(this.state.channelName);
    return joinSucceed ? (
      <View style={styles2.fullView}>
        <RtcLocalView.SurfaceView
          style={styles2.remote}
          channelId={this.state.channelName}
          renderMode={VideoRenderMode.Hidden}
          zOrderMediaOverlay
        />
        {this._renderRemoteVideos()}
      </View>
    ) : null;
  };

  _renderRemoteVideos = () => {
    const {peerIds} = this.state;
    return (
      <TouchableOpacity
        onPress={() =>
          this.setState({hideVideoFuntion: !this.state.hideVideoFuntion})
        }
        style={styles2.remoteContainer}
      >
        {peerIds.map((value) => (
          <RtcRemoteView.SurfaceView
            style={styles2.max}
            uid={value}
            channelId={this.state.channelName}
            renderMode={VideoRenderMode.Hidden}
            // zOrderMediaOverlay={true}
          />
        ))}
      </TouchableOpacity>
    );
  };

  muteAudioCall = async () => {
    console.log(this.state.muteCall);
    if (this.state.muteCall === true) {
      await this.setState({muteCall: false});
    } else {
      await this.setState({muteCall: true});
    }
    // alert(this.state.muteCall)
    await this._engine.muteLocalAudioStream(this.state.muteCall);
  };

  speakerAudioCall = async () => {
    if (this.state.speakerCall === true) {
      await this.setState({speakerCall: false});
    } else {
      await this.setState({speakerCall: true});
    }
    // alert(this.state.speakerCall)
    await this._engine.setEnableSpeakerphone(this.state.speakerCall);
  };

  flipCamera = async () => {
    this._engine.switchCamera();
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        {this.state.ShowVideoCAllScreen === 0 ? (
          <>
            {/* {calltype === 4 } is VideoCAll  OutGoing Screen Show */}
            {this.state.calltype === 4 ? (
              <OutGoingVideo
                username={this.props.route.params.username}
                url={
                  this.props.route.params.profilePicture === ''
                    ? require('../assets/userpro.jpeg')
                    : {
                        uri: `${backendBaseUrl}data/${this.props.route.params.profilePicture}`,
                      }
                }
                onPress={this.endCall1}
              />
            ) : (
              <>
                {this.state.calltype === 5 ? (
                  <VideoCallIncoming
                    //  phone="phone-incoming"

                    username={this.props.route.params.username}
                    url={
                      this.props.route.params.profilePicture === ''
                        ? require('../assets/userpro.jpeg')
                        : {
                            uri: `${backendBaseUrl}data/${this.props.route.params.profilePicture}`,
                          }
                    }
                    onPress={() => this.endCall1()}
                    anserer={() => this.videoAccept()}
                  />
                ) : (
                  <>
                    {this.state.calltype === 7 ? (
                      <>
                        {this.state.ActiveCall === 1 ? (
                          <View style={styles2.max}>
                            {this._renderVideos()}

                            {this.state.hideVideoFuntion === true ? (
                              <View
                                style={{
                                  backgroundColor: 'black',
                                  borderTopLeftRadius: 15,
                                  borderTopRightRadius: 15,
                                  position: 'absolute',
                                  bottom: 0,
                                  alignItems: 'center',
                                  opacity: 0.8,
                                  height: 120,
                                  width: '100%',
                                }}
                              >
                                <View
                                  style={{
                                    width: 100,
                                    height: 4,
                                    backgroundColor: '#5b5353',
                                    borderRadius: 100,
                                    marginTop: 8,
                                  }}
                                />
                                <View
                                  style={{
                                    flex: 0.8,
                                    marginTop: 20,
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    alignContent: 'center',
                                  }}
                                >
                                  <TouchableOpacity
                                    style={styles.circle}
                                    onPress={() => this.speakerAudioCall()}
                                  >
                                    {this.state.speakerCall !== true ? (
                                      <AntDesign
                                        name="sound"
                                        color="white"
                                        size={25}
                                        style={{marginTop: 18}}
                                      />
                                    ) : (
                                      <Entypo
                                        name="sound"
                                        color="white"
                                        size={25}
                                        style={{marginTop: 18}}
                                      />
                                    )}
                                  </TouchableOpacity>

                                  <TouchableOpacity
                                    style={styles.circle}
                                    onPress={() => this.muteAudioCall()}
                                  >
                                    {this.state.muteCall === true ? (
                                      <FontAwesome
                                        name="microphone-slash"
                                        color="white"
                                        size={25}
                                        style={{marginTop: 18}}
                                      />
                                    ) : (
                                      <FontAwesome
                                        name="microphone"
                                        color="white"
                                        size={25}
                                        style={{marginTop: 18}}
                                      />
                                    )}
                                    {/* <FontAwesome name="microphone-slash" color="white" size={25} style={{ marginTop: 18 }} /> */}
                                  </TouchableOpacity>

                                  <TouchableOpacity
                                    style={styles.circle}
                                    onPress={() => this.flipCamera()}
                                  >
                                    {Platform.OS === 'ios' ? (
                                      <MaterialIcons
                                        name="flip-camera-android"
                                        color="white"
                                        size={25}
                                        style={{marginTop: 18}}
                                      />
                                    ) : (
                                      <MaterialIcons
                                        name="flip-camera-android"
                                        color="white"
                                        size={25}
                                        style={{marginTop: 18}}
                                      />
                                    )}
                                  </TouchableOpacity>

                                  <TouchableOpacity
                                    style={styles.circle1}
                                    onPress={this.endCall}
                                  >
                                    <Entypo
                                      name="cross"
                                      color="#fff"
                                      size={35}
                                      style={{}}
                                    />
                                  </TouchableOpacity>
                                </View>
                              </View>
                            ) : null}
                          </View>
                        ) : (
                          <CallerScreen
                            username={this.props.route.params.username}
                            muteCallstate={this.state.muteCall}
                            onPress={() => this.endCall()}
                            speakerCallState={this.state.speakerCall}
                            url={
                              this.props.route.params.profilePicture === ''
                                ? require('../assets/userpro.jpeg')
                                : {
                                    uri: `${backendBaseUrl}data/${this.props.route.params.profilePicture}`,
                                  }
                            }
                            onPressMute={() => this.muteAudioCall()}
                            onPressSpeaker={() => this.speakerAudioCall()}
                          />
                        )}
                      </>
                    ) : null}
                  </>
                )}
              </>
            )}
          </>
        ) : (
          <>
            {this.state.calltype === 2 ? (
              <CallPopupScreen
                username={this.props.route.params.username}
                // startTimer ={this.state.startTImer}
                onPress={() => this.endCall1()}
                url={
                  this.props.route.params.profilePicture === ''
                    ? require('../assets/userpro.jpeg')
                    : {
                        uri: `${backendBaseUrl}data/${this.props.route.params.profilePicture}`,
                      }
                }

                // onPressMute={()=>this._engine.muteLocalAudioStream(!muted)}
                // onPressMute={() => this.muteAudioCall()}
                // onPressSpeaker={() => this.speakerAudioCall()}
              />
            ) : (
              <>
                {this.state.calltype === 3 ? (
                  <IncomingCallScreen
                    username={this.props.route.params.username}
                    phone="phone-incoming"
                    url={
                      this.props.route.params.profilePicture === ''
                        ? require('../assets/userpro.jpeg')
                        : {
                            uri: `${backendBaseUrl}data/${this.props.route.params.profilePicture}`,
                          }
                    }
                    onPress={() => this.endCall1()}
                    anserer={() => this.incomingCAll()}
                  />
                ) : null}
              </>
            )}
          </>
        )}
      </SafeAreaView>
    );
  }
}

// Firebse Api  audio video  Calling funtionallity

const styles = StyleSheet.create({
  container: {
    // flex:1,
    height: '100%',
    backgroundColor: '#FFFF',
  },

  list: {
    flex: 1,
    // flexGrow: 1,
    // backgroundColor:'red',
    paddingHorizontal: 17,
    // bottom: -60,
    // height:height
  },

  btnSend: {
    backgroundColor: '#00BFFF',
    width: 40,
    height: 40,
    borderRadius: 360,
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconSend: {
    width: 30,
    height: 30,
    alignSelf: 'center',
  },
  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#FFFFFF',
    borderRadius: 30,
    borderBottomWidth: 1,
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    marginRight: 10,
  },
  inputs: {
    height: 40,
    marginLeft: 16,
    borderBottomColor: '#FFFFFF',
    flex: 1,
  },
  balloon: {
    maxWidth: 300,
    padding: 15,
    // borderRadius: 0,
    // backgroundColor:'#7643F8'
  },
  itemIn: {
    alignSelf: 'flex-start',
  },
  itemOut: {
    alignSelf: 'flex-end',
  },
  time: {
    alignSelf: 'flex-end',
    fontSize: 12,
    color: '#808080',
  },
  item: {
    flexDirection: 'row',
    // justifyContent: 'center',
    alignItems: 'center',
    // marginHorizontal:40
  },
  itemMsg: {
    // marginVertical: 14,
    // flex: 1,
    flexDirection: 'row',
    backgroundColor: '#eeeeee',
    borderRadius: 10,
    // padding:5,
  },
  sendingContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  itemContainer: {
    marginVertical: 14,
    flex: 1,
  },
  itemMessage: {
    fontSize: 15,
  },
  textInputView: {
    // flex:1,
    padding: 8,
    paddingVertical: 20,
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#FFFF',
    borderTopWidth: 0.3,
    borderTopColor: '#979797',
    bottom: 0,
    // color:'red'
  },
  textInput: {
    flex: 1,
    flexGrow: 1,
    borderWidth: 0,
    borderRadius: 10,
    borderColor: 'red',
    padding: 10,
    fontSize: 16,
    marginRight: 10,

    textAlignVertical: 'center',
    // alignSelf:'center',
    // justifyContent:'center',
    // textAlign:'center',
    height: 40,
    backgroundColor: '#BDD1E3',
    // width:'auto'
  },
  textInputButton: {
    flexShrink: 1,
  },
  TitleSurface: {
    marginTop: 10,
    paddingHorizontal: 20,
    paddingVertical: 5,
    // height: 80,
    // width: width/2.5,
    // flex:1,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 0,
    backgroundColor: '#EDF9FF', // '#F8F8F8',
    borderRadius: 5,
    // marginHorizontal:20,
    // paddingVertical:20
  },
  Orangeballoon: {
    backgroundColor: '#FF9942',
    marginHorizontal: 10,
    marginVertical: 10,
    width: width - 40,
    // maxWidth: 300,
    padding: 15,
    borderRadius: 40,
    alignSelf: 'center',
  },
  View2: {
    width: '30%',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection: 'row',
  },

  // modelView: {
  //   width: '100%',
  //   position: 'absolute',
  //   height: '25%',
  //   bottom: 0,
  //   borderTopRightRadius: 30,
  //   borderTopLeftRadius: 30,
  //   backgroundColor: '#ffffff',
  //   elevation: 10,
  //   alignItems: 'center',
  //   justifyContent: 'center',
  //   // marginTop:20
  // }, DUP
  lineTop: {
    alignSelf: 'center',
    width: '30%',
    position: 'absolute',
    top: 0,
    backgroundColor: '#ccc',
    borderRadius: 10,
    marginTop: 10,
    height: 5,
  },
  modelButtoncall: {
    flexDirection: 'row',
    width: '100%',
    // marginTop: 30, DUP
    // alignItems:"center",
    // paddingHorizontal:20,
    justifyContent: 'space-around',
    marginTop: '15%',
  },
  BlockModalView: {
    height: '25%',
    width: '100%',
    backgroundColor: '#ffffff',
    position: 'absolute',
    top: Platform.OS === 'ios' ? 30 : 0,
    elevation: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
  blockText: {
    position: 'absolute',
    top: 0,
    // backgroundColor:"red",
    width: '100%',
  },
  modelView: {
    width: '100%',
    position: 'absolute',
    height: '25%',
    bottom: 0,
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    backgroundColor: '#ffffff',
    elevation: 10,
    alignItems: 'center',
    justifyContent: 'center',
    borderTopColor: '#ccc',
    borderTopWidth: 1,
    // marginTop:20
  },
  modelButtoncall1: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-around',
    // backgroundColor:"red",
    marginTop: '20%',
  },
  circle: {
    alignSelf: 'center',
    width: 60,
    height: 60,
    borderRadius: 60 / 2,
    backgroundColor: '#5b5353',
    marginHorizontal: 10,
    alignItems: 'center',
    // opacity:0.6
  },
  circle1: {
    alignSelf: 'center',
    width: 60,
    height: 60,
    borderRadius: 60 / 2,
    backgroundColor: 'red',
    marginHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
    // opacity:0.6
  },
});

export default Call_Screen;
