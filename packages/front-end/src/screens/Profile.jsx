/* eslint-disable block-scoped-var */
import React, {useState, useEffect} from 'react';
import {
  Dimensions,
  Image,
  View,
  StyleSheet,
  StatusBar,
  Alert,
  LogBox,
  TouchableOpacity,
  ImageBackground,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import {Text} from 'react-native-paper';
import {CircleFade} from 'react-native-animated-spinkit';
import RNRestart from 'react-native-restart';
import AsyncStorage from '@react-native-async-storage/async-storage';
import messaging from '@react-native-firebase/messaging';
import {getStatusBarHeight} from 'react-native-status-bar-height';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import ImagePicker from 'react-native-image-picker';

import {GoogleSignin} from '@react-native-google-signin/google-signin';
import {LoginManager} from 'react-native-fbsdk';
import {createStackNavigator} from '@react-navigation/stack';
import {SafeAreaView} from 'react-native-safe-area-context';
import {url, backendBaseUrl} from '../const/urlDirectory';
import {theme} from '../core/theme';
import Button from '../components/Button';
import {checkConnection} from '../components/checkConnection';
import {string} from '../const/string';
import StaarzLogoHeader from '../components/StaarzLogoHeader';

const Stack = createStackNavigator();

function Profile(props) {
  // alert(JSON.stringify(userData));
  const windowWidth = Dimensions.get('window').width;
  const windowHeight = Dimensions.get('window').height;
  const {width} = Dimensions.get('window');
  const {height} = Dimensions.get('window');

  // const temp = {"userID":403,"userEmail":"ghdfgdf@dfsgdf.dfg","userPassword":"","username":"","facebookToken":"","googleToken":"","fbAuthStatus":0,"googleAuthStatus":0,"fullName":" Fhfgh gf","dob":"31-03-2021","locationText":"6-E, Nungambakkam High Road 1A, Dr, Thirumurthy Nagar, 1st Street, Nungambakkam, Chennai, Tamil Nadu 600034, India","lat":13.055441,"lon":80.2479924,"gender":"1","religionID":2,"introVideoID":0,"profilePicture":"","height":0,"weight":0,"alternateLocation":"","haveCar":0,"doesSmoke":0,"aboutMe":"","relationshipType":2,"packageID":0,"identifyID":0,"emailVerified":1,"isImageVerified":0,"isVideoVerified":0,"isProfileVerified":0,"facebookID":"","googleID":"","uploadedImage":"","clickedImage":"","fbProfileLink":"Dghgh","instaProfileLink":"Fghhjjjk"}
  const [userData, setUserdata] = useState([]); // route.params.userData);
  const [accessToken, setaccessToken] = useState();
  const [occupation, setOccupation] = useState();
  const [education, seteducation] = useState();
  const [UserProfile, setProfile] = useState('');
  const [Active, setActive] = useState(0);
  const [net, setnet] = React.useState(true);
  // const [educationList, seteducationList] = useState([]);

  const fullnameuse333 = userData.firstName;
  // alert(fullnameuse333)
  const fullnameuser = userData.lastName;
  let fullnameuser1 = '';
  if (!fullnameuser) {
    fullnameuser1 = '';
    // alert("ok")
  } else {
    fullnameuser1 = userData.lastName;
    // alert(fullnameuser1)
  }

  const [educationList, seteducationList] = useState([
    {label: 'High School', value: 1},
    {label: 'Bachelor', value: 2},
    {label: 'Master', value: 3},
    {label: 'Doctor', value: 4},
    {label: 'Prof.', value: 5},
  ]);
  const [collage, setcollage] = useState('');
  const [OccupationList, setOccupationList] = useState([
    {label: 'Engineer', value: 1},
    {label: 'Doctor', value: 2},
    {label: 'Lawyer', value: 3},
  ]);

  const {navigation} = props;
  const {route} = props;
  console.log('userdata----------------------', userData);

  useEffect(() => {
    getdata();
  }, []);

  const getdata = async () => {
    const Token = await AsyncStorage.getItem('Token');
    // navigation.navawait AsyncStorage.getItem('Token')igate("LoginScreen")
    const email = await AsyncStorage.getItem('email');
    console.log('email-------', email);
    const user = await AsyncStorage.getItem('user');

    const user1 = JSON.parse(user);
    // alert(JSON.stringify(user1))
    console.log(
      'user===============----user-------------->>>>>>>>>>>user>>>>>>>>>-------------',
      user1.profilePicture,
    );

    setUserdata(user1);
    setaccessToken(Token);
    userinfo(user1, Token);

    checkConnection().then((res) => {
      if (res === false) {
        // dropDownAlertRef.alertWithType('error', 'Error', string.network);
        setnet(res);
      } else {
        setnet(res);
        // getasync1()
      }
    });
  };

  const UserStatus = (data) => {
    const data1 = JSON.stringify({
      onlineStatus: data.status,
      userID: data.userId,
      token: data.Token,
    });

    const config = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: data1,
    };
    fetch(url.setOnlineStatus, config)
      .then((response) => response.json())
      .then((responseJson) => {
        // alert(JSON.stringify(responseJson))
        console.log('---', JSON.stringify(responseJson));
        if (responseJson.status === 200) {
          // setIsloading(false)
        }
      })
      .catch((error) => {
        // alert(error)
        console.error(error);
        // setIsloading(false)
      });
  };

  LogBox.ignoreAllLogs();

  const _signOut = async () => {
    // alert("ok")
    // Remove user session from the device.
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      AsyncStorage.clear();
      // Removing user Info
      navigation.reset({
        index: 0,
        routes: [{name: 'StartScreen'}],
      });
    } catch (error) {
      console.error(error);
    }
  };

  const clearAsyncStorage = async () => {
    UserStatus({
      status: '0',
      Token: JSON.parse(accessToken),
      userId: userData.userID,
    });
    Alert.alert('Logout', 'Are you sure want to logout ?', [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {text: 'OK', onPress: () => logoutFuntion()},
    ]);
  };

  const logoutFuntion = async () => {
    await messaging()
      .deleteToken()
      .catch((error) => {
        console.log(
          '[FCMService] delete token error ----------->>>>>>>>>>',
          error,
        );
      });

    // await messaging().deleteToken();
    LoginManager.logOut();
    await AsyncStorage.removeItem('fcmToken');
    await AsyncStorage.clear();

    const fcmToken = await AsyncStorage.getItem('fcmToken');

    console.log("AsyncStorage.removeItem('fcmToken')--------------", fcmToken);

    RNRestart.Restart();
    navigation.replace('StartScreen');
  };

  // {upload profile pic} //
  const handleChoosePhoto = async () => {
    checkConnection().then((res) => {
      if (res === false) {
        // dropDownAlertRef.alertWithType('error', 'Error', string.network);
        setnet(res);
      } else {
        setnet(res);
        handleChoosePhoto1();
        // getasync1()
      }
    });
  };

  const handleChoosePhoto1 = async () => {
    const options = {
      noData: true,
      mediaType: 'photo',
      maxWidth: 500,
      maxHeight: 500,
      quality: 0.5,
    };
    // ImagePicker.launchImageLibrary(options, response => {
    // setIsloading(true)
    await ImagePicker.showImagePicker(options, (response) => {
      // alert(JSON.stringify(response))
      console.log('-------responsem pic--', JSON.stringify(response));
      // return false
      console.log('-------accessToken pic--', JSON.stringify(accessToken));
      if (response.uri) {
        // imageType = response.type
        const data = new FormData();
        data.append('userID', userData.userID);
        data.append('profileImage', {
          uri: response.uri,
          type: response.type,
          name: `uploaded.${response.fileName}`,
        });
        const config = {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
          },
          body: data,
        };
        // const url= 'https://staarz.tomsweb.xyz/upload/single';
        // alert(url.uploadSingleImage)
        fetch(url.uploadYourPhoto, config)
          .then((res) => res.json())
          .then(async (res) => {
            //  alert(JSON.stringify(res));
            console.log('----------', JSON.stringify(res));
            if (res.status === 200) {
              const Token = await AsyncStorage.getItem('Token');
              // navigation.navawait AsyncStorage.getItem('Token')igate("LoginScreen")
              const email = await AsyncStorage.getItem('email');
              console.log('email-------', email);
              const user = await AsyncStorage.getItem('user');

              const user1 = JSON.parse(user);
              userinfo(user1, Token);
              // alert(JSON.stringify(res))
              // setIsloading(false)
              const someProperty = user1;
              someProperty.profilePicture = res.res.filename;

              AsyncStorage.setItem('user', JSON.stringify(someProperty));

              // let user = await AsyncStorage.getItem('user')/

              // user1 = JSON.parse(user);

              // alert(user1.profilePicture)
              // console.log("user===============----user-------------->>>>>>>>>>>user>>>>>>>>>-------------",user1.profilePicture)

              // setUserData(someProperty)
            } else {
              // alert(responseJson.message);
            }
          })
          .catch((err) => {
            // setIsloading(false)
            // alert(err)
          });
      } else {
        // setIsloading(false)
      }
    });
  };

  // userinfo

  const userinfo = (user1, Token) => {
    // alert(JSON.stringify(user1))
    console.log(JSON.stringify(user1));
    // let hellperArray = image ;
    // setIsloading(true)

    fetch(url.userInformation, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        userID: user1.userID,
        token: JSON.parse(Token),
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        //  alert(JSON.stringify(responseJson.res))
        console.log(
          'Api call ----userinfo----------',
          JSON.stringify(responseJson.res),
        );

        //  var userData = JSON.stringify(responseJson.res)
        // alert(JuserData)
        if (responseJson.status === 200) {
          if (responseJson.res[0].profilePicture === '') {
            // alert((responseJson.res[0]).occupation)
            setActive(1);
          } else {
            setProfile(responseJson.res[0].profilePicture);
            setActive(1);
          }

          if (responseJson.res[0].occupation === 0) {
            // alert((responseJson.res[0]).occupation)
          } else {
            OccupationList.forEach((i, x) => {
              if (responseJson.res[0].occupation === i.value) {
                // alert(i.label)
                setOccupation(i.label);
              }
            });
            //  setOccupation((responseJson.res[0]).occupation)
          }

          // if((responseJson.res[0]).company === ""){

          // }
          // else{
          // setcollage((responseJson.res[0]).company)
          // }

          if (responseJson.res[0].education === 0) {
            // ???
          } else {
            educationList.forEach((i, x) => {
              if (responseJson.res[0].education === i.value) {
                // alert(i.label)
                seteducation(i.label);
              }
            });
            setcollage(responseJson.res[0].education);
          }

          // setIsloading(false)
        }
      })
      .catch((error) => {
        // alert(error)
        // setIsloading(false)
        // alert(error)
        console.error('error', error);
      });
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
      {/* <StaarzLogo /> */}
      <View style={{flex: 1, alignItems: 'center'}}>
        <StatusBar barStyle="dark-content" backgroundColor="white" />
        <StaarzLogoHeader />

        {/* <View style={{ flexDirection: "row", position: "absolute", top: 0, alignSelf: "center", alignItems: "center" }}>
          <Image source={require('../assets/starzlogo_23X26.png')} style={sideLogoStyles.imageLogo} />
          <Image source={require('../assets/STAARZ_109X19_black.png')} style={sideLogoStyles.image} />
        </View> */}
        {!net ? (
          <View
            style={{alignItems: 'center', justifyContent: 'center', flex: 1}}
          >
            <Image
              source={require('../assets/internat.png')}
              style={{height: 50, width: 50}}
            />
            <Text style={{fontSize: 17, fontWeight: '700', marginTop: 5}}>
              {string.network}
            </Text>
            <TouchableOpacity
              color="white"
              mode="contained"
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                top: windowHeight * 0.3,
              }}
              onPress={() => getdata()}
            >
              <View
                style={{
                  backgroundColor: '#7643F8',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 50,
                  paddingHorizontal: 30,
                  paddingVertical: 20,
                  marginTop: 10,
                }}
              >
                {
                  Platform.OS === 'ios' ? (
                    // <Ionicons name="reload" color="black" size={30} />
                    <Text style={{fontSize: hp('2'), color: '#fff'}}>
                      {string.Try_Again}
                    </Text>
                  ) : (
                    <Text style={{fontSize: hp('2'), color: '#fff'}}>
                      {string.Try_Again}
                    </Text>
                  )
                  // <MaterialCommunityIcons name="reload" color="black" size={30} />
                }
              </View>
            </TouchableOpacity>
          </View>
        ) : (
          <>
            <View style={{marginTop: 100}}>
              {Active === 0 ? (
                <CircleFade
                  size={30}
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    alignSelf: 'center',
                  }}
                  color="#5E2EBA"
                />
              ) : (
                <Image
                  //  source={{uri : 'https://reactnativecode.com/wp-content/uploads/2018/01/2_img.png'}}
                  // resizeMode={"cover"}~
                  // resizeMode="contain"
                  source={
                    UserProfile === ''
                      ? require('../assets/userpro.jpeg')
                      : {uri: `${backendBaseUrl}data/${Profile}`}
                  }
                  style={{
                    // width: PixelRatio.getPixelSizeForLayoutSize(80),
                    // height: 180,
                    width: 150,
                    height: 150,
                    borderRadius: 150 / 2,
                  }}
                />
              )}
            </View>
            {/* {userData.map((i,x)=>{
   console.log("fullname-----",i.fullName),
   <Text style={{fontWeight:"700",textAlign:'center',}}>{userData[0].fullName}</Text>
 })} */}

            <View style={{marginTop: 20, justifyContent: 'center'}}>
              <Text
                numberOfLines={1}
                style={{
                  textAlign: 'center',
                  color: '#000000',
                  fontSize:
                    width === 320 || height === 610.6666666666666 ? 20 : 24,
                  lineHeight: 27,
                  fontFamily: 'SFUIText-Semibold',
                }}
              >
                {`${userData.firstName} ${fullnameuser1}`}
              </Text>
              <Text
                style={{
                  textAlign: 'center',
                  fontSize: 16,
                  color: '#8A98AC',
                  lineHeight: 20,
                  fontFamily: 'SFUIText-Semibold',
                }}
              >
                {occupation}
              </Text>
              <Text
                style={{
                  textAlign: 'center',
                  fontSize: 16,
                  color: '#8A98AC',
                  lineHeight: 20,
                  fontFamily: 'SFUIText-Semibold',
                }}
              >
                {education}
              </Text>

              <View
                style={{
                  justifyContent: 'center',
                  flexDirection: 'row',
                  marginTop: 15,
                }}
              >
                <TouchableOpacity
                  color="white"
                  mode="contained"
                  style={{padding: 10}}
                  onPress={() =>
                    navigation.navigate('Setting_Profile', {
                      userID: userData.userID,
                    })
                  }
                >
                  <View
                    style={{
                      backgroundColor: 'white',
                      justifyContent: 'center',
                      alignItems: 'center',
                      height: 60,
                      width: 60,
                      borderRadius: 60 / 2,
                      shadowColor: '#000000',
                      shadowOpacity: Platform.OS === 'ios' ? 0.9 : 1,
                      shadowRadius: 3,
                      elevation: 3,
                    }}
                  >
                    <Image
                      source={require('../assets/Vector.png')}
                      style={{
                        height: wp(7),
                        width: wp(7),
                        resizeMode: 'contain',
                      }}
                    />
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={
                    () => handleChoosePhoto()

                    // navigation.navigate('AddImageScreen',
                    // {
                    //   userID :userData.userID
                    //           }
                    // )
                  }
                  color="white"
                  mode="contained"
                  style={{padding: 10}}
                >
                  <View
                    style={{
                      backgroundColor: 'white',
                      justifyContent: 'center',
                      alignItems: 'center',
                      height: 60,
                      width: 60,
                      borderRadius: 50,
                      shadowColor: '#000000',
                      shadowOpacity: 0.9,
                      shadowRadius: 3,
                      elevation: 3,
                    }}
                  >
                    {/* <FontAwesome name="camera" color="#B9BAC8" size={30} /> */}
                    <ImageBackground
                      source={require('../assets/Path.png')}
                      style={{
                        height: wp(6.2),
                        width: wp(7.1),
                        alignSelf: 'center',
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}
                    >
                      <Image
                        source={require('../assets/Path1.png')}
                        style={{
                          height: wp(2.2),
                          width: wp(2.2),
                          resizeMode: 'contain',
                        }}
                      />
                    </ImageBackground>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  color="white"
                  mode="contained"
                  style={{padding: 10}}
                  onPress={() =>
                    navigation.navigate('EditProfileScreen', {
                      userID: userData,
                    })
                  }
                >
                  <View
                    style={{
                      backgroundColor: 'white',
                      justifyContent: 'center',
                      alignItems: 'center',
                      height: 60,
                      width: 60,
                      borderRadius: 50,
                      shadowColor: '#000000',
                      shadowOpacity: 0.9,
                      shadowRadius: 3,
                      elevation: 3,
                    }}
                  >
                    {/* <MaterialCommunityIcons name="pencil" color="#B9BAC8" size={30} /> */}
                    <Image
                      source={require('../assets/Icon.png')}
                      style={{
                        height: wp(6.3),
                        width: wp(6.3),
                        resizeMode: 'contain',
                      }}
                    />
                  </View>
                </TouchableOpacity>
              </View>
            </View>

            {!userData.fbAuthStatus ? (
              <Text />
            ) : (
              <View>
                <Button
                  mode="outlined"
                  // styles={width:200}
                  onPress={() => clearAsyncStorage()}
                >
                  <Text style={{fontFamily: 'MPLUS1p-Bold'}}>Logout</Text>
                </Button>
              </View>
            )}

            {userData.googleAuthStatus ? (
              <View>
                <Button
                  mode="outlined"
                  // styles={width:200}
                  onPress={() => clearAsyncStorage()}
                >
                  <Text style={{fontFamily: 'MPLUS1p-Bold'}}>Logout</Text>
                </Button>
              </View>
            ) : (
              <Text />
            )}

            <View>
              {userData.fbAuthStatus || userData.googleAuthStatus ? (
                <Text />
              ) : (
                <>
                  <Button
                    mode="outlined"
                    // styles={width:200}
                    onPress={() => clearAsyncStorage()}
                  >
                    <Text style={{fontFamily: 'MPLUS1p-Bold'}}>Logout</Text>
                  </Button>
                </>
              )}
            </View>
          </>
        )}
      </View>
    </SafeAreaView>
  );
}

const swipeCardStyles = StyleSheet.create({
  container: {
    // top:0,
    // width:"100%",
    top: 10 + getStatusBarHeight(),
  },
  card: {
    flex: 1,
    borderRadius: 4,
    borderWidth: 2,
    width: '100%',
    borderColor: '#E8E8E8',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  text: {
    textAlign: 'center',
    fontSize: 50,
    // backgroundColor: "transparent"
  },
});

function StaarzLogo() {
  return (
    <View style={sideLogoStyles.container}>
      <Image
        source={require('../assets/starzlogo_23X26.png')}
        style={sideLogoStyles.imageLogo}
      />
      <Image
        source={require('../assets/STAARZ_109X19_black.png')}
        style={sideLogoStyles.image}
      />
    </View>
  );
}
const sideLogoStyles = StyleSheet.create({
  image: {
    marginTop: 0,
    marginLeft: 20,

    resizeMode: 'cover',
  },
  imageLogo: {
    // marginBottom: 8,
    resizeMode: 'cover',
  },
  container: {
    position: 'absolute',
    // top: 10 + getStatusBarHeight(),
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 0,
  },
});

function Background({children}) {
  return (
    <ImageBackground resizeMode="repeat" style={backgroundStyles.background}>
      <KeyboardAvoidingView
        style={backgroundStyles.container}
        behavior="padding"
      >
        {children}
      </KeyboardAvoidingView>
    </ImageBackground>
  );
}
const backgroundStyles = StyleSheet.create({
  background: {
    flex: 1,
    width: '100%',
    backgroundColor: theme.colors.surface,
  },
  container: {
    // flex: 1,
    // padding: 20,
    width: '100%',
    // maxWidth: 340,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Profile;
