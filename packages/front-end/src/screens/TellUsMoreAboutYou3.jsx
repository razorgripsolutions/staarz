import React, {useState} from 'react';
import {
  Image,
  View,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  StatusBar,
  Dimensions,
  ScrollView,
  Platform,
  SafeAreaView,
  ActivityIndicator,
  FlatList,
} from 'react-native';
import {Text} from 'react-native-paper';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {checkConnection} from '../components/checkConnection';
import {string} from '../const/string';
import {InterestList} from './main/home';
import {Interests} from './main/home/components/Interest';
import {Colors} from '../constants/colors';
import {useAuthProvider} from '../../App';
import DropDownPicker from 'react-native-dropdown-picker';
import Button from '../components/Button';

export const OccupationList = [
  {label: 'Engineer', value: 1},
  {label: 'Doctor', value: 2},
  {label: 'Lawyer', value: 3},
];

export const SchoolList = [
  {label: 'London art college', value: 0},
  {label: 'RMIT', value: 1},
  {label: 'La trobe', value: 2},
];

export const educationList = [
  {label: 'High School', value: 1},
  {label: 'Bachelor', value: 2},
  {label: 'Master', value: 3},
  {label: 'Doctor', value: 4},
  {label: 'Prof.', value: 5},
];

const windowHeight = Dimensions.get('window').height;

function TellUsMoreAboutYou3({navigation}) {
  const {userState, register} = useAuthProvider();

  const [aboutMe, setAboutMe] = useState('');
  const [occupation, setOccupation] = useState('');
  const [companyName, setCompanyName] = useState('');
  const [school, setSchool] = useState('');
  const [education, setEducation] = useState('');

  const [selectedInterest, setSelectedInterest] = React.useState([]);
  const [isLoading, setIsloading] = useState(false);
  const [net, setnet] = React.useState(true);
  const {width} = Dimensions.get('window');
  const {height} = Dimensions.get('window');
  const getfuntion = () => {
    checkConnection().then((res) => {
      if (res === false) {
        setnet(res);
      } else {
        setnet(res);
      }
    });
  };

  const tellUsMoreAboutYouSubmit33 = () => {
    handleContinue();
  };

  const tellUsMoreAboutYouSubmit3 = () => {
    checkConnection().then((res) => {
      if (res === false) {
        setnet(res);
      } else {
        setnet(res);
        tellUsMoreAboutYouSubmit33();
      }
    });
  };

  const handleContinue = async () => {
    await register({
      ...userState?.registrationData,
      occupation: OccupationList.find((item) => item.value === +occupation)
        ?.label,
      companyName: companyName,
      interests: selectedInterest.map((item) => item.name),
      aboutMe: aboutMe,
    });
    navigation.navigate('AddPhotoVideo');
  };

  const skippage = () => {
    checkConnection().then((res) => {
      if (res === false) {
        setnet(res);
      } else {
        setnet(res);
        skippage1();
      }
    });
  };

  const skippage1 = () => {
    NavToSwifeScreen();
  };

  const handleSelectInterest = React.useCallback(
    (selectedItem) => {
      const isInterestSelected = selectedInterest.find(
        (list) => list === selectedItem,
      );
      if (isInterestSelected)
        setSelectedInterest(
          selectedInterest.filter((item) => item.id !== selectedItem.id),
        );
      else {
        let newSelectedInterest = [...selectedInterest];
        newSelectedInterest.push(selectedItem);
        setSelectedInterest(newSelectedInterest);
      }
    },
    [setSelectedInterest, selectedInterest],
  );

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          width: '90%',
          justifyContent: 'center',
          alignSelf: 'center',
        }}
      >
        <StatusBar barStyle="dark-content" backgroundColor="white" />

        <View
          style={{
            flexDirection: 'row',
            position: 'absolute',
            top: Platform.OS === 'ios' ? 0 : 0,
            alignItems: 'center',
            justifyContent: 'center',
            width: '100%',
            backgroundColor: '#fff',
            zIndex: 10,
          }}
        >
          <Image
            source={require('../assets/starzlogo.png')}
            style={sideLogoStyles.imageLogo}
          />
          <Text
            style={{
              fontSize: wp(6),
              fontFamily: 'MPLUS1p-Bold',
              color: '#243443',
              left: -2,
              lineHeight: 33,
              textAlign: 'center',
            }}
          >
            STAARY
          </Text>
        </View>
        {!net ? (
          <View
            style={{alignItems: 'center', justifyContent: 'center', flex: 1}}
          >
            <Image
              source={require('../assets/internat.png')}
              style={{height: 50, width: 50}}
            />
            <Text style={{fontSize: 17, fontWeight: '700', marginTop: 5}}>
              {string.network}
            </Text>
            <TouchableOpacity
              color="white"
              mode="contained"
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                top: windowHeight * 0.3,
              }}
              onPress={() => getfuntion()}
            >
              <View
                style={{
                  backgroundColor: '#7643F8',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 50,
                  paddingHorizontal: 30,
                  paddingVertical: 20,
                  marginTop: 10,
                }}
              >
                {Platform.OS === 'ios' ? (
                  <Text style={{fontSize: hp('2'), color: '#fff'}}>
                    {string.Try_Again}
                  </Text>
                ) : (
                  <Text style={{fontSize: hp('2'), color: '#fff'}}>
                    {string.Try_Again}
                  </Text>
                )}
              </View>
            </TouchableOpacity>
          </View>
        ) : (
          <>
            <View
              style={{
                flex: 1,
                width: '100%',
                justifyContent: 'center',
                alignContent: 'center',
                alignSelf: 'center',
              }}
            >
              <ScrollView
                showsVerticalScrollIndicator={false}
                keyboardShouldPersistTaps="handled"
                showsHorizontalScrollIndicator={false}
              >
                <Text
                  style={{
                    color: '#000000',
                    fontWeight: Platform.OS === 'ios' ? '900' : 'bold',
                    fontSize: wp('6.5'),
                    textAlign: 'center',
                    lineHeight: 36,
                    fontFamily: 'MPLUS1p-Bold',
                    marginTop: 60,
                  }}
                >
                  Tell us more about you
                </Text>
                <Text
                  style={{
                    fontWeight: '500',
                    fontSize:
                      width === 320 || height === 610.6666666666666 ? 16 : 18,
                    textAlign: 'center',
                    color: '#8A98AC',
                    marginBottom: 35,
                    lineHeight: 27,
                  }}
                >
                  Lets get to know each other :)
                </Text>
                <View>
                  <Text
                    style={{
                      color: '#101010',
                      textAlign: 'left',
                      marginBottom: 15,
                      alignContent: 'flex-start',
                      fontWeight: '600',
                      fontSize:
                        width === 320 || height === 610.6666666666666 ? 14 : 16,
                      lineHeight: 21,
                      fontFamily: 'SFUIText-Semibold',
                    }}
                  >
                    About me
                  </Text>
                  <View
                    style={{
                      borderRadius: 5,
                      height: Platform.OS === 'ios' ? null : null,
                    }}
                  >
                    <TextInput
                      style={{
                        height: Platform.OS === 'ios' ? 100 : null,
                        paddingHorizontal: 11,
                        borderRadius: 5,
                        borderColor: '#ccc',
                        borderWidth: 1,
                        textAlignVertical: 'top',
                        color: Colors.gray['900'],
                        width: '100%',
                      }}
                      inputContainerStyle={{borderBottomWidth: 0}}
                      numberOfLines={6}
                      multiline
                      underlineColorAndroid="rgba(0,0,0,0)"
                      placeholderTextColor={Colors.gray['500']}
                      maxLength={300}
                      onChangeText={(text) => setAboutMe(text)}
                      value={aboutMe}
                      placeholder="Tell us more about you...."
                      keyboardType="email-address"
                    />

                    <View
                      style={{
                        alignContent: 'flex-start',
                        ...(Platform.OS === 'ios' && {
                          zIndex: 10,
                        }),
                      }}
                    >
                      <Text
                        style={{
                          color: '#101010',
                          textAlign: 'left',
                          marginBottom: 8,
                          alignContent: 'flex-start',
                          fontWeight: '600',
                          fontSize:
                            width === 320 || height === 610.6666666666666
                              ? 14
                              : 16,
                          lineHeight: 21,
                          fontFamily: 'SFUIText-Semibold',
                          marginTop: 15,
                        }}
                      >
                        What is your occupation?
                      </Text>
                      <DropDownPicker
                        items={OccupationList}
                        labelStyle={{
                          fontSize: 14,
                          textAlign: 'left',
                          color: Colors.gray['600'],
                        }}
                        dropDownDirection="TOP"
                        defaultValue={occupation}
                        containerStyle={{height: hp(5.5)}}
                        style={{backgroundColor: '#fff', width: '100%'}}
                        itemStyle={{
                          justifyContent: 'flex-start',
                        }}
                        dropDownStyle={{backgroundColor: '#fff'}}
                        onChangeItem={(item) => setOccupation(item.value)}
                      />
                    </View>

                    <View style={{alignContent: 'flex-start', width: '100%'}}>
                      <Text
                        style={{
                          color: '#101010',
                          textAlign: 'left',
                          marginBottom: 0,
                          marginTop: 15,
                          alignContent: 'flex-start',
                          fontWeight: '600',
                          fontSize:
                            width === 320 || height === 610.6666666666666
                              ? 14
                              : 16,
                          lineHeight: 21,
                          fontFamily: 'SFUIText-Semibold',
                        }}
                      >
                        Company
                      </Text>
                      <TextInput
                        returnKeyType="next"
                        placeholder="Company Name"
                        value={companyName}
                        placeholderTextColor={Colors.gray['500']}
                        maxLength={15}
                        onChangeText={(text) => setCompanyName(text)}
                        style={{
                          paddingHorizontal: 11,
                          backgroundColor: 'white',
                          borderRadius: 5,
                          width: '100%',
                          height: hp(5.5),
                          marginTop: 8,
                          borderWidth: 1,
                          color: Colors.gray['900'],
                          borderColor: '#ccc',
                        }}
                      />
                    </View>

                    <View
                      style={{
                        alignContent: 'flex-start',
                        width: '100%',
                        ...(Platform.OS === 'ios' && {
                          zIndex: 9,
                        }),
                      }}
                    >
                      <Text
                        style={{
                          color: '#101010',
                          textAlign: 'left',
                          marginBottom: 8,
                          marginTop: 15,
                          alignContent: 'flex-start',
                          fontWeight: '600',
                          fontSize:
                            width === 320 || height === 610.6666666666666
                              ? 14
                              : 16,
                          lineHeight: 21,
                          fontFamily: 'SFUIText-Semibold',
                        }}
                      >
                        School
                      </Text>
                      <DropDownPicker
                        items={SchoolList}
                        labelStyle={{
                          fontSize: 14,
                          textAlign: 'left',
                          color: Colors.gray['600'],
                        }}
                        dropDownDirection="TOP"
                        defaultValue={school}
                        containerStyle={{height: hp(5.5)}}
                        style={{backgroundColor: '#fff', width: '100%'}}
                        itemStyle={{
                          justifyContent: 'flex-start',
                        }}
                        dropDownStyle={{backgroundColor: '#fff'}}
                        onChangeItem={(item) => setSchool(item.value)}
                      />
                    </View>

                    <View
                      style={{
                        alignContent: 'flex-start',
                        width: '100%',
                        ...(Platform.OS === 'ios' && {
                          zIndex: 8,
                        }),
                      }}
                    >
                      <Text
                        style={{
                          color: '#101010',
                          textAlign: 'left',
                          marginBottom: 8,
                          marginTop: 15,
                          alignContent: 'flex-start',
                          fontWeight: '600',
                          fontSize:
                            width === 320 || height === 610.6666666666666
                              ? 14
                              : 16,
                          lineHeight: 21,
                          fontFamily: 'SFUIText-Semibold',
                        }}
                      >
                        Education
                      </Text>
                      <DropDownPicker
                        items={educationList}
                        labelStyle={{
                          fontSize: 14,
                          textAlign: 'left',
                          color: Colors.gray['600'],
                        }}
                        dropDownDirection="TOP"
                        defaultValue={education}
                        containerStyle={{height: hp(5.5)}}
                        style={{backgroundColor: '#fff', width: '100%'}}
                        itemStyle={{
                          justifyContent: 'flex-start',
                        }}
                        dropDownStyle={{backgroundColor: '#fff'}}
                        onChangeItem={(item) => setEducation(item.value)}
                      />
                    </View>

                    <View style={{alignContent: 'flex-start', width: '100%'}}>
                      <Text
                        style={{
                          color: '#101010',
                          textAlign: 'left',
                          marginBottom: 8,
                          marginTop: 15,
                          alignContent: 'flex-start',
                          fontWeight: '600',
                          fontSize:
                            width === 320 || height === 610.6666666666666
                              ? 14
                              : 16,
                          lineHeight: 21,
                          fontFamily: 'SFUIText-Semibold',
                        }}
                      >
                        Choose your interests
                      </Text>

                      <View style={styles.interestContainer}>
                        <FlatList
                          showsHorizontalScrollIndicator={false}
                          horizontal
                          data={InterestList}
                          keyExtractor={(item) => item.id}
                          renderItem={({item}) => {
                            const isInterestSelected = selectedInterest.find(
                              (list) => list.id === item.id,
                            );
                            return (
                              <Interests
                                handleSelectInterest={() =>
                                  handleSelectInterest(item)
                                }
                                isInterestSelected={isInterestSelected}
                                interest={item}
                              />
                            );
                          }}
                          contentContainerStyle={styles.interestListContainer}
                        />
                      </View>
                    </View>
                    <View style={{height: 80}} />
                  </View>
                </View>
              </ScrollView>
            </View>
            <View style={bottomButton.container}>
              <View style={bottomButton.bottomView}>
                {isLoading ? (
                  <ActivityIndicator
                    style={bottomButton.login}
                    size="large"
                    color="#8897AA"
                  />
                ) : (
                  <Button
                    style={bottomButton.login}
                    color="#8897AA"
                    disabled={
                      !(
                        aboutMe ||
                        occupation ||
                        companyName ||
                        school ||
                        education
                      )
                    }
                    mode="contained"
                    onPress={tellUsMoreAboutYouSubmit3}
                  >
                    <Text
                      style={{
                        textTransform: 'capitalize',
                        fontWeight: 'bold',
                        fontSize: 18,
                        color: 'white',
                      }}
                    >
                      Next
                    </Text>
                  </Button>
                )}
                <TouchableOpacity onPress={() => skippage()}>
                  <Text
                    style={{fontWeight: '500', fontSize: 12, color: '#B093FA'}}
                  >
                    Skip this page
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </>
        )}
      </View>
    </SafeAreaView>
  );
}

const bottomButton = StyleSheet.create({
  container: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    bottom: 0,
    backgroundColor: 'white',
  },
  bottomView: {
    bottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  login: {
    marginBottom: 20,
    borderRadius: 8,
  },
});

const sideLogoStyles = StyleSheet.create({
  image: {
    marginTop: 0,
    marginLeft: 20,
  },
  imageLogo: {
    width: wp(7.5),
    height: wp(6.5),
    left: -4,
  },
  container: {
    position: 'absolute',
    top: getStatusBarHeight(),
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    backgroundColor: 'white',
    zIndex: 10,
    width: '100%',
  },
});

const styles = StyleSheet.create({
  interestContainer: {
    paddingTop: 10,
  },
  interestListContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: '96%',
  },
});

export default TellUsMoreAboutYou3;
