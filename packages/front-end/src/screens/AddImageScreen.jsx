import React, {useState, useEffect} from 'react';
import {
  Image,
  View,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  FlatList,
  SafeAreaView,
  Platform,
} from 'react-native';

import AsyncStorage from '@react-native-async-storage/async-storage';
import {Text} from 'react-native-paper';

import {getStatusBarHeight} from 'react-native-status-bar-height';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
// import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP,
} from 'react-native-responsive-screen';
import ImagePicker from 'react-native-image-picker';
import {CircleFade} from 'react-native-animated-spinkit';
import {url, backendBaseUrl} from '../const/urlDirectory';
import {theme} from '../core/theme';
import Button from '../components/Button';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
function AddImageScreen({navigation, route}) {
  const [userData, setUserData] = useState([]);
  const [Update, setupdate] = useState(true);
  const [accessToken, setAccessToken] = useState(route.params.accessToken);
  const [dataSource, setDataSource] = useState([
    // https://homepages.cae.wisc.edu/~ece533/images/airplane.png
    {
      id: 0,
      src: '',
      title: 'Add photo',
      icon: 'plus-circle',
      Imagetype: 1,
      mediaName: '',
    },
    {
      id: 1,
      src: '',
      title: 'Add photo',
      icon: 'plus-circle',
      Imagetype: 1,
      mediaName: '',
    },
    {
      id: 2,
      src: '',
      title: 'Add photo',
      icon: 'plus-circle',
      Imagetype: 1,
      mediaName: '',
    },
    {
      id: 3,
      src: '',
      title: 'Add photo',
      icon: 'plus-circle',
      Imagetype: 1,
      mediaName: '',
    },
    {
      id: 4,
      src: '',
      title: 'Add photo',
      icon: 'plus-circle',
      Imagetype: 1,
      mediaName: '',
    },
    {
      id: 5,
      src: '',
      title: 'Add photo',
      icon: 'plus-circle',
      Imagetype: 1,
      mediaName: '',
    },
    {
      id: 6,
      src: '',
      title: 'Add video',
      icon: 'video-camera',
      Imagetype: 0,
      mediaName: '',
    },
    {
      id: 7,
      src: '',
      title: 'Add video',
      icon: 'video-camera',
      Imagetype: 0,
      mediaName: '',
    },
    {
      id: 8,
      src: '',
      title: 'Add video',
      icon: 'video-camera',
      Imagetype: 0,
      mediaName: '',
    },
  ]);
  const [isloading, setIsLoading] = useState(false);

  useEffect(() => {
    // fetchProfileForMatch();
    getid();
  }, []);

  const getid = async () => {
    const user = await AsyncStorage.getItem('user');

    const user1 = JSON.parse(user);
    console.log(
      'user===============----user-------------->>>>>>>>>>>user>>>>>>>>>-------------',
      user1.profilePicture,
    );
    const userId = user1.userID;
    setUserData(user1);
    apicall(userId);
    // BAD!!! will cause a BUG
    // eslint-disable-next-line no-undef
    userinfo(userId, Token);
  };

  // GetuserGallerpic
  const apicall = (userId) => {
    // alert(u
    // let hellperArray = image ;
    setIsLoading(true);

    fetch(url.getUserPicGallary, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        userID: userId,
        // authToken: rchatdata.authToken
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        //  alert(JSON.stringify(responseJson.result))
        console.log(
          'Api call --------------',
          JSON.stringify(responseJson.result),
        );
        const data = responseJson.result;
        console.log('data---', data);
        if (responseJson.status === 200) {
          data.forEach((i, x) => {
            if (i.id === 'galleryPhotos1') {
              // alert(i.id)
              if (i.image === '') {
                // alert("ok")
                const qty = dataSource;
                setDataSource(qty);
              } else {
                const qty = dataSource;
                const ids = 0;
                qty[0].src = `${backendBaseUrl}data/${i.image}`;
                qty[0].mediaName = i.image;
                setDataSource(qty);
              }
            } else if (i.id === 'galleryPhotos2') {
              if (i.image === '') {
                // alert("ok")
                const qty = dataSource;
                setDataSource(qty);
              } else {
                const qty = dataSource;
                const ids = 1;
                qty[1].src = `${backendBaseUrl}data/${i.image}`;
                qty[1].mediaName = i.image;
                setDataSource(qty);
              }
              // alert(i.id)
            } else if (i.id === 'galleryPhotos3') {
              if (i.image === '') {
                // alert("ok")
                const qty = dataSource;
                setDataSource(qty);
              } else {
                const qty = dataSource;
                const ids = 2;
                qty[2].src = `${backendBaseUrl}data/${i.image}`;
                qty[2].mediaName = i.image;
                setDataSource(qty);
              }
            } else if (i.id === 'galleryPhotos4') {
              if (i.image === '') {
                // alert("ok")
                const qty = dataSource;
                setDataSource(qty);
              } else {
                const qty = dataSource;
                const ids = 3;
                qty[3].src = `${backendBaseUrl}data/${i.image}`;
                qty[3].mediaName = i.image;
                setDataSource(qty);
              }
            } else if (i.id === 'galleryPhotos5') {
              if (i.image === '') {
                // alert("ok")
                const qty = dataSource;
                setDataSource(qty);
              } else {
                const qty = dataSource;
                const ids = 4;
                qty[4].src = `${backendBaseUrl}data/${i.image}`;
                qty[4].mediaName = i.image;
                setDataSource(qty);
              }
            } else if (i.id === 'galleryPhotos6') {
              if (i.image === '') {
                // alert("ok")
                const qty = dataSource;
                setDataSource(qty);
              } else {
                const qty = dataSource;
                const ids = 5;
                qty[5].src = `${backendBaseUrl}data/${i.image}`;
                qty[5].mediaName = i.image;
                setDataSource(qty);
              }
            } else if (i.id === 'galleryVideo1') {
              if (i.image === '') {
                // alert("ok")
                const qty = dataSource;
                setDataSource(qty);
              } else {
                const qty = dataSource;
                const ids = 6;
                qty[6].src = `${backendBaseUrl}data/${i.image}`;
                qty[6].mediaName = i.image;
                setDataSource(qty);
              }
            } else if (i.id === 'galleryVideo2') {
              if (i.image === '') {
                // alert("ok")
                const qty = dataSource;
                setDataSource(qty);
              } else {
                const qty = dataSource;
                const ids = 7;
                qty[7].src = `${backendBaseUrl}data/${i.image}`;
                qty[7].mediaName = i.image;
                setDataSource(qty);
              }
            } else if (i.id === 'galleryVideo3') {
              if (i.image === '') {
                // alert("ok")
                const qty = dataSource;
                setDataSource(qty);
              } else {
                const qty = dataSource;
                const ids = 8;
                qty[8].src = `${backendBaseUrl}data/${i.image}`;
                qty[8].mediaName = i.image;
                setDataSource(qty);
              }
            }
          });

          const qty = dataSource; // make a copy

          // let data = responseJson.result
          // console.log("--------data",data.image)
          // console.log("data--",data.le[ngth)
          // if(data.length >= 9){

          // }
          // else{}  ;
          //  let data1 = data.concat(AddButton)/
          //  console.log("data1--",data1.length)
          //  alert(data1)
          //  setimages(data1)
          setIsLoading(false);
        }
      })
      .catch((error) => {
        // alert(error)
        setIsLoading(false);
        console.error(error);
      });
  };

  const onAddPhotoVideoSubmit = () => {
    setIsLoading(true);
    // alert(JSON.stringify({
    //   galleryPhotos1:dataSource[0].mediaName,
    //   galleryPhotos2:dataSource[1].mediaName,
    //   galleryPhotos3:dataSource[2].mediaName,
    //   galleryPhotos4:dataSource[3].mediaName,
    //   galleryPhotos5:dataSource[4].mediaName,
    //   galleryPhotos6:dataSource[5].mediaName,
    //   galleryVideo1:dataSource[6].mediaName,
    //   galleryVideo2:dataSource[7].mediaName,
    //   galleryVideo3:dataSource[8].mediaName,
    //   userID : userData.userID
    // }))

    fetch(url.addPhotoVideoSubmit, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        galleryPhotos1: dataSource[0].mediaName,
        galleryPhotos2: dataSource[1].mediaName,
        galleryPhotos3: dataSource[2].mediaName,
        galleryPhotos4: dataSource[3].mediaName,
        galleryPhotos5: dataSource[4].mediaName,
        galleryPhotos6: dataSource[5].mediaName,
        galleryVideo1: dataSource[6].mediaName,
        galleryVideo2: dataSource[7].mediaName,
        galleryVideo3: dataSource[8].mediaName,
        status: 5,
        userID: userData.userID,
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        // alert(JSON.stringify(responseJson))
        // alert("ok")
        if (responseJson.status === 200) {
          // navigation.navigate('TellUsMoreAboutYou1',{
          //   userData: userData,
          //   accessToken: accessToken
          // })
          // alert("200")
          setupdate(true);
          navigation.goBack();
          // alert("suuces")
          apicall(userData.userID);
          setIsLoading(false);
        }
        if (responseJson.status === 404) {
          // alert("Oops!! Error")
          setIsLoading(false);
        }
        if (responseJson.status === 400) {
          // alert("Oops!! Something went wrong.")
          setIsLoading(false);
        }
      })
      .catch((error) => {
        // alert('Oops error!!')
        console.error(error);
        setIsLoading(false);
      });
  };

  const onCardButtonPress = async (item) => {
    setIsLoading(true);
    const options = {
      noData: true,
      mediaType: item.Imagetype ? 'photo' : 'video',
      maxWidth: 500,
      maxHeight: 500,
      quality: 0.5,
    };
    await ImagePicker.launchImageLibrary(options, (response) => {
      // alert(JSON.stringify(response))
      console.log(JSON.stringify(response));
      if (response.uri) {
        // imageType = response.type
        const data = new FormData();
        const tempURI = response.uri;
        data.append('userID', userData.userID);
        data.append('galleryImage', {
          uri: response.uri,
          type: response.type ? response.type : 'video/mp4',
          name: response.type ? response.type : 'video/mp4',
        });
        const config = {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
          },
          body: data,
        };
        fetch(url.uploadGalleryMedia, config)
          .then((res) => res.json())
          .then((res) => {
            //  alert(JSON.stringify(res));
            if (res.status === 200) {
              setupdate(false);
              // alert(JSON.stringify(res))
              const qty = dataSource; // make a copy
              const ids = item.id;
              qty[item.id].src = tempURI;
              qty[item.id].mediaName = res.res.filename;
              setDataSource(qty);
              //  alert(JSON.stringify(qty))
              setIsLoading(false);
            } else {
              setIsLoading(false);
            }
          })
          .catch((err) => {
            setIsLoading(false);
            alert(err);
          });
      } else {
        setIsLoading(false);
      }
    });
  };

  const renderItem = ({item}) => (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10,
      }}
    >
      {item.src ? (
        <>
          {item.title === 'Add photo' ? (
            <TouchableOpacity
              style={
                (cardButton.card,
                {
                  borderRadius: 10,
                  borderColor: '#8A98AC',
                  borderWidth: 1.2,
                  borderStyle: 'dashed',
                  width: heightPercentageToDP('13'),
                  height: heightPercentageToDP('13'),
                })
              }
              color="#F8FCFF"
              mode="contained"
              onPress={() => onCardButtonPress(item)}
            >
              <View style={styles.imageThumbnail}>
                <Image
                  source={{uri: item.src}}
                  style={{
                    width: heightPercentageToDP('13'),
                    height: heightPercentageToDP('13'),
                    justifyContent: 'center',
                    alignItems: 'center',
                    resizeMode: 'cover',
                    borderColor: '#8A98AC',
                    borderWidth: 1.2,
                    borderRadius: 10,
                  }}
                />
              </View>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              style={
                (cardButton.card,
                {
                  borderRadius: 10,
                  borderColor: '#8A98AC',
                  borderWidth: 1.2,
                  borderStyle: 'dashed',
                  width: heightPercentageToDP('13'),
                  height: heightPercentageToDP('13'),
                })
              }
              color="#F8FCFF"
              mode="contained"
              onPress={() => onCardButtonPress(item)}
            >
              {/* <View style={styles.imageThumbnail}><Entypo style={{ width: heightPercentageToDP('13'), height: heightPercentageToDP('15'), borderColor: "#8A98AC", borderWidth: 1.2, }} name="video" size={20} color="#8A98AC" />
            </View> */}
              <View
                style={{
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderColor: '#8A98AC',
                  borderWidth: 1.2,
                }}
              >
                <Entypo name="video" size={20} color="#8A98AC" />
              </View>
            </TouchableOpacity>
          )}
        </>
      ) : (
        <TouchableOpacity
          style={
            (cardButton.card,
            {
              borderRadius: 10,
              borderColor: '#8A98AC',
              borderWidth: 1.2,
              borderStyle: 'dashed',
              width: heightPercentageToDP('13'),
              height: heightPercentageToDP('13'),
              marginVertical: 10,
            })
          }
          color="#F8FCFF"
          mode="contained"
          onPress={() => onCardButtonPress(item)}
        >
          <View
            style={{
              flex: 1,
              borderRadius: 10,
              alignItems: 'center',
              justifyContent: 'center',
              borderColor: '#8A98AC',
              borderStyle: 'dashed',
              backgroundColor: '#e5eef6',
            }}
          >
            <FontAwesome name={item.icon} size={20} color="#8A98AC" />
            <Text
              style={{
                color: '#8A98AC',
                fontSize: heightPercentageToDP('1.2'),
                textAlign: 'center',
              }}
            >
              {item.title}
            </Text>
          </View>
        </TouchableOpacity>
      )}

      {/* <TouchableOpacity style={cardButton.card,{borderColor:"#8A98AC", borderWidth:1.2, borderStyle: 'dashed', width:heightPercentageToDP('13'),height:heightPercentageToDP('15'),marginVertical:10}}  color='#F8FCFF'  mode="contained" onPress={ () =>onCardButtonPress(item)}>
  <View style={{flex:1,alignItems:"center",justifyContent:"center"}}>
              <FontAwesome  name={item.icon} size={20} color="#8A98AC" />
              <Text style={{color:"#8A98AC",fontSize:heightPercentageToDP('1.2'),textAlign:"center"}}>{item.title}</Text>
            </View>

            </TouchableOpacity> */}

      {/* {item.src ?<>{(item.title=="Add photo")?<TouchableOpacity style={cardButton.card,{marginTop:10, borderColor:"#8A98AC", borderWidth:1.2, borderStyle: 'dashed', width:"33%", height:"50%"}}  color='#F8FCFF'  mode="contained" onPress={ () =>onCardButtonPress(item)}><View style={styles.imageThumbnail}><Image
                  source={{ uri: item.src }}
                  style={{ marginTop:17,width: "33%", height: "50%", justifyContent: 'center', alignItems: 'center',resizeMode: "cover", borderColor:"#8A98AC", borderWidth:1.2}}
                /></View></TouchableOpacity>:<TouchableOpacity style={cardButton.card,{ borderColor:"#8A98AC", borderWidth:1.2, borderStyle: 'dashed', width:"33%", height:"50%"}}  color='#F8FCFF'  mode="contained" onPress={ () =>onCardButtonPress(item)}><View style={styles.imageThumbnail}><Entypo style={{ marginTop:17,width: "33%", height: "50%", justifyContent: 'center', alignItems: 'center',resizeMode: "cover", borderColor:"#8A98AC", borderWidth:1.2}} name="video" size={28} color="#8A98AC" /></View></TouchableOpacity>}
          </>
          :<><Button style={cardButton.card,{borderColor:"#8A98AC", borderWidth:1.2, borderStyle: 'dashed', width:"33%", height:"50%"}}  color='#F8FCFF'  mode="contained" onPress={ () =>onCardButtonPress(item)}><View style={styles.imageThumbnail}>
              <Icon  name={item.icon} size={28} color="#8A98AC" />
              <Text style={{color:"#8A98AC", fontSize:13}}>{item.title}</Text>
            </View></Button>
          </>
        } */}
    </View>
  );

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
      {/* <Text style={{fontWeight:'700', letterSpacing: 1, fontWeight:"800", fontSize:28,marginBottom:10, marginTop:"30%"}}>Add photos/video</Text> */}
      {/* <Text style={{fontWeight:'500', fontSize:17, textAlign:'center', letterSpacing: 1.5, color:'#919FB1', }}>You can change this later</Text> */}
      {isloading ? (
        <CircleFade
          size={110}
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
            flex: 1,
          }}
          color="#5E2EBA"
        />
      ) : (
        <>
          {/* <BackButton goBack={navigation.goBack} />
          <StaarzLogo /> */}
          <View
            style={{
              flexDirection: 'row',
              top: Platform.OS === 'ios' ? 0 : 0,
              alignItems: 'center',
              justifyContent: 'center',
              width: '100%',
              backgroundColor: '#fff',
              zIndex: 10,
            }}
          >
            <View style={{width: '20%'}}>
              <TouchableOpacity onPress={() => navigation.goBack()} style={{}}>
                <FontAwesome
                  name="angle-left"
                  size={25}
                  color="#000"
                  style={{left: 20}}
                />
              </TouchableOpacity>
            </View>
            <View
              style={{
                width: '60%',
                alignItems: 'center',
                flexDirection: 'row',
                justifyContent: 'center',
              }}
            >
              <Image
                source={require('../assets/starzlogo.png')}
                style={sideLogoStyles.imageLogo}
              />
              {/* <Image source={require('../assets/STAARZ_109X19_black.png')} style={sideLogoStyles.image} /> */}
              <Text
                style={{
                  fontSize: wp(6),
                  fontFamily: 'MPLUS1p-Bold',
                  color: '#243443',
                  left: -2,
                  lineHeight: 33,
                  textAlign: 'center',
                }}
              >
                STAARZ
              </Text>
            </View>
            <View style={{backgroundColor: 'red', width: '20%'}} />
            {/* <Text style={{ color: "#000000", fontWeight: Platform.OS==="ios" ? "bold" : "bold", fontSize: wp('7'), marginTop: "20%", textAlign: "center", lineHeight: 36, fontFamily: "SFUIText-Bold", }}>Your Gallery</Text> */}
          </View>
          <Text
            style={{
              color: '#000000',
              fontWeight: Platform.OS === 'ios' ? 'bold' : 'bold',
              fontSize: wp('7'),
              marginTop: '10%',
              textAlign: 'center',
              lineHeight: 36,
              fontFamily: 'SFUIText-Bold',
            }}
          >
            Your Gallery
          </Text>

          <View style={styles.container}>
            {!isloading ? (
              <FlatList
                data={dataSource}
                style={{marginTop: 0}}
                renderItem={renderItem}
                numColumns={3}
                // updateCellsBatchingPeriod={1}
              />
            ) : (
              <View style={{justifyContent: 'center', alignItems: 'center'}}>
                <CircleFade size={110} color="#5E2EBA" />
              </View>
            )}
          </View>
          <View style={bottomButton.bottomView}>
            <Button
              style={bottomButton.login}
              disabled={Update}
              color="#8897AA"
              mode="contained"
              onPress={onAddPhotoVideoSubmit}
            >
              <Text
                style={{
                  textTransform: 'capitalize',
                  fontWeight: 'bold',
                  fontSize: 18,
                  color: 'white',
                }}
              >
                Done
              </Text>
            </Button>
          </View>
        </>
      )}
    </SafeAreaView>
  );
}

const bottomButton = StyleSheet.create({
  bottomView: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: heightPercentageToDP('3'),
  },
  login: {
    marginBottom: 30,
    borderRadius: 8,
    width: '90%',
  },
});

const cardButton = StyleSheet.create({
  card: {
    borderRadius: 8,
    alignItems: 'center',
    // , backgroundColor: "green",
    flexDirection: 'column',
  },
});

function StaarzLogo() {
  return (
    <View style={sideLogoStyles.container}>
      <Image
        source={require('../assets/starzlogo_23X26.png')}
        style={sideLogoStyles.imageLogo}
      />
      <Image
        source={require('../assets/STAARZ_109X19_black.png')}
        style={sideLogoStyles.image}
      />
    </View>
  );
}

const sideLogoStyles = StyleSheet.create({
  image: {
    marginTop: 0,
    marginLeft: 20,
  },
  imageLogo: {
    // left: 10,
    width: wp(7.5),
    height: wp(6.5),
    left: -4,
    // marginBottom: 8,
  },
  container: {
    // position: 'absolute',
    top: -10 + getStatusBarHeight(),
    alignItems: 'center',
    flexDirection: 'row',
    // justifyContent:"center",
    marginTop: 0,
  },
});

const styles = StyleSheet.create({
  forgotPassword: {
    width: '100%',
    alignItems: 'flex-end',
    marginRight: 10,
    marginBottom: 24,
  },
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  forgot: {
    fontSize: 13,
    marginTop: 10,
    marginRight: -2,
    color: theme.colors.secondary,
  },
  rememberMe: {
    fontSize: 13,
    marginTop: 10,
    marginLeft: 10,
    color: theme.colors.secondary,
  },
  link: {
    fontWeight: '100',
    color: '#9872FA',
    lineHeight: 30,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'white',
    // backgroundColor:"red",
    marginTop: heightPercentageToDP('3'),
    width: '100%',
  },
  imageThumbnail: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    // width:"100%",
    // backgroundColor: "red",

    flex: 1,

    // height: "100%",
    // backgroundColor:"red"
  },
});

export default AddImageScreen;
