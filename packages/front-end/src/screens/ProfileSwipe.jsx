import React, {useState, useEffect, useCallback, useRef} from 'react';
import {
  Dimensions,
  Image,
  View,
  StyleSheet,
  ScrollView,
  StatusBar,
  TouchableOpacity,
  ImageBackground,
  Platform,
} from 'react-native';
import {Colors} from '../constants/colors';
import {Text} from 'react-native-paper';
import Swiper from 'react-native-deck-swiper';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import {CircleFade} from 'react-native-animated-spinkit';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {SafeAreaView} from 'react-native-safe-area-context';
import StaarzLogoHeader from '../components/StaarzLogoHeader';
import {string} from '../const/string';
import {checkConnection} from '../components/checkConnection';
import {SwipeProfile} from './main/home';
import Tooltip from 'react-native-walkthrough-tooltip';
import {
  useBoostUser,
  useGetUsers,
  useUnwindUser,
} from '../api/hooks/profile_swipe';
import {useLikeUser, useUnLikeUser} from '../api/hooks/likes';
import Toast from 'react-native-toast-message';
import {useAuthProvider} from '../../App';
import RNPermissions, {RESULTS} from 'react-native-permissions';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const {width} = Dimensions.get('window');
const {height} = Dimensions.get('window');

const ProfileImage = [
  'https://images.unsplash.com/photo-1526509569184-2fe126e71cd3?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mjl8fHdvbWFuJTIwZGF0aW5nfGVufDB8MXwwfHw%3D&auto=format&fit=crop&w=500&q=60',
  'https://images.unsplash.com/photo-1601288848351-48adce9d676a?crop=entropy&cs=tinysrgb&fm=jpg&ixlib=rb-1.2.1&q=60&raw_url=true&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8d2hpdGUlMjB3b21hbnxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500',
  'https://images.unsplash.com/photo-1528165203550-a62234a03220?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NDB8fHdvbWFuJTIwZGF0aW5nfGVufDB8MXwwfHw%3D&auto=format&fit=crop&w=500&q=60',
  'https://images.unsplash.com/photo-1561982479-c36896dad276?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NDR8fHdvbWFuJTIwZGF0aW5nfGVufDB8MXwwfHw%3D&auto=format&fit=crop&w=500&q=60',
  'https://images.unsplash.com/photo-1644520755723-868f336428c6?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Njl8fHdvbWFuJTIwZGF0aW5nfGVufDB8MXwwfHw%3D&auto=format&fit=crop&w=500&q=60',
  'https://images.unsplash.com/photo-1573495612522-d994e72e5f56?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NzB8fHdvbWFuJTIwZGF0aW5nfGVufDB8MXwwfHw%3D&auto=format&fit=crop&w=500&q=60',
  'https://images.unsplash.com/photo-1643617018979-8f0b75f70bbb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8ODN8fHdvbWFuJTIwZGF0aW5nfGVufDB8MXwwfHw%3D&auto=format&fit=crop&w=500&q=60',
  'https://images.unsplash.com/photo-1503443207922-dff7d543fd0e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8bWVufGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=800&q=60',
  'https://images.unsplash.com/photo-1506794778202-cad84cf45f1d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8bWVufGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=800&q=60',
  'https://images.unsplash.com/photo-1508341591423-4347099e1f19?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8bWVufGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=800&q=60',
  'https://images.unsplash.com/photo-1492446845049-9c50cc313f00?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8N3x8bWVufGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=800&q=60',
  'https://images.unsplash.com/photo-1488371934083-edb7857977df?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTF8fG1lbnxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=800&q=60',
  'https://images.unsplash.com/photo-1552374196-1ab2a1c593e8?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTl8fG1lbnxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=800&q=60',
];

function ProfileSwipe(props) {
  const swiperRef = useRef(null);
  const [activeProfile, setActiveProfile] = useState(0);
  const [net, setnet] = React.useState(true);

  const {navigation} = props;
  const {userState, signIn} = useAuthProvider();
  const {mutate, data, isLoading: isloading} = useGetUsers();
  const {isLoading: isLikeLoading, mutateAsync: likeInvoker} = useLikeUser();
  const {isLoading: isUnlikeLoading, mutateAsync: unlikeInvoker} =
    useUnLikeUser();
  const {isLoading: isBoostLoading, mutateAsync: boostInvoker} = useBoostUser();
  const {isLoading: isUnwindLoading, mutateAsync: unwindInvoker} =
    useUnwindUser();

  useEffect(() => {
    getasync();
  }, [net]);

  const handleNotificationsPermissions = useCallback(() => {
    RNPermissions.checkNotifications()
      .then((response) => {
        console.log('sdfdfdsf', response);
        if (response.status === RESULTS.DENIED) {
          return RNPermissions.requestNotifications(['alert', 'badge', 'sound'])
            .then((result) => {})
            .catch((e) => {});
        }
      })
      .catch((e) => console.log(e));
  }, [RNPermissions]);

  useEffect(() => {
    handleNotificationsPermissions();
  }, []);

  const handleOnSwipeRight = useCallback(
    (newCardIndex) => {
      setActiveProfile(newCardIndex);
    },
    [setActiveProfile],
  );

  const getasync = async () => {
    getasync1();
    // Newtwork connection check
    checkConnection().then((res) => {
      if (res === false) {
        setnet(res);
      } else {
        setnet(res);
      }
    });
  };

  const getasync1 = async () => {
    fetchProfileForMatch();
  };

  const fetchProfileForMatch = async () => {
    mutate();
  };

  const boostButton = async () => {
    if (userState?.userData?.boosted === 1) {
      return Toast.show({
        type: 'error',
        text1: 'Oops..',
        text2: 'Your profile has already been boosted',
      });
    }
    boostInvoker({})
      .then(async () => {
        Toast.show({
          type: 'success',
          text1: 'Success',
          text2: `Boost  successfully`,
        });
        await signIn(userState?.userToken, {
          ...userState?.userData,
          boosted: 1,
        });
      })
      .catch((e) => {
        return Toast.show({
          type: 'error',
          text1: 'Boost error',
          text2: 'An error occured',
        });
      });
  };

  const unwindButton = () => {
    unwindInvoker()
      .then((response) => {
        console.log('response', response);
        Toast.show({
          type: 'success',
          text1: 'Success',
          text2: `Unwind  successfully`,
        });
      })
      .catch((e) => {
        return Toast.show({
          type: 'error',
          text1: 'Unwind error',
          text2: 'An error occured',
        });
      });
  };

  const dislikeButton = (id) => {
    unlikeInvoker({
      userId: id.user,
    })
      .then(async (res) => {
        if (res.data) {
          Toast.show({
            type: 'success',
            text1: 'Success',
            text2: `You disliked successfully`,
          });
        }
      })
      .catch((e) => {
        return Toast.show({
          type: 'error',
          text1: 'Dislike error',
          text2: 'An error occured',
        });
      });
  };

  const likeButton = (id) => {
    likeInvoker({
      userId: id.user,
    })
      .then(async (res) => {
        if (res.data) {
          Toast.show({
            type: 'success',
            text1: 'Success',
            text2: `You liked successfully`,
          });
        }
      })
      .catch((e) => {
        return Toast.show({
          type: 'error',
          text1: 'Like error',
          text2: 'An error occured',
        });
      });
  };

  function SwipeButtons(user) {
    const [rewindTooltipVisibility, setRewindTooltipVisibility] =
      useState(false);
    const [likesTooltipVisibility, setLikesTooltipVisibility] = useState(false);
    const [boostTooltipVisibility, setBoostTooltipVisibility] = useState(false);
    const [isFirstLogin, setIsFirstLogin] = useState(true);

    // useEffect(() => {
    //   if (isFirstLogin) setRewindTooltipVisibility(true);
    // }, []);

    return (
      <>
        <View style={swipeButtonsStyles.container}>
          <TouchableOpacity
            color="white"
            mode="contained"
            style={{padding: 10}}
            // disabled={isUnlikeLoading}
            onPress={() => {
              swiperRef?.current?.swipeLeft();
              dislikeButton(user);
            }}
          >
            <View
              style={{
                backgroundColor: 'white',
                justifyContent: 'center',
                alignItems: 'center',
                height: wp('15'),
                width: wp('15'),
                borderRadius: wp('15') / 2,
              }}
            >
              <Image
                source={require('../assets/cross.png')}
                style={{width: hp(2.5), height: hp(2.5)}}
              />
            </View>
          </TouchableOpacity>
          <View styles={{flexDirection: 'row'}}>
            <Tooltip
              isVisible={rewindTooltipVisibility}
              placement="top"
              contentStyle={{
                backgroundColor: Colors.packages.goldDark,
                ...styles.tooltipContentStyle,
              }}
              content={
                <View style={styles.tooltipContent}>
                  <Image
                    source={require('../assets/E_Gold.png')}
                    style={styles.tooltipLogoStyle}
                  />

                  <View style={{flexDirection: 'column'}}>
                    <Text style={styles.tooltipTextStyle}>
                      Made a Mistake? to View the
                    </Text>
                    <Text style={styles.tooltipTextStyle}>
                      previous people click here
                    </Text>
                  </View>
                </View>
              }
            >
              <TouchableOpacity
                color="white"
                mode="contained"
                style={{padding: 10}}
                disabled={isUnwindLoading}
                // onPress={() => {
                //   if (isFirstLogin) {
                //     setRewindTooltipVisibility(false);
                //     setLikesTooltipVisibility(true);
                //   } else unwindButton();
                // }}

                onPress={() => {
                  unwindButton();
                }}
              >
                <View
                  style={{
                    backgroundColor: 'white',
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: wp('15'),
                    width: wp('15'),
                    borderRadius: wp('15') / 2,
                  }}
                >
                  <Image
                    source={require('../assets/rewind.png')}
                    style={{width: hp(3.8), height: hp(3.8)}}
                  />
                </View>
              </TouchableOpacity>
            </Tooltip>
          </View>
          <TouchableOpacity
            color="white"
            mode="contained"
            style={{padding: 10}}
          >
            <View
              style={{
                backgroundColor: 'white',
                justifyContent: 'center',
                alignItems: 'center',
                height: wp('15'),
                width: wp('15'),
                borderRadius: wp('15') / 2,
              }}
            >
              <Image
                source={require('../assets/superLike.png')}
                style={{width: hp(2.5), height: hp(3.3), resizeMode: 'stretch'}}
              />
            </View>
          </TouchableOpacity>
          <View styles={{flexDirection: 'row'}}>
            <Tooltip
              isVisible={likesTooltipVisibility}
              placement="top"
              contentStyle={{
                backgroundColor: Colors.packages.platinumLight,
                ...styles.tooltipContentStyle,
              }}
              content={
                <View style={styles.tooltipContent}>
                  <Image
                    source={require('../assets/E_Purpal.png')}
                    style={styles.tooltipLogoStyle}
                  />
                  <Text style={styles.tooltipTextStyle}>
                    Click here for people you like!
                  </Text>
                </View>
              }
            >
              <TouchableOpacity
                color="white"
                mode="contained"
                style={{padding: 10}}
                // disabled={isLikeLoading}
                // onPress={() => {
                //   if (isFirstLogin) {
                //     setLikesTooltipVisibility(false);
                //     setBoostTooltipVisibility(true);
                //   } else {
                //     swiperRef?.current?.swipeRight();
                //     likeButton(user);
                //   }
                // }}

                onPress={() => {
                  likeButton(user);
                  swiperRef?.current?.swipeRight();
                }}
              >
                <View
                  style={{
                    backgroundColor: 'white',
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: wp('15'),
                    width: wp('15'),
                    borderRadius: wp('15') / 2,
                  }}
                >
                  <Image
                    source={require('../assets/BLike.png')}
                    style={{
                      width: hp(3.2),
                      height: hp(3.1),
                      resizeMode: 'stretch',
                    }}
                  />
                </View>
              </TouchableOpacity>
            </Tooltip>
          </View>
          <View styles={{flexDirection: 'row'}}>
            <Tooltip
              isVisible={boostTooltipVisibility}
              placement="top"
              contentStyle={{
                backgroundColor: Colors.lightTintColor,
                ...styles.tooltipContentStyle,
              }}
              content={
                <View style={styles.tooltipContent}>
                  <Image
                    source={require('../assets/E_Plus.png')}
                    style={styles.tooltipLogoStyle}
                  />

                  <View style={{flexDirection: 'column'}}>
                    <Text style={styles.tooltipTextStyle}>
                      Be the top profile for the
                    </Text>
                    <Text style={styles.tooltipTextStyle}>
                      next hour! boost yourself
                    </Text>
                  </View>
                </View>
              }
            >
              <TouchableOpacity
                color="white"
                mode="contained"
                style={{padding: 10}}
                disabled={isBoostLoading}
                // onPress={() => {
                //   if (isFirstLogin) {
                //     setBoostTooltipVisibility(false);
                //     setIsFirstLogin(false);
                //   } else boostButton();
                // }}
                onPress={() => {
                  boostButton();
                }}
              >
                <View
                  style={{
                    backgroundColor: 'white',
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: wp('15'),
                    width: wp('15'),
                    borderRadius: wp('15') / 2,
                  }}
                >
                  <Image
                    source={require('../assets/boost.png')}
                    style={{
                      width: hp(3.6),
                      height: hp(3.6),
                      resizeMode: 'stretch',
                    }}
                  />
                </View>
              </TouchableOpacity>
            </Tooltip>
          </View>
        </View>
      </>
    );
  }
  const swipeButtonsStyles = StyleSheet.create({
    container: {
      position: 'absolute',
      height: 80,
      width: '90%',
      bottom: 20,
      alignSelf: 'center',
      justifyContent: 'space-evenly',
      alignItems: 'center',
      flexDirection: 'row',
    },
  });

  function ProfilesBasicInfo(userProfile) {
    const newProfile = userProfile.user?.[userProfile.activeIndex];

    return (
      <View style={profilesBasicInfoStyle.container}>
        <View style={{height: 25, width: '100%'}} />
        <View style={profilesBasicInfoStyle.row}>
          <Text style={profilesBasicInfoStyle.fullName}>
            {newProfile?.fullName}
          </Text>
        </View>
        <View style={profilesBasicInfoStyle.row}>
          <Text style={profilesBasicInfoStyle.genderAgeHeight}>
            {newProfile?.gender}
            <Text style={profilesBasicInfoStyle.genderAgeHeight}> | </Text>
            {newProfile?.height}
          </Text>
        </View>
        <View style={profilesBasicInfoStyle.row1}>
          <EvilIcons name="location" color="#fff" size={25} />
          <Text style={profilesBasicInfoStyle.address}>
            {newProfile?.liveAt ?? 'n/a'}
          </Text>
        </View>
        <View style={profilesBasicInfoStyle.row}>
          <Text style={profilesBasicInfoStyle.dist}>10 KM</Text>
        </View>
      </View>
    );
  }

  const profilesBasicInfoStyle = StyleSheet.create({
    container: {
      flexDirection: 'column',
      color: 'white',
      position: 'absolute',
      height: 120,
      width: '100%',
      bottom: 100,
      paddingLeft: 30,
      justifyContent: 'flex-start',
      alignItems: 'flex-start',
    },
    fullName: {
      color: 'white',
      fontFamily: 'MPLUS1p-Bold',
      lineHeight: 23,
      fontSize: 18,
      letterSpacing: 1.09,
      fontWeight: '800',
    },
    genderAgeHeight: {
      color: '#FFFFFF',
      lineHeight: 24,
      fontFamily: 'MPLUS1p-Bold',
      fontSize: 13,
      letterSpacing: 1.09,
      fontWeight: '500',
    },
    address: {
      color: 'white',
      left: 5,
      lineHeight: 19,
      fontSize: 13.11,
    },
    dist: {
      color: 'white',
      left: 30,

      lineHeight: 19,
      fontSize: 13.11,
    },
    row1: {
      flexDirection: 'row',
      alignItems: 'center',
    },
  });

  const styles = StyleSheet.create({
    tooltipLogoStyle: {
      height: 40,
      width: 40,
      borderRadius: 40 / 2,
      resizeMode: 'cover',
      marginRight: 10,
    },
    tooltipTextStyle: {
      fontSize: 12,
      color: Colors.white,
    },
    tooltipContent: {
      flexDirection: 'row',
      marginLeft: 10,
      marginRight: 10,
    },
    tooltipContentStyle: {
      borderRadius: 15,
    },
    container: {},
    leftBox: {
      width: windowWidth * 0.35,
      height: windowHeight * 0.5,
    },
    rightBox: {
      width: windowWidth * 0.35,
      height: windowHeight * 0.5,
    },
    centerBox: {
      width: windowWidth * 0.35,
      height: windowHeight * 0.5,
    },
  });

  const swipePhoto = (swipeDirection) => {
    if (swipeDirection === 0) {
      swiperRef.current.swipeBack();
    } else {
      swiperRef.current.swipeCard();
    }
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
      <ScrollView showsVerticalScrollIndicator={false} style={{flex: 1}}>
        <View style={swipeCardStyles.container1}>
          <StatusBar barStyle="dark-content" backgroundColor="white" />
          <StaarzLogoHeader />
          {!net ? (
            <View
              style={{alignItems: 'center', justifyContent: 'center', flex: 1}}
            >
              <Image
                source={require('../assets/internat.png')}
                style={{height: 50, width: 50}}
              />
              <Text style={{fontSize: 17, fontWeight: '700', marginTop: 5}}>
                {string.network}
              </Text>
              <TouchableOpacity
                color="white"
                mode="contained"
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  top: windowHeight * 0.3,
                }}
                onPress={() => getasync()}
              >
                <View
                  style={{
                    backgroundColor: '#7643F8',
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 50,
                    paddingHorizontal: 30,
                    paddingVertical: 20,
                    marginTop: 10,
                  }}
                >
                  {Platform.OS === 'ios' ? (
                    <Text style={{fontSize: hp('2'), color: '#fff'}}>
                      {string.Try_Again}
                    </Text>
                  ) : (
                    <Text style={{fontSize: hp('2'), color: '#fff'}}>
                      {string.Try_Again}
                    </Text>
                  )}
                </View>
              </TouchableOpacity>
            </View>
          ) : (
            <>
              {isloading ? (
                <CircleFade
                  size={70}
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    alignSelf: 'center',
                    top: windowHeight * 0.4,
                  }}
                  color="#5E2EBA"
                />
              ) : (
                <View style={swipeCardStyles.container}>
                  {data ? (
                    <>
                      {data?.data?.length === 0 ? (
                        <>
                          <View
                            style={{
                              alignItems: 'center',
                              justifyContent: 'center',
                              display: 'flex',
                              flex: 0.8,
                            }}
                          >
                            <Text
                              style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                fontSize: hp('3'),
                                fontWeight: '800',
                                textAlign: 'center',
                              }}
                            >
                              No match found
                            </Text>
                            <View
                              style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'space-around',
                              }}
                            >
                              <TouchableOpacity
                                color="white"
                                mode="contained"
                                style={{
                                  justifyContent: 'center',
                                  alignItems: 'center',
                                }}
                                onPress={() => getasync()}
                              >
                                <View
                                  style={{
                                    backgroundColor: '#7643F8',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    borderRadius: 50,
                                    paddingHorizontal: 30,
                                    paddingVertical: 20,
                                    marginTop: 10,
                                  }}
                                >
                                  {Platform.OS === 'ios' ? (
                                    <Text
                                      style={{
                                        fontSize: hp('2'),
                                        color: '#fff',
                                      }}
                                    >
                                      Refresh
                                    </Text>
                                  ) : (
                                    <Text
                                      style={{
                                        fontSize: hp('2'),
                                        color: '#fff',
                                      }}
                                    >
                                      Refresh
                                    </Text>
                                  )}
                                </View>
                              </TouchableOpacity>
                            </View>
                          </View>
                        </>
                      ) : (
                        <>
                          <Swiper
                            ref={swiperRef}
                            cards={data?.data}
                            renderCard={(card) => {
                              const activeUser = data?.data?.[activeProfile];

                              return (
                                <View style={swipeCardStyles.card}>
                                  <ImageBackground
                                    resizeMode="cover"
                                    style={backgroundStyles.background}
                                    source={{
                                      uri:
                                        activeUser?.profilePicture ??
                                        ProfileImage?.[activeProfile],
                                    }}
                                  >
                                    <ProfilesBasicInfo
                                      user={data?.data}
                                      activeIndex={activeProfile}
                                    />
                                    <SwipeButtons
                                      user={activeUser?.id}
                                      activeUser={activeUser}
                                    />

                                    <View
                                      style={{
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        justifyContent: 'space-between',
                                        zIndex: 500,
                                      }}
                                    >
                                      <TouchableOpacity
                                        onPress={() => swipePhoto(0)}
                                        style={{
                                          width: windowWidth * 0.35,
                                          height: windowHeight * 0.5,
                                        }}
                                      />
                                      <TouchableOpacity
                                        onPress={() =>
                                          navigation.navigate('UserDetails')
                                        }
                                        style={{
                                          width: windowWidth * 0.35,
                                          height: windowHeight * 0.5,
                                        }}
                                      />
                                      <TouchableOpacity
                                        onPress={() => swipePhoto(1)}
                                        style={{
                                          width: windowWidth * 0.35,
                                          height: windowHeight * 0.5,
                                        }}
                                      />
                                    </View>
                                  </ImageBackground>
                                </View>
                              );
                            }}
                            onSwipedLeft={(newCardIndex) => {
                              dislikeButton(newCardIndex);
                            }}
                            cardVerticalMargin={80}
                            onSwipedRight={(newCardIndex) => {
                              likeButton(newCardIndex);
                            }}
                            onSwipedTop={(data) => {}}
                            disableBottomSwipe
                            onSwiped={(newCardIndex) =>
                              handleOnSwipeRight(newCardIndex)
                            }
                            onSwipedAll={() => {
                              fetchProfileForMatch();
                            }}
                            cardIndex={0}
                            cardStyle={{
                              height:
                                // eslint-disable-next-line no-nested-ternary
                                Platform.OS === 'ios'
                                  ? // eslint-disable-next-line no-nested-ternary
                                    width === 320 ||
                                    height === 610.6666666666666
                                    ? hp('79')
                                    : height === 736
                                    ? hp('83')
                                    : hp('79.5')
                                  : width === 320 ||
                                    height === 610.6666666666666
                                  ? hp('90')
                                  : hp('87'),

                              width: '100%',
                              borderWidth: data?.data.length > 0 ? 3 : 0,
                              borderColor:
                                data?.data.length > 0
                                  ? '#7643F8'
                                  : 'rgba(255,255,255,0.5)',
                              top: 20,
                              marginHorizontal: -20,
                            }}
                            backgroundColor="#fff"
                            overlayLabels={{
                              left: {
                                title: 'NOPE',
                                style: {
                                  label: {
                                    borderColor: 'red',
                                    color: 'red',
                                    borderWidth: 5,
                                  },
                                  wrapper: {
                                    top: 120,
                                    right: 120,
                                    alignItems: 'center',
                                    transform: [{rotate: '45deg'}],
                                  },
                                },
                              },
                              right: {
                                title: 'LIKE',
                                style: {
                                  label: {
                                    borderColor: '#7fff00',
                                    borderWidth: 5,
                                    color: '#7fff00',
                                  },
                                  wrapper: {
                                    alignItems: 'center',
                                    top: 100,
                                    left: 120,
                                    transform: [{rotate: '-45deg'}],
                                  },
                                },
                              },
                              top: {
                                title: 'SUPER LIKE',
                                style: {
                                  label: {
                                    borderColor: '#7b68ee',
                                    color: '#7b68ee',
                                    borderWidth: 5,
                                  },
                                  wrapper: {
                                    flexDirection: 'column',
                                    alignItems: 'center',
                                    bottom: -60,
                                    justifyContent: 'center',
                                    transform: [{rotate: '20deg'}],
                                  },
                                },
                              },
                            }}
                            animateOverlayLabelsOpacity
                            animateCardOpacity
                            stackSeparation={2}
                            showSecondCard
                          />
                        </>
                      )}
                    </>
                  ) : (
                    <>
                      <View
                        style={{
                          marginTop: hp('30'),
                        }}
                      >
                        <Text
                          style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            fontSize: hp('2'),
                            fontWeight: '900',
                            textAlign: 'center',
                            color: 'black',
                          }}
                        >
                          Oops, could not display screen
                        </Text>
                        <View
                          style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'space-around',
                          }}
                        >
                          <TouchableOpacity
                            activeOpacity={0.9}
                            onPress={() => mutate()}
                            style={{
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}
                          >
                            <View
                              style={{
                                backgroundColor: '#7643F8',
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: 40,
                                paddingHorizontal: 20,
                                paddingVertical: 10,
                                marginTop: 16,
                              }}
                            >
                              {Platform.OS === 'ios' ? (
                                <Text
                                  style={{
                                    fontSize: hp('2'),
                                  }}
                                >
                                  Refresh
                                </Text>
                              ) : (
                                <Text
                                  style={{
                                    color: '#fff',
                                  }}
                                >
                                  Refresh
                                </Text>
                              )}
                            </View>
                          </TouchableOpacity>
                        </View>
                      </View>
                    </>
                  )}
                </View>
              )}
            </>
          )}
        </View>
        <SwipeProfile user={data?.data} activeIndex={activeProfile} />
      </ScrollView>
    </SafeAreaView>
  );
}

const swipeCardStyles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: hp('2'),
  },
  container1: {
    flex: 1,
    width: '100%',
    height: hp(100),
  },
  card: {
    flex: 1,
    borderRadius: 4,
    width: '100%',
    height: hp(100),
    borderColor: '#E8E8E8',
  },
  text: {
    textAlign: 'center',
    fontSize: 50,
  },
});

const backgroundStyles = StyleSheet.create({
  background: {
    height: '100%',
    width: '100%',
  },
  container: {
    padding: 20,
    width: '100%',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default ProfileSwipe;
