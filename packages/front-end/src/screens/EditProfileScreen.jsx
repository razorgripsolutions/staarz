import React, {useCallback, useEffect, useState} from 'react';
import {
  Text,
  TouchableOpacity,
  TextInput,
  StyleSheet,
  View,
  SafeAreaView,
  Dimensions,
  ScrollView,
  Switch,
  Platform,
} from 'react-native';
import {useAuthProvider} from '../../App';
import {theme} from '../core/theme';
import {Colors} from '../constants/colors';
import Tabs from 'react-native-tabs';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import DropDown from '../components/DropDown';
import Button from '../components/Button';
import UserHeaderSetting from '../components/UserHeaderSetting';
import {
  favMovieGenreList,
  favMusicStyleList,
  favouriteTravelList,
  favPetList,
} from './TellUsMoreAboutYou1';
import {RelationshipList, ReligionList} from './RegisterUserBioScreen';
import {educationList, OccupationList} from './TellUsMoreAboutYou3';
import {useUpdateProfile} from '../api/hooks/update_profile';
import Toast from 'react-native-toast-message';

const {width} = Dimensions.get('window');
const {height} = Dimensions.get('window');

export default function EditProfileScreen(props) {
  const {userState, signIn} = useAuthProvider();
  const {navigation} = props;

  const {mutateAsync, isLoading} = useUpdateProfile();

  const [gender, setGender] = useState();
  const [email, setEmail] = React.useState();
  const [fullName, setFullName] = useState('');
  const [about, setAbout] = useState('');
  const [doYouSmoke, setDoYouSmoke] = useState(false);
  const [haveACar, setHaveACar] = useState(false);

  const [NewMatchSwitch, setNewMatchSwitch] = useState(false);
  const [occupation, setOccupation] = useState('');
  const [religion, setReligion] = useState('');
  const [favMovieGenre, setFavMovieGenre] = useState('');
  const [companyName, setCompanyName] = useState('');
  const [education, setEducation] = useState('');
  const [favMusicStyle, setFavMusicStyle] = useState('');
  const [isCentimeterEnabled, setIsCentimeterEnabled] = useState(true);
  const [yourHeight, setYourHeight] = useState(0);
  const [favTravelStyle, setFavTravelStyle] = useState('');
  const [favPet, setFavPet] = useState({});
  const [lookingFor, setLookingFor] = useState('');

  const toggleSwitch = () => {
    setIsCentimeterEnabled((previousState) => !previousState);
    setYourHeight();
  };

  useEffect(() => {
    if (userState?.userData) {
      handleSetValuesFromContext();
    }
  }, [userState, handleSetValuesFromContext]);

  const handleSetValuesFromContext = useCallback(() => {
    const genderValue = userState?.userData?.intrestedIn === 'Male' ? '0' : '1';
    const occupationValue = OccupationList?.find(
      (item) => item.label === userState?.userData?.occupation,
    )?.value;
    const typeOfRelationshipValue = RelationshipList.find(
      (item) => item.label === userState?.userData?.typeOfRelationship,
    )?.value;
    const userHasACar = userState?.userData?.haveCar === 1 ? true : false;
    const userSmokes = userState?.userData?.smokes === 1 ? true : false;
    setFullName(userState?.userData?.fullName);
    setEmail(userState?.userData?.emailAddress);
    setGender(genderValue);
    setAbout(userState?.userData?.aboutMe);
    setOccupation(occupationValue);
    setCompanyName(userState?.userData?.companyName);
    setLookingFor(typeOfRelationshipValue);
    setYourHeight(userState?.userData?.height);
    setHaveACar(userHasACar);
    setDoYouSmoke(userSmokes);
  }, [
    userState,
    setHaveACar,
    setDoYouSmoke,
    setYourHeight,
    setFullName,
    setEmail,
    setGender,
    setAbout,
    setOccupation,
    setCompanyName,
    setLookingFor,
  ]);

  const handleUpdateProfile = useCallback(async () => {
    try {
      let getNewData = {
        ...userState?.userData,
        fullName: fullName,
        gender: gender === '0' ? 'Male' : 'Female',
        aboutMe: about,
        typeOfRelationship: RelationshipList.find(
          (item) => item.value === +lookingFor,
        )?.label,
        occupation: OccupationList.find((item) => item.value === +occupation)
          ?.label,
        smokes: doYouSmoke ? 1 : 0,
        haveCar: haveACar ? 1 : 0,
        pets: favPet,
        height: yourHeight,
      };
      const response = await mutateAsync({
        request: {...getNewData},
        userId: userState?.userData?.id,
      });
      if (response?.data === 'OK') {
        await signIn(userState?.userToken, {
          ...getNewData,
        });
        return Toast.show({
          type: 'success',
          text1: 'Success',
          text2: `Your profile was updated successfully`,
        });
      }
    } catch (error) {
      Toast.show({
        type: 'error',
        text1: 'Application error',
        text2: 'Could not process your requst',
      });
    }
  }, [
    signIn,
    yourHeight,
    fullName,
    doYouSmoke,
    haveACar,
    userState,
    gender,
    about,
    lookingFor,
    occupation,
  ]);

  const handleOccupation = useCallback(
    (item) => {
      setOccupation(item.value);
    },
    [setOccupation],
  );

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: theme.colors.surface}}>
      <UserHeaderSetting goBack={navigation.goBack} userName="Edit profile" />
      <ScrollView
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps="always"
        style={{flex: 1}}
      >
        <View
          style={{
            width: '100%',
            padding: 20,
            alignSelf: 'center',
            marginTop: -20,
          }}
        >
          <Text
            style={{
              marginTop: 10,
              marginBottom: 5,
              fontWeight: '600',
              fontSize: width === 320 || height === 610.6666666666666 ? 14 : 16,
              lineHeight: 21,
              fontFamily: 'SFUIText-Semibold',
              color: '#98A5B7',
            }}
          >
            Full name
          </Text>
          <TextInput
            style={styles.input}
            onChangeText={(text) => setFullName(text)}
            value={fullName}
            maxLength={25}
            placeholder="fullName"
          />
        </View>
        <View
          style={{
            width: '100%',
            padding: 20,
            alignSelf: 'center',
            marginTop: -20,
          }}
        >
          <Text
            style={{
              marginTop: 10,
              marginBottom: 5,
              fontWeight: '600',
              fontSize: width === 320 || height === 610.6666666666666 ? 14 : 16,
              lineHeight: 21,
              fontFamily: 'SFUIText-Semibold',
              color: '#98A5B7',
            }}
          >
            Email
          </Text>
          <TextInput
            style={styles.input}
            onChangeText={(text) => setEmail(text)}
            editable={false}
            value={email}
            placeholder="email"
            keyboardType="email-address"
          />
        </View>

        <Text
          style={{
            marginTop: 10,
            marginBottom: 5,
            marginLeft: 20,
            fontWeight: '600',
            fontSize: width === 320 || height === 610.6666666666666 ? 14 : 16,
            lineHeight: 21,
            fontFamily: 'SFUIText-Semibold',
            color: '#98A5B7',
          }}
        >
          Gender
        </Text>
        <View style={{paddingHorizontal: 20}}>
          <View
            style={{
              width: '80%',
              marginTop: 55,
              flexDirection: 'row',
              justifyContent: 'center',
            }}
          >
            <Tabs
              selected={gender}
              style={{
                backgroundColor: '#EDEEF7',
                width: '100%',
                borderRadius: 8,
                borderColor: 'white',
              }}
              selectedStyle={{color: 'white'}}
              onSelect={(el) => setGender(el.props.name)}
            >
              <Text
                name="0"
                style={{
                  color: '#8A98AC',
                  fontFamily:
                    gender === '0' ? 'MPLUS1p-Bold' : 'MPLUS1p-Medium',
                }}
                selectedIconStyle={{
                  backgroundColor: '#8A98AC',
                  borderRadius: 8,
                  borderTopColor: '#8A98AC',
                  color: '#8a98ac',
                  borderTopLeftRadius: 8,
                  borderBottomLeftRadius: 8,
                }}
              >
                Male
              </Text>
              <Text
                name="1"
                style={{
                  color: '#8A98AC',
                  fontFamily:
                    gender === '1' ? 'MPLUS1p-Bold' : 'MPLUS1p-Medium',
                }}
                selectedIconStyle={{
                  backgroundColor: '#8A98AC',
                  borderRadius: 8,
                  borderTopColor: '#8A98AC',
                  color: '#8a98ac',
                  borderTopRightRadius: 8,
                  borderBottomRightRadius: 8,
                }}
              >
                Female
              </Text>
            </Tabs>
          </View>
        </View>
        <Text
          style={{
            color: '#101010',
            textAlign: 'left',
            marginBottom: 0,
            marginTop: 15,
            alignContent: 'flex-start',
            fontWeight: 'bold',
            fontSize: width === 320 || height === 610.6666666666666 ? 14 : 16,
            lineHeight: 21,
            fontFamily: 'SFUIText-Semibold',
            marginLeft: 20,
          }}
        >
          About
        </Text>
        <View
          style={{
            borderRadius: 5,
            padding: 5,
            borderColor: '#ccc',
            borderWidth: 0.5,
            marginHorizontal: 20,
            marginTop: 10,
          }}
        >
          <TextInput
            style={styles.AboutInput}
            inputContainerStyle={{borderBottomWidth: 0}}
            numberOfLines={2}
            multiline
            underlineColorAndroid="rgba(0,0,0,0)"
            maxLength={300}
            onChangeText={(text) => setAbout(text)}
            value={about}
            returnKeyType="done"
            keyboardType="email-address"
            autoCapitalize="none"
            textContentType="emailAddress"
            placeholder="Tell us more about you...."
          />
        </View>
        <View style={{width: '90%', marginHorizontal: 20}}>
          <View style={{flexDirection: 'row'}}>
            <View style={{flex: 1}}>
              <Text
                style={{
                  color: '#101010',
                  textAlign: 'left',
                  marginBottom: 8,
                  marginTop: 15,
                  alignContent: 'flex-start',
                  fontWeight: '600',
                  fontSize:
                    width === 320 || height === 610.6666666666666 ? 14 : 16,
                  lineHeight: 21,
                  fontFamily: 'SFUIText-Semibold',
                }}
              >
                Your height
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                alignItems: 'flex-end',
                alignContent: 'flex-end',
              }}
            >
              <Text
                numberOfLines={1}
                style={{
                  textAlign: 'left',
                  marginBottom: 8,
                  marginTop: 15,
                  alignContent: 'flex-start',
                  fontWeight: '600',
                  fontSize:
                    width === 320 || height === 610.6666666666666 ? 14 : 16,
                  lineHeight: 21,
                  fontFamily: 'SFUIText-Semibold',
                  color: '#919FB1',
                }}
              >
                {yourHeight}
                {isCentimeterEnabled ? 'cm' : 'ft'}
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row'}}>
            <View style={{flex: 1}}>
              <Text
                numberOfLines={1}
                style={{
                  marginTop: 30,
                  color: '#98A5B7',
                  fontWeight: '600',
                  fontSize: 20,
                  lineHeight: 22,
                  fontFamily: 'SFUIText-regular',
                }}
              >
                {isCentimeterEnabled ? 'Centimeters' : 'Feet'}
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                alignItems: 'flex-end',
                alignContent: 'flex-end',
                marginTop: 10,
              }}
            >
              <Switch
                style={{marginTop: 11}}
                trackColor={{false: '#d8e6f8', true: '#81b0ff'}}
                thumbColor={NewMatchSwitch ? '#81b0ff' : '#fff'}
                ios_backgroundColor="#ECF3FB"
                onValueChange={toggleSwitch}
                value={isCentimeterEnabled}
              />
            </View>
          </View>
          <TextInput
            value={yourHeight}
            style={{
              height: 40,
              marginTop: 10,
              borderBottomColor: '#98A5B7',
              borderBottomWidth: 1,
              width: '100%',
            }}
            maxLength={isCentimeterEnabled ? 3 : 3}
            onChangeText={(text) => setYourHeight(text)}
            returnKeyType="done"
            placeholder="input your height"
            placeholderTextColor="#98A5B7"
            keyboardType="number-pad"
          />
        </View>
        <View
          style={{
            alignContent: 'center',
            alignSelf: 'center',
            width: '90%',
          }}
        >
          <Text
            style={{
              color: '#101010',
              textAlign: 'left',
              marginBottom: 0,
              marginTop: 15,
              alignContent: 'flex-start',
              fontWeight: '600',
              fontSize: width === 320 || height === 610.6666666666666 ? 14 : 16,
              lineHeight: 21,
              fontFamily: 'SFUIText-Semibold',
            }}
          >
            Do you have a car?
          </Text>
          <View style={{flexDirection: 'row', marginTop: 10}}>
            <TouchableOpacity
              style={{
                backgroundColor: !haveACar ? '#F44335' : '#F3F6FF',
                width: 50,
                height: 50,
                borderRadius: 15,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => {
                setHaveACar(false);
              }}
            >
              <FontAwesome
                name="remove"
                size={20}
                color={!haveACar ? '#F3F6FF' : '#F44335'}
              />
            </TouchableOpacity>
            <View style={{padding: 10}} />
            <TouchableOpacity
              style={{
                backgroundColor: !haveACar ? '#F3F6FF' : '#00AEB0',
                width: 50,
                height: 50,
                borderRadius: 15,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => {
                setHaveACar(true);
              }}
            >
              <FontAwesome
                name="check"
                size={20}
                color={!haveACar ? '#00AEB0' : '#F3F6FF'}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={{height: 15}} />
        <View
          style={{
            alignContent: 'center',
            alignSelf: 'center',
            width: '90%',
          }}
        >
          <Text
            style={{
              color: '#101010',
              textAlign: 'left',
              marginBottom: 0,
              marginTop: 8,
              alignContent: 'flex-start',
              fontWeight: '600',
              fontSize: width === 320 || height === 610.6666666666666 ? 14 : 16,
              lineHeight: 21,
              fontFamily: 'SFUIText-Semibold',
            }}
          >
            Do you smoke?
          </Text>
          <View style={{flexDirection: 'row', marginTop: 10}}>
            <TouchableOpacity
              style={{
                backgroundColor: !doYouSmoke ? '#F44335' : '#F3F6FF',
                width: 50,
                height: 50,
                borderRadius: 15,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => {
                setDoYouSmoke(false);
              }}
            >
              <FontAwesome
                name="remove"
                size={20}
                color={!doYouSmoke ? '#F3F6FF' : '#F44335'}
              />
            </TouchableOpacity>
            <View style={{padding: 10}} />
            <TouchableOpacity
              style={{
                backgroundColor: !doYouSmoke ? '#F3F6FF' : '#00AEB0',
                width: 50,
                height: 50,
                borderRadius: 15,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => {
                setDoYouSmoke(true);
              }}
            >
              <FontAwesome
                name="check"
                size={20}
                color={!doYouSmoke ? '#00AEB0' : '#F3F6FF'}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={{
            alignContent: 'center',
            alignSelf: 'center',
            width: '90%',
            ...(Platform.OS === 'ios' && {
              zIndex: 20,
            }),
          }}
        >
          <DropDown
            heading="Occupation"
            item={OccupationList}
            defaultValue={occupation}
            onChangeItem={(item) => handleOccupation(item)}
          />
        </View>
        <View
          style={{
            alignContent: 'center',
            alignSelf: 'center',
            width: '90%',
            zIndex: 10,
          }}
        >
          <Text
            style={{
              color: '#101010',
              textAlign: 'left',
              marginBottom: 0,
              marginTop: 15,
              alignContent: 'flex-start',
              fontWeight: '600',
              fontSize: width === 320 || height === 610.6666666666666 ? 14 : 16,
              lineHeight: 21,
              fontFamily: 'SFUIText-Semibold',
            }}
          >
            Company
          </Text>
          <TextInput
            returnKeyType="next"
            placeholder="Company Name"
            value={companyName}
            maxLength={15}
            onChangeText={(text) => setCompanyName(text)}
            style={{
              marginTop: 10,
              color: '#C2C3CF',
              backgroundColor: '#fff',
              borderRadius: 5,
              width: '100%',
              height: 50,
              borderWidth: 0.7,
              borderColor: '#ccc',
              paddingHorizontal: 15,
            }}
            theme={{
              colors: {text: 'black', primary: 'rgb(33, 151, 186)'},
            }}
          />
        </View>
        <View
          style={{
            alignContent: 'center',
            alignSelf: 'center',
            width: '90%',
            ...(Platform.OS === 'ios' && {
              zIndex: 20,
            }),
          }}
        >
          {educationList && educationList.length > 0 ? (
            <DropDown
              heading="Education"
              item={educationList}
              defaultValue={education}
              onChangeItem={(item) => setEducation(item.value)}
            />
          ) : null}
        </View>
        <View
          style={{
            alignContent: 'center',
            alignSelf: 'center',
            width: '90%',
            ...(Platform.OS === 'ios' && {
              zIndex: 9,
            }),
          }}
        >
          {favMusicStyleList && favMusicStyleList.length > 0 ? (
            <DropDown
              heading="Favorite music style"
              item={favMusicStyleList}
              defaultValue={favMusicStyle}
              onChangeItem={(item) => setFavMusicStyle(item.value)}
            />
          ) : null}
        </View>
        <View
          style={{
            alignContent: 'center',
            alignSelf: 'center',
            width: '90%',
            ...(Platform.OS === 'ios' && {
              zIndex: 8,
            }),
          }}
        >
          {favMovieGenreList && favMovieGenreList.length > 0 ? (
            <DropDown
              heading="Favorite movie genre"
              item={favMovieGenreList}
              defaultValue={favMovieGenre}
              onChangeItem={(item) => setFavMovieGenre(item.value)}
            />
          ) : null}
        </View>

        <View
          style={{
            alignContent: 'center',
            alignSelf: 'center',
            width: '90%',
            ...(Platform.OS === 'ios' && {
              zIndex: 7,
            }),
          }}
        >
          <DropDown
            heading="Favorite pet"
            item={favPetList}
            defaultValue={[]}
            max={5}
            min={0}
            multiple
            onChangeItemMultiple={(item) => setFavPet({...item})}
          />
        </View>
        <View
          style={{
            alignContent: 'center',
            alignSelf: 'center',
            width: '90%',
            ...(Platform.OS === 'ios' && {
              zIndex: 6,
            }),
          }}
        >
          {ReligionList && ReligionList.length > 0 ? (
            <DropDown
              heading="Religion"
              item={ReligionList}
              defaultValue={religion}
              onChangeItem={(item) => setReligion(item.value)}
            />
          ) : null}
        </View>
        <View
          style={{
            alignContent: 'center',
            alignSelf: 'center',
            width: '90%',
            ...(Platform.OS === 'ios' && {
              zIndex: 5,
            }),
          }}
        >
          {favouriteTravelList && favouriteTravelList.length > 0 ? (
            <DropDown
              heading="Favorite travel style"
              item={favMusicStyleList}
              defaultValue={favTravelStyle}
              onChangeItem={(item) => setFavTravelStyle(item.value)}
            />
          ) : null}
        </View>
        <View
          style={{
            alignContent: 'center',
            alignSelf: 'center',
            width: '90%',
            ...(Platform.OS === 'ios' && {
              zIndex: 4,
            }),
          }}
        >
          {RelationshipList && RelationshipList.length > 0 ? (
            <DropDown
              heading={`What type of relationship are you looking for?`}
              item={RelationshipList}
              defaultValue={lookingFor}
              onChangeItem={(item) => setLookingFor(item.value)}
            />
          ) : null}
        </View>
        <View
          style={{
            width: '100%',
            paddingHorizontal: 20,
            justifyContent: 'space-between',
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: 30,
            marginBottom: 30,
          }}
        >
          <Text
            style={{
              color: '#101010',
              textAlign: 'left',
              marginBottom: 8,
              marginTop: 15,
              alignContent: 'flex-start',
              fontWeight: '600',
              fontSize: width === 320 || height === 610.6666666666666 ? 14 : 16,
              lineHeight: 21,
              fontFamily: 'SFUIText-Semibold',
            }}
          >
            User Photos
          </Text>
          <View
            style={{
              backgroundColor: '#ECF3FB',
              borderRadius: 100,
              paddingHorizontal: Platform.OS === 'ios' ? 5 : 2,
              paddingVertical: Platform.OS === 'ios' ? 5 : null,
            }}
          >
            <TouchableOpacity
              onPress={() => navigation.navigate('AddImageScreen')}
              style={{
                width: 100,
                height: 30,
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <Text
                style={{
                  color: '#101010',
                  textAlign: 'left',
                  alignContent: 'flex-start',
                  fontWeight: '600',
                  fontSize:
                    width === 320 || height === 610.6666666666666 ? 14 : 16,
                  lineHeight: 21,
                  fontFamily: 'SFUIText-Semibold',
                }}
              >
                Show
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={{
            width: '100%',
            paddingHorizontal: 20,
            justifyContent: 'space-between',
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: 10,
            zIndex: 1,
          }}
        >
          <Button
            onPress={handleUpdateProfile}
            isLoading={isLoading}
            style={bottomButton.login}
            color="#8897AA"
            mode="contained"
          >
            {isLoading ? (
              <Text
                style={{
                  textTransform: 'capitalize',
                  fontWeight: 'bold',
                  fontSize: 18,
                  color: 'white',
                }}
              >
                Updating...
              </Text>
            ) : (
              <Text
                style={{
                  textTransform: 'capitalize',
                  fontWeight: 'bold',
                  fontSize: 18,
                  color: 'white',
                }}
              >
                Done
              </Text>
            )}
          </Button>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  input: {
    height: 45,
    fontSize: width === 320 || height === 610.6666666666666 ? 14 : 14,
    lineHeight: 26,
    fontWeight: '600',
    borderBottomColor: '#E9E9E9',
    borderBottomWidth: 1,
    width: '90%',
  },
  compny: {
    height: 40,
    padding: 10,
    marginTop: 10,
    marginBottom: 5,
    borderColor: '#ccc',
    borderRadius: 5,
    borderWidth: 1,
    width: '100%',
  },
  AboutInput: {
    padding: 10,
    paddingVertical: 8,
    borderRadius: 5,
    marginTop: Platform.OS === 'ios' ? -10 : -10,
    height: Platform.OS === 'ios' ? 100 : 130,
    fontFamily: 'SFUIText-Medium',
    fontSize: 15,
    lineHeight: 20,
    color: Colors.gray['700'],
    textAlignVertical: 'top',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    borderBottomColor: '#ccc',
    width: '100%',
    alignContent: 'flex-start',
  },
});
const bottomButton = StyleSheet.create({
  bottomView: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 35,
  },
  login: {
    marginBottom: height >= 844 ? 10 : 30,
    borderRadius: 8,
  },
});
