import React, {useState} from 'react';
import {
  Image,
  View,
  StyleSheet,
  StatusBar,
  TouchableOpacity,
  ImageBackground,
  KeyboardAvoidingView,
  ActivityIndicator,
  Platform,
} from 'react-native';
import {Text} from 'react-native-paper';
// import Background from '../components/Background'
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Button from '../components/Button';
import {theme} from '../core/theme';

const styles = StyleSheet.create({
  background: {
    flex: 1,
    width: '100%',
    backgroundColor: theme.colors.surface,
  },
  container: {
    flex: 1,
    // padding: 20,
    width: '100%',
    // maxWidth: 340,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
// import RNHeicConverter from 'react-native-heic-converter';
// import ImagePicker from 'react-native-customized-image-picker';
// import ImagePicker from 'react-native-image-crop-picker';
const uploadYourPictureBackgroundStyles = StyleSheet.create({
  background: {
    width: '100%',
    height: hp('60'),
    position: 'absolute',
    top: '0%',
    resizeMode: 'cover',
    marginHorizontal: -100,
  },
  container: {
    flex: 1,
    // padding: 20,
    width: '100%',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
});

const bottomButton = StyleSheet.create({
  bottomView: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    // position: 'absolute',
    bottom: hp('2'),
    paddingLeft: 20,
    paddingRight: 20,
  },
  login: {
    marginBottom: 30,
    top: '20%',
    borderRadius: 8,
  },
});
const sideLogoStyles = StyleSheet.create({
  image: {
    marginTop: 0,
    marginLeft: 20,
  },
  imageLogo: {
    marginBottom: 8,
  },
  container: {
    position: 'absolute',
    top: Platform.OS === 'ios' ? hp('4') : hp('3'),
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 0,
    zIndex: 10,
  },
});

const uploadButton = StyleSheet.create({
  button: {
    // marginTop: hp('48'),
    marginTop: hp('52'),
  },
  navigation: {
    marginVertical: hp('5'),
    top: -14,
  },
});

function Test_Screen({navigation, route}) {
  const [userData, setUserData] = useState();
  const [accessToken, setAccessToken] = useState();
  const [uploadedImage, setUploadedImage] = useState(null);
  const [isLoading, setIsloading] = useState(false);
  // alert(JSON.stringify(userData))
  const handleChoosePhoto = () => {
    console.log('TODO IMPLEMENT!');
  };

  return (
    <Background>
      {/* <BackButton goBack={navigation.goBack} /> */}
      <StatusBar backgroundColor="transparent" translucent />
      <StaarzLogo />
      <UploadYourPictureBackground />
      {isLoading ? (
        <Image
          blurRadius={Platform.OS === 'ios' ? 0.7 : 0.7}
          source={require('../assets/verifyYourPictureButton.png')}
          style={uploadButton.button}
        />
      ) : (
        <TouchableOpacity onPress={handleChoosePhoto}>
          <Image
            source={require('../assets/verifyYourPictureButton.png')}
            style={uploadButton.button}
          />
        </TouchableOpacity>
      )}
      <Text style={{fontWeight: '800', letterSpacing: 2, fontSize: hp('2.8')}}>
        Verify your picture
      </Text>
      <Text
        style={{
          textAlign: 'center',
          letterSpacing: 1,
          lineHeight: 25,
          color: 'black',
          marginTop: 15,
          fontSize: hp('1.8'),
        }}
      >
        Please open your camera for face{'\n'}verification
      </Text>
      <Image
        source={require('../assets/verifyYourPictureNavigationImage.png')}
        style={uploadButton.navigation}
      />
      <View style={bottomButton.bottomView}>
        {isLoading ? (
          <ActivityIndicator
            style={bottomButton.login}
            size="large"
            color="#8897AA"
          />
        ) : (
          <Button style={bottomButton.login} color="#8897AA" mode="contained">
            <Text
              style={{
                textTransform: 'capitalize',
                fontWeight: 'bold',
                fontSize: 18,
                color: 'white',
              }}
            >
              Next
            </Text>
          </Button>
        )}
      </View>
    </Background>
  );
}

function Background({children}) {
  return (
    <ImageBackground resizeMode="repeat" style={styles.background}>
      <KeyboardAvoidingView style={styles.container} behavior="padding">
        {children}
      </KeyboardAvoidingView>
    </ImageBackground>
  );
}

function UploadYourPictureBackground({children}) {
  return (
    <ImageBackground
      source={require('../assets/newBackground.jpg')}
      // resizeMode="stretch"
      style={uploadYourPictureBackgroundStyles.background}
    >
      <KeyboardAvoidingView
        style={uploadYourPictureBackgroundStyles.container}
        behavior="padding"
      >
        {children}
      </KeyboardAvoidingView>
    </ImageBackground>
  );
}

function StaarzLogo() {
  return (
    <View style={sideLogoStyles.container}>
      <Image
        source={require('../assets/starzlogo_23X26.png')}
        style={sideLogoStyles.imageLogo}
      />
      <Image
        source={require('../assets/STAARZ_109X19_black.png')}
        style={sideLogoStyles.image}
      />
    </View>
  );
}

export default Test_Screen;
