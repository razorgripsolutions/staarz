/* eslint-disable no-constant-condition */
// staarztesting@gmail.com
// Shubham.tectum@gmail.com
// https://blog.logrocket.com/managing-network-connection-status-in-react-native/

import React, {useState, useEffect, useRef} from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  StatusBar,
  Linking,
  View,
  Image,
  ImageBackground,
  KeyboardAvoidingView,
  ActivityIndicator,
  Dimensions,
  Alert,
  Platform,
  NativeModules,
} from 'react-native';
import {Text} from 'react-native-paper';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

// import { Text, Checkbox } from 'react-native-paper'
// import { theme } from '../core/theme'
import {CheckBox} from 'react-native-elements';
import DropdownAlert from 'react-native-dropdownalert';
import NetInfo from '@react-native-community/netinfo';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  AccessToken,
  GraphRequest,
  GraphRequestManager,
  LoginManager,
} from 'react-native-fbsdk';
import {
  GoogleSignin,
  statusCodes,
} from '@react-native-google-signin/google-signin';
import Zocial from 'react-native-vector-icons/Zocial';
import {url} from '../const/urlDirectory';
import {passwordValidator} from '../helpers/passwordValidator';
import {emailValidator} from '../helpers/emailValidator';
import Button from '../components/Button';
import TextInput from '../components/TextInput';
import {checkConnection} from '../components/checkConnection';
import {string} from '../const/string';

const {width} = Dimensions.get('window'); // full width
const {height} = Dimensions.get('window');
// alert(width);
// 414 736
function LoginScreen(props) {
  const propsdata = props;
  const [checked, setChecked] = React.useState(false);
  const [email, setEmail] = useState({value: '', error: ''});
  const [password, setPassword] = useState({value: '', error: ''});
  const [is_Connected, setis_Connected] = useState(false);
  const [passtrue, setpasstrue] = useState(false);
  const {navigation} = props;
  const {route} = props;
  const [userData, setUserData] = useState({
    userID: 0,
    userEmail: '',
    userPassword: '',
    username: '',
    facebookToken: '',
    googleToken: '',
    facebookID: '',
    rocketChatLoginResponse: '',
    googleID: '',
    fbAuthStatus: 0,
    googleAuthStatus: 0,
    fullName: '',
    firstName: '',
    lastName: '',
    dob: '0000-00-00',
    locationText: '',
    lat: 0,
    lon: 0,
    gender: 0,
    religionID: 0,
    introVideoID: 0,
    profilePicture: '',
    height: 0,
    weight: 0,
    alternateLocation: '',
    haveCar: 0,
    doesSmoke: 0,
    aboutMe: '',
    relationshipType: 0,
    packageID: 0,
    genderInterest: 0,
    identifyID: 0,
    emailVerified: 1,
    isImageVerified: 0,
    isVideoVerified: 0,
    isProfileVerified: 0,
    photoVerificationPath: '',
    videoVerificationPath: '',
    identifyWith: 0,
    galleryPhotos1: '',
    galleryPhotos2: '',
    galleryPhotos3: '',
    galleryPhotos4: '',
    galleryPhotos5: '',
    galleryPhotos6: '',
    galleryVideo1: '',
    galleryVideo2: '',
    galleryVideo3: '',
    fbProfileLink: '',
    instaProfileLink: '',
    favoritSong: '',
    bestHobby: '',
    favoritePet: '',
    travelToLike: '',
    occupation: '',
    job: '',
    favPet: '',
    company: '',
    education: '',
    school: '',
    interest1: 0,
    interest2: 0,
    interest3: 0,
    interest4: 0,
    interest5: 0,
    yourHobby: '',
    yourMusic: '',
    yourMovie: '',
    Compny: '',
    anyPet: '',
    fevtravelStayle: '',
    lookingFor: 0,
    uploadedImage: '',
    clickedImage: '',
  });

  const [rChatData, setrChatData] = useState([]);
  const [result, setUser_Info] = useState([]);
  // alert(JSON.stringify(userData))
  const [accessToken, setAccessToken] = useState('');
  const [isLoading, setIsloading] = useState(false);
  const [isFBLoading, setIsFBloading] = useState(false);
  const [isGoogleLoading, setIsGoogleloading] = useState(false);
  const [isSettingUserdata, setIsSettingUserdata] = useState(false);
  const dropDownAlertRef = useRef();
  checkConnection().then((res) => {
    if (res === false) {
      dropDownAlertRef.alertWithType('error', 'Error', string.network);
    }
  });

  const onClose = () => {
    if (Platform.OS === 'android') {
      NativeModules.UIMailLauncher.launchMailApp(); // UIMailLauncher is the
      return;
    }
    Linking.openURL('message:0'); // iOS
  };

  useEffect(() => {
    // alert(JSON.stringify(data)
    // const userInfo = setUserInfo(null);

    // const gettingLoginStatus = setGettingLoginStatus(true);
    // const isLoading = setIsloading(false);
    // const isFBLoading = setIsFBloading(false);
    // const isGoogleLoading = setIsGoogleloading(false);

    // Initial configuration
    GoogleSignin.configure({
      // Mandatory method to call before calling signIn()
      // scopes: ['https://www.googleapis.com/auth/drive.readonly'],
      // Repleace with your webClientId
      // Generated from Firebase console
      androidClientId:
        '868382892767-68on9u7k2enkl210456hrs2akjfi2dte.apps.googleusercontent.com',

      webClientId:
        '868382892767-vib6n749aunhm5pis4sol0utop2n4aeu.apps.googleusercontent.com',
      iosClientId:
        '868382892767-ogn6qb7fif5k97a78g1n3vtaugann7gf.apps.googleusercontent.com',
    });

    NetInfo.NetInfoSubscribation = NetInfo.addEventListener((state) => {
      console.log('Connection type', state.type);
      console.log('Connection type', state);
      console.log('Is connected?', state.isConnected);
      if (state.isConnected === false) {
        // dropDownAlertRef.alertWithType('error', 'Error', "You are not connected to the internet");
      }
      setis_Connected(state.isConnected);
    });
    // Check if user is already signed in
    // _isSignedIn();
    if (NetInfo.NetInfoSubscribation) {
      NetInfo.NetInfoSubscribation();
    }

    // alert(JSON.stringify(propsdata.route.params))
    if (propsdata.route.params) {
      dropDownAlertRef.alertWithType(
        null,
        string.ForgetPAssMessage,
        string.f_Title,
      );
    }

    // isFacebookSignedIn();
    // getfcmToken()
  }, []);

  // getfcmToken = async () => {

  // }

  // GOOGLE CODE START
  // const _isSignedIn = async () => {
  //   const isSignedIn = await GoogleSignin.isSignedIn();
  //   if (isSignedIn) {
  //     // alert('User is already signed in');
  //     // Set User Info if user is already signed in
  //     // _getCurrentUserInfo();
  //   } else {
  //     console.log('Please Login');
  //   }
  // };
  // const _getCurrentUserInfo = async () => {
  //   try {
  //     let info = await GoogleSignin.signInSilently();
  //     console.log('User Info --> ', info);
  //     _handleGoogleSignUp(info);
  //   } catch (error) {
  //     if (error.code === statusCodes.SIGN_IN_REQUIRED) {
  //       // alert('User has not signed in yet');
  //       console.log('User has not signed in yet');
  //     } else {
  //       // alert("Unable to get user's info");
  //       console.log("Unable to get user's info");
  //     }
  //   }
  // };

  const CheckConnectivity = () => {
    // alert(password.value)
    checkConnection().then((res) => {
      if (res === false) {
        dropDownAlertRef.alertWithType('error', 'Error', string.network);
      } else {
        onLoginPressed();
      }
    });

    // NetInfo.fetch().then(state => {
    //   console.log("state::::" + state.isConnected)
    //   if (state.isConnected === true) {
    //     // Alert.alert("You are online!");
    //     onLoginPressed()
    //     // navigation.navigate('TellUsMoreAboutYou1',{userData:[]})
    //   } else {
    //     dropDownAlertRef.alertWithType('error', 'Error', "You are not connected to the internet");

    //     // Alert.alert("You are not connected to the internet");
    //   }
    // });
  };

  const _signIn = () => {
    checkConnection().then((res) => {
      if (res === false) {
        dropDownAlertRef.alertWithType('error', 'Error', string.network);
      } else {
        _signIn1();
      }
    });
  };

  const _signIn1 = async () => {
    // alert("ojk")
    // It will prompt google Signin Widget
    try {
      await GoogleSignin.hasPlayServices({
        // Check if device has Google Play Services installed
        // Always resolves to true on iOS
        showPlayServicesUpdateDialog: true,
      });
      const userInfo = await GoogleSignin.signIn();
      console.log('User Info --> ', userInfo);
      _handleGoogleSignin(userInfo);
      setUser_Info(userInfo);
    } catch (error) {
      console.log('Message', JSON.stringify(error));
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log('User Cancelled the Login Flow');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log('Signing In');
      } else if (
        (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE,
        console.log(`Signing In error.code ${error.code}`))
      ) {
        alert('Play Services Not Available or Outdated');
      } else {
        console.log('--', error.message);
        alert(error.message);
      }
    }
  };
  const _signOut = async () => {
    // Remove user session from the device.
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      // Removing user Info
    } catch (error) {
      console.error(error);
    }
  };
  const _handleGoogleSignin = async (aResult) => {
    try {
      if (true) {
        const tempData = userData;
        tempData.fullName = aResult.user.name;
        tempData.userEmail = aResult.user.email;
        tempData.profilePicture = aResult.user.photo;
        tempData.googleToken = aResult.idToken;
        tempData.googleID = aResult.user.id;
        tempData.googleAuthStatus = 1;
        // alert(JSON.stringify(result))
        setUserData({tempData});
        await fetch(url.googleSignIn, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({googleID: result.user.id}),
        })
          .then((response) => response.json())
          .then(async (responseJson) => {
            // alert("Succees")
            // console.log("responseJson---------googlelogin---->>>>>>>>>>>>>>>>>>>>>>>>>>", responseJson.user.profileCompletionStatus)
            // console.log(JSON.stringify(responseJson))

            // console.log(JSON.stringify(responseJson))
            // alert(responseJson.user.profileCompletionStatus)
            if (responseJson.status === 200) {
              const Complete_status = responseJson.user.profileCompletionStatus;

              const accessToken1 = responseJson.accessToken;
              const someProperty = userData;
              someProperty.userID = responseJson.user.userID;
              someProperty.rocketChatLoginResponse =
                responseJson.rocketChatLoginResponse;
              someProperty.fullName = responseJson.user.fullName;
              someProperty.fullName = responseJson.user.fullName;
              someProperty.dob = responseJson.user.dob;
              someProperty.locationText = responseJson.user.locationText;
              someProperty.lat = responseJson.user.lat;
              someProperty.lon = responseJson.user.lon;
              someProperty.gender = responseJson.user.gender;
              someProperty.religionID = responseJson.user.religionID;
              someProperty.relationshipType =
                responseJson.user.relationshipType;
              someProperty.genderInterest = responseJson.user.genderInterest;
              someProperty.fbProfileLink = responseJson.user.fbProfileLink;
              someProperty.instaProfileLink =
                responseJson.user.instaProfileLink;
              someProperty.profilePicture = responseJson.user.clickedImage;

              if (Complete_status === 1) {
                navigation.replace('RegisterUserBioScreen', {
                  userData: someProperty,
                  accessToken: accessToken1,
                  // religionOptionsList: religionOptionsList,
                  // relationshipTypeList: relationshipTypeList
                });
              } else if (Complete_status === 2) {
                navigation.replace('socialProfile', {
                  userData: someProperty,
                  accessToken: accessToken1,
                  // religionOptionsList: religionOptionsList,
                  // relationshipTypeList: relationshipTypeList
                });
              } else if (Complete_status === 3) {
                navigation.replace('uploadYourPicture', {
                  userData: someProperty,
                  accessToken: accessToken1,
                  // religionOptionsList: religionOptionsList,
                  // relationshipTypeList: relationshipTypeList
                });
              } else if (Complete_status === 4) {
                navigation.replace('addPhotoVideo', {
                  userData: someProperty,
                  accessToken: accessToken1,
                  // religionOptionsList: religionOptionsList,
                  // relationshipTypeList: relationshipTypeList
                });
              } else if (Complete_status === 5) {
                // var tempData = userData;
                // tempData.userID = responseJson.user.userID;
                // setUserData(tempData)
                // alert(JSON.stringify(responseJson.rocketChatLoginResponse))
                console.log(
                  'rocketChatLoginResponse----facebook-->>>>>>>>>>>>>>>>>>>>>>>>>>',
                  responseJson.rocketChatLoginResponse,
                );
                AsyncStorage.setItem(
                  'Token',
                  JSON.stringify(responseJson.accessToken),
                );
                AsyncStorage.setItem('user', JSON.stringify(responseJson.user));
                AsyncStorage.setItem(
                  'rChatData',
                  JSON.stringify(responseJson.rocketChatLoginResponse),
                );
                const useridusefcm = responseJson.user.userID;
                const accessTokenuseFcm = responseJson.accessToken;
                console.log(
                  'useridusefcm--------------------------',
                  useridusefcm,
                  'accessTokenuseFcm--------------------',
                  accessTokenuseFcm,
                );
                fcmtokenApi(useridusefcm, accessTokenuseFcm);
                // let rchatNeewduserId = responseJson.user.userID;
                // await getrchattoken(rchatNeewduserId);
                navigation.replace('BottomTabs');
              }
            }
            if (responseJson.status === 401) {
              // alert("fell")
              // alert("User is not properly registered. code 401")
              // setPassword({error:responseJson.message})
            }
            if (responseJson.status === 404) {
              // alert("Oops!! Something went wrong. 404")
              // alert(`Google Login : ${responseJson.message} Plese register first`);
              _handleGoogleSignUp(result);
              // alert("register")
              // navigation.reset({
              //   index: 0,
              //   routes: [{ name: 'LoginScreen' }],
              // })
            }
            if (responseJson.status === 400) {
              // alert("fell")
              // alert(responseJson.message + "400")
              // setPassword({error:responseJson.message})
            }
          })
          .catch((error) => {
            // alert(error)
            console.error('-error-', error);
          });
      }
    } catch ({message}) {
      alert(`Google Login update error: ${message}`);
    }
  };

  // rchatToken
  // const getrchattoken = async (rchatNeewduserId) => {
  //   // alert(rchatNeewduserId)
  //   fetch(url.getUserRCIDByUserId, {
  //     method: 'POST',
  //     headers: {
  //       Accept: 'application/json',
  //       'Content-Type': 'application/json',
  //     },
  //     body: JSON.stringify({
  //       userID: rchatNeewduserId,
  //       // authToken: rchatdata.authToken
  //     })

  //   }).then((response) => response.json())
  //     .then((responseJson) => {
  //       //  alert(JSON.stringify(responseJson.result))
  //       console.log("getUserRCIDByUserId------------------id--- get-----------------", JSON.stringify(responseJson))
  //       if (responseJson.status === 200) {

  //         // AsyncStorage.setItem('rChatData', JSON.stringify(responseJson.result[0]))
  //         //  setData(responseJson.result)

  //       }

  //     })
  //     .catch((error) => {
  //       alert(error)
  //       //ß setIsloading(false)
  //       // alert('Oops error!!')
  //       console.error(error);
  //     });
  // }

  // google siup
  const _handleGoogleSignUp = async (aResult) => {
    // alert(result.user.name)
    try {
      if (true) {
        // ???
        const tempData = userData;
        tempData.fullName = aResult.user.name;
        tempData.userEmail = aResult.user.email;
        tempData.profilePicture = '';
        tempData.googleToken = aResult.idToken; // aResult.accessToken;
        tempData.googleID = aResult.user.id;
        tempData.googleAuthStatus = 1;
        // alert(JSON.stringify(aResult))
        setUserData({tempData});

        fetch(url.googleSignUp, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(userData),
        })
          .then((response) => response.json())
          .then((responseJson) => {
            console.log(
              'goolge login in tetsing ------',
              JSON.stringify(responseJson),
            );
            if (responseJson.status === 200) {
              const aTempData = userData;
              // tempData.userID = responseJson.registrationresult.insertId;
              // tempData.rocketChatLoginResponse = responseJson.rocketChatLoginResponse

              aTempData.userID = responseJson.user.userID;
              aTempData.rocketChatLoginResponse =
                responseJson.rocketChatLoginResponse;
              // setUserData(tempData)
              navigation.replace('RegisterUserBioScreen', {
                userData,
                accessToken: responseJson.accessToken,
                // religionOptionsList: religionOptionsList,
                // relationshipTypeList: relationshipTypeList
              });
            }
            if (responseJson.status === 404) {
              // alert("User already exist, Please login.")
              // alert(responseJson.message)
              _signOut();
              // navigation.reset({
              //   index: 0,
              //   routes: [{ name: 'LoginScreen' }],
              // })
            }
            if (responseJson.status === 400) {
              // alert("Oops!! Something went wrong.")
              alert(` ${responseJson.message}`);
            }
          })
          .catch((error) => {
            alert(error);
            console.error('----sigup error', error);
          });
      }
    } catch (e) {
      Alert.alert('Oops!', 'Login failed!');
      return {error: true};
    }
  };
  // sigup

  // GOOGLE CODE END

  const onLoginPressed = () => {
    const emailError = emailValidator(email.value);
    const passwordError = passwordValidator(password.value);
    if (emailError || passwordError) {
      setEmail({...email, error: emailError});
      setPassword({...password, error: passwordError});
      return;
    }
    setIsloading(true);
    fetch(url.login, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        userEmail: email.value,
        userPassword: password.value,
        rememberMe: checked,
      }),
    })
      .then((response) => response.json())
      .then(async (responseJson) => {
        // alert(JSON.stringify(responseJson))
        // console.log("token-----", responseJson.accessToken)
        // console.log(responseJson.rocketChatLoginResponse)

        console.log(
          '-----------user------------->>>>>>>login onLoginPressednormal>>>>>>>>>>>>>,',
          JSON.stringify(responseJson),
        );
        // return false
        if (responseJson.status === 200) {
          const Complete_status = responseJson.user.profileCompletionStatus;
          if (Complete_status === 1) {
            const accessToken1 = responseJson.accessToken;

            const someProperty = userData;
            someProperty.userID = responseJson.user.userID;
            someProperty.rocketChatLoginResponse =
              responseJson.rocketChatLoginResponse;

            // setUserData(someProperty)

            navigation.replace('RegisterUserBioScreen', {
              userData: someProperty,
              accessToken: accessToken1,
            });
          } else if (Complete_status === 2) {
            console.log('bioScreen -----', JSON.stringify(responseJson.user));
            const accessToken1 = responseJson.accessToken;
            const someProperty = userData;
            someProperty.userID = responseJson.user.userID;
            someProperty.rocketChatLoginResponse =
              responseJson.rocketChatLoginResponse;
            someProperty.fullName = responseJson.user.fullName;
            someProperty.firstName = responseJson.user.fullName;
            someProperty.dob = responseJson.user.dob;
            someProperty.locationText = responseJson.user.locationText;
            someProperty.lat = responseJson.user.lat;
            someProperty.lon = responseJson.user.lon;
            someProperty.gender = responseJson.user.gender;
            someProperty.religionID = responseJson.user.religionID;
            someProperty.relationshipType = responseJson.user.relationshipType;
            someProperty.genderInterest = responseJson.user.genderInterest;
            navigation.replace('socialProfile', {
              userData: someProperty,
              accessToken: accessToken1,
            });
          } else if (Complete_status === 3) {
            const accessToken1 = responseJson.accessToken;
            const someProperty = userData;
            someProperty.userID = responseJson.user.userID;
            someProperty.rocketChatLoginResponse =
              responseJson.rocketChatLoginResponse;
            someProperty.fullName = responseJson.user.fullName;
            someProperty.firstName = responseJson.user.fullName;
            someProperty.dob = responseJson.user.dob;
            someProperty.locationText = responseJson.user.locationText;
            someProperty.lat = responseJson.user.lat;
            someProperty.lon = responseJson.user.lon;
            someProperty.gender = responseJson.user.gender;
            someProperty.religionID = responseJson.user.religionID;
            someProperty.relationshipType = responseJson.user.relationshipType;
            someProperty.genderInterest = responseJson.user.genderInterest;
            someProperty.fbProfileLink = responseJson.user.fbProfileLink;
            someProperty.instaProfileLink = responseJson.user.instaProfileLink;

            navigation.replace('uploadYourPicture', {
              userData: someProperty,
              accessToken: accessToken1,
            });
          } else if (Complete_status === 4) {
            const accessToken1 = responseJson.accessToken;
            const someProperty = userData;
            someProperty.userID = responseJson.user.userID;
            someProperty.rocketChatLoginResponse =
              responseJson.rocketChatLoginResponse;
            someProperty.fullName = responseJson.user.fullName;
            someProperty.firstName = responseJson.user.fullName;
            someProperty.dob = responseJson.user.dob;
            someProperty.locationText = responseJson.user.locationText;
            someProperty.lat = responseJson.user.lat;
            someProperty.lon = responseJson.user.lon;
            someProperty.gender = responseJson.user.gender;
            someProperty.religionID = responseJson.user.religionID;
            someProperty.relationshipType = responseJson.user.relationshipType;
            someProperty.genderInterest = responseJson.user.genderInterest;
            someProperty.fbProfileLink = responseJson.user.fbProfileLink;
            someProperty.instaProfileLink = responseJson.user.instaProfileLink;
            someProperty.profilePicture = responseJson.user.clickedImage;
            navigation.replace('addPhotoVideo', {
              userData: someProperty,
              accessToken: accessToken1,
            });
          } else if (Complete_status === 5) {
            // Complete User Register condition Navigation
            const useridusefcm = responseJson.user.userID;
            const accessTokenuseFcm = responseJson.accessToken;
            console.log(
              'rocketChatLoginResponse--------------------------',
              responseJson.rocketChatLoginResponse,
              'accessTokenuseFcmnew--------------------',
              responseJson.rocketChatLoginResponse,
            );
            fcmtokenApi(useridusefcm, accessTokenuseFcm);
            setUserData(responseJson.user);
            setAccessToken(responseJson.accessToken);
            setIsloading(false);

            AsyncStorage.setItem(
              'Token',
              JSON.stringify(responseJson.accessToken),
            );
            AsyncStorage.setItem('user', JSON.stringify(responseJson.user));
            AsyncStorage.setItem(
              'rChatData',
              JSON.stringify(responseJson.rocketChatLoginResponse),
            );
            UserStatus({
              status: '1',
              Token: responseJson.accessToken,
              userId: JSON.parse(responseJson.user.userID),
            });

            navigation.replace('BottomTabs', {
              userData: responseJson.user,
              accessToken,
              rChatData: responseJson.rocketChatLoginResponse,
            });

            // Complete User Register condition Navigation
          }

          // alert(responseJson.user.profileCompletionStatus)
        }
        if (responseJson.status === 404) {
          setIsloading(false);
          // alert("Password Wrong, Please login.")

          setPassword({error: responseJson.message});
        }
        if (responseJson.status === 401) {
          // alert("User is not properly registered. code 401")
        }
        if (responseJson.status === 400) {
          setIsloading(false);
          // console.log("400-------", responseJson.message)
          // alert("Network issue, please try again.")
        } else {
          setIsloading(false);
        }
      })
      .catch((error) => {
        setIsloading(false);
        // alert(error)
        console.error(error);
      });
  };

  // FACEBOOK CODE START
  const isFacebookSignedIn = () => {
    AccessToken.getCurrentAccessToken().then(
      (data) => {
        console.log(data);
        initFacebookCurrentUser(data.accessToken);
      }, // Refresh it every time
    );
  };
  const initFacebookCurrentUser = (token) => {
    fetch(
      `https://graph.facebook.com/v2.5/me?fields=email,name,friends&access_token=${token}`,
    )
      .then((response) => response.json())
      .then((json) => {
        console.log('json-----------', JSON.stringify(json));
        // Some user object has been set up somewhere, build that user here
        FBlogIn(json, token);
      })
      .catch(() => {
        throw new Error('ERROR GETTING DATA FROM FACEBOOK');
      });
  };
  const FBlogIn = async (userInfo, token) => {
    // alert(JSON.stringify(userInfo))
    console.log('userinfo----details', JSON.stringify(userInfo));
    try {
      if (true) {
        const tempData = userData;
        tempData.fullName = userInfo.name;
        tempData.userEmail = userInfo.email;
        tempData.profilePicture = userInfo.picture.data.url; // userInfo.picture.url;
        tempData.facebookToken = token;
        tempData.facebookID = userInfo.id;
        tempData.fbAuthStatus = 1;
        setUserData(tempData);
        // alert("ok")
        await fetch(url.fbSignIn, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({facebookID: userInfo.id}),
        })
          .then((response) => response.json())
          .then(async (responseJson) => {
            // alert(JSON.stringify(responseJson))

            // console.log("responseJson-----------facebook-rocketChatLoginResponse-->>>>>>>>>>>>>>>>>>>>>>>>>>", responseJson)
            if (responseJson.status === 200) {
              const Complete_status = responseJson.user.profileCompletionStatus;

              const accessToken1 = responseJson.accessToken;
              const someProperty = userData;
              someProperty.userID = responseJson.user.userID;
              someProperty.rocketChatLoginResponse =
                responseJson.rocketChatLoginResponse;
              someProperty.fullName = responseJson.user.fullName;
              someProperty.firstName = responseJson.user.fullName;
              someProperty.dob = responseJson.user.dob;
              someProperty.locationText = responseJson.user.locationText;
              someProperty.lat = responseJson.user.lat;
              someProperty.lon = responseJson.user.lon;
              someProperty.gender = responseJson.user.gender;
              someProperty.religionID = responseJson.user.religionID;
              someProperty.relationshipType =
                responseJson.user.relationshipType;
              someProperty.genderInterest = responseJson.user.genderInterest;
              someProperty.fbProfileLink = responseJson.user.fbProfileLink;
              someProperty.instaProfileLink =
                responseJson.user.instaProfileLink;
              someProperty.profilePicture = responseJson.user.clickedImage;

              if (Complete_status === 1) {
                navigation.replace('RegisterUserBioScreen', {
                  userData: someProperty,
                  accessToken: accessToken1,
                  // religionOptionsList: religionOptionsList,
                  // relationshipTypeList: relationshipTypeList
                });
              } else if (Complete_status === 2) {
                navigation.replace('socialProfile', {
                  userData: someProperty,
                  accessToken: accessToken1,
                  // religionOptionsList: religionOptionsList,
                  // relationshipTypeList: relationshipTypeList
                });
              } else if (Complete_status === 3) {
                navigation.replace('uploadYourPicture', {
                  userData: someProperty,
                  accessToken: accessToken1,
                  // religionOptionsList: religionOptionsList,
                  // relationshipTypeList: relationshipTypeList
                });
              } else if (Complete_status === 4) {
                navigation.replace('addPhotoVideo', {
                  userData: someProperty,
                  accessToken: accessToken1,
                  // religionOptionsList: religionOptionsList,
                  // relationshipTypeList: relationshipTypeList
                });
              } else if (Complete_status === 5) {
                // var tempData = userData;
                // tempData.userID = responseJson.user.userID;
                // setUserData(tempData)
                // alert(JSON.stringify(responseJson.rocketChatLoginResponse))
                console.log(
                  'rocketChatLoginResponse----facebook-->>>>>>>>>>>>>>>>>>>>>>>>>>',
                  responseJson.rocketChatLoginResponse,
                );
                AsyncStorage.setItem(
                  'Token',
                  JSON.stringify(responseJson.accessToken),
                );
                AsyncStorage.setItem('user', JSON.stringify(responseJson.user));
                AsyncStorage.setItem(
                  'rChatData',
                  JSON.stringify(responseJson.rocketChatLoginResponse),
                );
                const useridusefcm = responseJson.user.userID;
                const accessTokenuseFcm = responseJson.accessToken;
                console.log(
                  'useridusefcm--------------------------',
                  useridusefcm,
                  'accessTokenuseFcm--------------------',
                  accessTokenuseFcm,
                );
                fcmtokenApi(useridusefcm, accessTokenuseFcm);
                // let rchatNeewduserId = responseJson.user.userID;
                // await getrchattoken(rchatNeewduserId);
                navigation.replace('BottomTabs');
              }
            }
            if (responseJson.status === 404) {
              // alert("User already exist, Please login.")
              // alert(`Facebook Login : ${responseJson.message}  Plese Register first`);
              FBSignUp(userInfo, token);
              // navigation.reset({
              //   index: 0,
              //   routes: [{ name: 'LoginScreen' }],
              // })
            }
            if (responseJson.status === 400) {
              // alert("Oops!! Something went wrong.")
            }
          })
          .catch((error) => {
            alert(error);
            console.error(error);
          });
      }
    } catch ({message}) {
      alert(`Facebook Login update error: ${message}`);
    }
  };
  const getInfoFromToken = (token) => {
    const PROFILE_REQUEST_PARAMS = {
      fields: {
        string: 'id, name, first_name, last_name,email,picture',
      },
    };
    const profileRequest = new GraphRequest(
      '/me',
      {token, parameters: PROFILE_REQUEST_PARAMS},
      (error, aResult) => {
        if (error) {
          console.log(`login info has error: ${error}`);
        } else {
          console.log('json-----result-------', JSON.stringify(aResult));
          console.log('token------------', JSON.stringify(token));
          FBlogIn(aResult, token);
        }
      },
    );
    new GraphRequestManager().addRequest(profileRequest).start();
  };

  const loginWithFacebook = () => {
    checkConnection().then((res) => {
      if (res === false) {
        dropDownAlertRef.alertWithType('error', 'Error', string.network);
      } else {
        loginWithFacebook1();
      }
    });
  };

  const loginWithFacebook1 = async () => {
    try {
      LoginManager.setLoginBehavior('web_only');
      await LoginManager.logInWithPermissions(['public_profile', 'email']).then(
        (login) => {
          if (login.isCancelled) {
            console.log('Login cancelled');
          } else {
            AccessToken.getCurrentAccessToken().then((data) => {
              const newAccessToken = data.accessToken.toString();
              console.log('accessToken---facbook------', data);
              getInfoFromToken(newAccessToken);
            });
          }
        },
        (error) => {
          console.log(`Login fail with error000: ${error}`);
        },
      );
    } catch ({message}) {
      console.log('User have facebook app');
      if (Platform.OS === 'android') {
        LoginManager.setLoginBehavior('web_only');
      }
      await LoginManager.logInWithPermissions([
        'public_profile',
        'email',
        'mobile',
      ]).then(
        (login) => {
          if (login.isCancelled) {
            console.log('Login cancelled');
          } else {
            AccessToken.getCurrentAccessToken().then((data) => {
              const newAccessToken = data.accessToken.toString();
              console.log('newAccessToken---facbook------', data);
              getInfoFromToken(newAccessToken);
            });
          }
        },
        (error) => {
          console.log(`Login fail with error----: ${error}`);
        },
      );
    }
  };
  // FACEBOOK CODE END

  const FBSignUp = async (userInfo, token) => {
    console.log('-----', JSON.stringify(userInfo));
    // alert("ok")
    let fburl = '';
    if (userInfo.picture.data.url === '') {
      fburl = '';
    } else {
      fburl = userInfo.picture.data.url;
    }
    // alert(fburl)
    try {
      if (true) {
        const tempData = userData;
        tempData.fullName = userInfo.name;
        tempData.userEmail = userInfo.email;
        tempData.profilePicture = ''; // userInfo.picture.url; //userInfo.picture.data.url
        tempData.facebookToken = token;
        tempData.facebookID = userInfo.id;
        tempData.fbAuthStatus = 1;
        setUserData(tempData);
        // alert(JSON.stringify(tempData))
        console.log(userData);
        fetch(url.fbSignUp, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(userData),
        })
          .then((response) => response.json())
          .then((responseJson) => {
            console.log(
              '---------FBSignU-pFBSignU-pFBSignUp----------',
              JSON.stringify(responseJson),
            );
            if (responseJson.status === 200) {
              const newTempData = userData;
              newTempData.userID = responseJson.user.userID;
              newTempData.rocketChatLoginResponse =
                responseJson.rocketChatLoginResponse;
              setUserData(newTempData);
              navigation.replace('RegisterUserBioScreen', {
                userData,
                accessToken: responseJson.accessToken,
                // religionOptionsList: religionOptionsList,
                // relationshipTypeList: relationshipTypeList
              });
            }
            if (responseJson.status === 404) {
              // alert(responseJson.msg)
              LoginManager.logOut();
              // navigation.reset({
              //   index: 0,
              //   routes: [{ name: 'LoginScreen' }],
              // })
            }
            if (responseJson.status === 401) {
              // alert(responseJson.msg)
              LoginManager.logOut();
              // navigation.reset({
              //   index: 0,
              //   routes: [{ name: 'LoginScreen' }],
              // })
            }
            if (responseJson.status === 400) {
              alert(responseJson.message);
            }
          })
          .catch((error) => {
            alert(error);
            console.error(error);
          });
      }
    } catch ({message}) {
      alert(`Facebook Login update error: ${message}`);
    }
  };

  //  @   fcm token Api  strat //

  const fcmtokenApi = async (useridusefcm, accessTokenuseFcm) => {
    const fcmToken = await AsyncStorage.getItem('fcmToken');
    // alert(fcmToken)
    console.log('=-------fcmToken----------------------------', fcmToken);

    return fetch(url.setFirebaseTokenByUserId, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        token: accessTokenuseFcm,
        userID: useridusefcm,
        firebase_token: fcmToken,
        device_type: Platform.OS === 'ios' ? 'ios' : 'android',
      }),
    })
      .then((response) => response.json())
      .then((response) => {
        console.log('fcmtoken  Api response-------------------', response);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  //  @   fcm token Api End//

  // madhuri@testing.com
  // aishwerya

  //    USer Active Status //

  const UserStatus = (data) => {
    // alert(JSON.stringify({
    //     onlineStatus:data.status,
    //     userID:data.userId,
    //     token:data.Token
    //   }))

    const data1 = JSON.stringify({
      onlineStatus: data.status,
      userID: data.userId,
      token: data.Token,
    });

    const config = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: data1,
    };
    fetch(url.setOnlineStatus, config)
      .then((response) => response.json())
      .then((responseJson) => {
        // alert(JSON.stringify(responseJson))
        console.log('---', JSON.stringify(responseJson));
        if (responseJson.status === 200) {
          // setIsloading(false)
        }
      })
      .catch((error) => {
        // alert(error)
        console.error(error);
        // setIsloading(false)
      });
  };

  return (
    <LoginBackground>
      <KeyboardAvoidingView
        style={{
          alignItems: 'center',
          width: '100%',
          // height: "80%",
          flex: 8,

          backgroundColor: 'rgba(0, 0, 0, 0.69)',
        }}
        behavior={Platform.OS === 'ios' ? 'padding' : 'padding'}
      >
        <>
          <View
            style={{
              height: '40%',
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
              width: '100%',
            }}
            testID="login-page"
          >
            <StatusBar
              barStyle="light-content"
              backgroundColor="rgba(0, 0, 0, 0.7)"
            />
            <StaarzLogo />
          </View>
          <View style={{height: '60%', alignItems: 'center', width: '100%'}}>
            <View style={{flex: 1, alignItems: 'center'}}>
              <View
                style={{
                  position: 'absolute',
                  bottom: 160,
                  alignItems: 'center',
                }}
              >
                <Button
                  style={{
                    borderRadius: 45,
                    width: wp(90),
                    bottom:
                      width === 320 || height === 610.6666666666666 ? -25 : 8,
                  }}
                  color="#2673CB"
                  mode="contained"
                  onPress={loginWithFacebook}
                >
                  <Zocial name="facebook" color="white" size={hp('2.3')} />
                  {/* <Image source={require('../assets/facebook1.png')} style={{height:20,width:20,resizeMode:"contain"}} /> */}
                  <Text
                    style={{
                      textTransform: 'none',
                      fontWeight: '500',
                      fontSize: hp('2.1'),
                      color: 'white',
                    }}
                  >
                    &nbsp;&nbsp;Connect with Facebook
                  </Text>
                </Button>
                <Button
                  style={{
                    borderRadius: 45,
                    width: wp(90),
                    bottom:
                      width === 320 || height === 610.6666666666666 ? -25 : 12,
                  }}
                  color="#FC3850"
                  mode="contained"
                  onPress={_signIn}
                >
                  {/* <Text style={{ marginLeft: 100 ,}}> */}
                  {/* <Image source={require('../assets/google.png')} style={{height:18,width:20,resizeMode:"contain"}} /> */}
                  <Zocial
                    name="googleplus"
                    color="white"
                    size={hp('2.3')}
                    style={{}}
                  />
                  {/* </Text> */}
                  <Text
                    style={{
                      textTransform: 'none',
                      fontWeight: '500',
                      fontSize: hp('2.1'),
                      color: 'white',
                    }}
                  >
                    &nbsp;&nbsp;Connect with Google
                  </Text>
                </Button>

                <View
                  style={{
                    height: 30,
                    width: width / 1.01,
                    alignItems: 'center',
                    bottom:
                      width === 320 || height === 610.6666666666666 ? -40 : 0,
                  }}
                >
                  <Image
                    source={require('../assets/or.png')}
                    style={orDividerLineStyle.or}
                  />
                </View>
              </View>

              <View style={{position: 'absolute', bottom: 0}}>
                <TextInput
                  style={{backgroundColor: '#3F2A30'}}
                  label="Email"
                  // labelStyle={{ backgroundColor: "red" }}
                  returnKeyType="next"
                  value={email.value}
                  // scrollEnabled={false}
                  onChangeText={(text) => setEmail({value: text, error: ''})}
                  error={!!email.error}
                  errorText={email.error}
                  autoCapitalize="none"
                  autoCompleteType="email"
                  // icon={!passtrue ? "eye" : "eye-with-line"}
                  // onPress={() => setpasstrue(!passtrue)}
                  // colorIcon={"#fff"}
                  // icon={!passtrue ? "eye" : "eye-with-line"}
                  // onPress={() => setpasstrue(!passtrue)}
                  textContentType="emailAddress"
                  keyboardType="email-address"
                  // colorIcon={"#fff"}
                  // onLayoutAnimatedText={(i, x) => console.log(i)}

                  placeholder="Enter your email"
                  // theme={{ colors: { placeholder: '#fff', text: '#fff', primary: '#fff', borderWidth: 0.5, underlineColor: 'transparent', } }}
                  // style={{ borderColor: 'blue', height: 50, color: "red" }}
                  theme={{
                    borderRadius: 25,
                    colors: {
                      placeholder: 'white',
                      text: 'white',
                      primary: '#FFFFFF',
                      underlineColor: 'transprent',
                    },
                  }}
                />

                {/* <LinearGradient
                  colors={['rgba(0, 0, 0, 0.1)', 'rgba(0, 0, 0, 0.1)']}
                  style={{ width: 50, marginHorizontal: 10, alignItems: "center", top: 10, zIndex: 100, }}
                > */}
                {/* <Text style={{ color: "#fff", zIndex: 30, width: 50, textAlign: "center", backgroundColor: "#3F2A31", top: 10, }}>Email</Text> */}
                {/* </LinearGradient> */}
                {/* <View style={{ height: 20, width: "100%", borderWidth: 1, color: "#fff", borderColor: "#fff" }} /> */}

                <TextInput
                  style={{
                    backgroundColor: '#3F2A30',
                    // opacity: 0.59
                  }}
                  label="Password"
                  // selectionColor="red"
                  // labelStyley={}
                  // placeholderTextColor={'red'}
                  returnKeyType="done"
                  value={password.value}
                  onChangeText={(text) => setPassword({value: text, error: ''})}
                  error={!!password.error}
                  errorText={password.error}
                  url={
                    passtrue
                      ? require('../assets/eyeIcon.png')
                      : require('../assets/eyeIcon.png')
                  }
                  onPress={() => setpasstrue(!passtrue)}
                  colorIcon="#fff"
                  secureTextEntry={passtrue}
                  placeholder="Enter your password"
                  theme={{
                    colors: {
                      placeholder: '#fff',
                      text: '#fff',
                      primary: '#FFFFFF',
                      borderWidth: 0.5,
                      underlineColor: 'transparent',
                    },
                  }}
                />
              </View>
            </View>
          </View>
        </>
      </KeyboardAvoidingView>
      <View
        style={{
          alignItems: 'center',
          width: '100%',
          height: '20%',
          flex: 2,
          // alignSelf: 'center',
          // alignItems: 'center',
          // justifyContent: 'center',
          backgroundColor: 'rgba(0, 0, 0, 0.69)',
          // backgroundColor: "red"
        }}
      >
        <View
          style={{
            flexDirection: 'row',
            width: '100%',
            alignItems: 'center',
            height: '29%',
          }}
        >
          <View style={{width: '50%', zIndex: 10}}>
            <CheckBox
              disabled={false}
              value={checked}
              checked={checked}
              size={18}
              containerStyle={{
                backgroundColor: 'transparent',
                borderWidth: 0,
                borderRadius: 8,
                paddingVertical: 5,
              }}
              onPress={() => {
                setChecked(!checked);
              }}
              textStyle={{
                color: 'white',
                fontSize: hp('1.7'),
                alignItems: 'center',
                textAlign: 'center',
                marginLeft: 4,
                lineHeight: 20,
              }}
              title="Remember me"
            />
          </View>
          <View
            style={{
              width: '50%',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <TouchableOpacity
              onPress={() => navigation.navigate('ForgotPasswordScreen')}
            >
              <Text
                style={{
                  color: '#CBCEE8',
                  textAlign: 'right',
                  fontSize: hp('1.7'),
                  right: -20,
                }}
              >
                Forgot password?{' '}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={bottomButton.bottomView}>
          {isLoading ? (
            <ActivityIndicator
              style={bottomButton.login}
              size="large"
              color="#8897AA"
            />
          ) : (
            <Button
              style={bottomButton.login}
              color="#8897AA"
              mode="contained"
              onPress={CheckConnectivity}
            >
              <Text
                style={{
                  textTransform: 'capitalize',
                  fontWeight: 'bold',
                  fontSize: hp('2.3'),
                  color: 'white',
                }}
              >
                Login
              </Text>
            </Button>
          )}
          <View>
            <Text
              style={{color: 'white', letterSpacing: 0.5, fontSize: hp('1.8')}}
            >
              Still doesnt have an account?{' '}
            </Text>
          </View>
          <TouchableOpacity
            style={{}}
            onPress={() => navigation.navigate('RegisterScreen')}
          >
            <Text
              style={{
                fontWeight: 'bold',
                color: '#8A98AC',
                lineHeight: 21,
                fontSize: hp('1.8'),
              }}
            >
              Sign up
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      <DropdownAlert
        ref={dropDownAlertRef}
        // defaultContainer={{
        //   paddingTop: Platform.OS === "android" ? 0 : 0, flexDirection: 'row', width: "90%", alignSelf: "center", borderTopRightRadius: 20, borderBottomLeftRadius: 20,
        //   borderBottomRightRadius: 20, backgroundColor: "#7643F6", top: Platform.OS === "ios" ? "10%" : "2%",
        // }}
        imageSrc={require('../assets/ForgetMEssage.png')}
        imageStyle={{width: wp(7), height: wp(7), alignSelf: 'center'}}
        containerStyle={{
          padding: 15,
          backgroundColor: '#7643F6',
          borderTopRightRadius: 20,
          borderBottomLeftRadius: 20,
          borderBottomRightRadius: 20,
          top: Platform.OS === 'ios' ? '10%' : '2%',
          width: '90%',
          alignSelf: 'center',
        }}
        closeInterval={10000}
        // onClose={(data) =>onClose(data)}
        messageStyle={{
          fontSize: wp(2.8),
          color: '#FFFFFF',
          top: -3,
          fontFamily: 'MPLUS1p-Bold',
        }}
        titleStyle={{
          fontSize: wp(2.8),
          color: '#FFFFFF',
          fontFamily: 'MPLUS1p-Bold',
        }}
        defaultTextContainer={{fontSize: wp(1), left: 10}}
      />
    </LoginBackground>

    // </SafeAreaView >
  );
}

function LoginBackground({children}) {
  return (
    <ImageBackground
      source={require('../assets/loginscreenbg.png')}
      resizeMode="stretch"
      style={loginBackgroundStyles.background}
    >
      {/* <KeyboardAvoidingView style={loginBackgroundStyles.container}
      behavior={Platform.OS === "ios" ? "position" : "padding"}
    // behavior={Platform.OS === "ios" ? "padding" : "height"}
    // behavior="padding"
    > */}

      {children}

      {/* </KeyboardAvoidingView> */}
    </ImageBackground>
  );
}

function OrDividerLine() {
  return (
    <View>
      <Image
        source={require('../assets/or.png')}
        style={orDividerLineStyle.or}
      />
    </View>
  );
}

const orDividerLineStyle = StyleSheet.create({
  or: {
    // marginTop: 25,
    // marginBottom: 200,
    // color: "red",
    // backgroundColor:"green",
    // alignSelf: 'center',
    width: wp(90),
  },
});

const socialMediaLoginButtonStyle = StyleSheet.create({
  facebook: {
    marginBottom: 8,
    width: 340,
  },
  google: {
    width: 340,
  },
});

function StaarzLogo() {
  return (
    <View
      style={{
        flexDirection: 'row',
        marginTop: '15%',
      }}
    >
      <Image source={require('../assets/logo_41x48.png')} />
      <Image
        source={require('../assets/staarz_180X33_white.png')}
        style={sideLogoStyles.image}
      />
    </View>
  );
}

const sideLogoStyles = StyleSheet.create({
  image: {
    marginTop: 8,
    marginLeft: 20,
  },
});

const loginBackgroundStyles = StyleSheet.create({
  background: {
    width: '100%',
    height: '100%',
    // position: 'absolute',
    // top: '0%',
    flex: 1,
  },
  container: {
    // flex: 1,

    width: '100%',
    height: '100%',
    // alignSelf: 'center',
    // alignItems: 'center',
    // justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
});

const bottomButton = StyleSheet.create({
  bottomView: {
    width: '90%',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor:"red",
    // position:Platform.OS === 'ios'?  'absolute' :"relative",
    position: 'absolute',
    bottom: Platform.OS === 'ios' ? hp(1.5) : hp(1),
  },
  login: {
    marginBottom: 10,
    paddingHorizontal: 20,
    borderRadius: 8,
  },
});

export default LoginScreen;
