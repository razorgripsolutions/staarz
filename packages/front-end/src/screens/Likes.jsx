/* eslint-disable react/no-unused-state */
import React, {Component} from 'react';
import {
  TextInput,
  ScrollView,
  Dimensions,
  StatusBar,
  Image,
  View,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  Platform,
} from 'react-native';
import {Text} from 'react-native-paper';

import NetInfo from '@react-native-community/netinfo';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import {CircleFade} from 'react-native-animated-spinkit';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {backendBaseUrl, url} from '../const/urlDirectory';

import {string} from '../const/string';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const {width} = Dimensions.get('window'); // full width
const {height} = Dimensions.get('window');
const data = ['Liked', 'Liked Me', 'Matches'];
let userId = '';
export default class Likes extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userid: '',
      isloading: false,
      user: [],
      userData: [],
      Likes: [],
      whoLikeMe: [],
      liketext: '',
      net: true,
      liketext1: '',
      activeButton: 0,
      whoLikeMepic: [],
      match: [],
      matchtext: '',
      accessToken: '',
      hellper: [
        {
          fullName: 'Hello',
          isSuperLike: 1,
          profilePicture: 'image1631684340782.jpeg',
          userID: 1051,
        },
        {
          fullName: 'Watson',
          isSuperLike: 1,
          profilePicture: 'image1629380516451.jpeg',
          userID: 856,
        },
      ],
      subsciption_data: [],
    };
  }

  // alert(JSON.stringify(userData));

  // const temp = {"userID":403,"userEmail":"ghdfgdf@dfsgdf.dfg","userPassword":"","username":"","facebookToken":"","googleToken":"","fbAuthStatus":0,"googleAuthStatus":0,"fullName":" Fhfgh gf","dob":"31-03-2021","locationText":"6-E, Nungambakkam High Road 1A, Dr, Thirumurthy Nagar, 1st Street, Nungambakkam, Chennai, Tamil Nadu 600034, India","lat":13.055441,"lon":80.2479924,"gender":"1","religionID":2,"introVideoID":0,"profilePicture":"","height":0,"weight":0,"alternateLocation":"","haveCar":0,"doesSmoke":0,"aboutMe":"","relationshipType":2,"packageID":0,"identifyID":0,"emailVerified":1,"isImageVerified":0,"isVideoVerified":0,"isProfileVerified":0,"facebookID":"","googleID":"","uploadedImage":"","clickedImage":"","fbProfileLink":"Dghgh","instaProfileLink":"Fghhjjjk"}
  // const [userData, setUserData] = useState([]); //route.params.userData);
  // const [accessToken, setAccessToken] = useState(); //route.params.accessToken);
  // const [instaProfileLink, setInstaProfileLink] = useState("");
  // const [whoLikeMepic, setwhoLikeMe] = useState([]);
  // const [Likes, setMyLikes] = useState([]);
  // const [isloading, setIsloading] = useState(false);
  // const [email, setEmail] = useState({ value: '', });
  // const [liketext, setliketext] = useState("");

  // const [liketext1, setliketext1] = useState("");

  // const [activeButton, setactiveButton] = useState([""])
  // alert(JSON.stringify(userData))
  // console.log(JSON.stringify(userData));
  // const navigation  = props.navigation;

  // const route = props.route;

  // useEffect(() => {

  //   // }, 500);

  //   getid();

  // },[]);

  async componentDidMount() {
    //  BackHandler.addEventListener("hardwareBackPress", this.backAction);
    this.setState({activeButton: 0});
    NetInfo.fetch().then((state) => {
      console.log('Connection type', state.type);
      // alert(state.isConnected);
      this.setState({net: state.isConnected});
    });

    this.getid();
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      // do something
    });

    // checkConnection().then(res => {
    //   if (res === false) {
    //     // dropDownAlertRef.alertWithType('error', 'Error', string.network);
    //     this.setState({ net: res })
    //   } else {
    //     // getasync1()
    //   }
    // })
  }

  async componentWillUnmount() {
    // BackHandler.removeEventListener("hardwareBackPress", this.backAction);
    this._unsubscribe();
  }

  getid = async () => {
    //  alert("ok")

    const Token = await AsyncStorage.getItem('Token');
    const user = await AsyncStorage.getItem('user');

    const user1 = JSON.parse(user);
    console.log(
      'user===============----user-------------->>>>>>>>>>>user>>>>>>>>>-------------',
      user1.profilePicture,
    );
    userId = user1.userID;
    this.setState({userData: user1, accessToken: Token});

    setInterval(() => {
      this.getSubscription(userId);
    }, 3000);
    // setInterval(() => {
    this.MyLikes(userId);
    this.whoLikeMe(userId);
    // }, 5000);
  };

  // Api CAll GEt DAta

  MyLikes = (aUserId) => {
    // alert("ok")
    this.setState({isloading: true});
    fetch(url.MyLikes, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        userID: aUserId,
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        //  alert(JSON.stringify(responseJson.result))
        console.log('myLike--------------', responseJson.result);
        if (responseJson.status === 200) {
          // setMyLikes(responseJson.result)
          this.setState({
            Likes: responseJson.result,
            isloading: false,
          });
        }
        this.arrayholder = responseJson.result;
        this.setState({isloading: false});
      })
      .catch((error) => {
        // alert(error)
        // dropDownAlertRef.alertWithType('error', 'Error', string.network);
        console.log('--=-=-=-=-=error', error);
        // if (error === "TypeError:Network request failed") {

        // }
        this.setState({isloading: false});
        // ß setIsloading(false)
        // alert('Oops error!!')
      });
  };

  whoLikeMe = (aUserId) => {
    this.setState({isloading: true});
    // alert(this.state.subsciption_data.length)
    // alert("who")
    fetch(url.whoLikeMe, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        userID: aUserId,
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        //  alert(JSON.stringify(responseJson.result))
        console.log('userId--------------', responseJson.result);
        if (responseJson.status === 200) {
          // setwhoLikeMe(responseJson.result)
          const someData = responseJson.result;
          console.log('datadatadatadatadata----', someData);
          this.setState({
            whoLikeMepic: someData,
            // whoLikeMepic: data.concat(this.state.hellper),
            isloading: false,
          });
        }
        this.arrayholder1 = responseJson.result;
      })
      .catch((error) => {
        // alert(error)
        // dropDownAlertRef.alertWithType('error', 'Error', string.network);
        this.setState({isloading: false});
        // ß setIsloading(false)
        // alert('Oops error!!')
        console.error(error);
      });
  };

  // End

  SearchFilterFunction = (text) => {
    if (this.arrayholder.length === 0) {
      console.log('text--', text);
    } else {
      const newData = this.arrayholder.filter((item) => {
        console.log('item===', item);
        const itemData = `${item.fullName.toUpperCase()}   
              ${item.fullName.toUpperCase()} ${item.fullName.toUpperCase()}`;

        const textData = text.toUpperCase();

        return itemData.indexOf(textData) > -1;
      });

      this.setState({Likes: newData});
    }

    this.setState({
      liketext: text,
    });
  };

  SearchFilterFunction2 = (text) => {
    // passing the inserted text in textinput
    if (this.arrayholder1.length === 0) {
      console.log('text--', text);
    } else {
      const newData = this.arrayholder1.filter((item) => {
        // applying filter for the inserted text in search bar
        const itemData = item.fullName
          ? item.fullName.toUpperCase()
          : ''.toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });

      this.setState({whoLikeMepic: newData});
    }

    this.setState({
      liketext1: text,
    });
  };

  SearchFilterFunction3 = (text) => {
    // passing the inserted text in textinput
    if (this.arrayholder3.length !== 0) {
      const newData3 = this.arrayholder3.filter((item) => {
        // applying filter for the inserted text in search bar
        const itemData3 = item.fullName
          ? item.fullName.toUpperCase()
          : ''.toUpperCase();
        const textData3 = text.toUpperCase();
        return itemData3.indexOf(textData3) > -1;
      });
      this.setState({match: newData3});
    } else {
      console.log(text);
    }

    this.setState({
      matchtext: text,
    });
  };

  apicall = (aUserId) => {
    // alert(userData)
    this.setState({isloading: false});
    fetch(url.channellist, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        userId: aUserId,
        // authToken: rchatdata.authToken
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        //  alert(JSON.stringify(responseJson.result))
        console.log(
          'datamesagechangelist--------------',
          JSON.stringify(responseJson.result),
        );
        if (responseJson.status === 200) {
          this.setState({
            match: responseJson.result,
            isloading: false,
          });
          if (responseJson.result.length === 0) {
            this.arrayholder3 = '';
          } else {
            this.arrayholder3 = responseJson.result;
          }
        }
      })
      .catch((error) => {
        // alert(error)
        this.setState({isloading: false});
        // ß setIsloading(false)
        // alert('Oops error!!')
        console.error(error);
      });
  };

  // SubScriptiomn APi claling

  getSubscription = (aUserId) => {
    // alert(userData)
    // this.setState({ isloading: true })
    fetch(url.fetchUserSubscribtion, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        aUserId,
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.status === 200) {
          console.log('subsciption-data ---------------', responseJson.result);
          // alert(JSON.stringify(responseJson.result))
          this.setState({subsciption_data: responseJson.result});
          // var data = responseJson.result;
          // data.map((i, x) => {
          //   console.log("data-new    ----", i.pakageID)
          //
          // })
          // setsubscribion(responseJson.result.pakageID)
          this.setState({isloading: false});
        }
      })
      .catch((error) => {
        this.setState({isloading: false});
        // alert('Oops error!!')
        console.error(error);
      });
  };

  ButtonPerform = async (index) => {
    await this.setState({activeButton: index});
    if (this.state.activeButton === 0) {
      NetInfo.fetch().then((state) => {
        console.log('Connection type', state.type);
        // alert(state.isConnected);
        if (state.isConnected === true) {
          this.MyLikes(userId);
        } else {
          this.setState({net: state.isConnected});
        }
      });
    } else if (this.state.activeButton === 1) {
      NetInfo.fetch().then((state) => {
        console.log('Connection type', state.type);
        // alert(state.isConnected);
        if (state.isConnected === true) {
          this.whoLikeMe(userId);
          this.getSubscription(userId);
        } else {
          this.setState({net: state.isConnected});
        }
      });
    } else if (this.state.activeButton === 2) {
      NetInfo.fetch().then((state) => {
        console.log('Connection type', state.type);
        // alert(state.isConnected);
        if (state.isConnected === true) {
          this.apicall(userId);
        } else {
          this.setState({net: state.isConnected});
        }
      });
    } else {
      console.log('---');
    }
  };

  LikesFuntion = (item, index) => {
    const indexlenth = this.state.Likes.length;
    // console.warn(indexlenth);
    return (
      <TouchableOpacity
        onPress={() =>
          this.props.navigation.navigate('UserDetails', {
            matchUserData: item,
            userData: this.state.userData,
            accessToken: this.state.accessToken,
          })
        }
        style={{
          width: '50%',
          alignItems: 'center',
          justifyContent: 'space-around',
          marginVertical: 5,
          marginBottom:
            // eslint-disable-next-line no-nested-ternary
            index % 2 === 0
              ? index === indexlenth - 1 || index === indexlenth - 2
                ? 0
                : null
              : index === indexlenth - 1
              ? 0
              : null,
        }}
      >
        <Image
          style={{width: '95%', height: 185, borderRadius: 5}}
          // source={{uri: 'https://reactnative.dev/img/tiny_logo.png'}}
          source={
            item.profilePicture === ''
              ? require('../assets/userpro.jpeg')
              : {uri: `${backendBaseUrl}data/${item.profilePicture}`}
          }
          // source={require('./../assets/tempSwipeImage.png')}
        />
        <Text
          numberOfLines={1}
          style={{
            margin: 20,
            position: 'absolute',
            bottom: -15,
            left: -3,
            fontWeight: '600',
            lineHeight: 14,
            fontSize: 13,
            color: '#FFFFFF',
            fontFamily: 'SFUIText-Bold',
          }}
        >
          {item.fullName}
        </Text>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: '#fff',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <StatusBar barStyle="dark-content" backgroundColor="white" />
        {/* <StaarzLogoHeader /> */}
        <View
          style={{
            flexDirection: 'row',
            position: 'absolute',
            top: Platform.OS === 'ios' ? 48 : 0,
            alignSelf: 'center',
            alignItems: 'center',
          }}
        >
          <Image
            source={require('../assets/starzlogo.png')}
            style={sideLogoStyles.imageLogo}
          />
          <Text
            style={{
              fontSize: wp(6),
              fontFamily: 'MPLUS1p-Bold',
              color: '#243443',
              left: -2,
              lineHeight: 33,
              textAlign: 'center',
            }}
          >
            STAARZ
          </Text>
        </View>

        {!this.state.net ? (
          <View
            style={{alignItems: 'center', justifyContent: 'center', flex: 1}}
          >
            <Image
              source={require('../assets/internat.png')}
              style={{height: 50, width: 50}}
            />
            <Text style={{fontSize: 17, fontWeight: '700', marginTop: 5}}>
              {string.network}
            </Text>
            <TouchableOpacity
              color="white"
              mode="contained"
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                top: windowHeight * 0.3,
              }}
              onPress={() => this.componentDidMount()}
            >
              <View
                style={{
                  backgroundColor: '#7643F8',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 50,
                  paddingHorizontal: 30,
                  paddingVertical: 20,
                  marginTop: 10,
                }}
              >
                {
                  Platform.OS === 'ios' ? (
                    // <Ionicons name="reload" color="black" size={30} />
                    <Text style={{fontSize: hp('2'), color: '#fff'}}>
                      {string.Try_Again}
                    </Text>
                  ) : (
                    <Text style={{fontSize: hp('2'), color: '#fff'}}>
                      {string.Try_Again}
                    </Text>
                  )
                  // <MaterialCommunityIcons name="reload" color="black" size={30} />
                }
              </View>
            </TouchableOpacity>
          </View>
        ) : (
          <>
            {this.state.isloading ? (
              <CircleFade
                size={110}
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  alignSelf: 'center',
                }}
                color="#5E2EBA"
              />
            ) : (
              <>
                {this.state.activeButton === 0 ? (
                  <View
                    style={{
                      backgroundColor: '#f5f5f5',
                      height: 45,
                      alignSelf: 'center',
                      width: '93%',
                      position: 'absolute',
                      top: Platform.OS === 'ios' ? 100 : 50,
                      borderRadius: 5,
                      flexDirection: 'row',
                      alignItems: 'center',
                      padding: 10,
                    }}
                  >
                    <FontAwesome
                      name="search"
                      color="#b0c4de"
                      size={20}
                      style={{width: '10%', left: 5}}
                    />
                    <TextInput
                      style={{
                        width: '90%',
                        height: 40,
                        backgroundColor: '#f5f5f5',
                        color: '#000',
                      }}
                      // underlineColorAndroid='transparent'
                      value={this.state.liketext}
                      onChangeText={(text) => this.SearchFilterFunction(text)}
                      placeholder="Search..."
                      placeholderTextColor="#b0c4de"
                      // theme={{ colors: { placeholder: 'white', text: 'white', primary: 'white',underlineColor:'transparent',background : '#003489'}}}
                    />
                  </View>
                ) : (
                  <>
                    {this.state.activeButton === 1 ? (
                      <View
                        style={{
                          backgroundColor: '#f5f5f5',
                          height: 45,
                          alignSelf: 'center',
                          width: '93%',
                          position: 'absolute',
                          top: Platform.OS === 'ios' ? 100 : 50,
                          borderRadius: 5,
                          flexDirection: 'row',
                          alignItems: 'center',
                          padding: 10,
                        }}
                      >
                        <FontAwesome
                          name="search"
                          color="#b0c4de"
                          size={20}
                          style={{width: '10%', left: 5}}
                        />
                        <TextInput
                          style={{
                            width: '90%',
                            height: 40,
                            backgroundColor: '#f5f5f5',
                            color: '#000',
                          }}
                          // underlineColorAndroid='transparent'
                          value={this.state.liketext1}
                          onChangeText={(text) =>
                            this.SearchFilterFunction2(text)
                          }
                          placeholder="Search..."
                          placeholderTextColor="#b0c4de"
                          // theme={{ colors: { placeholder: 'white', text: 'white', primary: 'white',underlineColor:'transparent',background : '#003489'}}}
                        />
                      </View>
                    ) : (
                      <View
                        style={{
                          backgroundColor: '#f5f5f5',
                          height: 45,
                          alignSelf: 'center',
                          width: '93%',
                          position: 'absolute',
                          top: Platform.OS === 'ios' ? 100 : 50,
                          borderRadius: 5,
                          flexDirection: 'row',
                          alignItems: 'center',
                          padding: 10,
                        }}
                      >
                        <FontAwesome
                          name="search"
                          color="#b0c4de"
                          size={20}
                          style={{width: '10%', left: 5}}
                        />
                        <TextInput
                          style={{
                            width: '90%',
                            height: 40,
                            backgroundColor: '#f5f5f5',
                            color: '#000',
                          }}
                          // underlineColorAndroid='transparent'
                          value={this.state.matchtext}
                          onChangeText={(text) =>
                            this.SearchFilterFunction3(text)
                          }
                          placeholder="Search..."
                          placeholderTextColor="#b0c4de"
                          // theme={{ colors: { placeholder: 'white', text: 'white', primary: 'white',underlineColor:'transparent',background : '#003489'}}}
                        />
                      </View>
                    )}
                  </>
                )}
                <View
                  style={{
                    height: 40,
                    width: '100%',
                    marginTop: Platform.OS === 'ios' ? 150 : 100,
                  }}
                >
                  <View
                    style={{
                      height: 40,
                      alignSelf: 'flex-start',
                      width: '100%',
                    }}
                  >
                    <ScrollView
                      horizontal
                      style={{left: 16}}
                      showsHorizontalScrollIndicator={false}
                    >
                      {data.map((item, index) => (
                        <TouchableOpacity
                          // eslint-disable-next-line react/no-array-index-key
                          key={index}
                          onPress={() => this.ButtonPerform(index)}
                          style={{marginHorizontal: 5, marginLeft: 5}}
                        >
                          {/* {console.log("activeButton,", activeButton)} */}
                          <Text
                            style={{
                              fontSize:
                                width === 320 || height === 610.6666666666666
                                  ? 12
                                  : hp('1.6'),
                              color:
                                this.state.activeButton === index
                                  ? '#000'
                                  : '#8A98AC',
                              fontWeight: '500',
                              paddingVertical: 8,
                              lineHeight: 17,
                              fontFamily: 'SFUIText-Bold',
                            }}
                          >
                            {item}
                          </Text>
                        </TouchableOpacity>
                      ))}
                    </ScrollView>
                  </View>
                </View>
                {this.state.activeButton === 0 ? (
                  <View>
                    {this.state.Likes.length === 0 ? (
                      <View
                        style={{
                          alignItems: 'center',
                          justifyContent: 'center',
                          marginTop: 200,
                        }}
                      >
                        <Text
                          style={{
                            color: '#000',
                            fontSize: 20,
                            fontWeight: '600',
                          }}
                        >
                          You have not swiped anyone
                        </Text>
                      </View>
                    ) : null}
                  </View>
                ) : null}
                {this.state.activeButton === 1 ? (
                  <View>
                    {this.state.whoLikeMepic.length === 0 ? (
                      <View
                        style={{
                          alignItems: 'center',
                          justifyContent: 'center',
                          marginTop: 200,
                        }}
                      >
                        <Text
                          style={{
                            color: '#000',
                            fontSize: 20,
                            fontWeight: '600',
                          }}
                        >
                          No match found
                        </Text>
                      </View>
                    ) : null}
                  </View>
                ) : null}
                {this.state.activeButton === 2 ? (
                  <View>
                    {this.state.match.length === 0 ? (
                      <View
                        style={{
                          alignItems: 'center',
                          justifyContent: 'center',
                          marginTop: 200,
                        }}
                      >
                        <Text
                          style={{
                            color: '#000',
                            fontSize: 20,
                            fontWeight: '600',
                          }}
                        >
                          No match found
                        </Text>
                      </View>
                    ) : null}
                  </View>
                ) : null}

                {this.state.activeButton === 0 ? (
                  <FlatList
                    // horizontal
                    numColumns={2}
                    style={{
                      height: '100%',
                      width: '93%',
                      backgroundColor: '#fff',
                    }}
                    data={this.state.Likes}
                    renderItem={({item, index}) =>
                      this.LikesFuntion(item, index)
                    }
                  />
                ) : (
                  <>
                    {this.state.activeButton === 1 ? (
                      <FlatList
                        // horizontal
                        numColumns={2}
                        style={{height: '100%', width: '93%'}}
                        data={this.state.whoLikeMepic}
                        renderItem={({item, index}) => (
                          // console.log(item.isSuperLike),
                          <TouchableOpacity
                            onPress={() =>
                              // eslint-disable-next-line no-nested-ternary
                              this.state.subsciption_data.length === 0
                                ? item.isSuperLike === 1
                                  ? this.props.navigation.navigate(
                                      'UserDetails',
                                      {
                                        matchUserData: item,
                                        userData: this.state.userData,
                                        accessToken: this.state.accessToken,
                                      },
                                    )
                                  : alert('Please subscribe the package')
                                : this.props.navigation.navigate(
                                    'UserDetails',
                                    {
                                      matchUserData: item,
                                      userData: this.state.userData,
                                      accessToken: this.state.accessToken,
                                    },
                                  )
                            }
                            style={{
                              width: '50%',
                              alignItems: 'center',
                              justifyContent: 'space-around',
                              marginVertical: 5,
                              marginBottom:
                                // eslint-disable-next-line no-nested-ternary
                                index % 2 === 0
                                  ? index ===
                                      this.state.whoLikeMepic.length - 1 ||
                                    index === this.state.whoLikeMepic.length - 2
                                    ? 0
                                    : null
                                  : index === this.state.whoLikeMepic.length - 1
                                  ? 0
                                  : null,
                            }}
                          >
                            <Image
                              style={{
                                width: '95%',
                                height: 185,
                                borderRadius: 5,
                                opacity:
                                  // eslint-disable-next-line no-nested-ternary
                                  this.state.subsciption_data.length !== 0
                                    ? null
                                    : item.isSuperLike !== 0
                                    ? null
                                    : 0.09,
                                borderColor:
                                  item.isSuperLike === 0 ? null : '#7643F8',
                                borderWidth: item.isSuperLike === 0 ? null : 1,
                                shadowOffset: {
                                  width: 0,
                                  height: 2,
                                },
                                shadowOpacity: 0.5,
                                shadowRadius: 3.84,

                                // elevation: 10,
                                shadowColor:
                                  item.isSuperLike === 1 ? '#7643F8' : null,
                              }}
                              source={
                                item.profilePicture === ''
                                  ? require('../assets/userpro.jpeg')
                                  : {
                                      uri: `${backendBaseUrl}data/${item.profilePicture}`,
                                    }
                              }
                            />

                            <Text
                              numberOfLines={1}
                              style={{
                                margin: 20,
                                position: 'absolute',
                                bottom: -15,
                                left: -3,
                                fontWeight: '600',
                                lineHeight: 14,
                                fontSize: 13,
                                color: '#FFFFFF',
                                fontFamily: 'SFUIText-Bold',
                              }}
                            >
                              {item.fullName}
                            </Text>
                          </TouchableOpacity>
                        )}
                      />
                    ) : (
                      <FlatList
                        numColumns={2}
                        style={{
                          height: '100%',
                          width: '93%',
                          marginBottom: Platform.OS === 'ios' ? 0 : 0,
                        }}
                        data={this.state.match}
                        renderItem={({item, index}) => (
                          <TouchableOpacity
                            onPress={() =>
                              this.props.navigation.navigate('PrivateMessage', {
                                chnnelId: item.channelID, // Agora channel id ......get om message screen
                                profilePicture: item.profilePicture,
                                username: item.fullName,
                                // userId :userId,
                                reciverId: item.userID,
                              })
                            }
                            style={{
                              width: '50%',
                              alignItems: 'center',
                              justifyContent: 'space-around',
                              marginVertical: 5,
                              marginBottom:
                                // eslint-disable-next-line no-nested-ternary
                                index % 2 === 0
                                  ? index === this.state.match.length - 1 ||
                                    index === this.state.match.length - 2
                                    ? 0
                                    : null
                                  : index === this.state.match.length - 1
                                  ? 0
                                  : null,
                            }}
                          >
                            <Image
                              style={{
                                width: '95%',
                                height: 180,
                                borderRadius: 5,
                              }}
                              source={
                                item.profilePicture === ''
                                  ? require('../assets/userpro.jpeg')
                                  : {
                                      uri: `${backendBaseUrl}data/${item.profilePicture}`,
                                    }
                              }

                              // source={require('./../assets/tempSwipeImage.png')}
                            />
                            <Text
                              numberOfLines={1}
                              style={{
                                margin: 20,
                                position: 'absolute',
                                bottom: -15,
                                left: -3,
                                fontWeight: '600',
                                lineHeight: 14,
                                fontSize: 13,
                                color: '#FFFFFF',
                                fontFamily: 'SFUIText-Bold',
                              }}
                            >
                              {item.fullName}
                            </Text>
                          </TouchableOpacity>
                        )}
                      />
                    )}
                  </>
                )}
              </>
            )}
          </>
        )}
      </View>
    );
  }
}

function StaarzLogo({url: aUrl, goBack}) {
  return (
    <View style={sideLogoStyles.container}>
      {/* <View style={{padding:10,flexDirection:'row',justifyContent:"space-between",paddingHorizontal:20,alignItems:"center",backgroundColor:"#fff",width:"100%",}}> */}
      <TouchableOpacity
        onPress={goBack}
        style={{width: '10%', paddingLeft: 20}}
      >
        <FontAwesome name="angle-left" size={25} color="#000" />
      </TouchableOpacity>
      <View
        style={{
          width: '80%',
          alignItems: 'center',
          justifyContent: 'center',
          flexDirection: 'row',
        }}
      >
        <Image
          source={require('../assets/starzlogo.png')}
          style={sideLogoStyles.imageLogo}
        />
        <Image
          source={require('../assets/STAARZ_109X19_black.png')}
          style={sideLogoStyles.image}
        />
      </View>

      <View style={{width: '10%'}}>
        <Image
          source={aUrl}
          style={{
            width: 30,
            height: 30,
            borderRadius: 40 / 2,
            aspectRatio: 1 / 1,
          }}
        />
      </View>
    </View>
  );
}
// const sideLogoStyles = StyleSheet.create({
//   image: {
//     marginTop: 0,
//     marginLeft: 20
//   },
//   imageLogo: {
//     // marginBottom: 8,
//   },
//   container: {
//     position: 'absolute',
//     top: Platform.OS === 'ios' ? 10 + getStatusBarHeight() : 10,
//     alignItems: 'center',
//     flexDirection: 'row',
//     marginTop: 0,
//   },
// })
const sideLogoStyles = StyleSheet.create({
  image: {
    marginTop: 0,
    marginLeft: 20,

    resizeMode: 'cover',
  },
  imageLogo: {
    // marginBottom: 8,
    left: -4,
    width: wp(7.5),
    height: wp(6.5),
    resizeMode: 'cover',
  },
  container: {
    position: 'absolute',
    // top: 10 + getStatusBarHeight(),
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 0,
  },
});
