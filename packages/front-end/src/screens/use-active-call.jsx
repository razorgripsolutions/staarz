import React, {useContext} from 'react';
import {Store} from '../components/Context';

export const useActiveCall = (Component) => {
  const {setActiveCall} = useContext(Store);
  return <Component {...this.props} setActiveCall={setActiveCall} />;
};
