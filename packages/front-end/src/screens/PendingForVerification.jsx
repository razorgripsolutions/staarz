import React, {useCallback, useEffect, useRef, useState} from 'react';
import {Image, View, StyleSheet, TouchableOpacity} from 'react-native';
import {Text} from 'react-native-paper';
import {CircleFade} from 'react-native-animated-spinkit';
import {checkConnection} from '../components/checkConnection';
import {useAuthProvider} from '../../App';
import {useBasicEmailSignup} from '../api/hooks/email_signup';
import Background from '../components/Background';
import DropdownAlert from 'react-native-dropdownalert';
import {useNetInfo} from '@react-native-community/netinfo';
import Toast from 'react-native-toast-message';

function PendingForVerification({navigation, route}) {
  const {userState} = useAuthProvider();
  const {mutateAsync} = useBasicEmailSignup();
  const {isInternetReachable} = useNetInfo();

  const dropDownAlertRef = useRef();

  checkConnection().then((res) => {
    if (res === false) {
      dropDownAlertRef.alertWithType(
        'error',
        'Error',
        'You are not connected to the internet',
      );
    }
  });
  useEffect(() => {
    onRequestForVerification();
  }, []);

  const onRequestForVerification = useCallback(async () => {
    const {
      emailAddress,
      password,
      gender,
      dob,
      religion,
      location,
      fullName,
      intrestedIn,
      typeOfRelationship,
      height,
      occupation,
      haveCar,
      aboutMe,
      smokes,
      interests,
      musics,
      pets,
    } = userState.registrationData;
    try {
      const response = await mutateAsync({
        emailAddress,
        password,
        gender,
        dob,
        religion,
        location,
        fullName,
        intrestedIn,
        typeOfRelationship,
        height,
        occupation,
        haveCar,
        aboutMe,
        smokes,
        interests,
        musics,
        pets,
      });
      console.log('registraion response ----->', response);
      if (response.data.emailAddress) {
        return navigation.navigate('SuccessScreen');
      } else if (response.data.message === 'error') {
        Toast.show({
          type: 'error',
          text1: 'Sign up error',
          text2: 'Could not create account',
        });
        return navigation.navigate('LoginScreen');
      } else {
        Toast.show({
          type: 'error',
          text1: 'Validation error',
          text2: 'Verification failed',
        });
        return navigation.navigate('UploadYourPicture');
      }
    } catch (error) {
      Toast.show({
        type: 'error',
        text1: 'Validation error',
        text2: 'Verification failed',
      });
      return navigation.navigate('UploadYourPicture');
    }
  }, [isInternetReachable, navigation, mutateAsync]);

  return (
    <Background>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Image
          source={require('../assets/starzlogo.png')}
          style={{width: 65, height: 65}}
        />
        <Text style={{width: 8}} />
        <Text
          style={{
            fontWeight: '700',
            fontSize: 45,
            lineHeight: 65,
            color: '#101010',
            fontFamily: 'MPLUS1p-Bold',
          }}
        >
          STAARY
        </Text>
      </View>
      <Text
        style={{
          fontSize: 19,
          color: '#101010',
          lineHeight: 27,
          fontWeight: '500',
        }}
      >
        Pending for verification
      </Text>
      <Text style={{height: 40}} />
      <CircleFade size={100} color="#5E2EBA" />
      <View style={bottomButton.bottomView}>
        <View>
          <Text
            style={{
              color: '#000000',
              marginBottom: 4,
              fontWeight: '500',
              lineHeight: 19,
              fontSize: 16,
              fontFamily: 'MPLUS1p-Medium',
              letterSpacing: -0.888889,
            }}
          >
            Please Wait
          </Text>
        </View>
        <TouchableOpacity>
          <Text
            style={{
              color: '#8A98AC',
              lineHeight: 16,
              fontSize: 14,
              letterSpacing: -0.777778,
            }}
          >
            Working on the Task
          </Text>
        </TouchableOpacity>
      </View>
      <DropdownAlert ref={dropDownAlertRef} />
    </Background>
  );
}

const bottomButton = StyleSheet.create({
  bottomView: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 35,
  },
  login: {
    marginBottom: 30,
    borderRadius: 8,
  },
});

export default PendingForVerification;
