import React, {useState, useEffect} from 'react';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {CircleFade} from 'react-native-animated-spinkit';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {
  Dimensions,
  View,
  Image,
  Text,
  TouchableOpacity,
  StyleSheet,
  SafeAreaView,
  StatusBar,
  Platform,
} from 'react-native';
import {checkConnection} from '../components/checkConnection';
import {string} from '../const/string';
import TextInput from '../components/TextInput';
import Button from '../components/Button';
import {emailValidator} from '../helpers/emailValidator';
// import DropdownAlert from 'react-native-dropdownalert';
import {url} from '../const/urlDirectory';

function ForgotPasswordScreen({navigation}) {
  const [email, setEmail] = useState({value: '', error: ''});
  const [is_Connected, setis_Connected] = useState(true);
  const [isLoading, setisLoading] = useState(false);
  const [net, setnet] = React.useState(true);

  const {width} = Dimensions.get('window');
  const windowHeight = Dimensions.get('window').height;

  // checkConnection().then(res => {
  //   if (res == false) {
  //     dropDownAlertRef.alertWithType('error', 'Error',string.network);
  //   }
  // })

  useEffect(() => {
    ApiCAlling();
  }, []);

  const ApiCAlling = async () => {
    // alert("ok")
    checkConnection().then((res) => {
      if (res === false) {
        // dropDownAlertRef.alertWithType('error', 'Error', string.network);
        setnet(res);
      } else {
        setnet(res);
        // creatChannel();
      }
    });
  };
  const sendResetPasswordEmail = () => {
    checkConnection().then((res) => {
      if (res === false) {
        // dropDownAlertRef.alertWithType('error', 'Error', string.network);
        setnet(res);
      } else {
        setnet(res);
        sendResetPasswordEmail1();
      }
    });
  };
  // const _fetchData = async () => {
  //   try {
  //     // alertWithType parameters: type, title, message, payload, interval.
  //     // payload object that includes a source property overrides the image source prop. (optional: object)
  //     // interval takes precedence over the closeInterval prop. (optional: number)
  //     // this.dropDownAlertRef.alertWithType('info', 'Info', 'Start fetch data.');
  //     await fetch('https://httpbin.org/get');
  //     dropDownAlertRef.alertWithType(null, string.ForgetPAssMessage, string.f_Title);
  //   } catch (error) {
  //     // this.dropDownAlertRef.alertWithType('error', 'Error', error);
  //   }
  // };
  const sendResetPasswordEmail1 = () => {
    const emailError = emailValidator(email.value);
    if (emailError) {
      setEmail({...email, error: emailError});
      return;
    }

    forgetPasds(email);
    // navigation.navigate('LoginScreen')
  };

  const forgetPasds = async (data) => {
    if (is_Connected === false) {
      // dropDownAlertRef.alertWithType('error', 'Error', "You are not connected to the internet");
      setnet(false);
    } else {
      console.log(typeof data.value);
      //  alert(data.value)
      setisLoading(true);

      const data1 = JSON.stringify({
        userEmail: data.value,
      });

      // AgMPEQDx
      const config = {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: data1,
      };

      // alert("OK")
      console.log('data1', data1);
      fetch(url.userForgotPassword, config)
        .then((responseJson) => {
          console.log(responseJson);
        })
        .then((response) => {
          //  console.error("responseJson-----",response);
          // alert("New Password sent to Email")
          setisLoading(false);
          navigation.push('LoginScreen', {value: '1'});
        })
        .catch((error) => {
          // alert(error)
          setisLoading(false);
          // alert('Oops error!!')
          console.error(error);
        });
    }
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
      <StatusBar barStyle="light-content" backgroundColor="white" />
      {isLoading ? (
        <CircleFade
          size={110}
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            top: windowHeight * 0.4,
            alignSelf: 'center',
          }}
          color="#5E2EBA"
        />
      ) : (
        <View style={{flex: 1, alignItems: 'center', marginHorizontal: 20}}>
          <View
            style={{
              flexDirection: 'row',
              top: Platform.OS === 'ios' ? 0 : 0,
              alignItems: 'center',
              justifyContent: 'center',
              width: '100%',
              backgroundColor: '#fff',
              zIndex: 10,
            }}
          >
            <View style={{width: '20%'}}>
              <TouchableOpacity
                onPress={
                  () => navigation.goBack()
                  // navigation.push('LoginScreen', { value: "1" })
                }
                style={{}}
              >
                <FontAwesome
                  name="angle-left"
                  size={25}
                  color="#000"
                  style={{}}
                />
              </TouchableOpacity>
            </View>
            <View
              style={{
                width: '60%',
                alignItems: 'center',
                flexDirection: 'row',
                justifyContent: 'center',
              }}
            >
              <Image
                source={require('../assets/starzlogo.png')}
                style={sideLogoStyles.imageLogo}
              />
              {/* <Image source={require('../assets/STAARZ_109X19_black.png')} style={sideLogoStyles.image} /> */}
              <Text
                style={{
                  fontSize: wp(6),
                  fontFamily: 'MPLUS1p-Bold',
                  color: '#243443',
                  left: -2,
                  lineHeight: 33,
                  textAlign: 'center',
                }}
              >
                STAARZ
              </Text>
            </View>
            <View style={{backgroundColor: 'red', width: '20%'}} />
          </View>

          {!net ? (
            <View
              style={{alignItems: 'center', justifyContent: 'center', flex: 1}}
            >
              <Image
                source={require('../assets/internat.png')}
                style={{height: 50, width: 50}}
              />
              <Text style={{fontSize: 17, fontWeight: '700', marginTop: 5}}>
                {string.network}
              </Text>
              <TouchableOpacity
                color="white"
                mode="contained"
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  top: windowHeight * 0.3,
                }}
                onPress={() => ApiCAlling()}
              >
                <View
                  style={{
                    backgroundColor: '#7643F8',
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 50,
                    paddingHorizontal: 30,
                    paddingVertical: 20,
                    marginTop: 10,
                  }}
                >
                  {
                    Platform.OS === 'ios' ? (
                      // <Ionicons name="reload" color="black" size={30} />
                      <Text style={{fontSize: hp('2'), color: '#fff'}}>
                        {string.Try_Again}
                      </Text>
                    ) : (
                      <Text style={{fontSize: hp('2'), color: '#fff'}}>
                        {string.Try_Again}
                      </Text>
                    )
                    // <MaterialCommunityIcons name="reload" color="black" size={30} />
                  }
                </View>
              </TouchableOpacity>
            </View>
          ) : (
            <View
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                flex: 1,
                width: '100%',
              }}
            >
              <Image
                source={require('../assets/starzlogo.png')}
                style={{
                  width: 160,
                  height: 160,
                  // marginTop: hp(5),
                }}
              />
              <TextInput
                label="E-mail address"
                returnKeyType="done"
                style={{fontSize: 14}}
                value={email.value}
                onChangeText={(text) => setEmail({value: text, error: ''})}
                error={!!email.error}
                errorText={email.error}
                autoCapitalize="none"
                autoCompleteType="email"
                textContentType="emailAddress"
                keyboardType="email-address"
                description="You will receive email with password reset link."
              />
              <Button
                mode="contained"
                onPress={sendResetPasswordEmail}
                style={{marginTop: 16, width: '100%'}}
              >
                Send Instructions
              </Button>
              {/* <TouchableOpacity style={{}}>
              <Text>Send Instructions</Text>
            </TouchableOpacity> */}
              {/* <DropdownAlert ref={ref => dropDownAlertRef = ref} />

      */}
            </View>
          )}
        </View>
      )}
      {/* <DropdownAlert
        ref={ref => dropDownAlertRef = ref}

        imageSrc={require('../assets/ForgetMEssage.png')}
        imageStyle={{ width: wp(7), height: wp(7), alignSelf: "center", }}
        containerStyle={{
          padding: 15,
          backgroundColor: "#7643F6", borderTopRightRadius: 20, borderBottomLeftRadius: 20,
          borderBottomRightRadius: 20, top: Platform.OS == "ios" ? "10%" : "2%", width: "95%", alignSelf: "center"
        }}
        titleStyle={{ fontSize: wp(3), color: "#FFFFFF" }}
        defaultTextContainer={{ fontSize: wp(2), left: 10 }}

      /> */}
    </SafeAreaView>
  );
}

const sideLogoStyles = StyleSheet.create({
  image: {
    // marginTop: 5,
    marginLeft: 20,
  },
  imageLogo: {
    // marginTop: 8,
    // marginBottom: 8,
    width: wp(7.5),
    height: wp(6.5),
    left: -4,
  },
  container: {
    position: 'absolute',
    top: 0 + getStatusBarHeight(),
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    backgroundColor: 'white',
    zIndex: 10,
    width: '100%',
  },
});

export default ForgotPasswordScreen;
