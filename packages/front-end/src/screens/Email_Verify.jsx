import React, {useState, useEffect} from 'react';
import {
  TouchableOpacity,
  Modal,
  StyleSheet,
  ActivityIndicator,
  Dimensions,
  ScrollView,
  Text,
  View,
  Image,
  Platform,
} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {CircleFade} from 'react-native-animated-spinkit';
import OTPTextView from 'react-native-otp-textinput';
import {SafeAreaView} from 'react-native-safe-area-context';
import {url} from '../const/urlDirectory';
import {theme} from '../core/theme';
import {checkConnection} from '../components/checkConnection';
import Button from '../components/Button';
// import { block } from 'react-native-reanimated'
import {string} from '../const/string';

function Email_Verify({navigation, route}) {
  //  alert('vghvghv==>'+JSON.stringify(route.params))
  const [Verification, setVerification] = useState(false);
  const [sliderOneValue, setSliderOneValue] = React.useState([5, 20]);

  const [Loading, setLoading] = useState(false);
  const [accessToken, setAccessToken] = useState();
  // console.log("accessToken========", accessToken);

  const [modalVisible, setModalVisible] = useState(false);
  const [otpInput, setotpInput] = useState('');
  const [inputText, setinputText] = useState('');
  const [otp, setotp] = useState('');
  const [isLoading, setIsloading] = useState(false);

  // const [userData, setUserData] = useState([]);

  const [userData, setUserData] = useState(route.params.userData);
  const [net, setnet] = React.useState(false);
  const windowWidth = Dimensions.get('window').width;
  const windowHeight = Dimensions.get('window').height;

  // checkConnection().then(res => {
  //   if (res === false) {
  //     dropDownAlertRef.alertWithType('error', 'Error', "You are not connected to the internet");
  //   }
  // })

  const verifyOtp = async () => {
    checkConnection().then((res) => {
      if (res === false) {
        setnet(res);
        // dropDownAlertRef.alertWithType('error', 'Error', string.network);
      } else {
        setnet(res);
        verifyOtp1();
      }
    });
  };

  const verifyOtp1 = () => {
    setLoading(true);
    let data;
    if (Platform.OS === 'ios') {
      // console.log('otp==>'+otp)

      data = JSON.parse(otp);
    } else {
      data = JSON.parse(otp);
    }

    console.log(
      'jsondata----',
      JSON.stringify({
        userEmail: route.params.userData.userEmail,
        verificationCode: data,
        userPassword: route.params.userPassword,
      }),
    );
    fetch(url.matchEmailVerificationCode, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        userEmail: route.params.userData.userEmail,
        verificationCode: data,
        status: 1,
        userPassword: route.params.userPassword,
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('responseJson Email Verify========', responseJson);

        if (responseJson.status === 200) {
          // console.log("accessToken Email Verify========", responseJson.accessToken);

          const someProperty = userData;
          someProperty.userID = responseJson.user.userID;
          someProperty.rocketChatLoginResponse =
            responseJson.rocketChatLoginResponse;

          setUserData(someProperty);
          setAccessToken(responseJson.accessToken);
          // console.log("someProperty Email Verify========",JSON.stringify(userData));
          setLoading(false);
          setModalVisible(true);
          setVerification(true);

          // navigation.push("BottomTabs")
        }

        if (responseJson.status === 401) {
          // alert("")
          setModalVisible(true);
          setLoading(false);
          setVerification(false);
          // setisLoading(false);
          // navigation.reset({
          //   index: 0,
          //   routes: [{ name: 'LoginScreen' }],
          // })
        }
        if (responseJson.status === 400) {
          alert(responseJson.message);
          // setModalVisible(true)
          setVerification(false);
          setLoading(false);
          // setisLoading(false);
          // navigation.reset({
          //   index: 0,
          //   routes: [{ name: 'LoginScreen' }],
          // })
        }
        if (responseJson.status === 404) {
          // setModalVisible(true)
          alert(responseJson.msg);
          setVerification(false);
          // setisLoading(false);
          // navigation.reset({
          //   index: 0,
          //   routes: [{ name: 'LoginScreen' }],
          // })
        }
      })
      .catch((error) => {
        // setisLoading(false)
        // alert('Oops error!!')
        setLoading(false);
        console.error(error);
      });
  };

  useEffect(() => {
    getfuntion();
  }, []);

  const getfuntion = () => {
    checkConnection().then((res) => {
      if (res === false) {
        setnet(res);
      } else {
        setnet(res);
      }
    });
  };
  const chekValid = () => {
    if (otp.length === 4) {
      verifyOtp();
      // setModalVisible(true)
    } else {
      alert('plese fill otp');
    }
  };

  const styles = StyleSheet.create({
    centeredView: {
      flex: 1,
      // justifyContent: "center",
      // alignItems: "center",
      // marginTop: 22,
      // marginHorizontal: 20,
      // borderColor:"#7643F8"
      backgroundColor: '#fff',
    },
    centeredView1: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      // marginTop: 22,
      // marginHorizontal:60,
      // borderColor:"#7643F8"
      backgroundColor: '#fff',
      opacity: 1,
    },
    modalView: {
      margin: 20,
      backgroundColor: 'white',
      borderRadius: 20,
      padding: 35,
      borderColor: '#7643F8',
      borderWidth: Platform.OS === 'ios' ? null : 1,
      alignItems: 'center',
      shadowColor: '#7643F8',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.65,
      shadowRadius: 4,
      elevation: 5,
    },
    button: {
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 5,
      position: 'absolute',
      bottom: 10,
      width: heightPercentageToDP('20'),
      backgroundColor: '#8897AA',
      height: heightPercentageToDP('5.5'),
      padding: heightPercentageToDP('1'),
      elevation: 2,
    },
    buttonOpen: {
      backgroundColor: '#F2F3F6',
    },
    buttonClose: {
      width: '100%',
      backgroundColor: '#8897AA',
    },
    textStyle: {
      fontSize: heightPercentageToDP('2'),
      color: 'white',
      fontWeight: 'bold',
      textAlign: 'center',
    },
    modalText: {
      color: !Verification ? 'red' : '#000',
      marginBottom: 15,
      paddingVertical: heightPercentageToDP('2'),
      textAlign: 'center',
      fontWeight: 'bold',
    },

    image: {
      width: 100,
      height: 100,
      marginBottom: 8,
    },
    text: {
      width: '100%',
      height: 30,
      // textAlign: 'center',
      marginTop: 20,
      marginBottom: 8,
      fontWeight: 'bold',
      fontSize: 20,
      // lineHeight: 74.97,
      letterSpacing: 2,
      color: !Verification ? '#7643F8' : '#7643F8',
    },
    welcome: {
      fontSize: 20,
      // textAlign: 'center',
      margin: 10,
    },
    instructions: {
      fontSize: 18,
      fontWeight: '500',
      textAlign: 'center',
      color: '#333333',
      // marginBottom: 20,
    },
    instructions1: {
      fontSize: 18,
      fontWeight: '500',
      textAlign: 'center',
      color: '#333333',
      marginTop: 100,
    },
    textInputContainer: {
      marginTop: 20,
      marginBottom: 20,
    },
    roundedTextInput: {
      borderRadius: 10,
      borderWidth: 4,
    },
    buttonWrapper: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      marginBottom: 20,
      width: '60%',
    },
    textInput: {
      height: 40,
      width: '80%',
      // borderColor: '#000', // DUP
      borderWidth: 1,
      padding: 10,
      fontSize: 16,
      letterSpacing: 5,
      marginBottom: 10,
      textAlign: 'center',
      borderColor: 'red',
    },
    buttonStyle: {
      // marginHorizontal: 20,
      backgroundColor: 'red',
      width: 50,
      height: 50,
    },
    login: {
      // marginBottom: 10,
      // marginHorizontal: 20,
      // backgroundColor:"red",
      borderRadius: 8,
      // position:"absoulute"
      bottom: 0,
      // position:"absolute",
      // bottom:-heightPercentageToDP('7'),
      // marginTop:heightPercentageToDP('50')
    },
  });

  const dismiss = () => {
    setModalVisible(!modalVisible);
    //  navigation.navigate("RegisterScreen")
  };

  const next = () => {
    // console.log()
    // setModalVisible(!modalVisible)
    setModalVisible(!modalVisible);
    navigation.replace('RegisterUserBioScreen', {
      userData,
      accessToken,
    });
  };

  return (
    <SafeAreaView style={styles.centeredView}>
      <View style={{flex: 1, marginHorizontal: 20}}>
        <View
          style={{
            flexDirection: 'row',
            top: Platform.OS === 'ios' ? 0 : 0,
            alignItems: 'center',
            justifyContent: 'center',
            width: '100%',
            zIndex: 10,
          }}
        >
          <View style={{width: '20%'}}>
            <TouchableOpacity
              onPress={() => navigation.goBack()}
              style={{left: -15}}
            >
              <FontAwesome
                name="angle-left"
                size={25}
                color="#000"
                style={{left: 20}}
              />
            </TouchableOpacity>
          </View>
          <View
            style={{
              width: '60%',
              alignItems: 'center',
              flexDirection: 'row',
              justifyContent: 'center',
            }}
          >
            <Image
              source={require('../assets/starzlogo.png')}
              style={sideLogoStyles.imageLogo}
            />
            {/* <Image source={require('../assets/STAARZ_109X19_black.png')} style={sideLogoStyles.image} /> */}
            <Text
              style={{
                fontSize: wp(6),
                fontFamily: 'MPLUS1p-Bold',
                color: '#243443',
                left: -2,
                lineHeight: 33,
                textAlign: 'center',
              }}
            >
              STAARZ
            </Text>
          </View>
          <View style={{backgroundColor: 'red', width: '20%'}} />
        </View>
        {!net ? (
          <View
            style={{alignItems: 'center', justifyContent: 'center', flex: 1}}
          >
            <Image
              source={require('../assets/internat.png')}
              style={{height: 50, width: 50}}
            />
            <Text style={{fontSize: 17, fontWeight: '700', marginTop: 5}}>
              {string.network}
            </Text>
            <TouchableOpacity
              color="white"
              mode="contained"
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                top: windowHeight * 0.3,
              }}
              onPress={() => getfuntion()}
            >
              <View
                style={{
                  backgroundColor: '#7643F8',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 50,
                  paddingHorizontal: 30,
                  paddingVertical: 20,
                  marginTop: 10,
                }}
              >
                {
                  Platform.OS === 'ios' ? (
                    // <Ionicons name="reload" color="black" size={30} />
                    <Text style={{fontSize: hp('2'), color: '#fff'}}>
                      {string.Try_Again}
                    </Text>
                  ) : (
                    <Text style={{fontSize: hp('2'), color: '#fff'}}>
                      {string.Try_Again}
                    </Text>
                  )
                  // <MaterialCommunityIcons name="reload" color="black" size={30} />
                }
              </View>
            </TouchableOpacity>
          </View>
        ) : (
          <ScrollView style={{flex: 1}}>
            {Loading ? (
              <CircleFade
                size={120}
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  alignSelf: 'center',
                  top: windowHeight * 0.4,
                }}
                color="#5E2EBA"
              />
            ) : (
              <>
                <Text style={styles.instructions1}>
                  Enter the verification code we
                </Text>
                <Text style={styles.instructions}>
                  {' '}
                  just sent you on your Email.
                </Text>
                <OTPTextView
                  // ref={(e) => (this.input1 = e)}
                  handleTextChange={(e) => {
                    setotp(e);
                  }}
                  containerStyle={styles.textInputContainer}
                  textInputStyle={styles.roundedTextInput}
                  inputCount={4}
                  tintColor="#7643F8"
                  // keyboardType="numeric"
                  inputCellLength={1}
                />
                {/* <View style={{position:"absolute",bottom:0,backgroundColor:"red"}}> */}
                {isLoading ? (
                  <ActivityIndicator
                    style={styles.login}
                    size="large"
                    color="#8897AA"
                  />
                ) : (
                  <Button
                    style={styles.login}
                    color={otp.length !== 4 ? '#ccc' : '#8897AA'}
                    mode="contained"
                    onPress={() => (otp.length === 4 ? chekValid() : null)}
                  >
                    <Text
                      style={{
                        textTransform: 'capitalize',
                        fontWeight: 'bold',
                        fontSize: hp('2.3'),
                        color: 'white',
                      }}
                    >
                      Done
                    </Text>
                  </Button>
                )}
                {/* </View> */}
              </>
            )}
          </ScrollView>
        )}
        <Modal
          animationType="slide"
          transparent
          visible={modalVisible}
          // onRequestClose={() => {
          //   Alert.alert("Modal has been closed.");
          //   setModalVisible(!modalVisible);
          // }}
        >
          <View style={styles.centeredView1}>
            <View style={styles.modalView}>
              <View
                style={{
                  elevation: 5,
                  alignItems: 'center',
                  justifyContent: 'center',
                  width: heightPercentageToDP('8'),
                  height: heightPercentageToDP('8'),
                  borderRadius: heightPercentageToDP('8') / 2,
                  backgroundColor: !Verification ? '#7643F8' : '#7643F8',
                  borderWidth: 2,
                  borderColor: !Verification ? '#fff' : '#fff',
                  position: 'absolute',
                  top: -heightPercentageToDP('4'),
                }}
              >
                {Verification ? (
                  <FontAwesome
                    name="thumbs-o-up"
                    color="white"
                    size={25}
                    style={{}}
                  />
                ) : (
                  <FontAwesome
                    name="thumbs-o-down"
                    color="white"
                    size={25}
                    style={{}}
                  />
                )}
              </View>

              <Text style={styles.text}>STAARZ</Text>
              <Image
                source={require('../assets/starzlogo.png')}
                style={styles.image}
              />

              {Verification ? (
                <Text style={styles.modalText}>Email Verification Success</Text>
              ) : (
                <Text style={styles.modalText}>
                  Verification Failed {'\n'}Please Enter the correct OTP.
                </Text>
              )}

              {Verification ? (
                <TouchableOpacity onPress={() => next()} style={styles.button}>
                  <Text style={styles.textStyle}>Next</Text>
                </TouchableOpacity>
              ) : (
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: 20,
                    alignSelf: 'center',
                    width: heightPercentageToDP('25'),
                  }}
                >
                  {/* <TouchableOpacity onPress={() => setModalVisible(!modalVisible)}>
                <Text style={{color:"#000"}}>Resend</Text>
                </TouchableOpacity> */}
                  <TouchableOpacity onPress={() => dismiss()}>
                    <Text style={{color: '#7643F8'}}>Dismiss</Text>
                  </TouchableOpacity>
                </View>
              )}
            </View>
          </View>
        </Modal>
      </View>
    </SafeAreaView>
  );
}

function StaarzLogo() {
  return (
    <View style={sideLogoStyles.container}>
      <Image
        source={require('../assets/starzlogo_23X26.png')}
        style={sideLogoStyles.imageLogo}
      />
      <Image
        source={require('../assets/STAARZ_109X19_black.png')}
        style={sideLogoStyles.image}
      />
    </View>
  );
}

const sideLogoStyles = StyleSheet.create({
  image: {
    marginTop: 0,
    marginLeft: 20,
  },
  imageLogo: {
    // left: 10,
    width: wp(7.5),
    height: wp(6.5),
    left: -4,
    // marginBottom: 8,
  },
  container: {
    zIndex: 10,
    width: '100%',
    position: 'absolute',

    top: Platform.OS !== 'ios' ? 0 : 0,
    // top: 0 + getStatusBarHeight(),
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    // marginTop: 0,
    // backgroundColor:"white",

    color: theme.colors.secondary,
  },
  // image: {
  //   width: 24,
  //   height: 24,
  // }
});

export default Email_Verify;
