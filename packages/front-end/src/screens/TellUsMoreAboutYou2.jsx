import React, {useState} from 'react';
import {
  Image,
  View,
  StyleSheet,
  TextInput,
  StatusBar,
  TouchableOpacity,
  Switch,
  Dimensions,
  ActivityIndicator,
  SafeAreaView,
  ScrollView,
  Platform,
} from 'react-native';
import {Text} from 'react-native-paper';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {theme} from '../core/theme';
import {checkConnection} from '../components/checkConnection';
import {string} from '../const/string';
import {useAuthProvider} from '../../App';
import PlacesInput from 'react-native-places-input';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Button from '../components/Button';

function TellUsMoreAboutYou2({navigation}) {
  const {userState, register} = useAuthProvider();
  const [yourHeight, setYourHeight] = useState(0);
  const [whereYouLive, setWhereYouLive] = useState('');
  const [haveACar, setHaveACar] = useState(false);
  const [doYouSmoke, setDoYouSmoke] = useState(false);

  const [isCentimeterEnabled, setIsCentimeterEnabled] = useState(true);
  const [isLoading, setIsloading] = useState(false);
  const toggleSwitch = () => {
    setIsCentimeterEnabled((previousState) => !previousState);
    setYourHeight();
  };
  const [net, setnet] = React.useState(true);
  const windowHeight = Dimensions.get('window').height;
  const {width} = Dimensions.get('window');
  const {height} = Dimensions.get('window');
  const skip_TellUsMoreAboutYou3 = () => {
    checkConnection().then((res) => {
      if (res === false) {
        setnet(res);
      } else {
        setnet(res);
        skip_TellUsMoreAboutYou34();
      }
    });
  };

  const skip_TellUsMoreAboutYou34 = async () => {
    await register({
      ...userState?.registrationData,
      haveCar: haveACar,
      smokes: doYouSmoke,
      height: `${yourHeight}m`,
    });
    navigation.replace('TellUsMoreAboutYou3');
  };

  const getfuntion = () => {
    checkConnection().then((res) => {
      if (res === false) {
        setnet(res);
      } else {
        setnet(res);
      }
    });
  };

  const tellUsMoreAboutYouSubmit2 = () => {
    checkConnection().then((res) => {
      if (res === false) {
        setnet(res);
      } else {
        setnet(res);
        tellUsMoreAboutYouSubmit23();
      }
    });
  };
  const tellUsMoreAboutYouSubmit23 = () => {
    navigation.replace('TellUsMoreAboutYou3');
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
      <View style={{flex: 1, width: '90%', alignSelf: 'center'}}>
        <StatusBar barStyle="dark-content" backgroundColor="white" />
        <View
          style={{
            flexDirection: 'row',
            top: Platform.OS === 'ios' ? 0 : 0,
            alignItems: 'center',
            justifyContent: 'center',
            width: '100%',
            backgroundColor: '#fff',
            zIndex: 10,
          }}
        >
          <Image
            source={require('../assets/starzlogo.png')}
            style={sideLogoStyles.imageLogo}
          />
          <Text
            style={{
              fontSize: wp(6),
              fontFamily: 'MPLUS1p-Bold',
              color: '#243443',
              left: -2,
              lineHeight: 33,
              textAlign: 'center',
            }}
          >
            STAARY
          </Text>
        </View>

        {!net ? (
          <View
            style={{alignItems: 'center', justifyContent: 'center', flex: 1}}
          >
            <Image
              source={require('../assets/internat.png')}
              style={{height: 50, width: 50}}
            />
            <Text style={{fontSize: 17, fontWeight: '700', marginTop: 5}}>
              {string.network}
            </Text>
            <TouchableOpacity
              color="white"
              mode="contained"
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                top: windowHeight * 0.3,
              }}
              onPress={() => getfuntion()}
            >
              <View
                style={{
                  backgroundColor: '#7643F8',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 50,
                  paddingHorizontal: 30,
                  paddingVertical: 20,
                  marginTop: 10,
                }}
              >
                {Platform.OS === 'ios' ? (
                  <Text style={{fontSize: hp('2'), color: '#fff'}}>
                    {string.Try_Again}
                  </Text>
                ) : (
                  <Text style={{fontSize: hp('2'), color: '#fff'}}>
                    {string.Try_Again}
                  </Text>
                )}
              </View>
            </TouchableOpacity>
          </View>
        ) : (
          <>
            <ScrollView
              style={styles.scrollView}
              showsVerticalScrollIndicator={false}
              keyboardShouldPersistTaps="handled"
              showsHorizontalScrollIndicator={false}
            >
              <Text
                style={{
                  color: '#000000',
                  fontWeight: Platform.OS === 'ios' ? '900' : 'bold',
                  fontSize: wp('6.5'),
                  marginTop: '8%',
                  textAlign: 'center',
                  lineHeight: 36,
                  fontFamily: 'MPLUS1p-Bold',
                }}
              >
                Tell us more about you
              </Text>
              <Text
                style={{
                  fontWeight: '500',
                  fontSize:
                    width === 320 || height === 610.6666666666666 ? 16 : 18,
                  textAlign: 'center',
                  color: '#8A98AC',
                  marginBottom: 35,
                  lineHeight: 27,
                }}
              >
                Lets get to know each other :){' '}
              </Text>

              <View style={{width: '100%', zIndex: 11}}>
                <View style={{flexDirection: 'row'}}>
                  <View style={{flex: 1}}>
                    <Text
                      style={{
                        color: '#101010',
                        textAlign: 'left',
                        marginBottom: 15,
                        alignContent: 'flex-start',
                        fontWeight: '600',
                        fontSize:
                          width === 320 || height === 610.6666666666666
                            ? 14
                            : 16,
                        lineHeight: 21,
                        fontFamily: 'SFUIText-Semibold',
                      }}
                    >
                      Your height
                    </Text>
                  </View>
                  <View
                    style={{
                      flex: 1,
                      alignItems: 'flex-end',
                      alignContent: 'flex-end',
                    }}
                  >
                    <Text
                      style={{
                        marginBottom: 15,
                        fontWeight: '600',
                        fontSize:
                          width === 320 || height === 610.6666666666666
                            ? 16
                            : 18,
                        color: '#919FB1',
                      }}
                    >
                      {yourHeight}
                      {isCentimeterEnabled ? 'm' : 'ft'}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <View style={{flex: 1}}>
                    <Text
                      style={{
                        color: '#98A5B7',
                        textAlign: 'left',
                        marginBottom: 15,
                        alignContent: 'flex-start',
                        fontWeight: '600',
                        fontSize:
                          width === 320 || height === 610.6666666666666
                            ? 16
                            : 18,
                        lineHeight: 21,
                        fontFamily: 'MPLUS1p-Medium',
                      }}
                    >
                      {isCentimeterEnabled ? 'Centimeters' : 'Feet'}
                    </Text>
                  </View>
                  <View
                    style={{
                      flex: 1,
                      alignItems: 'flex-end',
                      alignContent: 'flex-end',
                    }}
                  >
                    <Switch
                      trackColor={{false: '#d8e6f8', true: '#81b0ff'}}
                      thumbColor={isCentimeterEnabled ? '#fff' : '#fff'}
                      ios_backgroundColor="#ECF3FB"
                      onValueChange={toggleSwitch}
                      value={isCentimeterEnabled}
                    />
                  </View>
                </View>
                <TextInput
                  value={yourHeight}
                  style={{
                    height: 40,
                    borderBottomColor: '#98A5B7',
                    borderBottomWidth: 1,
                    marginBottom: 15,
                    width: '100%',
                  }}
                  maxLength={isCentimeterEnabled ? 3 : 3}
                  onChangeText={(text) => setYourHeight(text)}
                  placeholder="input your height"
                  placeholderTextColor="#98A5B7"
                  keyboardType="number-pad"
                />
              </View>

              <View
                style={{alignContent: 'flex-start', width: '100%', zIndex: 11}}
              >
                <Text
                  style={{
                    color: '#101010',
                    textAlign: 'left',
                    marginBottom: 15,
                    alignContent: 'flex-start',
                    fontWeight: '600',
                    fontSize:
                      width === 320 || height === 610.6666666666666 ? 14 : 16,
                    lineHeight: 21,
                    fontFamily: 'SFUIText-Semibold',
                  }}
                >
                  Where do you live?
                </Text>
                <PlacesInput
                  googleApiKey="AIzaSyClkOf_6gTnXWGDgp9Atw7Y2iyr4IRTWkk"
                  placeHolder="Select Location"
                  language="en-US"
                  onSelect={(place) => {
                    setWhereYouLive(place.result.formatted_address);
                  }}
                  stylesInput={{
                    borderWidth: 1,
                    borderColor: 'white',
                    borderRadius: 8,
                    color: '#8A98AC',
                    fontSize: 18,
                    backgroundColor: '#EDEEF7',
                  }}
                  stylesContainer={{
                    position: 'relative',
                    alignSelf: 'stretch',
                    margin: 0,
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,
                    shadowOpacity: 0,
                  }}
                  stylesList={{
                    borderColor: '#0000',
                    borderLeftWidth: 1,
                    borderRightWidth: 1,
                    borderBottomWidth: 1,
                    left: -1,
                    right: -1,
                    backgroundColor: '#EDEEF7',
                  }}
                />
              </View>

              <View style={{height: 15}} />
              <View style={{alignContent: 'center', width: '90%'}}>
                <Text
                  style={{
                    color: '#101010',
                    textAlign: 'left',
                    marginBottom: 15,
                    alignContent: 'flex-start',
                    fontWeight: '600',
                    fontSize:
                      width === 320 || height === 610.6666666666666 ? 14 : 16,
                    lineHeight: 21,
                    fontFamily: 'SFUIText-Semibold',
                  }}
                >
                  Do you have a car?
                </Text>
                <View style={{flexDirection: 'row', marginTop: -10}}>
                  <TouchableOpacity
                    style={{
                      backgroundColor: !haveACar ? '#F44335' : '#F3F6FF',
                      width: 50,
                      height: 50,
                      borderRadius: 15,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                    onPress={() => {
                      setHaveACar(false);
                    }}
                  >
                    <FontAwesome
                      name="remove"
                      size={20}
                      color={!haveACar ? '#F3F6FF' : '#F44335'}
                    />
                  </TouchableOpacity>
                  <View style={{padding: 10}} />
                  <TouchableOpacity
                    style={{
                      backgroundColor: !haveACar ? '#F3F6FF' : '#00AEB0',
                      width: 50,
                      height: 50,
                      borderRadius: 15,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                    onPress={() => {
                      setHaveACar(true);
                    }}
                  >
                    <FontAwesome
                      name="check"
                      size={20}
                      color={!haveACar ? '#00AEB0' : '#F3F6FF'}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              <View style={{height: 15}} />
              <View
                style={{
                  alignContent: 'flex-start',
                  width: '100%',
                  zIndex: 8,
                  marginBottom: 205,
                }}
              >
                <Text
                  style={{
                    color: '#101010',
                    textAlign: 'left',
                    marginBottom: 15,
                    alignContent: 'flex-start',
                    fontWeight: '600',
                    fontSize:
                      width === 320 || height === 610.6666666666666 ? 14 : 16,
                    lineHeight: 21,
                    fontFamily: 'SFUIText-Semibold',
                  }}
                >
                  Do you smoke?
                </Text>
                <View style={{flexDirection: 'row', marginTop: -10}}>
                  <TouchableOpacity
                    style={{
                      backgroundColor: !doYouSmoke ? '#F44335' : '#F3F6FF',
                      width: 50,
                      height: 50,
                      borderRadius: 15,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                    onPress={() => {
                      setDoYouSmoke(false);
                    }}
                  >
                    <FontAwesome
                      name="remove"
                      size={20}
                      color={!doYouSmoke ? '#F3F6FF' : '#F44335'}
                    />
                  </TouchableOpacity>
                  <View style={{padding: 10}} />
                  <TouchableOpacity
                    style={{
                      backgroundColor: !doYouSmoke ? '#F3F6FF' : '#00AEB0',
                      width: 50,
                      height: 50,
                      borderRadius: 15,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                    onPress={() => {
                      setDoYouSmoke(true);
                    }}
                  >
                    <FontAwesome
                      name="check"
                      size={20}
                      color={!doYouSmoke ? '#00AEB0' : '#F3F6FF'}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </ScrollView>
            <View style={bottomButton.bottomView}>
              {isLoading ? (
                <ActivityIndicator
                  style={bottomButton.login}
                  size="large"
                  color="#8897AA"
                />
              ) : (
                <Button
                  style={bottomButton.login}
                  color="#8897AA"
                  disabled={!(yourHeight || whereYouLive)}
                  mode="contained"
                  onPress={tellUsMoreAboutYouSubmit2}
                >
                  <Text
                    style={{
                      textTransform: 'capitalize',
                      fontWeight: 'bold',
                      fontSize: 18,
                      color: 'white',
                    }}
                  >
                    Next
                  </Text>
                </Button>
              )}
              <TouchableOpacity onPress={() => skip_TellUsMoreAboutYou3()}>
                <Text
                  style={{
                    fontWeight: '500',
                    fontSize: 12,
                    color: '#B093FA',
                    lineHeight: 20,
                  }}
                >
                  Skip this page
                </Text>
              </TouchableOpacity>
            </View>
          </>
        )}
      </View>
    </SafeAreaView>
  );
}

const bottomButton = StyleSheet.create({
  bottomView: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 10,
  },
  login: {
    borderRadius: 8,
  },
});

const sideLogoStyles = StyleSheet.create({
  image: {
    marginTop: 0,
    marginLeft: 20,
  },
  imageLogo: {
    width: wp(7.5),
    height: wp(6.5),
    left: -4,
  },
  container: {
    zIndex: 10,
    width: '100%',
    position: 'absolute',
    top: Platform.OS === 'ios' ? hp('5') : hp('2'),
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    marginTop: 0,
    backgroundColor: 'white',
  },
});

const styles = StyleSheet.create({
  forgotPassword: {
    width: '100%',
    alignItems: 'flex-end',
    marginRight: 10,
    marginBottom: 24,
  },
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  forgot: {
    fontSize: 13,
    marginTop: 10,
    marginRight: -2,
    color: theme.colors.secondary,
  },
  rememberMe: {
    fontSize: 13,
    marginTop: 10,
    marginLeft: 10,
    color: theme.colors.secondary,
  },
  link: {
    fontWeight: '100',
    color: '#9872FA',
    lineHeight: 30,
  },
  container: {
    flex: 1,
    paddingTop: 10,
    width: '100%',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
});

export default TellUsMoreAboutYou2;
