import React, {useState} from 'react';
import {
  Image,
  View,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
  Dimensions,
  Platform,
} from 'react-native';
import {Text} from 'react-native-paper';
import DropDownPicker from 'react-native-dropdown-picker';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {SafeAreaView} from 'react-native-safe-area-context';
import Button from '../components/Button';
import {checkConnection} from '../components/checkConnection';
import {string} from '../const/string';
import {StaarzLogo} from './RegisterUserBioScreen';
import {Colors} from '../constants/colors';
import {useAuthProvider} from '../../App';

export const favouriteTravelList = [
  {label: 'Adventure', value: 1},
  {label: 'Private travel', value: 2},
  {label: 'Budget travel', value: 3},
  {label: 'Solo travel', value: 4},
];

export const favPetList = [
  {label: 'Cats', value: 1},
  {label: 'Dogs', value: 2},
  {label: 'Birds', value: 3},
  {label: 'Horses', value: 4},
  {label: 'Monkey', value: 5},
  {label: 'Rabbits', value: 6},
];

export const favMovieGenreList = [
  {label: 'Action', value: 0},
  {label: 'Horror', value: 1},
  {label: 'Romantic comedy', value: 2},
  {label: 'Comedy', value: 3},
  {label: 'Fun fiction', value: 4},
  {label: 'Romance', value: 5},
  {label: 'Religious', value: 6},
];

export const favMusicStyleList = [
  {label: 'Rock', value: 1},
  {label: 'Pop music', value: 2},
  {label: 'Hip hop', value: 3},
  {label: 'Classical music', value: 4},
  {label: 'Country music', value: 5},
  {label: 'RnB', value: 6},
  {label: 'Jazz', value: 7},
  {label: 'Reggae', value: 8},
];
function TellUsMoreAboutYou1({navigation}) {
  const {userState, register} = useAuthProvider();
  const [favPet, setFavPet] = useState({});
  const [favMusicStyle, setFavMusicStyle] = useState({});
  const [favMovieGenre, setFavMovieGenre] = useState('');
  const [favTravelStyle, setFavTravelStyle] = useState('');
  const [isLoading, setIsloading] = useState(false);
  const [net, setnet] = React.useState(true);
  const windowHeight = Dimensions.get('window').height;
  const {width} = Dimensions.get('window');
  const {height} = Dimensions.get('window');

  const getfuntion = () => {
    checkConnection().then((res) => {
      if (res === false) {
        setnet(res);
      } else {
        setnet(res);
      }
    });
  };

  const tellUsMoreAboutYouSubmit = () => {
    checkConnection().then((res) => {
      if (res === false) {
        setnet(res);
      } else {
        setnet(res);
        tellUsMoreAboutYouSubmit1();
      }
    });
  };

  const tellUsMoreAboutYouSubmit1 = async () => {
    const musicList = Object.values(favMusicStyle).map((item) => item.label);
    const petList = Object.values(favPet).map((item) => item.label);
    await register({
      ...userState?.registrationData,
      musics: musicList,
      pets: petList,
    });
    navigation.replace('TellUsMoreAboutYou2');
  };

  const skip_TellUsMoreAboutYou2 = () => {
    checkConnection().then((res) => {
      if (res === false) {
        setnet(res);
      } else {
        setnet(res);
        skip_TellUsMoreAboutYou23();
      }
    });
  };

  const skip_TellUsMoreAboutYou23 = () => {
    navigation.replace('TellUsMoreAboutYou2');
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
      <View style={{flex: 1, width: '90%', alignSelf: 'center'}}>
        <StaarzLogo goBack={navigation.goBack} />
        {!net ? (
          <View
            style={{alignItems: 'center', justifyContent: 'center', flex: 1}}
          >
            <Image
              source={require('../assets/internat.png')}
              style={{height: 50, width: 50}}
            />
            <Text style={{fontSize: 17, fontWeight: '700', marginTop: 5}}>
              {string.network}
            </Text>
            <TouchableOpacity
              color="white"
              mode="contained"
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                top: windowHeight * 0.3,
              }}
              onPress={() => getfuntion()}
            >
              <View
                style={{
                  backgroundColor: '#7643F8',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 50,
                  paddingHorizontal: 30,
                  paddingVertical: 20,
                  marginTop: 10,
                }}
              >
                {Platform.OS === 'ios' ? (
                  <Text style={{fontSize: hp('2'), color: '#fff'}}>
                    {string.Try_Again}
                  </Text>
                ) : (
                  <Text style={{fontSize: hp('2'), color: '#fff'}}>
                    {string.Try_Again}
                  </Text>
                )}
              </View>
            </TouchableOpacity>
          </View>
        ) : (
          <>
            <Text
              style={{
                color: '#000000',
                fontWeight: Platform.OS === 'ios' ? 'bold' : 'bold',
                fontSize: wp('6.5'),
                marginTop: '8%',
                textAlign: 'center',
                lineHeight: 36,
                fontFamily: 'MPLUS1p-Bold',
              }}
            >
              Tell us more about you
            </Text>
            <Text
              style={{
                fontWeight: '500',
                fontSize:
                  width === 320 || height === 610.6666666666666 ? 16 : 18,
                textAlign: 'center',
                color: '#8A98AC',
                marginBottom: 35,
                lineHeight: 27,
              }}
            >
              Lets get to know each other :){' '}
            </Text>
            <View
              style={{
                alignContent: 'flex-start',
                width: '100%',
                ...(Platform.OS === 'ios' && {
                  zIndex: 20,
                  width: '100%',
                }),
              }}
            >
              <Text
                style={{
                  color: '#101010',
                  textAlign: 'left',
                  marginBottom: 15,
                  alignContent: 'flex-start',
                  fontWeight: '600',
                  fontSize:
                    width === 320 || height === 610.6666666666666 ? 14 : 16,
                  lineHeight: 21,
                  fontFamily: 'SFUIText-Semibold',
                }}
              >
                What's your favorite music style?
              </Text>
              <DropDownPicker
                items={favMusicStyleList}
                dropDownMaxHeight={150}
                labelStyle={{
                  fontSize: 14,
                  textAlign: 'left',
                  color: Colors.gray['600'],
                }}
                multiple
                max={5}
                min={0}
                dropDownDirection="TOP"
                defaultValue={[]}
                containerStyle={{height: hp(5.5)}}
                style={{backgroundColor: '#fff', width: '100%', height: wp(13)}}
                itemStyle={{
                  justifyContent: 'flex-start',
                }}
                dropDownStyle={{backgroundColor: '#fff'}}
                onChangeItemMultiple={(item) => setFavMusicStyle({...item})}
              />
            </View>
            <View style={{height: 15}} />
            <View
              style={{
                alignContent: 'flex-start',
                width: '100%',
                ...(Platform.OS === 'ios' && {
                  zIndex: 11,
                }),
              }}
            >
              <Text
                style={{
                  color: '#101010',
                  textAlign: 'left',
                  marginBottom: 15,
                  alignContent: 'flex-start',
                  fontWeight: '600',
                  fontSize:
                    width === 320 || height === 610.6666666666666 ? 14 : 16,
                  lineHeight: 21,
                  fontFamily: 'SFUIText-Semibold',
                }}
              >
                What's your favorite movie genre?
              </Text>
              <DropDownPicker
                dropDownMaxHeight={150}
                items={favMovieGenreList}
                labelStyle={{
                  fontSize: 14,
                  textAlign: 'left',
                  color: Colors.gray['600'],
                }}
                dropDownDirection="TOP"
                containerStyle={{height: hp(5.5)}}
                style={{backgroundColor: '#fff', width: '100%'}}
                itemStyle={{
                  justifyContent: 'flex-start',
                }}
                defaultValue={favMovieGenre}
                dropDownStyle={{backgroundColor: '#fff'}}
                onChangeItem={(item) => {
                  setFavMovieGenre(item.value);
                }}
              />
            </View>
            <View style={{height: 15}} />
            <View
              style={{
                alignContent: 'flex-start',
                width: '100%',
                ...(Platform.OS === 'ios' && {
                  zIndex: 10,
                }),
              }}
            >
              <Text
                style={{
                  color: '#101010',
                  textAlign: 'left',
                  marginBottom: 15,
                  alignContent: 'flex-start',
                  fontWeight: '600',
                  fontSize:
                    width === 320 || height === 610.6666666666666 ? 14 : 16,
                  lineHeight: 21,
                  fontFamily: 'SFUIText-Semibold',
                }}
              >
                What's your favorite pet?
              </Text>
              <DropDownPicker
                dropDownMaxHeight={150}
                items={favPetList}
                labelStyle={{
                  fontSize: 14,
                  textAlign: 'left',
                  color: Colors.gray['600'],
                }}
                multiple
                max={5}
                min={0}
                dropDownDirection="TOP"
                defaultValue={[]}
                containerStyle={{height: hp(5.5)}}
                style={{backgroundColor: '#fff', width: '100%'}}
                itemStyle={{
                  justifyContent: 'flex-start',
                }}
                dropDownStyle={{backgroundColor: '#fff'}}
                onChangeItemMultiple={(item) => setFavPet({...item})}
              />
            </View>

            <View style={{height: 15}} />
            <View
              style={{
                alignContent: 'flex-start',
                width: '100%',
                marginBottom: 200,
                ...(Platform.OS === 'ios' && {
                  zIndex: 9,
                }),
              }}
            >
              <Text
                style={{
                  color: '#101010',
                  textAlign: 'left',
                  marginBottom: 15,
                  alignContent: 'flex-start',
                  fontWeight: '600',
                  fontSize:
                    width === 320 || height === 610.6666666666666 ? 14 : 16,
                  lineHeight: 21,
                  fontFamily: 'SFUIText-Semibold',
                }}
              >
                What's your favorite travel style?
              </Text>
              <DropDownPicker
                items={favouriteTravelList}
                labelStyle={{
                  fontSize: 14,
                  textAlign: 'left',
                  color: Colors.gray['600'],
                }}
                dropDownDirection="TOP"
                defaultValue={favTravelStyle}
                containerStyle={{height: hp(5.5)}}
                style={{backgroundColor: '#fff', width: '100%'}}
                itemStyle={{
                  justifyContent: 'flex-start',
                }}
                dropDownStyle={{backgroundColor: '#fff', zIndex: 10}}
                onChangeItem={(item) => setFavTravelStyle(item.value)}
              />
            </View>

            <View style={bottomButton.bottomView}>
              {isLoading ? (
                <ActivityIndicator
                  style={bottomButton.login}
                  size="large"
                  color="#8897AA"
                />
              ) : (
                <Button
                  style={bottomButton.login}
                  color="#8897AA"
                  disabled={
                    isLoading ||
                    favPet === {} ||
                    favMusicStyle === {} ||
                    favMovieGenre === '' ||
                    favTravelStyle === ''
                  }
                  mode="contained"
                  onPress={tellUsMoreAboutYouSubmit}
                >
                  <Text
                    style={{
                      textTransform: 'capitalize',
                      fontWeight: 'bold',
                      fontSize: hp('2.3'),
                      color: 'white',
                    }}
                  >
                    Next
                  </Text>
                </Button>
              )}
              <TouchableOpacity onPress={() => skip_TellUsMoreAboutYou2()}>
                <Text
                  style={{
                    fontWeight: '500',
                    fontSize: hp('1.8'),
                    color: '#B093FA',
                  }}
                >
                  Skip this page
                </Text>
              </TouchableOpacity>
            </View>
          </>
        )}
      </View>
    </SafeAreaView>
  );
}

const bottomButton = StyleSheet.create({
  bottomView: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 10,
    zIndex: -1,
  },
  login: {
    borderRadius: 8,
  },
});

export default TellUsMoreAboutYou1;
