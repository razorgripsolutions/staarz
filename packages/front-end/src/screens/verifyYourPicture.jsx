import React, {useState, useCallback, useRef, useEffect} from 'react';
import {
  Image,
  View,
  StyleSheet,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ImageBackground,
  Dimensions,
  ActivityIndicator,
  Platform,
  Alert,
} from 'react-native';
import {Text} from 'react-native-paper';
import * as ImagePicker from 'react-native-image-picker';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Button from '../components/Button';
import {checkConnection} from '../components/checkConnection';
import {string} from '../const/string';
import ActionSheet from 'react-native-actionsheet';
import {useAuthProvider} from '../../App';
import {getMessageFromCode} from './uploadYourPicture';
import RNPermissions, {
  Permission,
  PERMISSIONS,
  check,
  RESULTS,
  request,
  openSettings,
} from 'react-native-permissions';

const sideLogoStyles = StyleSheet.create({
  image: {
    marginTop: 0,
    marginLeft: 20,
  },
  imageLogo: {
    width: wp(7.5),
    height: wp(6.5),
    left: -4,
  },
  container: {
    position: 'absolute',
    top: Platform.OS === 'ios' ? hp('4') : hp('3'),
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 0,
    zIndex: 10,
  },
});

const bottomButton = StyleSheet.create({
  bottomView: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    bottom: hp('2'),
    paddingLeft: 20,
    paddingRight: 20,
  },
  login: {
    marginBottom: 30,
    borderRadius: 8,
  },
});

const uploadButton = StyleSheet.create({
  button: {
    marginTop: hp('48'),
  },
  navigation: {
    marginVertical: hp('5'),
    top: -14,
  },
});

const verifyYourPicture = ({navigation}) => {
  const [Active, setActive] = useState(true);
  const [isLoading, setIsloading] = useState(false);
  const [net, setnet] = React.useState(true);
  const windowHeight = Dimensions.get('window').height;
  const {width} = Dimensions.get('window');
  const {height} = Dimensions.get('window');

  const [path, setPath] = useState(null);
  const [file, setFile] = useState(null);

  const {userState, register} = useAuthProvider();

  const ActionSheetRef = useRef();

  const getfuntion = () => {
    checkConnection().then((res) => {
      if (res === false) {
        setnet(res);
      } else {
        setnet(res);
      }
    });
  };

  const uploadImage = useCallback(() => {
    setActive(false);
  }, [setActive]);

  const handleOpenSettings = useCallback(async () => {
    return openSettings()
      .then((res) => {
        console.log(res);
      })
      .catch((e) => {
        console.log(e);
      });
  }, [openSettings]);

  const handlePermission = useCallback(async () => {
    let status;
    if (Platform.OS === 'android') {
      status = check(PERMISSIONS.ANDROID.CAMERA).then((result) => {
        console.log('result1', result);
        if (result === RESULTS.DENIED) {
          return request(PERMISSIONS.ANDROID.CAMERA).then((requestResult) => {
            console.log('requestResult', requestResult);
          });
        } else if (result === RESULTS.BLOCKED) {
          return Alert.alert(
            'Open Settings',
            'You would need to  allow camera permissions in your settings.',
            [
              {
                text: 'No',
                onPress: () => {},
                style: 'cancel',
              },
              {
                text: 'Yes',
                onPress: handleOpenSettings,
              },
            ],
          );
        }
      });
    } else if (Platform.OS === 'ios') {
      status = check(PERMISSIONS.IOS.CAMERA).then((result) => {
        console.log('result1', result);
        if (result === RESULTS.DENIED) {
          return request(PERMISSIONS.IOS.CAMERA).then((requestResult) => {
            console.log('requestResult', requestResult);
          });
        } else if (result === RESULTS.BLOCKED) {
          return Alert.alert(
            'Open Settings',
            'You would need to  allow camera permissions in your settings.',
            [
              {
                text: 'No',
                onPress: () => {},
                style: 'cancel',
              },
              {
                text: 'Yes',
                onPress: handleOpenSettings,
              },
            ],
          );
        }
      });
    }
    return status;
  }, [handleOpenSettings]);

  useEffect(() => {
    handlePermission();
  }, []);

  const askToUpload = () => {
    Alert.alert(
      'Confirm',
      'Are you sure you want to upload this image?',
      [
        {
          text: 'No',
          onPress: () => {},
          style: 'cancel',
        },
        {
          text: 'Yes!',
          onPress: uploadImage,
        },
      ],
      {cancelable: false},
    );
  };

  const chooseImage = React.useCallback(
    (index) => {
      /* launch camera */
      if (index === 0) {
        const options = {
          saveToPhotos: true,
          mediaType: 'photo',
          includeBase64: true,
        };

        return ImagePicker.launchCamera(options, async (response) => {
          if (response.errorCode) {
            Toast.show({
              text1: 'Application Error',
              text2: getMessageFromCode(response.errorCode),
              type: 'error',
            });
          } else {
            setPath(response?.assets?.[0]?.uri);
            setFile(response?.assets?.[0]);
            askToUpload();
          }
        });
      }
    },
    [askToUpload, setFile],
  );

  const onVerifyYourPictureSubmit2 = async () => {
    await register({
      ...userState.registrationData,
      cameraImage: file,
    });
    navigation.navigate('PendingForVerification');
  };
  const onVerifyYourPictureSubmit = () => {
    checkConnection().then((res) => {
      if (res === false) {
        setnet(res);
      } else {
        setnet(res);
        onVerifyYourPictureSubmit2();
      }
    });
  };

  return (
    <>
      <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
        <View style={{alignItems: 'center', flex: 1}}>
          <StatusBar barStyle="dark-content" backgroundColor="white" />
          <View
            style={{
              flexDirection: 'row',
              alignSelf: 'center',
              top: 0,
              alignItems: 'center',
            }}
          >
            <Image
              source={require('../assets/starzlogo.png')}
              style={sideLogoStyles.imageLogo}
            />
            <Text
              style={{
                fontSize: wp(6),
                fontFamily: 'MPLUS1p-Bold',
                color: '#243443',
                left: -2,
                lineHeight: 33,
                textAlign: 'center',
              }}
            >
              STAARY
            </Text>
          </View>
          {!net ? (
            <View style={{alignItems: 'center', flex: 1}}>
              <Image
                source={require('../assets/internat.png')}
                style={{height: 50, width: 50}}
              />
              <Text style={{fontSize: 17, fontWeight: '700', marginTop: 5}}>
                {string.network}
              </Text>
              <TouchableOpacity
                color="white"
                mode="contained"
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  top: windowHeight * 0.3,
                }}
                onPress={() => getfuntion()}
              >
                <View
                  style={{
                    backgroundColor: '#7643F8',
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 50,
                    paddingHorizontal: 30,
                    paddingVertical: 20,
                    marginTop: 10,
                  }}
                >
                  {Platform.OS === 'ios' ? (
                    <Text style={{fontSize: hp('2'), color: '#fff'}}>
                      {string.Try_Again}
                    </Text>
                  ) : (
                    <Text style={{fontSize: hp('2'), color: '#fff'}}>
                      {string.Try_Again}
                    </Text>
                  )}
                </View>
              </TouchableOpacity>
            </View>
          ) : (
            <>
              <View style={{flex: 1, width: '100%'}}>
                <View
                  style={{
                    height: '20%',
                    alignItems: 'center',
                    paddingVertical: 10,
                  }}
                >
                  <Text
                    style={{
                      letterSpacing: 0.4,
                      fontSize:
                        width === 320 || height === 610.6666666666666 ? 22 : 26,
                      lineHeight: 36,
                      fontFamily: 'SFUIText-Semibold',
                      fontWeight: 'bold',
                    }}
                  >
                    Verify your picture
                  </Text>
                  <Text
                    style={{
                      textAlign: 'center',
                      letterSpacing: 1,
                      color: '#000000',
                      lineHeight: 24,
                      fontSize:
                        width === 320 || height === 610.6666666666666 ? 12 : 14,
                      fontWeight: '300',
                    }}
                  >
                    Please open your camera for face{'\n'}verification
                  </Text>
                </View>
                <TouchableOpacity
                  onPress={() => {
                    ActionSheetRef?.current?.show();
                  }}
                  style={{height: '60%'}}
                >
                  <View style={{height: '100%', alignItems: 'center'}}>
                    <ImageBackground
                      source={
                        !path
                          ? require('../assets/facial_recognition.png')
                          : {uri: path}
                      }
                      style={{
                        height: '100%',
                        width: '100%',
                        resizeMode: 'contain',
                      }}
                    />
                  </View>
                </TouchableOpacity>

                <View style={{height: '20%', alignItems: 'center'}}>
                  <Image
                    source={require('../assets/verifyYourPictureNavigationImage.png')}
                    style={uploadButton.navigation}
                  />
                  <View style={bottomButton.bottomView}>
                    {isLoading ? (
                      <ActivityIndicator
                        style={bottomButton.login}
                        size="large"
                        color="#8897AA"
                      />
                    ) : (
                      <Button
                        disabled={Active}
                        style={bottomButton.login}
                        color="#8897AA"
                        mode="contained"
                        onPress={onVerifyYourPictureSubmit}
                      >
                        <Text
                          style={{
                            textTransform: 'capitalize',
                            fontWeight: 'bold',
                            fontSize: 18,
                            color: 'white',
                          }}
                        >
                          Next
                        </Text>
                      </Button>
                    )}
                  </View>
                </View>
              </View>
            </>
          )}
        </View>
      </SafeAreaView>
      <ActionSheet
        ref={ActionSheetRef}
        title={'How do you like to add an image ?'}
        options={['Open camera', 'Cancel']}
        cancelButtonIndex={1}
        onPress={chooseImage}
      />
    </>
  );
};

export default verifyYourPicture;
