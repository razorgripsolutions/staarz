import React, {useState, useEffect} from 'react';
import {
  Image,
  View,
  FlatList,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
  SafeAreaView,
  Platform,
} from 'react-native';
import {Text} from 'react-native-paper';

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {StackActions} from '@react-navigation/native';
import {theme} from '../core/theme';
import {url} from '../const/urlDirectory';

import Button from '../components/Button';
import {checkConnection} from '../components/checkConnection';
import {string} from '../const/string';

function ChooesOption({navigation, route}) {
  const [signUpemail, setEmail] = useState({value: '', error: ''});
  const [userLoginId, setuserLoginId] = useState(route.params.userLoginId);
  // const [rchatdata, setrchatdata] =useState(route.params.rchatdata)
  const [onatherUserId, setonatherUserId] = useState(
    route.params.onatherUserId,
  );
  const [selcted, setselcted] = useState();

  // const [userData, setUserData] = useState(props);
  console.log(
    'userData------------------',
    userLoginId,
    'onatherUserId-------',
    onatherUserId,
  );
  const [Data, setData] = useState(false);
  const [isLoading, setisLoading] = useState(false);

  const [net, setnet] = React.useState(true);
  const [selcter, setselcter] = useState(null);
  // const [relationshipTypeList, setRelationshipTypeList] = useState([]);

  const windowWidth = Dimensions.get('window').width;
  const windowHeight = Dimensions.get('window').height;
  const {width} = Dimensions.get('window');
  const {height} = Dimensions.get('window');
  useEffect(() => {
    ApiCAlling();
  }, []);

  const ApiCAlling = async () => {
    checkConnection().then((res) => {
      if (res === false) {
        // dropDownAlertRef.alertWithType('error', 'Error', string.network);
        setnet(res);
      } else {
        setnet(res);
        creatChannel();
      }
    });
  };

  const selcterOption = (reasonId) => [
    // console.log("userLoginId  choees screen=============-----------",userLoginId),
    selctedButton(reasonId),
    // alert(reasonId)
  ];

  const selctedButton = (id) => {
    setselcted(id);
    console.log(selcted);
  };

  const creatChannel = () => {
    fetch(url.reasonOfBlockUserList, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(
          `reasonOfBlockUserList------------------${JSON.stringify(
            responseJson.result,
          )}`,
        );
        //  alert(responseJson.result);
        setData(responseJson.result);
      })
      .catch((error) => {
        alert('Oops error!!');
        console.error(error);
      });
  };

  // TODO - fix this shadow issue
  // eslint-disable-next-line no-shadow
  const NAV_TO_Are_You_Sure = async (userLoginId, onatherUserId, selcted) => {
    checkConnection().then((res) => {
      if (res === false) {
        // dropDownAlertRef.alertWithType('error', 'Error', string.network);
        setnet(res);
      } else {
        setnet(res);
        navigation.dispatch(
          StackActions.push('Are_You_Sure', {
            userLoginId,
            onatherUserId,
            selcted,
          }),
        );
      }
    });
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
      <View style={{flex: 1, alignItems: 'center', marginHorizontal: 20}}>
        <View
          style={{
            flexDirection: 'row',
            top: Platform.OS === 'ios' ? 0 : 0,
            alignItems: 'center',
            justifyContent: 'center',
            width: '100%',
            backgroundColor: '#fff',
            zIndex: 10,
          }}
        >
          <View style={{width: '20%'}}>
            <TouchableOpacity onPress={() => navigation.goBack()} style={{}}>
              <FontAwesome
                name="angle-left"
                size={25}
                color="#000"
                style={{}}
              />
            </TouchableOpacity>
          </View>
          <View
            style={{
              width: '60%',
              alignItems: 'center',
              flexDirection: 'row',
              justifyContent: 'center',
            }}
          >
            <Image
              source={require('../assets/starzlogo.png')}
              style={sideLogoStyles.imageLogo}
            />
            {/* <Image source={require('../assets/STAARZ_109X19_black.png')} style={sideLogoStyles.image} /> */}
            <Text
              style={{
                fontSize: wp(6),
                fontFamily: 'MPLUS1p-Bold',
                color: '#243443',
                left: -2,
                lineHeight: 33,
                textAlign: 'center',
              }}
            >
              STAARZ
            </Text>
          </View>
          <View style={{backgroundColor: 'red', width: '20%'}} />
        </View>
        {!net ? (
          <View
            style={{alignItems: 'center', justifyContent: 'center', flex: 1}}
          >
            <Image
              source={require('../assets/internat.png')}
              style={{height: 50, width: 50}}
            />
            <Text style={{fontSize: 17, fontWeight: '700', marginTop: 5}}>
              {string.network}
            </Text>
            <TouchableOpacity
              color="white"
              mode="contained"
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                top: windowHeight * 0.3,
              }}
              onPress={() => ApiCAlling()}
            >
              <View
                style={{
                  backgroundColor: '#7643F8',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 50,
                  paddingHorizontal: 30,
                  paddingVertical: 20,
                  marginTop: 10,
                }}
              >
                {
                  Platform.OS === 'ios' ? (
                    // <Ionicons name="reload" color="black" size={30} />
                    <Text style={{fontSize: hp('2'), color: '#fff'}}>
                      {string.Try_Again}
                    </Text>
                  ) : (
                    <Text style={{fontSize: hp('2'), color: '#fff'}}>
                      {string.Try_Again}
                    </Text>
                  )
                  // <MaterialCommunityIcons name="reload" color="black" size={30} />
                }
              </View>
            </TouchableOpacity>
          </View>
        ) : (
          <>
            <StaarzLogoText />
            <View
              style={{
                padding: 15,
                backgroundColor: '#f0f8ff',
                width: '100%',
                borderRadius: 5,
                position: 'absolute',
                top: 200,
                marginTop: 20,
              }}
            >
              <FlatList
                data={Data}
                scrollEnabled={false}
                renderItem={({item, index}) => (
                  //  console.log(index),
                  <View key={index} style={styles.otionContent}>
                    {/* {console.log("id,",selcted)} */}
                    <Text style={styles.selctoptionTExt}>{item.reason}</Text>
                    {selcted !== item.reasonId ? (
                      <TouchableOpacity
                        onPress={() => selcterOption(item.reasonId)}
                        style={styles.NullChekbox}
                      />
                    ) : (
                      <TouchableOpacity
                        onPress={() => setselcted(null)}
                        style={styles.chekborright}
                      >
                        <MaterialIcons
                          name="check"
                          size={13}
                          color="#fff"
                          style={{top: -1}}
                        />
                      </TouchableOpacity>
                    )}
                  </View>
                )}
              />
            </View>

            <View style={bottomButton.bottomView}>
              {isLoading ? (
                <ActivityIndicator
                  style={bottomButton.login}
                  size="large"
                  color="#8897AA"
                />
              ) : (
                <Button
                  style={bottomButton.login}
                  color="#8897AA"
                  mode="contained"
                  onPress={() =>
                    selcted === null
                      ? alert('Plese select reason')
                      : //  navigation.navigate("Are_You_Sure", { userLoginId: userLoginId, onatherUserId: onatherUserId, selcted: selcted, })}

                        NAV_TO_Are_You_Sure(userLoginId, onatherUserId, selcted)
                  }
                >
                  {/* console.log("userData------------------",userLoginId ,"onatherUserId-------",onatherUserId ,"selcted----------",selcted) */}

                  <Text
                    style={{
                      textTransform: 'capitalize',
                      fontWeight: 'bold',
                      fontSize: 18,
                      color: 'white',
                    }}
                  >
                    Next
                  </Text>
                </Button>
              )}
              <View>
                <Text
                  style={{
                    color: '#BFC7D3',
                    fontSize: 12,
                    lineHeight: 20,
                    fontWeight: '500',
                    fontFamily: 'MPLUS1p-Medium',
                  }}
                >
                  By reporting, you agree to our
                </Text>
              </View>
              <TouchableOpacity
                onPress={() => navigation.navigate('PrivacyPolicy')}
              >
                <Text
                  style={{
                    color: '#A17EFA',
                    fontSize: 13,
                    lineHeight: 20,
                    fontWeight: '500',
                    fontFamily: 'MPLUS1p-Medium',
                  }}
                >
                  Terms and Conditions
                </Text>
              </TouchableOpacity>
            </View>
          </>
        )}
      </View>
    </SafeAreaView>
  );
}

function StaarzLogo() {
  return (
    <View style={sideLogoStyles.container}>
      <Image
        source={require('../assets/starzlogo.png')}
        style={sideLogoStyles.imageLogo}
      />
      <Image
        source={require('../assets/STAARZ_109X19_black.png')}
        style={sideLogoStyles.image}
      />
    </View>
  );
}

function StaarzLogoText() {
  return (
    <View style={{position: 'absolute', top: '10%'}}>
      <Text
        style={{
          fontWeight: '900',
          letterSpacing: 0.4,
          fontSize: 24,
          alignItems: 'center',
          alignSelf: 'center',
          lineHeight: 36,
          fontFamily: 'MPLUS1p-Bold',
        }}
      >
        What happened?
      </Text>
      {/* <Text style={{ fontWeight: "bold", letterSpacing: 2, fontSize: 20, alignItems: "center", alignSelf: "center" }}>What happened?</Text> */}
      <Text
        style={{
          textAlign: 'center',
          letterSpacing: 1,
          fontSize: 18,
          color: 'grey',
          fontWeight: '600',
          top: 15,
          lineHeight: 22,
          fontFamily: 'MPLUS1p-Medium',
        }}
      >
        choosse between the{'\n'}options below to report
      </Text>
    </View>
  );
}

const bottomButton = StyleSheet.create({
  bottomView: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
  },
  login: {
    marginBottom: 15,
    borderRadius: 8,
  },
});

const sideLogoStyles = StyleSheet.create({
  image: {
    marginTop: 5,
    marginLeft: 20,
  },
  imageLogo: {
    // marginTop: 8,
    width: wp(7.5),
    height: wp(6.5),
    resizeMode: 'cover',
    left: -4,
    // marginBottom: 8,
  },
  container: {
    position: 'absolute',
    // top: 10, DUP
    top: 0,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    backgroundColor: 'white',
    zIndex: 10,
    width: '100%',
  },
});

const orDividerLineStyle = StyleSheet.create({
  or: {
    marginTop: 25,
    marginBottom: 10,
    alignSelf: 'center',
  },
});

const socialMediaLoginButtonStyle = StyleSheet.create({
  facebook: {
    // marginBottom: 8,
  },
  google: {
    // marginLeft:20
  },
});

const styles = StyleSheet.create({
  forgotPassword: {
    width: '100%',
    alignItems: 'flex-end',
    marginRight: 10,
    marginBottom: 24,
  },
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  forgot: {
    fontSize: 13,
    marginTop: 10,
    marginRight: -2,
    color: theme.colors.secondary,
  },
  rememberMe: {
    fontSize: 13,
    marginTop: 10,
    marginLeft: 10,
    color: theme.colors.secondary,
  },
  link: {
    // fontWeight: '100',
    color: '#A17EFA', // theme.colors.primary,
    lineHeight: 30,
    fontSize: 13,
  },
  otionContent: {
    width: '100%',
    backgroundColor: '#f0f8ff',
    justifyContent: 'space-between',
    alignItems: 'center',
    //   padd
    marginVertical: 12,
    flexDirection: 'row',
  },
  selctoptionTExt: {
    fontWeight: '600',

    width: '90%',
    //   backgroundColor:"red",

    fontFamily: 'SFUIText-Semibold',
    fontSize: 16,
    lineHeight: 22,
  },
  NullChekbox: {
    height: 15,
    width: 15,
    borderWidth: 2,
    borderColor: '#ccc',
    borderRadius: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  chekborright: {
    height: 15,
    width: 15,
    borderWidth: 2,
    borderColor: '#8a2be2',
    backgroundColor: '#8a2be2',
    borderRadius: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default ChooesOption;
