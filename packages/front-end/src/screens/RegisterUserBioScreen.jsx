import React, {useState, useEffect} from 'react';
import {
  Image,
  View,
  StyleSheet,
  TouchableOpacity,
  StatusBar,
  SafeAreaView,
  ScrollView,
  Dimensions,
  ActivityIndicator,
  Platform,
} from 'react-native';
import {Text} from 'react-native-paper';
import {CircleFade} from 'react-native-animated-spinkit';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import Moment from 'moment';
import DatePicker from 'react-native-date-picker';
import PlacesInput from 'react-native-places-input';
import Tabs from 'react-native-tabs';
import DropDownPicker from 'react-native-dropdown-picker';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {checkConnection} from '../components/checkConnection';
import {theme} from '../core/theme';
import TextInput from '../components/TextInput';
import Button from '../components/Button';
import {string} from '../const/string';
import {useAuthProvider} from '../../App';
import {Colors} from '../constants/colors';

const {width} = Dimensions.get('window');
const {height} = Dimensions.get('window');

export const ReligionList = [
  {label: 'Christian', value: 1},
  {label: 'Buddhism', value: 2},
  {label: 'Judaism', value: 3},
];

export const RelationshipList = [
  {label: 'Casual', value: 1},
  {label: 'Long-Term', value: 2},
  {label: 'Short-Term', value: 3},
];

function RegisterUserBioScreen({navigation}) {
  const [fullName, setfullName] = useState({});
  const [date, setDate] = useState(new Date());
  const [open, setOpen] = useState(false);
  const [dateBirthday, setDateBirthday] = useState(null);
  const [dateBirthdayText, setDateBirthdayText] = useState('Date');

  const [location, setLocation] = useState({add: '', lat: 0.0, lon: 0.0});
  const [gender, setGender] = useState('');
  const [religion, setReligion] = useState('');
  const [interestedIn, setInterestedIn] = useState(0);
  const [lookingFor, setLookingFor] = useState('');
  const [religionOptionsList, setReligionOptionsList] = useState(ReligionList);
  const [relationshipTypeList, setRelationshipTypeList] =
    useState(RelationshipList);

  const [isLoading, setIsloading] = useState(false);
  const [isPageLoading, setIsPageLoading] = useState(false);
  const [bottomHeight, setBottomHeight] = useState(10);

  const [net, setnet] = React.useState(true);
  const windowHeight = Dimensions.get('window').height;

  const {userState, register} = useAuthProvider();

  useEffect(
    () =>
      setDateBirthdayText(
        dateBirthday === null
          ? 'Date'
          : Moment(dateBirthday).format(
              Platform?.OS === 'ios' ? 'YYYY-MM-DD' : 'YYYY-MM-DD',
            ),
      ),
    [dateBirthday],
  );

  const getfuntion = () => {
    checkConnection().then((res) => {
      if (res === false) {
        setnet(res);
      } else {
        setnet(res);
      }
    });
  };

  const registerUserBioScreen = () => {
    checkConnection().then((res) => {
      if (res === false) {
        setnet(res);
      } else {
        setnet(res);
        registerUserBioScreen1();
      }
    });
  };

  function difference(date1, date2) {
    const date1utc = Date.UTC(
      date1.getFullYear(),
      date1.getMonth(),
      date1.getDate(),
    );
    const date2utc = Date.UTC(
      date2.getFullYear(),
      date2.getMonth(),
      date2.getDate(),
    );
    const day = 1000 * 60 * 60 * 24;
    return (date2utc - date1utc) / day;
  }

  const registerUserBioScreen1 = async () => {
    const today = new Date();
    const dateString = Moment(today).format('YYYY-MM-DD');
    // alert(date)
    const dateString1 = Moment(Moment(date, 'DD-MM-YYYY')).format('YYYY-MM-DD');
    if (date !== undefined) {
      const day1 = dateString1;

      const date1 = new Date(day1);
      const date2 = new Date(dateString);
      const time_difference = difference(date1, date2);
      const yeayold = Math.floor(time_difference / 365);
      if (yeayold < 16) {
        alert('Age must be greater than 16 ');
      } else {
        await register({
          ...userState?.registrationData,
          dob: dateBirthdayText,
          gender: gender === '0' ? 'Male' : 'Female',
          location: [+location.lat, +location.lon],
          religion: ReligionList.find((item) => item.value === +religion)
            ?.label,
          fullName: fullName.value,
          intrestedIn: interestedIn === '0' ? 'Male' : 'Female',
          typeOfRelationship: RelationshipList.find(
            (item) => item.value === +lookingFor,
          )?.label,
        });
        navigation.navigate('TellUsMoreAboutYou1');
      }
    } else {
      alert('Please select date');
      return false;
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <View
        style={{
          flex: 1,
          width: '90%',
          alignItems: 'center',
          justifyContent: 'center',
          alignSelf: 'center',
        }}
      >
        <StatusBar barStyle="dark-content" backgroundColor="white" />
        <StaarzLogo goBack={navigation.goBack} />

        {!net ? (
          <View
            style={{alignItems: 'center', justifyContent: 'center', flex: 1}}
          >
            <Image
              source={require('../assets/internat.png')}
              style={{height: 50, width: 50}}
            />
            <Text style={{fontSize: 17, fontWeight: '700', marginTop: 5}}>
              {string.network}
            </Text>
            <TouchableOpacity
              color="white"
              mode="contained"
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                top: windowHeight * 0.3,
              }}
              onPress={() => getfuntion()}
            >
              <View
                style={{
                  backgroundColor: '#7643F8',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 50,
                  paddingHorizontal: 30,
                  paddingVertical: 20,
                  marginTop: 10,
                }}
              >
                {Platform.OS === 'ios' ? (
                  <Text style={{fontSize: hp('2'), color: '#fff'}}>
                    {string.Try_Again}
                  </Text>
                ) : (
                  <Text style={{fontSize: hp('2'), color: '#fff'}}>
                    {string.Try_Again}
                  </Text>
                )}
              </View>
            </TouchableOpacity>
          </View>
        ) : (
          <>
            <View style={{flex: 9}}>
              <ScrollView
                style={styles.scrollView}
                showsVerticalScrollIndicator={false}
                keyboardShouldPersistTaps="always"
              >
                <Text
                  style={{
                    fontWeight: Platform.OS === 'ios' ? 'bold' : 'bold',
                    fontSize: wp('7'),
                    marginTop: 40,
                    justifyContent: 'center',
                    alignContent: 'center',
                    alignSelf: 'center',
                    lineHeight: 42,
                    fontFamily: 'SFUIText-Bold',
                    letterSpacing: 0.5,
                  }}
                >
                  Let&apos;s Get Started
                </Text>
                <Text
                  style={{
                    fontWeight: Platform.OS === 'ios' ? '600' : 'bold',
                    fontSize: hp(1.9),
                    textAlign: 'center',
                    letterSpacing: 1,
                    color: '#101010',
                    marginBottom: 15,
                    fontFamily: 'SFUIText-Bold',
                  }}
                >
                  Lets get to know each other
                </Text>
                {isPageLoading ? (
                  <View
                    style={{justifyContent: 'center', alignItems: 'center'}}
                  >
                    <CircleFade size={110} color="#5E2EBA" />
                  </View>
                ) : (
                  <View style={{width: '100%'}}>
                    <View style={{width: '100%'}}>
                      <TextInput
                        label="Full name"
                        placeholder="Enter your Full name"
                        value={fullName.value}
                        multiline={false}
                        onChangeText={(text) =>
                          setfullName({value: text, error: ''})
                        }
                        style={{width: wp(90), backgroundColor: '#fff'}}
                        theme={{
                          colors: {
                            placeholder: '#B9BAC8',
                            text: Colors.gray['600'],
                            primary: Colors.gray['600'],
                            underlineColor: 'transparent',
                            backgroundColor: '#fff',
                          },
                        }}
                        defaultValue={userState?.registrationData?.fullName}
                      />
                    </View>
                    <View style={{alignContent: 'flex-start', width: '100%'}}>
                      <Text
                        style={{
                          textAlign: 'left',
                          marginTop: 18,
                          marginBottom: 0,
                          alignContent: 'flex-start',
                          color: '#1E263C',
                          lineHeight: 22,
                          fontWeight: Platform.OS === 'ios' ? null : null,
                          fontFamily: 'SFUIText-Semibold',
                        }}
                      >
                        Birthdate
                      </Text>

                      <View
                        style={{
                          flexDirection: 'row',
                          width: '100%',
                          alignItems: 'center',
                        }}
                      >
                        <View style={{width: '100%'}}>
                          <TextInput
                            editable={false}
                            value={dateBirthdayText}
                            style={{width: wp(90)}}
                            theme={{
                              colors: {
                                placeholder: '#B9BAC8',
                                text: Colors.gray['600'],
                                primary: Colors.gray['600'],
                                underlineColor: 'transparent',
                              },
                            }}
                          />
                        </View>
                        <TouchableOpacity
                          onPress={() => {
                            setOpen(true);
                          }}
                          style={{position: 'absolute', right: wp(2)}}
                        >
                          <Image
                            source={require('../assets/calendar-day.png')}
                            style={{width: 45, height: 45, marginTop: 20}}
                          />
                        </TouchableOpacity>
                      </View>
                      <DatePicker
                        modal
                        open={open}
                        date={date}
                        onConfirm={(date) => {
                          setOpen(false);
                          setDate(date);
                          setDateBirthday(date);
                        }}
                        onCancel={() => {
                          setOpen(false);
                        }}
                        defaultValue={userState?.registrationData?.dob}
                        maximumDate={new Date()}
                        mode="date"
                        androidVariant="nativeAndroid"
                      />
                    </View>
                    <View
                      style={{
                        alignContent: 'flex-start',
                        width: '100%',
                        zIndex: 10,
                      }}
                    >
                      <Text
                        style={{
                          textAlign: 'left',
                          marginTop: 18,
                          marginBottom: 10,
                          alignContent: 'flex-start',
                          color: '#1E263C',
                          lineHeight: 22,
                          fontWeight: Platform.OS === 'ios' ? null : null,
                          fontFamily: 'SFUIText-Semibold',
                        }}
                      >
                        Location
                      </Text>
                      <View
                        style={{
                          width: '100%',
                          alignItems: 'center',
                          flexDirection: 'row',
                        }}
                      >
                        <PlacesInput
                          googleApiKey="AIzaSyClkOf_6gTnXWGDgp9Atw7Y2iyr4IRTWkk"
                          placeHolder="Select Location"
                          language="en-US"
                          onSelect={(place) => {
                            setLocation({
                              add: place.result.formatted_address,
                              lat: place.result.geometry.location.lat,
                              lon: place.result.geometry.location.lng,
                            });
                          }}
                          stylesInput={{
                            height: wp(13),
                            borderRadius: 8,
                            borderBottomLeftRadius: 8,
                            borderTopLeftRadius: 8,
                            color: '#8A98AC',
                            width: wp(90),
                            backgroundColor: '#EDEEF7',
                          }}
                          stylesContainer={{
                            position: 'relative',
                            alignSelf: 'stretch',
                            margin: 0,
                            top: 0,
                            left: 0,
                            right: 0,
                            bottom: 0,
                            shadowOpacity: 0,
                          }}
                          stylesList={{
                            borderColor: '#0000',
                            borderLeftWidth: 1,
                            borderRightWidth: 1,
                            borderBottomWidth: 1,
                            left: -1,
                            right: -1,
                          }}
                        />
                      </View>
                    </View>
                    <View style={{width: '100%', alignSelf: 'center'}}>
                      <Text
                        style={{
                          textAlign: 'left',
                          marginTop: 18,
                          marginBottom: 8,
                          alignContent: 'flex-start',
                          color: '#1E263C',
                          lineHeight: 22,
                          fontWeight: Platform.OS === 'ios' ? null : null,
                          fontFamily: 'SFUIText-Semibold',
                        }}
                      >
                        Gender
                      </Text>
                      <View
                        style={{
                          width: '70%',
                          paddingVertical:
                            width === 320 || height === 610.6666666666666
                              ? 20
                              : 26,
                          flexDirection: 'row',
                          justifyContent: 'center',
                          alignSelf: 'flex-start',
                          alignItems: 'center',
                        }}
                      >
                        <Tabs
                          selected={gender}
                          style={{
                            height: wp(13),
                            backgroundColor: '#EDEEF7',
                            width: '100%',
                            borderRadius: 8,
                          }}
                          selectedStyle={{
                            color: gender === '' ? '#8A98AC' : 'white',
                          }}
                          onSelect={(el) => setGender(el.props.name)}
                        >
                          <Text
                            name="0"
                            style={{
                              color: '#8A98AC',
                              fontFamily:
                                gender === '0'
                                  ? 'MPLUS1p-Bold'
                                  : 'MPLUS1p-Medium',
                            }}
                            selectedIconStyle={{
                              backgroundColor:
                                gender === '' ? '#EDEEF7' : '#8A98AC',
                              color: '#8a98ac',
                              borderTopLeftRadius: 8,
                              borderBottomLeftRadius: 8,
                              borderRadius: 8,
                              height: wp(13),
                            }}
                          >
                            Male
                          </Text>

                          <Text
                            name="1"
                            style={{
                              color: '#8A98AC',
                              fontFamily:
                                gender === '1'
                                  ? 'MPLUS1p-Bold'
                                  : 'MPLUS1p-Medium',
                            }}
                            selectedIconStyle={{
                              backgroundColor:
                                gender === '' ? '#EDEEF7' : '#8A98AC',
                              borderTopColor: gender === '' ? null : '#8A98AC',
                              color: '#8a98ac',
                              borderTopRightRadius: 8,
                              borderBottomRightRadius: 8,
                              borderRadius: 8,
                              height: wp(13),
                            }}
                          >
                            Female
                          </Text>
                        </Tabs>
                      </View>
                    </View>
                    <View
                      style={{
                        alignContent: 'flex-start',
                        width: '100%',
                        ...(Platform.OS === 'ios' && {
                          zIndex: 9,
                        }),
                      }}
                    >
                      <Text
                        style={{
                          textAlign: 'left',
                          marginTop: 18,
                          marginBottom: 10,
                          alignContent: 'flex-start',
                          color: '#1E263C',
                          lineHeight: 22,
                          fontWeight: Platform.OS === 'ios' ? null : null,
                          fontFamily: 'SFUIText-Semibold',
                        }}
                      >
                        What is your religion?
                      </Text>
                      <DropDownPicker
                        items={religionOptionsList}
                        dropDownMaxHeight={150}
                        labelStyle={{
                          fontSize: 14,
                          textAlign: 'left',
                          color: Colors.gray['600'],
                        }}
                        dropDownDirection="TOP"
                        defaultValue={religion}
                        containerStyle={{height: 50}}
                        style={{
                          width: '100%',
                          height: wp(13),
                          backgroundColor: '#fff',
                        }}
                        itemStyle={{
                          justifyContent: 'flex-start',
                        }}
                        dropDownStyle={{backgroundColor: '#fff'}}
                        onChangeItem={(item) => setReligion(item.value)}
                      />
                    </View>

                    <View
                      style={{
                        alignContent: 'flex-start',
                        width: '100%',
                        zIndex: 8,
                      }}
                    >
                      <Text
                        style={{
                          textAlign: 'left',
                          marginTop: 18,
                          marginBottom: 10,
                          alignContent: 'flex-start',
                          color: '#1E263C',
                          lineHeight: 22,
                          fontWeight: Platform.OS === 'ios' ? null : null,
                          fontFamily: 'SFUIText-Semibold',
                        }}
                      >
                        Who are you interested in?
                      </Text>
                      <View
                        style={{
                          paddingVertical: 28,
                          width: '100%',
                          flexDirection: 'row',
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}
                      >
                        <Tabs
                          selected={interestedIn}
                          style={{
                            backgroundColor: '#EDEEF7',
                            width: '100%',
                            borderWidth: 1,
                            borderRadius: 8,
                            borderColor: 'white',
                          }}
                          selectedStyle={{color: 'white'}}
                          onSelect={(el) => setInterestedIn(el.props.name)}
                        >
                          <Text
                            name="0"
                            style={{
                              color: '#8A98AC',
                              fontFamily:
                                interestedIn === '0'
                                  ? 'MPLUS1p-Bold'
                                  : 'MPLUS1p-Medium',
                            }}
                            selectedIconStyle={{
                              backgroundColor: '#8A98AC',
                              borderTopColor: '#8A98AC',
                              color: '#8a98ac',
                              borderTopLeftRadius: 8,
                              borderBottomLeftRadius: 8,
                            }}
                          >
                            Male
                          </Text>
                          <Text
                            name="1"
                            style={{
                              color: '#8A98AC',
                              fontFamily:
                                interestedIn === '1'
                                  ? 'MPLUS1p-Bold'
                                  : 'MPLUS1p-Medium',
                            }}
                            selectedIconStyle={{
                              backgroundColor: '#8A98AC',
                              borderTopColor: '#8A98AC',
                              color: '#8a98ac',
                            }}
                          >
                            Female
                          </Text>
                          <Text
                            name="2"
                            style={{
                              color: '#8A98AC',
                              fontFamily:
                                interestedIn === '2'
                                  ? 'MPLUS1p-Bold'
                                  : 'MPLUS1p-Medium',
                            }}
                            selectedIconStyle={{
                              backgroundColor: '#8A98AC',
                              borderTopColor: '#8A98AC',
                              color: '#8a98ac',
                              borderTopRightRadius: 8,
                              borderBottomRightRadius: 8,
                            }}
                          >
                            Both
                          </Text>
                        </Tabs>
                      </View>
                    </View>
                    <View
                      style={{
                        alignContent: 'flex-start',
                        width: '100%',
                        paddingBottom: 150,
                        ...(Platform.OS === 'ios' && {
                          zIndex: 5,
                        }),
                      }}
                    >
                      <Text
                        style={{
                          textAlign: 'left',
                          marginTop: 18,
                          marginBottom: 10,
                          alignContent: 'flex-start',
                          color: '#1E263C',
                          lineHeight: 22,
                          fontWeight: Platform.OS === 'ios' ? null : null,
                          fontFamily: 'SFUIText-Semibold',
                        }}
                      >
                        What type of relationship are you looking for?
                      </Text>

                      <DropDownPicker
                        items={relationshipTypeList}
                        labelStyle={{
                          fontSize: 14,
                          textAlign: 'left',
                          color: Colors.gray['600'],
                        }}
                        dropDownDirection="TOP"
                        onOpen={() => {
                          setBottomHeight(180);
                        }}
                        onClose={() => setBottomHeight(10)}
                        defaultValue={lookingFor}
                        containerStyle={{height: 50}}
                        style={{width: '100%', backgroundColor: '#fff'}}
                        itemStyle={{
                          justifyContent: 'flex-start',
                          borderColor: Colors.gray['600'],
                        }}
                        dropDownStyle={{backgroundColor: '#fff'}}
                        onChangeItem={(item) => setLookingFor(item.value)}
                      />
                    </View>
                  </View>
                )}
              </ScrollView>
            </View>
            <View style={{flex: 1}}>
              <View style={bottomButton.bottomView}>
                {isLoading ? (
                  <ActivityIndicator
                    style={bottomButton.login}
                    size="large"
                    color="#8897AA"
                  />
                ) : (
                  <Button
                    style={bottomButton.login}
                    disabled={!(dateBirthday && fullName && gender)}
                    color="#8897AA"
                    mode="contained"
                    onPress={registerUserBioScreen}
                  >
                    <Text
                      style={{
                        textTransform: 'capitalize',
                        fontWeight: 'bold',
                        fontSize: 18,
                        color: 'white',
                      }}
                    >
                      Next
                    </Text>
                  </Button>
                )}
              </View>
            </View>
          </>
        )}
      </View>
    </SafeAreaView>
  );
}

export function StaarzLogo({goBack}) {
  return (
    <View style={sideLogoStyles.container}>
      <View style={{width: '20%'}}>
        <TouchableOpacity onPress={goBack} style={{width: 15, height: 12}}>
          <Image
            style={{width: 30, height: 15}}
            source={require('../assets/arrow_back.png')}
          />
        </TouchableOpacity>
      </View>
      <View
        style={{
          width: '60%',
          alignItems: 'center',
          flexDirection: 'row',
          justifyContent: 'center',
        }}
      >
        <Image
          source={require('../assets/starzlogo.png')}
          style={sideLogoStyles.imageLogo}
        />
        <Text
          style={{
            fontSize: wp(6),
            fontFamily: 'MPLUS1p-Bold',
            color: '#243443',
            left: -2,
            lineHeight: 33,
            textAlign: 'center',
          }}
        >
          STAARY
        </Text>
      </View>
      <View style={{width: '20%'}} />
    </View>
  );
}

const bottomButton = StyleSheet.create({
  bottomView: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    zIndex: 3,
    alignSelf: 'center',
    bottom: 0,
  },
  login: {
    marginBottom: hp('2'),
    borderRadius: 8,
    width: '100%',
  },
});

const sideLogoStyles = StyleSheet.create({
  image: {
    marginLeft: 20,
  },
  imageLogo: {
    width: wp(7.5),
    height: wp(6.5),
    left: -4,
  },
  container: {
    position: 'absolute',
    top: Platform.OS !== 'ios' ? 0 : 0 + getStatusBarHeight(),
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    backgroundColor: '#fff',
    zIndex: 10,
    width: '100%',
  },
});

const styles = StyleSheet.create({
  forgotPassword: {
    width: '100%',
    alignItems: 'flex-end',
    marginRight: 10,
    marginBottom: 24,
  },
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  forgot: {
    fontSize: 13,
    marginTop: 10,
    marginRight: -2,
    color: theme.colors.secondary,
  },
  rememberMe: {
    fontSize: 13,
    marginTop: 10,
    marginLeft: 10,
    color: theme.colors.secondary,
  },
  link: {
    fontWeight: '100',
    color: '#9872FA', // theme.colors.primary,
    lineHeight: 30,
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    width: '100%',
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'center',
  },
});

export default RegisterUserBioScreen;
