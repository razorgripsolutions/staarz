export {default as StartScreen} from './StartScreen';
export {default as ForgotPasswordScreen} from './ForgotPasswordScreen';
export {default as Dashboard} from './Dashboard';
export {default as LoginScreen} from './LoginScreen';
export {default as RegisterScreen} from './RegisterScreen';
export {default as RegisterUserBioScreen} from './RegisterUserBioScreen';
export {default as socialProfile} from './socialProfile';
export {default as uploadYourPicture} from './uploadYourPicture';
export {default as verifyYourPicture} from './verifyYourPicture';
export {default as PendingForVerification} from './PendingForVerification';
export {default as addPhotoVideo} from './addPhotoVideo';
export {default as TellUsMoreAboutYou1} from './TellUsMoreAboutYou1';
export {default as TellUsMoreAboutYou2} from './TellUsMoreAboutYou2';
export {default as TellUsMoreAboutYou3} from './TellUsMoreAboutYou3';
export {default as BottomTabs} from './BottomTabs';
export {default as ProfileSwipe} from './ProfileSwipe';
export {default as Messages} from './Messages';
export {default as Likes} from './Likes';
export {default as Profile} from './Profile';
export {default as Gaming} from './Gaming';
export {default as demo} from './demo';
export {default as UserDetails} from './UserDetails';

export {default as Setting_Profile} from './Setting_Profile';

export {default as ChooesOption} from './ChooesOption';

export {default as Are_You_Sure} from './Are_You_Sure';

export {default as UnblockuserScreen} from './UnblockuserScreen';

export {default as AddImageScreen} from './AddImageScreen';

export {default as Call_Screen} from './Call_Screen';

// export {default as callkeepscreen} from '../components/callkeepscreen'
export {default as PrivateMessage} from './PrivateMessage';
export {default as voiceCall} from './voiceCall';

export {default as Email_Verify} from './Email_Verify';

export {default as EditProfileScreen} from './EditProfileScreen';

export {default as FullImageScreen} from './FullImageScreen';

export {default as PrivacyPolicy} from './PrivacyPolicy';
