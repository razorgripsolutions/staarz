const baseConfig = require('../../.eslintrc');

module.exports = {
  ...baseConfig,
  root: true,
  env: {
    browser: true,
    'jest/globals': true,
  },
  plugins: [
    'detox',
    'import',
    'react',
    '@typescript-eslint',
    ...baseConfig.plugins,
  ],
  overrides: [
    {
      files: ['**/*.e2e.js'],
      env: {
        'detox/detox': true,
      },
      rules: {
        'import/no-extraneous-dependencies': 'off',
      },
    },
  ],
};
