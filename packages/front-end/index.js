/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
//  import  socialProfile  from './src/screens';

AppRegistry.registerComponent(appName, () => App);
// export default sound;

// error React Native CLI uses autolinking for native dependencies, but the following modules are linked manually:
//   - @react-native-async-storage/async-storage (to unlink run: "react-native unlink @react-native-async-storage/async-storage")
//   - react-native-fbsdk (to unlink run: "react-native unlink react-native-fbsdk")
//   - react-native-image-picker (to unlink run: "react-native unlink react-native-image-picker")
//   - react-native-linear-gradient (to unlink run: "react-native unlink react-native-linear-gradient")
//   - react-native-vector-icons (to unlink run: "react-native unlink react-native-vector-icons")
// This is likely happening when upgrading React Native from below 0.60 to 0.60 or above. Going forward, you can unlink this dependency via "react-native unlink <dependency>" and it will be included in your app automatically. If a library isn't compatible with autolinking, disregard this message and notify the library maintainers.
