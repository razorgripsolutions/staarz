const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
var path = require('path');
// parse requests of content-type - application/json

const app = express();

app.use(bodyParser.json({limit : '500mb' ,extended : true }));

app.use(express.json())

// Add headers
app.use(function (req, res, next) {

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});

app.use(cors());
app.options("*", cors())


// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({  limit : '500mb' ,  extended : true, parameterLimit: 50000  }));

require('./app/routes/auth.routes')(app);
require('./app/routes/inventoryFacilities.routes')(app);
require('./app/routes/items.routes')(app);
require('./app/routes/plantbatches.routes')(app);


app.use(express.static(path.join(__dirname, 'build')));  // Handle React routing, return all requests to React app
  app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname, 'build', 'index.html'));
  });

// set port, listen for requests
const PORT = process.env.PORT || 7070;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
