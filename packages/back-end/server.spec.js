const request = require('supertest');
const {promises} = require('fs');
const path = require('path');
const app = require('./app');

describe('Server sanity', () => {
  // TODO - statics seems like legacy to support a react web client
  test('Root should serve statics (build)', async () => {
    const response = await request(app).get('/');
    const pathToHTML = path.join(__dirname, 'build', 'index.html');
    const htmlFile = (await promises.readFile(pathToHTML)).toString();

    expect(response.statusCode).toBe(200);
    expect(response.text).toBe(htmlFile);
  });
});
