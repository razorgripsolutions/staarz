const mysql = require('mysql2');
const db = require('../config/db.config');

const pool = mysql.createPool({
  multipleStatements: true,
  connectionLimit: 100,
  host: db.HOST,
  user: db.USER,
  password: db.PASSWORD,
  database: db.DB,
  port: 3306,
});

exports.getConnection = () =>
  new Promise((resolve, reject) => {
    pool.getConnection((err, con) => {
      console.log('connection err and ob', err, con);
      if (err) return reject(err);
      return resolve(con);
    });
  });
