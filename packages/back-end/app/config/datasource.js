const {DataSource} = require('typeorm');

function AppDataSource(entities) {
  if (!Array.isArray(entities)) throw Error('Invalid param');
  this.entities = entities;
}

AppDataSource.prototype.initialize = function initialize() {
  const myDataSource = new DataSource({
    type: 'mysql',
    host: process.env.DB_HOST,
    port: 3306,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DEFAULT_DB,
    entities: [...this.entities],
    logging: true,
    synchronize: true,
    legacySpatialSupport: false,
  });

  myDataSource
    .initialize()
    .then(() => {
      console.log('Data Source has been initialized!');
    })
    .catch((err) => {
      console.error('Error during Data Source initialization:', err);
    });
  return myDataSource;
};

module.exports = AppDataSource;
