const stripe = require('stripe')('sk_test_4eC39HqLyjWDarjtT1zdp7dc');
const BaseController = require('../core/controllerBase');

class PaymentController extends BaseController {
  constructor({userRepository}) {
    super();
    this.generatePaymentIntent = this.generatePaymentIntent.bind(this);
    this.generateEphemeraKey = this.generateEphemeraKey.bind(this);

    this.userRepository = userRepository;
  }

  async generateEphemeraKey(req, res) {
    try {
      const {userId} = req;
      const {apiVersion} = req.body;

      const user = await this.userRepository.findOneBy({id: userId});
      const customerId = user.stripeId;

      const ephemeralKey = await stripe.ephemeralKeys.create(
        {customer: customerId},
        {apiVersion},
      );

      this.ok(res, {ephemeralKey});
    } catch (err) {
      this.fail(res, err);
    }
  }

  async generatePaymentIntent(req, res) {
    try {
      const {userId} = req;
      const user = this.userRepository.findOneBy({id: userId});
      const customerId = user.stripeId;

      const paymentIntent = await stripe.paymentIntents.create({
        amount: 1099,
        currency: 'eur',
        customer: customerId,
        automatic_payment_methods: {
          enabled: true,
        },
      });

      this.ok(res, {paymentIntent});
    } catch (err) {
      this.fail(res, err);
    }
  }
}

module.exports = PaymentController;
