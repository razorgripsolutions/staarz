const express = require('express');
const passport = require('passport');
const OnUserCreated = require('./onUserCreated');
const {UserEntity} = require('../auth');

const {EventBus, EventName, authenticateToken} = require('../core');
const PaymentController = require('./paymentController');

const paymentRouter = express.Router();

function PaymentModule() {
  this.init = init;
}

function init({app, dataSource}) {
  const userRepository = dataSource.getRepository(UserEntity);
  const paymentController = new PaymentController({userRepository});

  EventBus.registerForEvent(
    EventName.ON_NEW_CLIENT,
    new OnUserCreated({userRepository}),
  );

  paymentRouter.post(
    '/stripe/ephemeralKey',
    paymentController.generateEphemeraKey,
  );
  paymentRouter.post(
    '/stripe/paymentintent',
    paymentController.generatePaymentIntent,
  );
  app.use('/payments', authenticateToken, paymentRouter);
}

module.exports = PaymentModule;
