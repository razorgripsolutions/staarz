const stripe = require('stripe')('sk_test_4eC39HqLyjWDarjtT1zdp7dc');

function OnUserCreated({userRepository}) {
  return async function createStripeCustomer(customerPayload) {
    try {
      const {userEmail, fullName} = customerPayload;
      const customer = await createCustomer({email: userEmail, name: fullName});
      const userToUpdate = {...customerPayload, stripeId: customer.id};
      await updateUser(userToUpdate);
    } catch (error) {
      console.log(error);
    }
  };

  function createCustomer(customerData) {
    return stripe.customers.create(customerData);
  }

  function updateUser(userToUpdate) {
    return userRepository.update(userToUpdate.id, userToUpdate);
  }
}

module.exports = OnUserCreated;
