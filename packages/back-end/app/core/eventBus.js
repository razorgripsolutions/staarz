const {EventEmitter} = require('events');

const emitter = new EventEmitter();
const EventBus = Object.freeze({
  fireEvent: (eventName, payload) => {
    emitter.emit(eventName, payload);
  },
  registerForEvent: (eventName, handler) => {
    emitter.on(eventName, handler);
  },
});

module.exports = EventBus;
