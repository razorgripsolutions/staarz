const axio = require('axios');

module.exports = class HttpService {
  constructor({baseURL, headers}) {
    this.httpClient = new axio.Axios({baseURL, headers});
  }

  post(url, payload) {
    return this.httpClient.post(url, payload);
  }
};
