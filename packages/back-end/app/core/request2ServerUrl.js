const serverUrl = (req) => `${req.protocol}://${req.get('host')}`;

module.exports = serverUrl;
