class BaseController {
  constructor() {
    if (this.constructor === BaseController) {
      throw new Error("Can't instantiate abstract class!");
    }
  }

  jsonResponse(res, code, message) {
    return res.status(code).json({message});
  }

  ok(res, data) {
    if (data) {
      res.type('application/json');
      return res.status(200).json(data);
    }
    return res.sendStatus(200);
  }

  created(res) {
    return res.sendStatus(201);
  }

  clientError(res, message) {
    return this.jsonResponse(res, 400, message || 'Unauthorized');
  }

  unauthorized(res, message) {
    return this.jsonResponse(res, 401, message || 'Unauthorized');
  }

  notFound(res, message) {
    return this.jsonResponse(res, 404, message || 'Not found');
  }

  fail(res, err) {
    return res.status(500).json({message: err});
  }
}

module.exports = BaseController;
