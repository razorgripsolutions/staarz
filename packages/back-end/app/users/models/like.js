const {EntitySchema} = require('typeorm');

class Likes {
  constructor({likedBy, likeableId, likeableType} = {}) {
    this.likedBy = likedBy;
    this.likeableId = likeableId;
    this.likeableType = likeableType;
  }
}

const LikesEntity = new EntitySchema({
  name: 'Likes',
  target: Likes,
  columns: {
    id: {
      primary: true,
      type: 'int',
      generated: true,
    },
    likedBy: {
      type: 'int',
      nullable: false,
    },
    likeableId: {
      type: 'int',
      nullable: false,
    },
    likeableType: {
      type: 'varchar',
      default: 'PROFILE',
      nullable: false,
    },
  },
});
module.exports = {Likes, LikesEntity};
