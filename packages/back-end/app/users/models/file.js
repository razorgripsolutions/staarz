const {EntitySchema} = require('typeorm');

class File {
  constructor({userId, fileType, fileLocation} = {}) {
    this.userId = userId;
    this.fileType = fileType;
    this.fileLocation = fileLocation;
  }
}

const FileEntity = new EntitySchema({
  name: 'Files',
  target: File,
  columns: {
    id: {
      primary: true,
      type: 'int',
      generated: true,
    },
    userId: {
      type: 'int',
      nullable: false,
    },
    fileType: {
      type: 'varchar',
      nullable: false,
    },
    fileLocation: {
      type: 'varchar',
      nullable: false,
    },
  },
  relations: {},
});
module.exports = {File, FileEntity};
