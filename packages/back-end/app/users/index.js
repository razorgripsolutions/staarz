const {Likes, LikesEntity} = require('./models/like');
const UserModule = require('./userModule');

module.exports = {
  Likes,
  LikesEntity,
  UserModule,
};
