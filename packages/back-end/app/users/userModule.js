const express = require('express');
const UserController = require('./userController');
const {LikesEntity} = require('./models/like');
const {UserEntity} = require('../auth/index');
const {EventBus, EventName, authenticateToken} = require('../core');

const usersRouter = express.Router();

class UserModule {
  init({app, dataSource}) {
    const likesRepository = dataSource.getRepository(LikesEntity);
    const userRepository = dataSource.getRepository(UserEntity);
    const userController = new UserController({
      reopsitory: likesRepository,
      userRepository,
    });

    usersRouter.get('/profile/:profileId', userController.getProfile);
    usersRouter.get('/profile/my/likes', userController.myLikes);
    usersRouter.post('/profile/:profileId/likes', userController.like);
    usersRouter.delete('/profile/:profileId/likes', userController.unLike);
    usersRouter.get('/profile/:profileId/likes', userController.profileLikes);
    usersRouter.post('/files', userController.uploadFiles);

    app.use('/users', authenticateToken, usersRouter);
  }
}

module.exports = UserModule;
