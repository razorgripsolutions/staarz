const BaseController = require('../core/controllerBase');
const {Likes} = require('./models/like');
const {request2ServerUrl} = require('../core');

class UserController extends BaseController {
  constructor({reopsitory, userRepository}) {
    super();
    this.like = this.like.bind(this);
    this.unLike = this.unLike.bind(this);
    this.profileLikes = this.profileLikes.bind(this);
    this.myLikes = this.myLikes.bind(this);
    this.uploadFiles = this.uploadFiles.bind(this);
    this.getProfile = this.getProfile.bind(this);

    this.reopsitory = reopsitory;
    this.userRepository = userRepository;
  }

  async like(req, res) {
    try {
      const {id} = req.user;
      const {profileId} = req.params;
      if (id === profileId) {
        return this.fail(res, 'Cannot like your own profile');
      }

      const like = new Likes({likeableId: profileId, likedBy: id});
      console.log(req.params, 'like');
      await this.reopsitory.save(like);
      await this.ok(res);
    } catch (err) {
      this.fail(res, err);
    }
  }

  async getProfile(req, res) {
    try {
      const {profileId} = req.params;
      const profile = await this.userRepository
        .createQueryBuilder('user')
        .where({id: profileId})
        .select([
          'emailAddress AS emailAddress',
          'id AS id',
          'profilePicture AS profilePicture',
          'fullName AS fullName',
        ])
        .getRawOne(); 
      console.log(profile);
      this.ok(res, profile);
    } catch (err) {
      this.fail(res, err);
    }
  }

  async profileLikes(req, res) {
    try {
      const {profileId} = req.params;
      const users = await this.reopsitory
        .createQueryBuilder('likes')
        .innerJoin('user', 'u', 'likes.likedBy = u.id')
        .where({likeableId: profileId})
        .select([
          'u.emailAddress AS emailAddress',
          'u.id AS id',
          'u.profilePicture AS profilePicture',
          'u.fullName AS fullName',
        ])
        .execute();
      this.ok(res, users);
    } catch (err) {
      console.log(err);
      this.fail(res, err);
    }
  }

  async myLikes(req, res) {
    const {id} = req.user;
    console.log(id, 'id');
    const users = await this.reopsitory
      .createQueryBuilder('likes')
      .innerJoin('user', 'u', 'likes.likeableId = u.id')
      .where({likedBy: id})
      .select([
        'u.emailAddress AS emailAddress',
        'u.id AS id',
        'u.profilePicture AS profilePicture',
        'u.fullName AS fullName',
      ])
      .execute();

    await this.ok(res, users);
  }

  async unLike(req, res) {
    try {
      const {id} = req.user;
      const {profileId} = req.params;
      console.log(id, 'id');
      await this.reopsitory
        .createQueryBuilder('likes')
        .delete()
        .where({likedBy: id, likeableId: profileId})
        .execute();
      this.ok(res);
    } catch (err) {
      console.log(err);
      this.fail(res, err);
    }
  }

  async uploadFiles(req, res) {
    if (!req.files) {
      await this.clientError(res, 'No files were uploaded.');
      return;
    }

    const serverUrl = request2ServerUrl(req);
    const uploadPath = `${process.env.PWD}/public/uploads`;
    Object.keys(req.files).forEach((key) => {
      const file = req.files[key];
      const filePath = `${uploadPath}/${file.name}`;
      file.mv(filePath, async (err) => {
        if (err) {
          await this.fail(res, err);
        }
      });
    });

    await this.ok(res);
  }
}

module.exports = UserController;
