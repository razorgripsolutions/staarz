const {EventName, EventBus} = require('../core');

function AuthService({userRepository}) {
  this.createOrGetUser = async (userData) => {
    const filter = userData.facebookID
      ? {facebookID: userData.facebookID}
      : {googleID: userData.googleID};

    const user = await getUser(filter);
    if (user) return user;
    return createUser(userData);
  };

  this.createUser = (userData) => createUser(userData);
  this.getUser = (filter) => getUser(filter);

  function getUser(filter) {
    return userRepository.findOneBy(filter);
  }

  function createUser(userData) {
    return new Promise((resolve, reject) => {
      userRepository
        .save(userData)
        .then((savedUser) => {
          EventBus.fireEvent(EventName.ON_NEW_CLIENT, savedUser);
          resolve(savedUser);
        })
        .catch((err) => {
          reject(err);
        });
    });
  }
}

module.exports = AuthService;
