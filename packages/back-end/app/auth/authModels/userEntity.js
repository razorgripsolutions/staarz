const {EntitySchema} = require('typeorm');

class User {
  constructor({
    emailAddress,
    facebookID,
    emailVerified,
    fullName,
    profilePicture,
    facebookToken,
    fbAuthStatus,
    googleID,
    googleAuthStatus,
    googleToken,
    dob,
    location,
    gender,
    religion,
  } = {}) {
    this.emailAddress = emailAddress;
    this.facebookID = facebookID;
    this.emailVerified = emailVerified;
    this.fullName = fullName;
    this.profilePicture = profilePicture;
    this.facebookToken = facebookToken;
    this.fbAuthStatus = fbAuthStatus;
    this.googleID = googleID;
    this.googleAuthStatus = googleAuthStatus;
    this.googleToken = googleToken;
    this.dob = dob;
    this.location = location;
    this.religion = religion;
    this.gender = gender;
  }
}

const UserEntity = new EntitySchema({
  name: 'User',
  target: User,
  columns: {
    id: {
      primary: true,
      type: 'int',
      generated: true,
    },
    emailAddress: {
      type: 'varchar',
      nullable: false,
      unique: true,
    },
    password: {
      type: 'text',
      nullable: true,
    },
    facebookID: {
      type: 'varchar',
      nullable: true,
    },
    emailVerified: {
      type: 'bool',
      default: false,
      nullable: false,
    },
    fullName: {
      type: 'varchar',
      nullable: false,
    },
    dob: {
      type: 'date',
      nullable: true,
    },
    gender: {
      type: 'varchar',
      nullable: true,
    },
    religion: {
      type: 'varchar',
      nullable: true,
    },
    location: {
      type: 'point',
      nullable: true,
      spatialFeatureType: 'Point',
      srid: 4326
    },
    profilePicture: {
      type: 'text',
      nullable: true,
    },
    facebookToken: {
      type: 'text',
      nullable: true,
    },
    fbAuthStatus: {
      type: 'int',
      default: 0,
      nullable: false,
    },
    googleToken: {
      type: 'text',
      nullable: true,
    },
    googleID: {
      type: 'varchar',
      nullable: true,
    },
    googleAuthStatus: {
      type: 'int',
      default: 0,
      nullable: false,
    },
    stripeId: {
      type: 'varchar',
      nullable: true,
    },
  },
});
module.exports = {User, UserEntity};
