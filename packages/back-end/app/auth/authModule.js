const express = require('express');
const passport = require('passport');

const authRouter = express.Router();
const AuthController = require('./authController');
const AuthService = require('./authService');
const facebookStrategy = require('./facebookPassportStretagy');
const googleStrategy = require('./googlePassportStretagy');
const {UserEntity} = require('./authModels/userEntity');
const PasswordService = require('./passwordService');

function AuthModule() {
  this.init = init;
}

function init({app, dataSource}) {
  const userRepository = dataSource.getRepository(UserEntity);
  const authService = new AuthService({userRepository});
  const authController = new AuthController({
    userRepository,
    userService: authService,
    passwordService: new PasswordService(),
  });

  facebookStrategy(authService);
  googleStrategy(authService);

  authRouter.post(
    '/facebook/token',
    passport.authenticate('facebook-token'),
    authController.facebookAuth,
  );

  authRouter.post(
    '/google/token',
    passport.authenticate('google-token'),
    authController.googleAuth,
  );

  authRouter.post('/signup', authController.signUp);
  authRouter.post('/signin', authController.signIn);
  app.use('/auth', authRouter);
}

module.exports = AuthModule;
