const passport = require('passport');
const FacebookTokenStrategy = require('passport-facebook-token');
const {User} = require('./authModels/userEntity');

function facebookStrategy(authService) {
  passport.use(
    new FacebookTokenStrategy(
      {
        clientID: process.env.FACEBOOK_CLIENT_ID,
        clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
        fbGraphVersion: 'v13.0',
      },
      async (accessToken, refreshToken, profile, done) => {
        const {id, name, email} = profile._json;
        const userModel = new User({
          facebookID: id,
          facebookToken: accessToken,
          fbAuthStatus: 1,
          fullName: name,
          emailAddress: email,
          emailVerified: true,
          profilePicture: profile.photos[0].value,
        });

        const user = await authService.createOrGetUser(userModel);
        done(null, user);
      },
    ),
  );
}

module.exports = facebookStrategy;
