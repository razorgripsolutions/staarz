const bcrypt = require('bcrypt');

const saltRounds = 10;

class PasswordService {
  hashPassword(plainPassword) {
    return new Promise((resolve, reject) => {
      bcrypt.hash(plainPassword, saltRounds, (err, hash) => {
        if (err) {
          reject(err);
        } else {
          resolve(hash);
        }
      });
    });
  }

  comparePassword(plainPassword, hash) {
    console.log(plainPassword, hash);
    return new Promise((resolve, reject) => {
      bcrypt.compare(plainPassword, hash, (err, result) => {
        if (err || !result) {
          console.log(err);
          reject(err);
        } else {
          resolve();
        }
        // result == true
      });
    });
  }
}

module.exports = PasswordService;
