const {User, UserEntity} = require('./authModels/userEntity');
const AuthModule = require('./authModule');

module.exports = {
  User,
  UserEntity,
  AuthModule,
};
