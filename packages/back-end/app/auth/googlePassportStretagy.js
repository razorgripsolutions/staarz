const passport = require('passport');
const GoogleTokenStrategy = require('passport-google-token').Strategy;
const {User} = require('./authModels/userEntity');

function googleStrategy(authService) {
  passport.use(
    new GoogleTokenStrategy(
      {
        clientID: process.env.GOOGLE_CLIENT_ID,
        clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      },
      async (accessToken, refreshToken, profile, done) => {
        const {id, name, email, verified_email} = profile._json;
        const userModel = new User({
          googleID: id,
          googleToken: accessToken,
          googleAuthStatus: 1,
          fullName: name,
          emailAddress: email,
          emailVerified: verified_email,
          profilePicture: profile.picture,
        });
        const user = await authService.createOrGetUser(userModel);
        done(null, user);
      },
    ),
  );
}

module.exports = googleStrategy;
