const jwt = require('jsonwebtoken');
const BaseController = require('../core/controllerBase');

function generateAccessToken(user) {
  return jwt.sign({...user}, process.env.TOKEN_SECRET, {
    expiresIn: process.env.TOKEN_EXPIRATION,
  });
}

function getUser(user) {
  const {emailAddress, id, fullName} = user;
  return {
    emailAddress,
    id,
    fullName,
  };
}
class AuthController extends BaseController {
  constructor({userRepository, userService, passwordService}) {
    super();
    this.facebookAuth = this.facebookAuth.bind(this);
    this.googleAuth = this.googleAuth.bind(this);
    this.signUp = this.signUp.bind(this);
    this.signIn = this.signIn.bind(this);

    this.userRepository = userRepository;
    this.userService = userService;
    this.passwordService = passwordService;
  }

  facebookAuth(req, res) {
    console.log(req.user);
    const payload = {access_token: generateAccessToken(getUser(req.user))};
    this.ok(res, payload);
  }

  googleAuth(req, res) {
    const payload = {access_token: generateAccessToken(getUser(req.user))};
    this.ok(res, payload);
  }

  async signIn(req, res) {
    try {
      const {emailAddress, password} = req.body;
      const filter = {emailAddress};

      const user = await this.userService.getUser(filter);
      console.log(emailAddress, password, user);
      if (!user) {
        return this.notFound(res, 'Invalid username or password');
      }
      await this.passwordService.comparePassword(password, user.password);

      const payload = {access_token: generateAccessToken(getUser(user))};
      return this.ok(res, payload);
    } catch (err) {
      console.log(err);
      return this.notFound(res, 'Invalid username or password');
    }
  }

  async signUp(req, res) {
    try {
      const {body} = req;
      const {password, location} = body;
      const hashPassword = await this.passwordService.hashPassword(password);
      const userData = {...body, password: hashPassword};
      if (location) {
        const lat = location[0];
        const lng = location[1];
        userData.location = `POINT(${lat}  ${lng})`;
      }
      const createdUser = await this.userService.createUser(userData);
      delete createdUser.password;
      this.ok(res, createdUser);
    } catch (err) {
      this.fail(res, 'error');
    }
  }
}

module.exports = AuthController;
