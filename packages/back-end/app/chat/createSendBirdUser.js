module.exports = function createSendBirdUser({httpClient}) {
  return function (user) {
    const payload = {
      user_id: user.id,
      nickname: user.fullName,
      profile_url: user.profilePicture,
      issue_access_token: false,
    };
    httpClient
      .post('/users', JSON.stringify(payload))
      .then((result) => {
        console.log(result.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };
};
