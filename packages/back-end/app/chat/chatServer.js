const createSendBirdUser = require('./createSendBirdUser');
const {EventBus, EventName} = require('../core');
const {UserEntity} = require('../auth');

function ChatModule() {
  this.init = init;
}

function init({HttpService, dataSource}) {
  const userRepository = dataSource.getRepository(UserEntity);

  const headers = {
    'Content-Type': 'application/json',
    charset: 'utf8',
    'Api-Token': process.env.SENDBIRD_API_TOKEN,
  };

  const httpClient = new HttpService({
    baseURL: process.env.SENDBIRD_API_URL,
    headers,
  });

  EventBus.registerForEvent(
    EventName.ON_NEW_CLIENT,
    createSendBirdUser({httpClient, userRepository}),
  );
}

module.exports = ChatModule;
