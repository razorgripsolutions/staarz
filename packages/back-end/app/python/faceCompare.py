import face_recognition
import sys

baseImage = sys.argv[1]
uploadedImage = sys.argv[2]
# print(baseImage, uploadedImage)

picture_of_me = face_recognition.load_image_file(baseImage)
my_face_encoding = face_recognition.face_encodings(picture_of_me)[0]

# my_face_encoding now contains a universal 'encoding' of my facial features that can be compared to any other picture of a face!

unknown_picture = face_recognition.load_image_file(uploadedImage)
unknown_face_encoding = face_recognition.face_encodings(unknown_picture)[0]

# Now we can see the two face encodings are of the same person with `compare_faces`!

results = face_recognition.compare_faces([my_face_encoding], unknown_face_encoding)

if results[0] == True:
    print(1)
else:
    print(0)


# import face_recognition
#
# picture_of_me = face_recognition.load_image_file("1.jpg")
# my_face_encoding = face_recognition.face_encodings(picture_of_me)[0]
#
# # my_face_encoding now contains a universal 'encoding' of my facial features that can be compared to any other picture of a face!
#
# unknown_picture = face_recognition.load_image_file("2.jpg")
# unknown_face_encoding = face_recognition.face_encodings(unknown_picture)[0]
#
# # Now we can see the two face encodings are of the same person with `compare_faces`!
#
# results = face_recognition.compare_faces([my_face_encoding], unknown_face_encoding)
#
# if results[0] == True:
#     print("It's a picture of me!")
# else:
#     print("It's not a picture of me!")