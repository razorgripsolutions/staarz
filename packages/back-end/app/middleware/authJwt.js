const jwt = require('jsonwebtoken');
const config = require('../config/auth.config');

const verifyToken = (req, res, next) => {
  const token = req.headers.authorization;

  if (!token) {
    return res.status(403).send({
      message: 'No token provided!',
    });
  }

  // let token = token.split(" ")[1];

  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) {
      return res.status(401).send({
        message: 'Unauthorized!',
      });
    }
    req.userId = decoded.id;
    next();
  });
};

const isAdmin = (req, res, next) => {
  next();
};

const isSeller = (req, res, next) => {
  next();
};

const isBuyer = (req, res, next) => {
  next();
};

const authJwt = {
  verifyToken,
  isAdmin,
  isBuyer,
  isSeller,
};

module.exports = authJwt;
