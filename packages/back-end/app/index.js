const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');

const app = express();

app.use(bodyParser.json({limit: '500mb', extended: true}));

app.use(express.json());

// Add headers
app.use((req, res, next) => {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');

  // Request methods you wish to allow
  res.setHeader(
    'Access-Control-Allow-Methods',
    'GET, POST, OPTIONS, PUT, PATCH, DELETE',
  );

  // Request headers you wish to allow
  res.setHeader(
    'Access-Control-Allow-Headers',
    'X-Requested-With,content-type',
  );

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});

app.use(cors());
app.options('*', cors());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(
  bodyParser.urlencoded({
    limit: '500mb',
    extended: true,
    parameterLimit: 50000,
  }),
);

require('./routes/auth.routes')(app);
require('./routes/inventoryFacilities.routes')(app);
require('./routes/items.routes')(app);
require('./routes/plantbatches.routes')(app);

const staticsPath = path.join(__dirname, '..', 'build');

app.use(express.static(staticsPath)); // Handle React routing, return all requests to React app
app.get('*', (req, res) => {
  res.sendFile(path.join(staticsPath, 'index.html'));
});

module.exports = app;
