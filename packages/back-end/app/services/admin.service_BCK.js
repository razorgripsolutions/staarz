const db = require('../db/db');

exports.findUserByEmail = async (email) => {
  // console.log( "hhhhh");
  const connection = await db.getConnection();

  return new Promise((resolve, reject) => {
    connection.query(
      `SELECT * FROM adminUsers WHERE adminEmail = ?`,
      [email],
      (err, result) => {
        connection.release();
        // console.log(JSON.stringify(result) + "@@@@@");
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.findReligionById = async (email) => {
  // console.log( "hhhhh");
  const connection = await db.getConnection();

  return new Promise((resolve, reject) => {
    connection.query(
      `SELECT * FROM religionList WHERE religionName = ?`,
      [email],
      (err, result) => {
        connection.release();
        // console.log(JSON.stringify(result) + "@@@@@");
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.adminSignUp = async (body) => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(
      `INSERT INTO adminUsers( adminEmail, adminPassword, status, userRole) VALUES(?,?,1,1)`,
      [body.adminEmail, body.adminPassword, 1, 1],
      (err, result) => {
        connection.release();
        if (err) return reject(err);

        // console.log("user registered --> "+ body.adminEmail);
        return resolve(result);
      },
    );
  });
};

exports.updateUserById = async (req) => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(
      `UPDATE users SET vendorKey= ?, userKey= ? where userId = ?`,
      [req.body.username, req.body.password, req.body.userId],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.updateReligionList = async (req) => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(
      `INSERT INTO religionList(religionName) VALUES(?)`,
      [req.religionName],
      (err, result) => {
        connection.release();
        // console.log(err, result);
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};
exports.deleteFromReligionList = async (req) => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(
      `DELETE FROM religionList WHERE religionID = ?`,
      [req.religionID],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.fetchReligionList = async () => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(`SELECT * FROM religionList`, (err, result) => {
      connection.release();
      if (err) return reject(err);
      return resolve(result);
    });
  });
};

exports.fetchInterestList = async () => {
  const connection = await db.getConnection();
  const resultObj = {};
  return new Promise((resolve, reject) => {
    connection.query(`SELECT * FROM 	interests`, (err, result) => {
      result.forEach((element) => {
        resultObj[element.interestsName] = element.interestsID;
      });
      connection.release();
      if (err) return reject(err);
      return resolve(resultObj);
    });
  });
};

exports.updateInterestList = async (req) => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(
      `INSERT INTO interests(interestsName) VALUES(?)`,
      [req.interestsName],
      (err, result) => {
        connection.release();
        // console.log(err, result);
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.deleteFromInterestList = async (req) => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(
      `DELETE FROM interests WHERE interestsID = ?`,
      [req.interestsID],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.favouriteMusicStyleList = async () => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(`SELECT * FROM allMusicList`, (err, result) => {
      connection.release();
      if (err) return reject(err);

      const data = result.map((rawData) => ({
        label: rawData.musicName,
        value: rawData.musicID,
        singer: rawData.singerName,
      }));
      return resolve(data);
    });
  });
};

exports.fetchTravelStyle = async () => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(`SELECT * FROM allTravelStyleList`, (err, result) => {
      connection.release();
      if (err) return reject(err);

      const data = result.map((rawData) => ({
        label: rawData.travelStyleName,
        value: rawData.travelStyleID,
      }));
      return resolve(data);
    });
  });
};

exports.fetchUserEducationList = async () => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(`SELECT * FROM allEducationList`, (err, result) => {
      connection.release();
      if (err) return reject(err);
      return resolve(result);
    });
  });
};

exports.fetchfavouriteMovieGenre = async () => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(`SELECT * FROM allMovieList`, (err, result) => {
      connection.release();
      if (err) return reject(err);

      const data = result.map((rawData) => ({
        label: rawData.movieName,
        value: rawData.movieID,
      }));
      return resolve(data);
    });
  });
};

exports.fetchRelationShipType = async () => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(`SELECT * FROM relationShipType`, (err, result) => {
      connection.release();
      if (err) return reject(err);

      console.log(JSON.stringify(result));
      const data = result.map((rawData) => ({
        label: rawData.relationShipName,
        value: rawData.relationShipID,
      }));
      return resolve(data);
    });
  });
};

exports.fetchReligion = async () => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(`SELECT * FROM religionList`, (err, result) => {
      connection.release();
      if (err) return reject(err);

      // console.log(JSON.stringify(result))
      const data = result.map((rawData) => ({
        label: rawData.religionName,
        value: rawData.religionID,
      }));
      return resolve(data);
    });
  });
};

exports.fetchUserPetList = async () => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(`SELECT * FROM allPetList`, (err, result) => {
      connection.release();
      if (err) return reject(err);

      // console.log(JSON.stringify(result))
      const data = result.map((rawData) => ({
        label: rawData.petType,
        value: rawData.petID,
        totalPet: rawData.totalPets,
      }));
      return resolve(data);
    });
  });
};

exports.fetchUserOccupationList = async () => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(`SELECT * FROM userOccupationList`, (err, result) => {
      connection.release();
      if (err) return reject(err);

      // console.log(JSON.stringify(result))
      const data = result.map((rawData) => ({
        label: rawData.occupationName,
        value: rawData.occupationID,
      }));
      return resolve(data);
    });
  });
};

exports.fetchAllUser = async () => {
  const connection = await db.getConnection();

  return new Promise((resolve, reject) => {
    connection.query(`SELECT * FROM users`, (err, result) => {
      connection.release();
      // console.log(result);
      if (err) return reject(err);
      return resolve(result);
    });
  });
};

exports.UpdateEducationList = async (data) => {
  const connection = await db.getConnection();

  return new Promise((resolve, reject) => {
    connection.query(
      `INSERT INTO allEducationList(educationName,universityName,universityLocation,universityLat,universityLong) VALUES(?,?,?,1,1) `,
      [data.educationName, data.universityName, data.universityLocation],
      (err, result) => {
        connection.release();
        // console.log(result);
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.deleteEducationList = async (req) => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(
      `DELETE FROM allEducationList WHERE educationID = ?`,
      [req.educationID],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.fetchEducationByID = async (ID) => {
  // console.log( "hhhhh");
  const connection = await db.getConnection();

  return new Promise((resolve, reject) => {
    connection.query(
      `SELECT * FROM allEducationList WHERE educationID = ?`,
      [ID],
      (err, result) => {
        connection.release();
        // console.log(JSON.stringify(result) + "@@@@@");
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.deleteFromPetList = async (req) => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(
      `DELETE FROM allPetList WHERE petType = ?`,
      [req.petType],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};
exports.UpdateUserPetList = async (data) => {
  const connection = await db.getConnection();

  return new Promise((resolve, reject) => {
    connection.query(
      `INSERT INTO allPetList(	petID,petType,totalPets) VALUES(?,?,?) `,
      [data.petID, data.petType, data.totalPets],
      (err, result) => {
        connection.release();
        // console.log(result);
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.updateUserOccupationList = async (data) => {
  const connection = await db.getConnection();

  return new Promise((resolve, reject) => {
    connection.query(
      `INSERT INTO userOccupationList(occupationName) VALUES(?) `,
      [data.occupationName],
      (err, result) => {
        connection.release();
        // console.log(result);
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.deleteUserOccupationList = async (req) => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    console.log(req);
    connection.query(
      `DELETE FROM userOccupationList WHERE occupationID = ?`,
      [req.occupationID],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.findPetById = async (ID) => {
  // console.log( "hhhhh");
  const connection = await db.getConnection();

  return new Promise((resolve, reject) => {
    connection.query(
      `SELECT * FROM allPetList WHERE petType = ?`,
      [ID],
      (err, result) => {
        connection.release();
        // console.log(JSON.stringify(result) + "@@@@@");
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.UserOccupationListByID = async (ID) => {
  // console.log( "hhhhh");
  const connection = await db.getConnection();

  return new Promise((resolve, reject) => {
    connection.query(
      `SELECT * FROM userOccupationList WHERE occupationName = ?`,
      [ID],
      (err, result) => {
        connection.release();
        // console.log(JSON.stringify(result) + "@@@@@");
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.fetchallHobbyList = async () => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(`SELECT * FROM allHobbiesList`, (err, result) => {
      connection.release();
      if (err) return reject(err);

      // console.log(JSON.stringify(result))
      const data = result.map((rawData) => ({
        label: rawData.hobbyID,
        value: rawData.hobbyName,
      }));
      return resolve(data);
    });
  });
};

exports.updateallHobbyList = async (data) => {
  const connection = await db.getConnection();

  return new Promise((resolve, reject) => {
    connection.query(
      `INSERT INTO allHobbiesList(hobbyName) VALUES(?) `,
      [data.hobbyName],
      (err, result) => {
        connection.release();
        // console.log(result);
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};
exports.fetchInterestById = async (ID) => {
  // console.log( "hhhhh");
  const connection = await db.getConnection();

  return new Promise((resolve, reject) => {
    connection.query(
      `SELECT * FROM interests WHERE interestsName = ?`,
      [ID],
      (err, result) => {
        connection.release();
        // console.log(JSON.stringify(result) + "@@@@@");
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.updatePetList = async (req) => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(
      `INSERT INTO allPetList(petType,totalPets) VALUES(?,?)`,
      [req.petType, req.totalPets],
      (err, result) => {
        connection.release();
        // console.log(err, result);
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.deleteallHobbyList = async (req) => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    console.log(req);
    connection.query(
      `DELETE FROM allHobbiesList WHERE hobbyName = ?`,
      [req],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.findHobbyByName = async (ID) => {
  // console.log( "hhhhh");
  const connection = await db.getConnection();

  return new Promise((resolve, reject) => {
    connection.query(
      `SELECT * FROM allHobbiesList WHERE hobbyName = ?`,
      [ID],
      (err, result) => {
        connection.release();
        // console.log(JSON.stringify(result) + "@@@@@");
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.fetchallMovieList = async () => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(`SELECT * FROM allMovieList`, (err, result) => {
      connection.release();
      if (err) return reject(err);

      // console.log(JSON.stringify(result))
      const data = result.map((rawData) => ({
        label: rawData.movieID,
        value: rawData.movieName,
      }));
      return resolve(data);
    });
  });
};

exports.addAllMovieList = async (req) => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(
      `INSERT INTO allMovieList(movieName) VALUES(?)`,
      [req.movieName],
      (err, result) => {
        connection.release();
        // console.log(err, result);
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.findMovieByName = async (ID) => {
  // console.log( "hhhhh");
  const connection = await db.getConnection();

  return new Promise((resolve, reject) => {
    connection.query(
      `SELECT * FROM allMovieList WHERE movieName = ?`,
      [ID],
      (err, result) => {
        connection.release();
        // console.log(JSON.stringify(result) + "@@@@@");
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.deleteAllMovieList = async (req) => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(
      `DELETE FROM allMovieList WHERE movieName = ?`,
      [req.movieName],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.fetchallMusicList = async () => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(`SELECT * FROM allMusicList`, (err, result) => {
      connection.release();
      if (err) return reject(err);

      // console.log(JSON.stringify(result))
      const data = result.map((rawData) => ({
        label: rawData.musicID,
        value: rawData.musicName,
      }));
      return resolve(data);
    });
  });
};

exports.addAllMusicList = async (req) => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(
      `INSERT INTO allMusicList(musicName) VALUES(?)`,
      [req.musicName],
      (err, result) => {
        connection.release();
        // console.log(err, result);
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.findMusicByName = async (email) => {
  // console.log( "hhhhh");
  const connection = await db.getConnection();

  return new Promise((resolve, reject) => {
    connection.query(
      `SELECT * FROM allMusicList WHERE musicName = ?`,
      [email],
      (err, result) => {
        connection.release();
        // console.log(JSON.stringify(result) + "@@@@@");
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.deleteAllMusicList = async (req) => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(
      `DELETE FROM allMusicList WHERE musicName = ?`,
      [req.musicName],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.fetchAllTravelStyleList = async () => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(`SELECT * FROM allTravelStyleList`, (err, result) => {
      connection.release();
      if (err) return reject(err);

      // console.log(JSON.stringify(result))
      const data = result.map((rawData) => ({
        label: rawData.travelStyleID,
        value: rawData.travelStyleName,
      }));
      return resolve(data);
    });
  });
};

exports.fetchEducationByID = async (ID) => {
  // console.log( "hhhhh");
  const connection = await db.getConnection();

  return new Promise((resolve, reject) => {
    connection.query(
      `SELECT * FROM allEducationList WHERE educationID = ?`,
      [ID],
      (err, result) => {
        connection.release();
        // console.log(JSON.stringify(result) + "@@@@@");
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.addAllTravelStyleList = async (req) => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(
      `INSERT INTO allTravelStyleList(travelStyleName) VALUES(?)`,
      [req.travelStyleName],
      (err, result) => {
        connection.release();
        // console.log(err, result);
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.findTravelStyleByName = async (email) => {
  // console.log( "hhhhh");
  const connection = await db.getConnection();

  return new Promise((resolve, reject) => {
    connection.query(
      `SELECT * FROM allTravelStyleList WHERE travelStyleName = ?`,
      [email],
      (err, result) => {
        connection.release();
        // console.log(JSON.stringify(result) + "@@@@@");
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.deleteAllTravelStyleList = async (req) => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(
      `DELETE FROM allTravelStyleList WHERE travelStyleName = ?`,
      [req.travelStyleName],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.fetchJobsList = async () => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(`SELECT * FROM allJobsList`, (err, result) => {
      connection.release();
      if (err) return reject(err);
      return resolve(result);
    });
  });
};

exports.addJobsList = async (data) => {
  const connection = await db.getConnection();

  return new Promise((resolve, reject) => {
    connection.query(
      `INSERT INTO allJobsList(jobName,jobCompanyName,jobLocation,jobLat,jobLong) VALUES(?,?,?,1,1) `,
      [data.jobName, data.jobCompanyName, data.jobLocation],
      (err, result) => {
        connection.release();
        // console.log(result);
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.deleteJobsList = async (req) => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(
      `DELETE FROM allJobsList WHERE jobID = ?`,
      [req.jobID],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};
