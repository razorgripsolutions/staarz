const db = require('../db/db');

exports.savePlantBatches = async (userId, licenseNumber, data) => {
  const connection = await db.getConnection();

  return new Promise((resolve, reject) => {
    connection.query(
      `INSERT INTO inventoryPlantBatches(userID, id, name, type, locationId, locationName, 
            locationTypeName, strainId, strainName, patientLicenseNumber, untrackedCount, trackedCount, packagedCount, 
            harvestedCount, destroyedCount, sourcePackageId, sourcePackageLabel, sourcePlantId, sourcePlantLabel, 
            sourcePlantBatchId, sourcePlantBatchName, plantedDate, lastModified, licenseNumber) VALUES (?, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
      [
        userId,
        data.Id,
        data.Name,
        data.Type,
        data.LocationId,
        data.LocationName,
        data.LocationTypeName,
        data.StrainId,
        data.StrainName,
        data.PatientLicenseNumber,
        data.UntrackedCount,
        data.TrackedCount,
        data.PackagedCount,
        data.HarvestedCount,
        data.DestroyedCount,
        data.SourcePackageId,
        data.SourcePackageLabel,
        data.SourcePlantId,
        data.SourcePlantLabel,
        data.SourcePlantBatchId,
        data.SourcePlantBatchName,
        data.PlantedDate,
        data.LastModified,
        licenseNumber,
      ],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.getPlantBatches = async (userId) => {
  const connection = await db.getConnection();

  return new Promise((resolve, reject) => {
    connection.query(
      `SELECT * FROM inventoryPlantBatches WHERE userID=?`,
      [userId],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.deletePlantBatchesByLicenseNumber = async (licenseNumber) => {
  const connection = await db.getConnection();

  return new Promise((resolve, reject) => {
    connection.query(
      `DELETE FROM inventoryPlantBatches WHERE licenseNumber=?`,
      [licenseNumber],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};
