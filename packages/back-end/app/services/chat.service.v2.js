const RocketChatApi = require('rocketchat-api');
const request = require('axios');
const rocketChatConfig = require('../config/rocketChat.config');

// const { default: logger } = require('../core/logger/app-logger');
// const server = '65.0.11.166'
// require('dotenv').config();
const server = rocketChatConfig.ROCKETCHAT_HOST;
const port = rocketChatConfig.ROCKETCHAT_PORT;
const protocol = rocketChatConfig.ROCKETCHAT_PROTOCOL;
// const adminClient = getNewClient();
const rc_user = rocketChatConfig.ROCKETCHAT_USER;

// function getNewClient() {

// const adminClient =  new RocketChatApi(protocol, server, port,rocketChatConfig.ROCKETCHAT_ADMIN_USER,rocketChatConfig.ROCKETCHAT_ADMIN_PASS, (err, responseBody, self)=>{
//     if(err)
//       return console.error('Error connecting to RocketChat', err)
//     console.info('RocketChat client connect')
//     // self refers to an instance of rocketChatClient, useful if you need subsequent calls
//     // responseBody is an object containing authToken and userId
//   });
// }
let adminClient;

/**
 * "Lazy" load rocket chat api
 */
const getRocketChatAdmin = () => {
  if (!adminClient) {
    adminClient = RocketChatApi(
      'https',
      'tectum.rocket.chat',
      443,
      'arvind@tectumtechnologies.com',
      '!23Qwe,./',
      (err, responseBody, self) => {
        if (err) return console.log('Error connecting to RocketChat', err);
        console.log('RocketChat client connect');
        // self refers to an instance of rocketChatClient, useful if you need subsequent calls
        // responseBody is an object containing authToken and userId
      },
    );
  }
  return adminClient;
};

/**
 * initialize admin client
 */
const initAdmin = () =>
  getRocketChatAdmin()
    .login(
      rocketChatConfig.ROCKETCHAT_ADMIN_USER,
      rocketChatConfig.ROCKETCHAT_ADMIN_PASS,
    )
    .then((body, self) => {
      // self is an instance of rocketChatClient,useful if you need subsequent calls
      console.log(body);
      // body is an object containing authToken and userId
    })
    .catch((err) => {
      console.log(err);
    });

/**
 *
 * @param {string} name
 * @param {string} email
 * @param {string} username
 */
const createUser = async (name, email, username) => {
  console.log('@@@@@@@@@@@@@@@@@');
  console.log(name, email, username);
  return new Promise((resolve, reject) => {
    const data = {
      name,
      email,
      password: 'password',
      username /** username should not be same as email */,
      verified: true,
      sendWelcomeEmail: false,
      joinDefaultChannels: false,
      requirePasswordChange: false,
      roles: ['user'],
    };

    getRocketChatAdmin().users.create(data, (err, body) => {
      if (err) {
        console.log(err);
        reject(err);
      } else {
        const newUserInfoObject = {
          id: body.user._id,
          username: body.user.username,
          name: body.user.name,
        };
        resolve(newUserInfoObject);
      }
    });
  });
};

/**
 * Create a new user in bulk. Requires create-user permission.
 *
 * @param {[{id:number,email:string}]} newuserInfo
 */
const createUserBulk = async (newuserInfo) =>
  new Promise((resolve, reject) => {
    const createdUsers = [];
    const counter = 0;
    console.info(JSON.stringify(newuserInfo));
    newuserInfo.forEach((elem) => {
      const data = {
        name: elem.email.split('@')[0],
        email: elem.email,
        password: 'password',
        username:
          elem.id +
          rocketChatConfig.ROCKETCHAT_USER /** username should not be same as email */,
        verified: true,
        sendWelcomeEmail: false,
        joinDefaultChannels: false,
        requirePasswordChange: false,
        roles: ['user'],
      };

      const created = new Promise((res, rej) => {
        getRocketChatAdmin().users.create(data, (err, body) => {
          if (err) {
            // reject(err);
            console.error(`[CHAT] CREATE_USER_API failed [${elem.email}]`);
            rej();
          } else {
            res([elem.id, data.user._id, data.user.username]);
          }
        });
      });
      created
        .then((result) => {
          createdUsers.push(data);
          if (counter === newuserInfo.length) resolve(createdUsers);
        })
        .catch((e) => {});
    });
  });

/**
 * Login to rocket chat server, on sucess give {authToken: string;userId: string;}
 *
 * @param {string} username : username/email id
 * @param {string} password
 *
 */
const userLogin = async (username, password) =>
  new Promise((resolve, reject) => {
    getRocketChatAdmin()
      .login(username, password)
      .then((body, self) => {
        // self is an instance of rocketChatClient,useful if you need subsequent calls
        // console.log(body)
        resolve(body);
        // body is an object containing authToken and userId
      })
      .catch((err) => {
        console.log(err);
      });
    // getNewClient().login(username, password).then(response => {
    //     resolve({ userId: response.userId, authToken: response.authToken })
    // }).catch(e => { console.error(e) });
  });

/**
 * Create a channel between poster & tasker by admin on task assignment
 *
 * @param {*} postername
 * @param {*} taskername
 * @param {*} channelname
 * @returns
 */
const createChannel = async (postername, taskername, channelname) => {
  const createNew = (name, members, callback) => {
    console.log(name, members);
    return getRocketChatAdmin().restClient.request(
      'POST',
      'channels.create',
      {name, members},
      callback,
    );
  };

  const promiseFinished = new Promise((resolve, reject) => {
    createNew(
      `${channelname}_${new Date().getTime()}`,
      [postername, taskername],
      (err, body) => {
        if (err) reject(err);
        else resolve(body);
      },
    );
  });

  // create channel
  const response = await promiseFinished;
  return {id: response.channel._id, name: response.channel.name};
};

/**
 * Set Channel Read Only On Task Completion : In read-only channels, only people with admin permissions can post messages.
 * @param {*} channelid
 */
const setChannelReadOnly = async (channelid) =>
  new Promise((resolve, reject) => {
    getRocketChatAdmin().channels.setReadOnly(channelid, true, (err, body) => {
      if (err) {
        reject(err);
      } else {
        resolve(body);
      }
    });
  });

/**
 * Gets a user's presence if the query string userId or username is provided,
 * @param {*} username
 */
const isUserOnline = async (username) =>
  new Promise((resolve, reject) => {
    getRocketChatAdmin().restClient.request(
      'GET',
      `users.getPresence?username=${username}`,
      null,
      (err, body) => {
        if (err) {
          reject(err);
        } else {
          resolve(body);
        }
      },
    );
  });

/**
 * Create a user authentication token. This is the same type of session token a user would get via login and will expire the same way.
 * Requires user-generate-access-token permission.
 * @param {*} userId
 */
const createUserToken = async (userId) => {
  const data = {userId};
  return new Promise((resolve, reject) => {
    getRocketChatAdmin().restClient.request(
      'POST',
      'users.createToken',
      data,
      (err, body) => {
        if (err) {
          reject(err);
        } else {
          resolve(body);
        }
      },
    );
  });
};

// const loginByToken = async (authToken) => {
//     return new Promise((resolve, reject) => {
//         chatClient.restClient.request("method", "login", [{ "resume": authToken }],  (err, body)=>{
//             if (err) {
//                 reject(err);
//             } else {
//                 resolve(body);
//             }
//         });
//     });
// }

/**
 *
 * @param {string} userId : sender chat userId
 * @param {string} authToken : sender auth token
 * @param {string} channelId : The room id of where the message is to be sent.
 * @param {string} msg : The text of the message to send
 */
const postMessage = async (userId, authToken, channelId, msg) => {
  console.log('######################');

  return new Promise((resolve, reject) => {
    // const chatClient = getNewClient();
    getRocketChatAdmin().restClient.setHeader('X-Auth-Token', authToken);
    getRocketChatAdmin().restClient.setHeader('X-User-Id', userId);

    const data = {
      roomId: channelId,
      text: msg,
    };

    getRocketChatAdmin().chat.postMessage(data, (err, body) => {
      if (err) {
        reject(err);
      } else {
        resolve(body);
      }
    });
  });
};

const channelList = async (userId, authToken) => {
  console.log('PPPPPPPPPPPPPP');

  return new Promise((resolve, reject) => {
    // const chatClient = getNewClient();
    getRocketChatAdmin().restClient.setHeader('X-Auth-Token', authToken);
    getRocketChatAdmin().restClient.setHeader('X-User-Id', userId);

    // let data = {
    //     roomId: channelId,
    //     text: msg
    // }

    getRocketChatAdmin().channels.listJoined({}, (err, body) => {
      if (err) {
        reject(err);
      } else {
        resolve(body);
      }
    });
  });
};

const channelmemeber = async (userId, authToken, channelId) =>
  // console.log(userId,authToken)

  new Promise((resolve, reject) => {
    const options = {
      method: 'GET',
      url: 'https://tectum.rocket.chat/api/v1/channels.members',
      qs: {roomId: channelId},
      headers: {'x-user-id': userId, 'x-auth-token': authToken},
    };

    request(options, (error, response, body) => {
      if (error) reject(error);
      else resolve(body);
      // console.log(body);
    });
  });

module.exports = {
  initAdmin,
  userLogin,
  createUser,
  createUserBulk,
  createChannel,
  setChannelReadOnly,
  postMessage,
  isUserOnline,
  createUserToken,
  channelList,
  channelmemeber,
};
