const db = require('../db/db');

exports.startUserSession = async (data) => {
  const connection = await db.getConnection();

  return new Promise((resolve, reject) => {
    connection.query(
      'INSERT INTO session_master(userId, username, password, isMetrcLoggedIn) VALUES (?,?,?,?)',
      [data.userId, data.username, data.password, true],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.findSessionByUserId = async (userId) => {
  const connection = await db.getConnection();

  return new Promise((resolve, reject) => {
    connection.query(
      'SELECT * FROM session_master where userId = ?',
      [userId],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.deleteByUserId = async (userId) => {
  const connection = await db.getConnection();

  return new Promise((resolve, reject) => {
    connection.query(
      'DELETE  FROM session_master where userId = ?',
      [userId],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};
