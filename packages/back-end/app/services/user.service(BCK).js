const exec = require('exec');
const db = require('../db/db');

exports.findUserByEmail = async (userID) => {
  const connection = await db.getConnection();

  return new Promise((resolve, reject) => {
    connection.query(
      `SELECT * FROM users where userID = ?`,
      [userID],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};
exports.findUserByEmail2 = async (userID) => {
  const connection = await db.getConnection();

  return new Promise((resolve, reject) => {
    connection.query(
      `SELECT * FROM users where userEmail = ?`,
      [userID],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.findUserByUserName = async (userName) => {
  const connection = await db.getConnection();

  return new Promise((resolve, reject) => {
    connection.query(
      `SELECT * FROM users where username = ?`,
      [userName],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.findUserByfacebookID = async (userID) => {
  const connection = await db.getConnection();

  return new Promise((resolve, reject) => {
    connection.query(
      `SELECT * FROM users where facebookID = ?`,
      [userID],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};
exports.findUserByGoogleID = async (userID) => {
  const connection = await db.getConnection();

  return new Promise((resolve, reject) => {
    connection.query(
      `SELECT * FROM users where googleID = ?`,
      [userID],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.fetchAllUser = async (body) => {
  const connection = await db.getConnection();

  return new Promise((resolve, reject) => {
    connection.query(`SELECT * FROM users`, (err, result) => {
      connection.release();
      // console.log(result);
      if (err) return reject(err);
      return resolve(result);
    });
  });
};

exports.userSignUp = async (body) => {
  console.log(body);
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    // body.fullName:fullName
    connection.query(
      `INSERT INTO users(userEmail, userPassword, emailVerified,username,RCID) VALUES(?,?,?,?,?)`,
      [body.userEmail, body.userPassword, 1, body.username, body.RCID],
      (err, result) => {
        connection.release();
        if (err) return reject(err);

        console.log(`user registered --> ${body.userEmail}`);
        return resolve(result);
      },
    );
  });
};
exports.userSignUpFB = async (body) => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    // console.log(body);

    connection.query(
      `INSERT INTO users(userEmail, facebookID, emailVerified,fullName,profilePicture,facebookToken,fbAuthStatus) VALUES(?,?,?,?,?,?,?)`,
      [
        body.userEmail,
        body.facebookID,
        1,
        body.fullName,
        body.profilePicture,
        body.facebookToken,
        body.fbAuthStatus,
      ],
      (err, result) => {
        connection.release();
        if (err) return reject(err);

        // console.log("user registered --> "+ body.userEmail);
        return resolve(result);
      },
    );
  });
};
exports.userSignUpGoogle = async (body) => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    // console.log(body);

    connection.query(
      `INSERT INTO users(userEmail, googleID, emailVerified,fullName,profilePicture,googleToken,googleAuthStatus) VALUES(?,?,?,?,?,?,?)`,
      [
        body.userEmail,
        body.googleID,
        1,
        body.fullName,
        body.profilePicture,
        body.googleToken,
        body.googleAuthStatus,
      ],
      (err, result) => {
        connection.release();
        if (err) return reject(err);

        // console.log("user registered --> "+ body.userEmail);
        return resolve(result);
      },
    );
  });
};

// exports.updateUserById = async (req) => {
//   const connection = await db.getConnection();
//   return new Promise((resolve, reject) => {
//     connection.query(`UPDATE users SET vendorKey= ?, userKey= ? where userId = ?`, [req.body.username, req.body.password, req.body.userId], function(err, result){
//       connection.release();
//       if(err) return reject(err)
//       else return resolve(result)
//     });
//   });
// }

exports.deleteUserByEmail = async (req) => {
  // console.log(req+"hi");

  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(
      `DELETE FROM users WHERE userID = ? `,
      [req],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.updateUserImageVerified = async (req) => {
  const connection = await db.getConnection();

  return new Promise((resolve, reject) => {
    connection.query(
      `UPDATE users SET isImageVerified= ? WHERE userID = ?`,
      [req.imageVerify, req.userID],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.updateUserVideoVerified = async (req) => {
  const connection = await db.getConnection();

  return new Promise((resolve, reject) => {
    connection.query(
      `UPDATE users SET isVideoVerified = ? WHERE userID = ?`,
      [req.videoVerify, req.userID],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.updateUserProfileVerified = async (req) => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(
      `UPDATE users SET isProfileVerified= ? WHERE userID = ?`,
      [req.profileVerify, req.userID],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.fetchTotalVerifiedUser = async () => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(
      `SELECT COUNT(*) FROM users WHERE isProfileVerified = 1`,
      (err, result) => {
        connection.release();
        // console.log(Object.values(result[0]));
        if (err) return reject(err);
        return resolve(Object.values(result[0]));
      },
    );
  });
};

exports.fetchVerifiedUser = async (req) => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(
      `SELECT isImageVerified,isVideoVerified,isProfileVerified FROM users WHERE userID=?`,
      [req.userID],
      (err, result) => {
        connection.release();
        // console.log(Object.values(result[0]));
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.registerUserBio = async (req) => {
  // console.log("======================")
  // console.log(req)
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    const datee = req.dob.split('-');
    const dte = new Date(datee[2], datee[1] - 1, datee[0]);
    connection.query(
      `UPDATE users SET fullName=?,dob=?,locationText=?,gender=?,religionID=?,lat=?,lon=?,genderInterest=?,lookingFor=?,firstName=?,lastName=? WHERE userID=?`,
      [
        req.fullName,
        dte,
        req.locationText,
        req.gender,
        req.religionID,
        req.lat,
        req.lon,
        req.genderInterest,
        req.lookingFor,
        req.fName,
        req.lName,
        req.userID,
      ],
      (err, result) => {
        // console.log(JSON.stringify(err))
        console.log(JSON.stringify(result));
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};
exports.editUser = async (req) => {
  const connection = await db.getConnection();
  console.log(JSON.stringify(req));
  return new Promise((resolve, reject) => {
    // datee = req.dob.split("-")
    // var dte = new Date(datee[2], datee[1] - 1, datee[0]);
    connection.query(
      `UPDATE users SET firstName=?,lastName=?,locationText=?,gender=?,religionID=?,lookingFor=?,height=?,aboutMe=?,userEmail=?,haveCar=?,doesSmoke=?,favoritSong=?,yourMusic=?,yourMovie=?,favoritePet=?,travelToLike=?,education=?,job=? WHERE userID=?`,
      [
        req.firstName,
        req.lastName,
        req.locationText,
        req.gender,
        req.religionID,
        req.lookingFor,
        req.height,
        req.aboutMe,
        req.userEmail,
        req.haveCar,
        req.doesSmoke,
        req.favoritSong,
        req.yourMusic,
        req.yourMovie,
        req.favoritePet,
        req.travelToLike,
        req.education,
        req.job,
        req.userID,
      ],
      (err, result) => {
        // console.log(JSON.stringify(err))
        // console.log(JSON.stringify(result))
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.moreAbout1 = async (req) => {
  // console.log(req)
  const connection = await db.getConnection();
  let query = ``;
  const queryAttr = [];
  return new Promise((resolve, reject) => {
    if (req.musicID) {
      query += `INSERT INTO userMusicList(musicID, userID) VALUES(?,?);`;
      queryAttr.push(req.musicID);
      queryAttr.push(req.userID);
    }
    if (req.movieID) {
      query += `INSERT INTO userMovieList(movieID, userID) VALUES(?,?);`;
      queryAttr.push(req.movieID);
      queryAttr.push(req.userID);
    }
    if (req.petID) {
      query += `INSERT INTO userPetList(petID, userID) VALUES(?,?);`;
      queryAttr.push(req.petID);
      queryAttr.push(req.userID);
    }
    if (req.travelStyleID) {
      query += `INSERT INTO userTravelStyleList(travelStyleID, userID) VALUES(?,?)`;
      queryAttr.push(req.travelStyleID);
      queryAttr.push(req.userID);
    }
    connection.query(query, queryAttr, (err, result) => {
      connection.release();
      if (err) return reject(err);
      return resolve(result);
    });
    // connection.query(`INSERT INTO userMusicList(musicID, userID) VALUES(?,?)`,
    //     [req.musicID, req.userID], function(err, result){
    //     // connection.release();
    //     if(err) return reject(err)
    //     if(result){
    //         connection.query(`INSERT INTO userMovieList(movieID, userID) VALUES(?,?)`,
    //             [req.movieID, req.userID], function(err, result){
    //                 // connection.release();
    //                 if(err) return reject(err)
    //                 // else return resolve(result)
    //                 if(result){
    //                     connection.query(`INSERT INTO userPetList(petID, userID) VALUES(?,?)`,
    //                         [req.petID, req.userID], function(err, result){
    //                             // connection.release();
    //                             if(err) return reject(err)
    //                             // else return resolve(result)
    //                             if(result){
    //                                 connection.query(`INSERT INTO userTravelStyleList(travelStyleID, userID) VALUES(?,?)`,
    //                                     [req.travelStyleID, req.userID], function(err, result){
    //                                         connection.release();
    //                                         if(err) return reject(err)
    //                                         else return resolve(result)
    //                                     });
    //                             }
    //                         });
    //                 }
    //             });
    //     }
    // });
  });
};

exports.moreAbout2 = async (req) => {
  console.log(req);

  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(
      `UPDATE users SET  height=?,alternateLocation=?,haveCar=?,doesSmoke=? WHERE userID=?`,
      [
        req.height,
        req.alternateLocation,
        req.haveCar,
        req.doesSmoke,
        req.userID,
      ],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
    // connection.query("UPDATE users SET  height=?,weight=?,locationText=?,haveCar=?,doesSmoke=? WHERE userID=?",[req.height,req.weight,req.locationText,req.haveCar,req.doesSmoke,req.userID], function(err, result){
    //     connection.release();
    //     if(err) return reject(err)
    //     else return resolve(result)
    // });
  });
};

exports.moreAbout3 = async (req) => {
  // console.log(`UPDATE users SET  aboutMe=?,occupation=?,company=?,school=?,education=?,interest1=?,interest2=?,interest3=?,interest4=?,interest5=? WHERE userID=?`,[req.aboutMe,req.occupation,req.company,req.school,req.education,req.interest1,req.interest2,req.interest3,req.interest4,req.interest5,req.userID])
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(
      `UPDATE users SET  aboutMe=?,occupation=?,company=?,school=?,education=?,interest1=?,interest2=?,interest3=?,interest4=?,interest5=? WHERE userID=?`,
      [
        req.aboutMe,
        req.occupation,
        req.company,
        req.school,
        req.education,
        req.interest1,
        req.interest2,
        req.interest3,
        req.interest4,
        req.interest5,
        req.userID,
      ],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.moreAbout4 = async (req) => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(
      `UPDATE users SET  yourHobby=?,yourMusic=?,yourMovie=?,anyPet=? WHERE userID=?`,
      [req.hobby, req.music, req.movie, req.pet, req.userID],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.lookingFor = async (req) => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(
      `UPDATE users SET  lookingFor=? WHERE userID=?`,
      [req.lookingFor, req.userID],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.profilePic = async (req) => {
  console.log(req);
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(
      `UPDATE users SET  profilePicture=? WHERE userID=?`,
      [req.profilePicture, req.userID],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.photoVerification = async (req) => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(
      `UPDATE users SET  photoVerificationPath=? WHERE userID=?`,
      [req.photoVerification, req.userID],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.videoverification = async (req) => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(
      `UPDATE users SET  videoverificationpath=? WHERE userID=?`,
      [req.videoVerification, req.userID],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.userGenderInterst = async (req) => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(
      `UPDATE users SET  genderInterest=? WHERE userID=?`,
      [req.genderInterest, req.userID],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.identifyWith = async (req) => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(
      `UPDATE users SET  identifyWith=? WHERE userID=?`,
      [req.identifyWith, req.userID],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.galleryPhotos = async (req) => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    // if(req.galleryIndex===1){

    connection.query(
      `UPDATE users SET  galleryPhotos1=?,galleryPhotos2=?,galleryPhotos3=?,galleryPhotos4=?,galleryPhotos5=?,galleryPhotos6=?,galleryVideo1=?,galleryVideo2=?,galleryVideo3=? WHERE userID=?`,
      [
        req.galleryPhotos1,
        req.galleryPhotos2,
        req.galleryPhotos3,
        req.galleryPhotos4,
        req.galleryPhotos5,
        req.galleryPhotos6,
        req.galleryVideo1,
        req.galleryVideo2,
        req.galleryVideo3,
        req.userID,
      ],
      (err, result) => {
        connection.release();
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.registerUserProfile = async (req) => {
  console.log(JSON.stringify(req));
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(
      `UPDATE users SET instaProfileLink=?,fbProfileLink=? WHERE userID=?`,
      [req.instaProfileLink, req.fbProfileLink, req.userID],
      (err, result) => {
        connection.release();
        // console.log(err, result);
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.fetchReligionList = async () => {
  console.log('fetchReligion');
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(`SELECT * FROM religionList`, (err, result) => {
      connection.release();
      if (err) return reject(err);

      console.log(JSON.stringify(result));
      const data = result.map((rawData) => ({
        label: rawData.religionName,
        value: rawData.religionID,
      }));
      return resolve(data);
    });
  });
};
exports.requestForVerification = async (req) => {
  const {capturedImage} = req;
  const {uploadedImage} = req;
  // console.log(req);
  // return false;
  return new Promise((resolve, reject) => {
    exec(
      ['python3.9', 'app/python/faceCompare.py', uploadedImage, capturedImage],
      (err, out) => {
        if (err) return reject(err);
        return resolve(out);
      },
    );
  });
};

exports.fetchAllFilterUser = async (body) => {
  console.log(body);
  const connection = await db.getConnection();
  const minAge = 1; // req.body.minAge
  const maxAge = 300; // req.body.maxAge
  const minAgeDate = new Date();
  const maxAgeDate = new Date();
  const minPastYear = minAgeDate.getFullYear() - minAge;
  const maxPastYear = maxAgeDate.getFullYear() - maxAge;
  minAgeDate.setFullYear(minPastYear);
  maxAgeDate.setFullYear(maxPastYear);

  const minDay = minAgeDate.getDate();
  const minMonth = minAgeDate.getMonth();
  const minYear = minAgeDate.getFullYear();
  const newMinAge = `${minYear}-${minMonth}-${minDay}`;

  const maxDay = maxAgeDate.getDate();
  const maxMonth = maxAgeDate.getMonth();
  const maxYear = maxAgeDate.getFullYear();
  const newMaxAge = `${maxYear}-${maxMonth}-${maxDay}`;
  // console.log(newMinAge);

  // console.log(minAgeDate.split(" ")[0]);
  // console.log(maxAgeDate.split(" ")[0]);
  // console.log(newMaxAge);
  const km = 50 * 0.621371192;

  // SELECT * , `SQRT`( `POW`(69.1 * (`lat` - 22.5505029), 2) + `POW`(69.1 * (75.7625071 - `lon`) * `COS`(`lat` / 57.3), 2)) AS `distance` FROM `users` HAVING `distance` < 25 ORDER BY `distance`
  // console.log("++++++++++++")

  // console.log(body.genderInterest)
  // console.log("++++++++++++")

  // console.log(newMaxAge)
  // console.log("++++++++++++")

  // console.log(newMinAge)
  // console.log("++++++++++++")

  // console.log(body.userID)
  // console.log("++++++++++++")

  // console.log(km)
  // console.log("==========")

  // console.log(`SELECT *, SQRT( POW(69.1 * (lat - ?), 2) + POW(69.1 * (? - lon) * COS(lat / 57.3), 2)) AS distance FROM users where gender=? AND dob>=? AND dob<=? AND userID!=? AND userID NOT IN (SELECT matchUserID FROM matchingTable WHERE userID=?) HAVING distance < ? LIMIT 5`,[body.lat, body.lon, body.genderInterest, newMaxAge, newMinAge, body.userID, body.userID,km])

  return new Promise((resolve, reject) => {
    connection.query(
      `SELECT *, SQRT( POW(69.1 * (lat - ?), 2) + POW(69.1 * (? - lon) * COS(lat / 57.3), 2)) AS distance FROM users where gender=? AND dob>=? AND dob<=? AND userID!=? AND userID NOT IN (SELECT matchUserID FROM matchingTable WHERE userID=?) HAVING distance < ? LIMIT 5`,
      [
        body.lat,
        body.lon,
        body.genderInterest,
        newMaxAge,
        newMinAge,
        body.userID,
        body.userID,
        km,
      ],
      (err, result) => {
        connection.release();
        console.log(result);
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
};

exports.updateMatching = async (body) => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    // console.log(body);
    const timestamp = new Date().getTime();
    // console.log(timestamp)

    // return resolve(timestamp);

    connection.query(
      `INSERT INTO matchingTable(userID, matchUserID, matchStatus,timestamp) VALUES(?,?,?,?)`,
      [body.userID, body.matchUserID, body.matchStatus, timestamp],
      (err, result) => {
        connection.release();
        if (err) return reject(err);

        return resolve(result);
      },
    );
  });
};
// genRepo.post("/generateReport", (req, res) => {
//     var  query  = req.body;
//     console.log(query);
//     // return false;
//     exec(['python', 'server/python/exportDocWhatsapp.py', query ], function(err, out) {
//         res.jsonp({
//             "statusCode": 200,
//             "items": out
//         });
//     });
// });

exports.fetchMatchingUsers = async (body) => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(
      `SELECT userID FROM matchingTable WHERE matchUserID=? AND matchStatus=1 `,
      [body.userID],
      (err, result) => {
        connection.release();
        if (err) return reject(err);

        return resolve(result);
      },
    );
  });
};

exports.getAllMemberOfChannel = async (RCID) => {
  const connection = await db.getConnection();
  return new Promise((resolve, reject) => {
    connection.query(
      `SELECT fullName,profilePicture FROM users WHERE RCID=? `,
      [RCID],
      (err, result) => {
        connection.release();
        if (err) return reject(err);

        console.log(result);
        return resolve(result);
      },
    );
  });
};
