const inventoryFacilitiesController = require('../controllers/inventoryFacilities.controller');
const {authJwt} = require('../middleware');

module.exports = function (app) {
  app.get(
    '/api/facilities/v1',
    [authJwt.verifyToken],
    inventoryFacilitiesController.getFacilities,
  );
};
