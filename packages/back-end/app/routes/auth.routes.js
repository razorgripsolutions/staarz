const multer = require('multer');
const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const {authJwt} = require('../middleware');
const authController = require('../controllers/auth.controller');
const userVerificationController = require('../controllers/userverification.controller');
const adminController = require('../controllers/admin.controller');

// const uploadPath = "/home/admin1/Desktop/starz/StarzCore/public/data"
const uploadPath = './public/data';

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, uploadPath);
  },
  filename: (req, file, cb) => {
    const ext = file.originalname.split('.').pop();
    cb(null, `image${Date.now()}.${ext}`);
  },
});
const verifyStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, uploadPath);
  },
  filename: (req, file, cb) => {
    const ext = file.originalname.split('.').pop();
    cb(null, `verifyImage${Date.now()}.${ext}`);
  },
});

const verifyVideoStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, uploadPath);
  },
  filename: (req, file, cb) => {
    const ext = file.originalname.split('.').pop();
    cb(null, `verifyVideo${Date.now()}.${ext}`);
  },
});

const galleryStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, uploadPath);
  },
  filename: (req, file, cb) => {
    console.log('file.originalname========================');
    console.log(file.originalname);
    let ext;
    if (
      file.originalname.split('.').pop() === 'undefined' &&
      file.mimetype === 'video/quicktime'
    ) {
      ext = 'mov';
    } else {
      ext = file.originalname.split('.').pop();
    }
    // const ext = file.originalname.split('.').pop();
    console.log('ext===========================================');
    console.log(ext);
    cb(null, `GalleryImage${Date.now()}.${ext}`);
  },
});

const profilePicStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, uploadPath);
  },
  filename: (req, file, cb) => {
    const ext = file.originalname.split('.').pop();
    cb(null, `profilePicImage${Date.now()}.${ext}`);
  },
});

const uploadStorage = multer({
  storage,
  fileFilter: (req, file, cb) => {
    console.log(req, file, cb);

    if (
      file.mimetype === 'image/jpeg' ||
      file.mimetype === 'image/jpg' ||
      file.mimetype === 'image/png'
    ) {
      cb(null, true);
    } else {
      cb(null, false);
      return cb('this is not PHOTO');
    }
  },
});

const verifyUploadStorage = multer({
  storage: verifyStorage,
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype === 'image/jpeg' ||
      file.mimetype === 'image/jpg' ||
      file.mimetype === 'image/png'
    ) {
      cb(null, true);
    } else {
      cb(null, false);
      return cb('this is not PHOTO');
    }
  },
});

const verifyVideoUploadStorage = multer({
  storage: verifyVideoStorage,
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype === 'video/avi' ||
      file.mimetype === 'video/mkv' ||
      file.mimetype === 'video/mp4'
    ) {
      cb(null, true);
    } else {
      cb(null, false);
      return cb('this is not video');
    }
  },
});

const profilePicUploadStorage = multer({
  storage: profilePicStorage,
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype === 'image/jpeg' ||
      file.mimetype === 'image/jpg' ||
      file.mimetype === 'image/png'
    ) {
      cb(null, true);
    } else {
      cb(null, false);
      return cb('this is not PHOTO');
    }
  },
});

const galleryImageStorage = multer({
  storage: galleryStorage,
  fileFilter: (req, file, cb) => {
    //     video/x-flv
    // video/mp4
    // application/x-mpegURL
    // video/MP2T
    // video/3gpp
    // video/quicktime
    // video/x-msvideo
    // video/x-ms-wmv

    if (
      file.mimetype === 'image/jpeg' ||
      file.mimetype === 'image/jpg' ||
      file.mimetype === 'image/png' ||
      file.mimetype === 'video/avi' ||
      file.mimetype === 'video/mkv' ||
      file.mimetype === 'video/mp4' ||
      file.mimetype === 'video/quicktime' ||
      file.mimetype === 'video'
    ) {
      cb(null, true);
    } else {
      cb(null, false);
      return cb('this is not allowed');
    }
  },
});

const videoStorage = multer({
  storage,
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype === 'video/avi' ||
      file.mimetype === 'video/mkv' ||
      file.mimetype === 'video/mp4'
    ) {
      cb(null, true);
    } else {
      cb(null, false);
      return cb('this is not video');
    }
  },
});

const uploadStorageMulti = multer({
  storage,
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype === 'image/jpeg' ||
      file.mimetype === 'image/jpg' ||
      file.mimetype === 'image/png'
    ) {
      cb(null, true);
    } else {
      cb(null, false);
      return cb('this is not PHOTO');
    }
  },
});

module.exports = function (app) {
  app.post('/auth/adminSignUp', authController.adminSignUp);
  app.post('/auth/adminSignIn', authController.adminSignIn);
  app.post('/auth/userSignIn', authController.userSignIn);
  app.post('/auth/userFBSignIn', authController.userFBSignIn);
  app.post('/auth/userGoogleSignIn', authController.userGoogleSignIn);

  app.post('/auth/userSignUp', authController.userSignUp);
  app.post('/auth/userFBSignUp', authController.userFBSignUp);
  app.post('/auth/userGoogleSignUp', authController.userGoogleSignUp);
  app.post('/auth/logout', authController.logout);
  // app.post("/metrc/auth/login", authController.metrcAuthenticate);
  app.post('/auth/fetchAllUserList', authController.usersList);
  app.post('/auth/deleteUserByEmailID', authController.deleteUserByEmail);
  app.post('/auth/fetchUserWithID', authController.fetchUserWithID);
  app.post('/auth/socialProfile', authController.socialProfile);
  app.post(
    '/auth/requestForVerification',
    userVerificationController.requestForVerification,
  );
  app.post('/auth/fetchUserWithFilter', authController.usersListFilter);

  app.post('/auth/registerUserBio', userVerificationController.registerUserBio);
  app.post('/auth/isImageVerified', userVerificationController.imageVerified);
  app.post('/auth/isVideoVerified', userVerificationController.videoVerified);
  app.post(
    '/auth/isProfileVerified',
    userVerificationController.profileVerified,
  );
  app.post(
    '/auth/fetchTotalVerifiedUserList',
    userVerificationController.fetchTotalVerifiedUser,
  );
  app.post(
    '/auth/fetchIsProfileVerified',
    userVerificationController.fetchVerifiedUserController,
  );
  app.post('/auth/profilePicPath', userVerificationController.profilePic);
  app.post(
    '/auth/photoVerificationPath',
    userVerificationController.photoVerification,
  );
  app.post(
    '/auth/videoVerificationPath',
    userVerificationController.videoverification,
  );
  app.post(
    '/auth/userGenderInterst',
    userVerificationController.userGenderInterst,
  );
  app.post('/auth/identifyWith', userVerificationController.identifyWith);
  app.post('/auth/galleryPhotos', userVerificationController.galleryPhotos);
  app.post('/auth/moreAbout1', userVerificationController.moreAbout1);
  app.post('/auth/moreAbout2', userVerificationController.moreAbout2);
  app.post('/auth/moreAbout3', userVerificationController.moreAbout3);
  app.post('/auth/moreAbout4', userVerificationController.moreAbout4);
  app.post('/auth/lookingFor', userVerificationController.lookingFor);
  app.post(
    '/auth/fetchReligionList',
    userVerificationController.fetchReligionList,
  );

  app.post('/auth/allUserList', adminController.usersList);
  app.post('/auth/fetchReligionListAdmin', adminController.fetchReligionList);
  app.post('/auth/updateReligionList', adminController.updateReligionList);
  app.post(
    '/auth/deleteFromReligionList',
    adminController.deleteFromReligionList,
  );
  app.post('/auth/fetchInterestList', adminController.fetchInterestList);
  app.post('/auth/updateInterestList', adminController.updateInterestList);
  app.post(
    '/auth/deleteFromInterestList',
    adminController.deleteFromInterestList,
  );
  app.post(
    '/auth/favouriteMusicStyleList',
    adminController.favouriteMusicStyleList,
  );
  app.post(
    '/auth/updateFavouriteMusicStylet',
    adminController.updateInterestList,
  );
  // app.post("/auth/deleteFromInterestList",adminController.deleteFromInterestList);
  app.post('/auth/fetchTravelStyle', adminController.fetchTravelStyle);
  // app.post("/auth/updateInterestList",adminController.updateInterestList);
  // app.post("/auth/deleteFromInterestList",adminController.deleteFromInterestList);
  app.post(
    '/auth/fetchUserEducationList',
    adminController.fetchUserEducationList,
  );
  // app.post("/auth/updateInterestList",adminController.updateInterestList);
  // app.post("/auth/deleteFromInterestList",adminController.deleteFromInterestList);
  app.post(
    '/auth/fetchfavouriteMovieGenre',
    adminController.fetchfavouriteMovieGenre,
  );
  // app.post("/auth/updateInterestList",adminController.updateInterestList);
  // app.post("/auth/deleteFromInterestList",adminController.deleteFromInterestList);
  app.post(
    '/auth/fetchRelationShipType',
    adminController.fetchRelationShipType,
  );
  app.post('/auth/fetchReligion', adminController.fetchReligion);
  // app.post("/auth/updateInterestList",adminController.updateInterestList);
  // app.post("/auth/deleteFromInterestList",adminController.deleteFromInterestList);
  app.post('/auth/fetchUserPetList', adminController.fetchUserPetList);
  app.post('/auth/updatePetList', adminController.updatePetList);
  app.post('/auth/deleteFromPetList', adminController.deleteFromPetList);
  app.post(
    '/auth/fetchUserOccupationList',
    adminController.fetchUserOccupationList,
  );
  app.post(
    '/auth/deleteUserOccupationList',
    adminController.deleteUserOccupationList,
  );
  app.post(
    '/auth/updateUserOccupationList',
    adminController.updateUserOccupationList,
  );
  // app.post("/auth/updateInterestList",adminController.updateInterestList);
  // app.post("/auth/deleteFromInterestList",adminController.deleteFromInterestList);
  // schol

  app.post('/auth/updateEducationList', adminController.UpdateEducationList);
  app.post('/auth/deleteEducationList', adminController.deleteEducationList);
  app.post('/auth/fetchEducationList', adminController.fetchUserEducationList);
  app.post('/auth/fetchallHobbyList', adminController.fetchallHobbyList);
  app.post('/auth/updateallHobbyList', adminController.updateallHobbyList);
  app.post('/auth/deleteallHobbyList', adminController.deleteallHobbyList);
  app.post('/auth/fetchallMovieList', adminController.fetchallMovieList);
  app.post('/auth/addAllMovieList', adminController.addAllMovieList);
  app.post('/auth/deleteAllMovieList', adminController.deleteAllMovieList);
  app.post('/auth/fetchallMusicList', adminController.fetchallMusicList);
  app.post('/auth/addAllMusicList', adminController.addAllMusicList);
  app.post('/auth/deleteAllMusicList', adminController.deleteAllMusicList);
  app.post(
    '/auth/fetchAllTravelStyleList',
    adminController.fetchAllTravelStyleList,
  );
  app.post(
    '/auth/addAllTravelStyleList',
    adminController.addAllTravelStyleList,
  );
  app.post(
    '/auth/deleteAllTravelStyleList',
    adminController.deleteAllTravelStyleList,
  );
  app.post('/auth/addJobsList', adminController.addJobsList);
  app.post('/auth/deleteJobsList', adminController.deleteJobsList);
  app.post('/auth/fetchJobsList', adminController.fetchJobsList);
  app.post('/auth/updateUser', adminController.updateUser);

  app.post('/auth/updateMatching', userVerificationController.updateMatching);
  app.post(
    '/auth/fetchMatchingUsers',
    userVerificationController.fetchMatchingUsers,
  );
  app.post('/auth/createChannel', userVerificationController.createChannel);
  app.post('/auth/sendMessage', userVerificationController.sendMessage);
  app.post('/auth/channelList', userVerificationController.channelList);
  // app.post("/auth/channelList",userVerificationController.channelList);

  app.post(
    '/upload/single',
    uploadStorage.single('uploadSingleImage'),
    (req, res, next) => {
      console.log(req);
      const files = req.file;
      console.log(files);
      if (!files) {
        const error = new Error('Please choose files');
        error.httpStatusCode = 400;
        return res.json({
          success: false,
          msg: 'Oops Error!!',
          res: error,
          status: 400,
        });
      }

      return res.json({
        success: true,
        msg: 'success',
        res: files,
        status: 200,
      });
      // res.send(files)
    },
  );

  app.post(
    '/upload/galleryImagePath',
    galleryImageStorage.single('galleryImage'),
    (req, res, next) => {
      console.log(req.file);
      const files = req.file;
      // console.log(files)
      if (!files) {
        const error = new Error('Please choose files');
        error.httpStatusCode = 400;
        return res.json({
          success: false,
          msg: 'Oops Error!!',
          res: error,
          status: 400,
        });
      }
      return res.json({
        success: true,
        msg: 'successful',
        res: files,
        status: 200,
      });

      // let data = {
      //   userID :Number(req.body.userID.replace(/\s+/g, ' ').trim()),
      //   galleryPhotos : files.filename,
      //   galleryIndex: Number(req.body.galleryIndex.replace(/\s+/g, ' ').trim())
      // }
      // setTimeout(()=>{console.log(data)},200)

      //  userVerificationController.galleryPhotos(data)

      //     return res.json({
      //       success: true,
      //       msg:"successful",
      //       res:files,
      //       status:200
      //     });
    },
  );

  app.post(
    '/upload/verifyVideoPath',
    verifyVideoUploadStorage.single('verifyVideo'),
    (req, res, next) => {
      // console.log(req.body)

      const files = req.file;
      // console.log(files)
      if (!files) {
        const error = new Error('Please choose files');
        error.httpStatusCode = 400;
        return res.json({
          success: false,
          msg: 'Oops Error!!',
          res: error,
          status: 400,
        });
      }
      return res.json({
        success: true,
        msg: 'successful',
        res: files,
        status: 200,
      });

      // let data = {
      //   userID :Number(req.body.userID.replace(/\s+/g, ' ').trim()),
      //   videoVerification : files.filename
      // }

      //  userVerificationController.videoverification(data)

      //     return res.json({
      //       success: true,
      //       msg:"successful",
      //       res:files,
      //       status:200
      //     });
    },
  );

  app.post(
    '/upload/profilePicPath',
    profilePicUploadStorage.single('profileImage'),
    (req, res, next) => {
      // console.log(req.body)

      const files = req.file;
      // console.log(files)
      if (!files) {
        const error = new Error('Please choose files');
        error.httpStatusCode = 400;
        return res.json({
          success: false,
          msg: 'Oops Error!!',
          res: error,
          status: 400,
        });
      }

      const data = {
        userID: Number(req.body.userID.replace(/\s+/g, ' ').trim()),
        profilePic: files.filename,
      };

      userVerificationController.profilePic(data);

      return res.json({
        success: true,
        msg: 'successful',
        res: files,
        status: 200,
      });
    },
  );

  app.post(
    '/upload/verifySingle',
    verifyUploadStorage.single('verifyImage'),
    (req, res, next) => {
      // console.log(req.body)

      const files = req.file;
      // console.log(files)
      if (!files) {
        const error = new Error('Please choose files');
        error.httpStatusCode = 400;
        return res.json({
          success: false,
          msg: 'Oops Error!!',
          res: error,
          status: 400,
        });
      }
      return res.json({
        success: true,
        msg: 'successful',
        res: files,
        status: 200,
      });

      // let data = {
      //   userID :Number(req.body.userID.replace(/\s+/g, ' ').trim()),
      //   photoVerification : files.filename
      // }

      //  userVerificationController.photoVerification(data)

      //     return res.json({
      //       success: true,
      //       msg:"successful",
      //       res:files,
      //       status:200
      //     });
    },
  );

  app.post(
    '/uploadMultiple',
    uploadStorageMulti.array('file', 9),
    (req, res, next) => {
      const {files} = req;
      if (!files) {
        const error = new Error('Please choose files');
        error.httpStatusCode = 400;
        return res.json({
          success: false,
          msg: 'Oops Error!!',
          res: error,
          status: 400,
        });
      }

      return res.json({
        success: true,
        msg: 'success !!',
        res: files,
        status: 200,
      });
    },
  );
  app.post('/uploadVideo', videoStorage.single('video'), (req, res) => {
    const files = req.file;

    if (!files) {
      const error = new Error('Please choose files');
      error.httpStatusCode = 400;
      return res.json({
        success: false,
        msg: 'Oops Error!!',
        res: error,
        status: 400,
      });
    }

    return res.json({
      success: true,
      msg: 'success !!',
      res: files,
      status: 200,
    });
  });

  multer({limits: {fieldSize: 25 * 1024 * 1024}});
  const Storage = multer.diskStorage({
    destination(req, file, callback) {
      callback(null, uploadPath);
    },
    filename(req, file, callback) {
      callback(null, `${file.fieldname}_${Date.now()}_${file.originalname}`);
    },
  });
  const upload = multer({storage: Storage});
  app.post('/api/upload', upload.single('imgUploader'), (req, res) => {
    console.log('file', req.files);
    console.log('body', req.body);
    res.status(200).json({
      status: 200,
      message: 'success!',
    });
  });

  app.use(bodyParser.json({limit: '500mb', extended: true}));
  app.use(
    bodyParser.urlencoded({
      limit: '500mb',
      extended: true,
      parameterLimit: 50000,
    }),
  );
  app.use(bodyParser.text({limit: '500mb'}));

  app.post('/mujheuploadkaro', (req, res) => {
    console.log(JSON.stringify(req.body));
    fs.writeFile('./out.png', req.body.imgsource, 'base64', (err) => {
      if (err) throw err;
    });
    res.status(200);
  });
};

// app.post('/upload-multiple-images', (req, res) => {
//   // 10 is the limit I've defined for number of uploaded files at once
//   // 'multiple_images' is the name of our file input field
//   let upload = multer({ storage: storage, fileFilter: helpers.imageFilter }).array('multiple_images', 10);
//
//   upload(req, res, function(err) {
//     if (req.fileValidationError) {
//       return res.send(req.fileValidationError);
//     }
//     else if (...) // The same as when uploading single images
//
//     let result = "You have uploaded these images: <hr />";
//     const files = req.files;
//     let index, len;
//
//     // Loop through all the uploaded images and display them on frontend
//     for (index = 0, len = files.length; index < len; ++index) {
//       result += `<img src="${files[index].path}" width="300" style="margin-right: 20px;">`;
//     }
//     result += '<hr/><a href="./">Upload more images</a>';
//     res.send(result);
//   });
// });
