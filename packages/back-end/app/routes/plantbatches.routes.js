const plantbatchesController = require('../controllers/plantbatches.controller');
const {authJwt} = require('../middleware');

module.exports = function (app) {
  app.get(
    '/api/plantbatches/v1/active',
    [authJwt.verifyToken],
    plantbatchesController.getActivePlantBatches,
  );
  app.get(
    '/api/plantbatches',
    [authJwt.verifyToken],
    plantbatchesController.getPlantBatches,
  );
};
