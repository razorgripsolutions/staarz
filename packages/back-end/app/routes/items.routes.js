const itemsController = require('../controllers/items.controller');
const {authJwt} = require('../middleware');

module.exports = function (app) {
  app.get(
    '/api/items/v1/categories',
    [authJwt.verifyToken],
    itemsController.getItemsCategoriesByLicenseNumber,
  );
};
