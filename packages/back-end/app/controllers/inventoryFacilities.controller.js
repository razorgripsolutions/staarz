const axios = require('axios');
const urlConfig = require('../config/urlConfig');
const sessionMasterService = require('../services/sessionMaster.service');

exports.getFacilities = async (req, res) => {
  let data = [];
  const result = await sessionMasterService.findSessionByUserId(req.userId);

  if (result.length < 1) {
    return res.status(401).send({
      message: 'Metrc Unauthorized!',
    });
  }

  await axios
    .get(`${urlConfig.METRC_URL}/facilities/v1`, {
      auth: {
        username: result[0].username,
        password: result[0].password,
      },
    })
    .then((response) => {
      data = response.data;
      res.status(200).send({
        data,
      });
    })
    .catch((error) =>
      res.status(401).send({
        message: 'Metrc Unauthorized!',
      }),
    );
};
