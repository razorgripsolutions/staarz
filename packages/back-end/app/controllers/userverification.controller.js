const axios = require('axios');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const config = require('../config/auth.config');
const userService = require('../services/user.service');
const adminService = require('../services/admin.service');
const urlConfig = require('../config/urlConfig');
const sessionMasterService = require('../services/sessionMaster.service');
const rocketChatService = require('../services/chat.service.v2');

exports.imageVerified = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  const result = await userService.findUserByEmail(req.body.userID);

  try {
    decoded = jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }

      const updateResult = await userService.updateUserImageVerified(req.body);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.videoVerified = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  const result = await userService.findUserByEmail(req.body.userID);

  try {
    decoded = jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }

      const updateResult = await userService.updateUserVideoVerified(req.body);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.profileVerified = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  const result = await userService.findUserByEmail(req.body.userID);

  try {
    decoded = jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }

      const updateResult = await userService.updateUserProfileVerified(
        req.body,
      );
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.fetchTotalVerifiedUser = async (req, res) => {
  // console.log(req.body)
  const {token} = req.body;
  let decoded = '';

  try {
    decoded = jwt.verify(token, config.secret);
    if (decoded) {
      const updateResult = await userService.fetchTotalVerifiedUser();
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.fetchVerifiedUserController = async (req, res) => {
  const {token} = req.body;
  let decoded = '';

  try {
    decoded = jwt.verify(token, config.secret);
    if (decoded) {
      const updateResult = await userService.fetchVerifiedUser(req.body);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
        // accessToken: token
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.registerUserBio = async (req, res) => {
  // console.log(JSON.stringify(req.body));
  const {token} = req.body;
  let decoded = '';
  const result = await userService.findUserByEmail(req.body.userID);
  // console.log(result)
  const fullName = req.body.fullName.replace(/\s+/g, ' ').trim();
  const firstName = fullName.substring(0, fullName.lastIndexOf(' ') + 1);
  const lastName = fullName.substring(
    fullName.lastIndexOf(' ') + 1,
    fullName.length,
  );

  let fName;
  let lName;

  if (firstName === '') {
    fName = lastName;
    lName = '';
  } else {
    fName = firstName;
    lName = lastName;
  }

  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }
      const data = {
        fullName,
        dob: req.body.dob,
        locationText: req.body.locationText,
        gender: req.body.gender,
        religionID: req.body.religionID,
        lat: req.body.lat,
        lon: req.body.lon,
        genderInterest: req.body.genderInterest,
        lookingFor: req.body.lookingFor,
        fName,
        lName,
        userID: req.body.userID,
      };

      const updateResult = await userService.registerUserBio(data);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
        // accessToken: token
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};
exports.editUser = async (req, res) => {
  console.log(JSON.stringify(req.body));
  const {token} = req.body;
  let decoded = '';
  const result = await userService.findUserByEmail(req.body.userID);
  console.log(result);

  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }

      const updateResult = await userService.editUser(req.body);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
        // accessToken: token
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.moreAbout1 = async (req, res) => {
  console.log('Called');
  // const token = req.body.token;
  let decoded = '';
  const result = await userService.findUserByEmail(req.body.userID);

  console.log(req.body);
  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }
      const updateResult = await userService.moreAbout1(req.body);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.moreAbout2 = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  const result = await userService.findUserByEmail(req.body.userID);

  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }
      const updateResult = await userService.moreAbout2(req.body);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.moreAbout3 = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  const result = await userService.findUserByEmail(req.body.userID);

  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }
      const updateResult = await userService.moreAbout3(req.body);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.moreAbout4 = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  const result = await userService.findUserByEmail(req.body.userID);

  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }
      const updateResult = await userService.moreAbout4(req.body);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.lookingFor = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  const result = await userService.findUserByEmail(req.body.userID);

  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }
      const updateResult = await userService.lookingFor(req.body);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.profilePic = async (req, res) => {
  // const token = req.body.token;
  // let decoded = '';
  const result = await userService.findUserByEmail(req.body.userID);

  try {
    const decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }
      const updateResult = await userService.profilePic(req.body);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
      });
    }
  } catch (err) {
    if (res) {
      res.json({
        success: false,
        msg: 'Oops Error!!',
        res: err,
        status: 400,
      });
    }
  }
};

exports.photoVerification = async (req, res) => {
  // console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")

  console.log(req);
  // console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
  // const token = req.body.token;
  // let decoded = '';
  const result = await userService.findUserByEmail(req.body.userID);

  try {
    const decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }
      const updateResult = await userService.photoVerification(req.body);
      if (res) {
        res.status(200).send({
          status: 200,
          message: 'update Successful',
          result: updateResult,
        });
      }
    }
  } catch (err) {
    if (res) {
      res.json({
        success: false,
        msg: 'Oops Error!!',
        res: err,
        status: 400,
      });
    }
  }
};

exports.videoverification = async (req, res) => {
  // const token = req.body.token;
  let decoded = '';
  const result = await userService.findUserByEmail(req.userID);

  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }
      const updateResult = await userService.videoverification(req);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
      });
    }
  } catch (err) {
    if (res) {
      res.json({
        success: false,
        msg: 'Oops Error!!',
        res: err,
        status: 400,
      });
    }
  }
};

exports.userGenderInterst = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  const result = await userService.findUserByEmail(req.body.userID);

  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }
      const updateResult = await userService.userGenderInterst(req.body);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.identifyWith = async (req, res) => {
  // const token = req.body.token;
  // let decoded = '';
  const result = await userService.findUserByEmail(req.body.userID);

  try {
    const decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }
      const updateResult = await userService.identifyWith(req.body);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.galleryPhotos = async (req, res) => {
  // const token = req.body.token;
  // let decoded = '';
  console.log(req.body);
  const result = await userService.findUserByEmail(req.body.userID);
  console.log(result.length);

  try {
    const decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }
      const updateResult = await userService.galleryPhotos(req.body);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
        // accessToken: token
      });
    }
  } catch (err) {
    if (res) {
      res.json({
        success: false,
        msg: 'Oops Error!!',
        res: err,
        status: 400,
      });
    }
  }
};

exports.fetchReligionList = async (req, res) => {
  console.log('fetchReligion controller');
  try {
    const data = await userService.fetchReligionList();
    res.status(200).send({
      status: 200,
      message: 'update Successful',
      result: data,
      // accessToken: token
    });
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};
exports.requestForVerification = async (req, res) => {
  // console.log(JSON.stringify(req.body))
  try {
    const data = await userService.requestForVerification(req.body);
    res.status(200).send({
      status: 200,
      message: 'Verification Successfull',
      result: parseInt(data),
      // accessToken: token
    });
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.updateMatching = async (req, res) => {
  // const token = req.body.token;
  // let decoded = '';
  const result = await userService.findUserByEmail(req.body.userID);

  try {
    const decoded = 1; //  jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(200).send({status: 404, message: 'User not exist!'});
      }
      const updateResult = await userService.updateMatching(req.body);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.fetchMatchingUsers = async (req, res) => {
  // const token = req.body.token;
  // let decoded = '';
  // let result = await userService.findUserByEmail(req.body.userID);

  try {
    const decoded = 1; //  jwt.verify(token, config.secret);
    if (decoded) {
      // if (result.length < 1) {
      //     return res.status(200).send({status:404, message: "User not exist!" });
      // }else{
      const matchingUsers = await userService.fetchMatchingUsers(req.body);
      res.status(200).send({
        status: 200,
        message: 'Matching Users',
        result: matchingUsers.map((data, i) => data.userID),
      });
      // }
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.createChannel = async (req, res) => {
  const {user1} = req.body;
  const {user2} = req.body;
  const {channelname} = req.body;
  const {userID1} = req.body;
  const {userID2} = req.body;
  console.log(req.body);

  try {
    const decoded = 1; //  jwt.verify(token, config.secret);
    if (decoded) {
      // if (result.length < 1) {
      //     return res.status(200).send({status:404, message: "User not exist!" });
      // }else{
      const createChannelResult = await rocketChatService.createChannel(
        user1,
        user2,
        channelname,
      );
      if (Object.entries(createChannelResult).length > 0) {
        const data = {
          channelID: createChannelResult.id,
          channelName: createChannelResult.name,
          userID1,
          userID2,
          username1: user1,
          username2: user2,
        };

        const chatList = await userService.createChatList(data);
        res.status(200).send({
          status: 200,
          message: 'chat created',
          result: chatList,
        });
      } else {
        res.status(400).send({
          status: 400,
          message: 'chat create failed !',
          // result: chatList
        });
      }

      // }
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.sendMessage = async (req, res) => {
  // const token = req.body.token;
  // let decoded = '';
  // let result = await userService.findUserByEmail(req.body.userID);
  // let userID = req.body.userID
  const {userId} = req.body;
  const {authToken} = req.body;
  const {channelId} = req.body;
  const {msg} = req.body;
  console.log('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@2');
  console.log(req.body);

  try {
    const decoded = 1; //  jwt.verify(token, config.secret);
    if (decoded) {
      // if (result.length < 1) {
      //     return res.status(200).send({status:404, message: "User not exist!" });
      // }else{

      const postMessageResult = await rocketChatService
        .postMessage(userId, authToken, channelId, msg)
        .then((response) => {
          userService
            .addLastMsg(response.ts, response.message.msg, channelId)
            .then(
              res.status(200).send({
                status: 200,
                message: '',
                result: response,
              }),
            );
        })
        .catch((error) => console.log(error));

      // }
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.channelList = async (req, res) => {
  // const token = req.body.token;
  // let decoded = '';
  const {userId} = req.body;
  // let authToken = req.body.authToken;

  try {
    const decoded = 1; //  jwt.verify(token, config.secret);
    if (decoded) {
      // if (result.length < 1) {
      //     return res.status(200).send({status:404, message: "User not exist!" });
      // }else{
      const memebersList = [];

      userService
        .fetchChatList(userId)
        .then((response) => {
          res.status(200).send({
            status: 200,
            result: response,
            // memberList:response.channels
          });
        })
        .catch((error) => console.log(error));
    }
  } catch (err) {
    console.log(err);
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};
