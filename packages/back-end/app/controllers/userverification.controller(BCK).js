const axios = require('axios');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const config = require('../config/auth.config');
const userService = require('../services/user.service');
const adminService = require('../services/admin.service');
const urlConfig = require('../config/urlConfig');
const sessionMasterService = require('../services/sessionMaster.service');
const rocketChatService = require('../services/chat.service.v2');

exports.imageVerified = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  const result = await userService.findUserByEmail(req.body.userID);

  try {
    decoded = jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }

      const updateResult = await userService.updateUserImageVerified(req.body);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.videoVerified = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  const result = await userService.findUserByEmail(req.body.userID);

  try {
    decoded = jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }

      const updateResult = await userService.updateUserVideoVerified(req.body);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.profileVerified = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  const result = await userService.findUserByEmail(req.body.userID);

  try {
    decoded = jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }

      const updateResult = await userService.updateUserProfileVerified(
        req.body,
      );
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.fetchTotalVerifiedUser = async (req, res) => {
  // console.log(req.body)
  const {token} = req.body;
  let decoded = '';

  try {
    decoded = jwt.verify(token, config.secret);
    if (decoded) {
      const updateResult = await userService.fetchTotalVerifiedUser();
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.fetchVerifiedUserController = async (req, res) => {
  const {token} = req.body;
  let decoded = '';

  try {
    decoded = jwt.verify(token, config.secret);
    if (decoded) {
      const updateResult = await userService.fetchVerifiedUser(req.body);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
        // accessToken: token
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.registerUserBio = async (req, res) => {
  // console.log(JSON.stringify(req.body));
  const {token} = req.body;
  let decoded = '';
  const result = await userService.findUserByEmail(req.body.userID);
  // console.log(result)
  const fullName = req.body.fullName.replace(/\s+/g, ' ').trim();
  const firstName = fullName.substring(0, fullName.lastIndexOf(' ') + 1);
  const lastName = fullName.substring(
    fullName.lastIndexOf(' ') + 1,
    fullName.length,
  );

  let fName;
  let lName;

  if (firstName === '') {
    fName = lastName;
    lName = '';
  } else {
    fName = firstName;
    lName = lastName;
  }

  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }
      const data = {
        fullName,
        dob: req.body.dob,
        locationText: req.body.locationText,
        gender: req.body.gender,
        religionID: req.body.religionID,
        lat: req.body.lat,
        lon: req.body.lon,
        genderInterest: req.body.genderInterest,
        lookingFor: req.body.lookingFor,
        fName,
        lName,
        userID: req.body.userID,
      };

      const updateResult = await userService.registerUserBio(data);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
        // accessToken: token
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};
exports.editUser = async (req, res) => {
  console.log(JSON.stringify(req.body));
  const {token} = req.body;
  let decoded = '';
  const result = await userService.findUserByEmail(req.body.userID);
  console.log(result);

  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }

      const updateResult = await userService.editUser(req.body);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
        // accessToken: token
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.moreAbout1 = async (req, res) => {
  console.log('Called');
  // const token = req.body.token;
  let decoded = '';
  const result = await userService.findUserByEmail(req.body.userID);

  console.log(req.body);
  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }
      const updateResult = await userService.moreAbout1(req.body);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.moreAbout2 = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  const result = await userService.findUserByEmail(req.body.userID);

  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }
      const updateResult = await userService.moreAbout2(req.body);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.moreAbout3 = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  const result = await userService.findUserByEmail(req.body.userID);

  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }
      const updateResult = await userService.moreAbout3(req.body);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.moreAbout4 = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  const result = await userService.findUserByEmail(req.body.userID);

  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }
      const updateResult = await userService.moreAbout4(req.body);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.lookingFor = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  const result = await userService.findUserByEmail(req.body.userID);

  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }
      const updateResult = await userService.lookingFor(req.body);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.profilePic = async (req, res) => {
  // const token = req.body.token;
  // let decoded = '';
  const result = await userService.findUserByEmail(req.body.userID);

  try {
    const decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }
      const updateResult = await userService.profilePic(req.body);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
      });
    }
  } catch (err) {
    if (res) {
      res.json({
        success: false,
        msg: 'Oops Error!!',
        res: err,
        status: 400,
      });
    }
  }
};

exports.photoVerification = async (req, res) => {
  // console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")

  console.log(req);
  // console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
  // const token = req.body.token;
  // let decoded = '';
  const result = await userService.findUserByEmail(req.body.userID);

  try {
    const decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }
      const updateResult = await userService.photoVerification(req.body);
      if (res) {
        res.status(200).send({
          status: 200,
          message: 'update Successful',
          result: updateResult,
        });
      }
    }
  } catch (err) {
    if (res) {
      res.json({
        success: false,
        msg: 'Oops Error!!',
        res: err,
        status: 400,
      });
    }
  }
};

exports.videoverification = async (req, res) => {
  // const token = req.body.token;
  let decoded = '';
  const result = await userService.findUserByEmail(req.userID);

  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }
      const updateResult = await userService.videoverification(req);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
      });
    }
  } catch (err) {
    if (res) {
      res.json({
        success: false,
        msg: 'Oops Error!!',
        res: err,
        status: 400,
      });
    }
  }
};

exports.userGenderInterst = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  const result = await userService.findUserByEmail(req.body.userID);

  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }
      const updateResult = await userService.userGenderInterst(req.body);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.identifyWith = async (req, res) => {
  // const token = req.body.token;
  // let decoded = '';
  const result = await userService.findUserByEmail(req.body.userID);

  try {
    const decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }
      const updateResult = await userService.identifyWith(req.body);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.galleryPhotos = async (req, res) => {
  // const token = req.body.token;
  let decoded = '';
  console.log(req.body);
  const result = await userService.findUserByEmail(req.body.userID);
  console.log(result.length);

  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }
      const updateResult = await userService.galleryPhotos(req.body);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
        // accessToken: token
      });
    }
  } catch (err) {
    if (res) {
      res.json({
        success: false,
        msg: 'Oops Error!!',
        res: err,
        status: 400,
      });
    }
  }
};

exports.fetchReligionList = async (req, res) => {
  console.log('fetchReligion controller');
  try {
    const data = await userService.fetchReligionList();
    res.status(200).send({
      status: 200,
      message: 'update Successful',
      result: data,
      // accessToken: token
    });
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};
exports.requestForVerification = async (req, res) => {
  // console.log(JSON.stringify(req.body))
  try {
    const data = await userService.requestForVerification(req.body);
    res.status(200).send({
      status: 200,
      message: 'Verification Successfull',
      result: parseInt(data),
      // accessToken: token
    });
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.updateMatching = async (req, res) => {
  // const token = req.body.token;
  let decoded = '';
  const result = await userService.findUserByEmail(req.body.userID);

  try {
    decoded = 1; //  jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(200).send({status: 404, message: 'User not exist!'});
      }
      const updateResult = await userService.updateMatching(req.body);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.fetchMatchingUsers = async (req, res) => {
  // const token = req.body.token;
  let decoded = '';
  // let result = await userService.findUserByEmail(req.body.userID);

  try {
    decoded = 1; //  jwt.verify(token, config.secret);
    if (decoded) {
      // if (result.length < 1) {
      //     return res.status(200).send({status:404, message: "User not exist!" });
      // }else{
      const matchingUsers = await userService.fetchMatchingUsers(req.body);
      res.status(200).send({
        status: 200,
        message: 'Matching Users',
        result: matchingUsers.map((data, i) => data.userID),
      });
      // }
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.createChannel = async (req, res) => {
  // const token = req.body.token;
  let decoded = '';
  // let result = await userService.findUserByEmail(req.body.userID);
  const {user1} = req.body;
  const {user2} = req.body;
  const {channelname} = req.body;

  try {
    decoded = 1; //  jwt.verify(token, config.secret);
    if (decoded) {
      // if (result.length < 1) {
      //     return res.status(200).send({status:404, message: "User not exist!" });
      // }else{
      const createChannelResult = await rocketChatService.createChannel(
        user1,
        user2,
        channelname,
      );
      res.status(200).send({
        status: 200,
        message: '',
        result: createChannelResult,
      });
      // }
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.sendMessage = async (req, res) => {
  // const token = req.body.token;
  let decoded = '';
  // let result = await userService.findUserByEmail(req.body.userID);
  const {userId} = req.body;
  const {authToken} = req.body;
  const {channelId} = req.body;
  const {msg} = req.body;
  console.log('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@2');
  console.log(req.body);

  try {
    decoded = 1; //  jwt.verify(token, config.secret);
    if (decoded) {
      // if (result.length < 1) {
      //     return res.status(200).send({status:404, message: "User not exist!" });
      // }else{

      const postMessageResult = await rocketChatService.postMessage(
        userId,
        authToken,
        channelId,
        msg,
      );

      res.status(200).send({
        status: 200,
        message: '',
        result: postMessageResult,
      });
      // }
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.channelList = async (req, res) => {
  // const token = req.body.token;
  let decoded = '';

  const {userId} = req.body;
  const {authToken} = req.body;

  try {
    decoded = 1; //  jwt.verify(token, config.secret);
    if (decoded) {
      // if (result.length < 1) {
      //     return res.status(200).send({status:404, message: "User not exist!" });
      // }else{
      let memebersList = [];

      rocketChatService
        .channelList(userId, authToken)
        .then(
          (response) => {
            // console.log("$$$$$$$$$$$$$$$$$$$$$$$");
            // console.log(response.channels)
            if (response.channels.length === 0) {
              res.status(200).send({
                status: 201,
                result: 'no channel found',
                memberList: response.channels,
              });
            }
            response.channels.forEach((channel, c_index) => {
              // console.log(channel.lastMessage)
              rocketChatService
                .channelmemeber(userId, authToken, channel._id)
                .then((result) => {
                  console.log('************************');
                  console.log(result);
                  // if(res1.length === 0){
                  //     res.status(200).send({
                  //         status:202,
                  //       msg: "channel found but no user found in database ",
                  //         memberList:response.channels,
                  //         result:res1
                  //     });
                  // }

                  const {members} = JSON.parse(result);
                  console.log(members, userId);
                  members.forEach((member, m_index) => {
                    if (member._id !== userId) {
                      // console.log(member._id , userId)

                      userService
                        .getAllMemberOfChannel(member._id)
                        .then((res1) => {
                          console.log('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
                          console.log(res1);
                          if (res1.length === 0) {
                            res.status(200).send({
                              status: 202,
                              msg: 'channel found but no user found in database ',
                              memberList: response.channels,
                              result: res1,
                            });
                          }
                          // profilePic, fullName, lastmsg,lmt,Rcid
                          memebersList = [
                            ...memebersList,
                            {
                              RCID: member._id,
                              profilePicture: res1[0].profilePicture,
                              fullName: res1[0].fullName,
                              lastMessage:
                                channel.lastMessage !== undefined
                                  ? channel.lastMessage.msg
                                  : '',

                              lastMsgTime:
                                channel.lastMessage !== undefined
                                  ? channel.lastMessage.ts
                                  : '',
                            },
                          ];

                          if (
                            response.channels.length - 1 === c_index &&
                            m_index === members.length - 1
                          ) {
                            res.status(200).send({
                              status: 200,
                              // result: channelListResult,
                              memberList: memebersList,
                            });
                          }
                        });
                    } else {
                      res.status(200).send({
                        status: 203,
                        msg: '',
                      });
                    }
                  });
                })
                .catch((err) => console.log(err));
              // console.log(response.channels.length, c_index)
            });
          },
          // channelListResult.map((dta,i)=>{
          //    let result =  rocketChatService.channelmemeber(userId, authToken,dta._id);
          //    console.log("WWWWWWWWWWWW")

          //    result.members.map((dt,i)=>{
          //     return memebersList.pushresult(dt._id)
          //    })

          // })
        )
        .catch((error) => console.log(error));

      // console.log(memebersList);
      //   console.log("res here")
      // res.status(200).send({
      //     status:200,
      //     message:"",
      //     // result: channelListResult,
      //     memberList:memebersList
      // });
      // }
    }
  } catch (err) {
    console.log(err);
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};
