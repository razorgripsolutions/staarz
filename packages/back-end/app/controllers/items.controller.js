const axios = require('axios');
const urlConfig = require('../config/urlConfig');
const sessionMasterService = require('../services/sessionMaster.service');

exports.getItemsCategoriesByLicenseNumber = async (req, res) => {
  const result = await sessionMasterService.findSessionByUserId(req.userId);

  if (result.length > 0) {
    let data = [];

    await axios
      .get(
        `${urlConfig.METRC_URL}/items/v1/categories?licenseNumber=${req.query.licenseNumber}`,
        {
          auth: {
            username: result[0].username,
            password: result[0].password,
          },
        },
      )
      .then((response) => {
        data = response.data;

        res.status(200).send({
          data,
        });
      })
      .catch((error) =>
        res.status(401).send({
          message: 'Metrc Unauthorized!',
        }),
      );
  } else {
    return res.status(401).send({
      message: 'Metrc Unauthorized!',
    });
  }
};
