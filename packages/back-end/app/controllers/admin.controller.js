const axios = require('axios');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const config = require('../config/auth.config');
const userService = require('../services/user.service');
const adminService = require('../services/admin.service');
const urlConfig = require('../config/urlConfig');
const sessionMasterService = require('../services/sessionMaster.service');

exports.updateReligionList = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  const result = await adminService.findReligionById(req.body.religionName);

  try {
    decoded = jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        const updateResult = await adminService.updateReligionList(req.body);
        res.status(200).send({
          status: 200,
          message: ' Successful',
          result: updateResult,
        });
      } else {
        res.status(200).send({
          status: 400,
          message: ' Religion Already Exist',
          // result: updateResult,
        });
      }
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};
exports.deleteFromReligionList = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  // let result = await adminService.findReligionById(req.body.religionID);

  try {
    decoded = jwt.verify(token, config.secret);
    if (decoded) {
      // if (result.length < 1) {
      //     return res.status(404).send({status:404, message: "User not exist!" });
      // }else {
      const updateResult = await adminService.deleteFromReligionList(req.body);
      res.status(200).send({
        status: 200,
        message: 'delete Successful',
        result: updateResult,
      });
      // }
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.fetchReligionList = async (req, res) => {
  // const token = req.body.token;
  let decoded = '';

  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      const data = await adminService.fetchReligionList();
      res.status(200).send({
        status: 200,
        message: 'Successful',
        result: data,
        // accessToken: token
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.fetchInterestList = async (req, res) => {
  // const token = req.body.token;
  let decoded = '';

  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      const data = await adminService.fetchInterestList();
      res.status(200).send({
        status: 200,
        message: 'Successful',
        result: data,
        // accessToken: token
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.updateInterestList = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  const data = await adminService.fetchInterestById(req.body.interestsName);
  try {
    decoded = jwt.verify(token, config.secret);
    if (decoded) {
      if (data.length < 1) {
        const updateResult = await adminService.updateInterestList(req.body);
        res.status(200).send({
          status: 200,
          message: ' Successful',
          result: updateResult,
        });
      } else {
        res.status(200).send({
          status: 400,
          message: 'interest already exist',
          // result: updateResult,
        });
      }
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.deleteFromInterestList = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  // let result = await adminService.findReligionById(req.body.interestsID);

  try {
    decoded = jwt.verify(token, config.secret);
    if (decoded) {
      // if (result.length < 1) {
      //     return res.status(404).send({status:404, message: "Interest not exist!" });
      // }else {
      const updateResult = await adminService.deleteFromInterestList(req.body);
      res.status(200).send({
        status: 200,
        message: 'delete Successful',
        result: updateResult,
      });
      // }
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.favouriteMusicStyleList = async (req, res) => {
  const {token} = req.body;
  let decoded = '';

  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      const data = await adminService.favouriteMusicStyleList();
      res.status(200).send({
        status: 200,
        message: 'Successful',
        result: data,
        // accessToken: token
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.fetchTravelStyle = async (req, res) => {
  const {token} = req.body;
  let decoded = '';

  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      const data = await adminService.fetchTravelStyle();
      res.status(200).send({
        status: 200,
        message: 'Successful',
        result: data,
        // accessToken: token
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.fetchUserEducationList = async (req, res) => {
  const {token} = req.body;
  let decoded = '';

  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      const data = await adminService.fetchUserEducationList();
      res.status(200).send({
        status: 200,
        message: 'Successful',
        result: data,
        // accessToken: token
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.fetchfavouriteMovieGenre = async (req, res) => {
  const {token} = req.body;
  let decoded = '';

  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      const data = await adminService.fetchfavouriteMovieGenre();
      res.status(200).send({
        status: 200,
        message: 'Successful',
        result: data,
        // accessToken: token
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.fetchRelationShipType = async (req, res) => {
  // const token = req.body.token;
  let decoded = '';

  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      const data = await adminService.fetchRelationShipType();
      res.status(200).send({
        status: 200,
        message: 'Successful',
        result: data,
        // accessToken: token
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.fetchReligion = async (req, res) => {
  // const token = req.body.token;
  let decoded = '';

  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      const data = await adminService.fetchReligion();
      res.status(200).send({
        status: 200,
        message: 'Successful',
        result: data,
        // accessToken: token
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.fetchUserPetList = async (req, res) => {
  // const token = req.body.token;
  let decoded = '';

  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      const data = await adminService.fetchUserPetList();
      res.status(200).send({
        status: 200,
        message: 'Successful',
        result: data,
        // accessToken: token
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.fetchUserOccupationList = async (req, res) => {
  // const token = req.body.token;
  let decoded = '';

  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      const data = await adminService.fetchUserOccupationList();
      res.status(200).send({
        status: 200,
        message: 'Successful',
        result: data,
        // accessToken: token
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.usersList = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  try {
    decoded = jwt.verify(token, config.secret);
    if (decoded) {
      const result = await adminService.fetchAllUser();
      // if (result.length < 1) {
      //     return res.status(404).send({status:404, message: " Users Not Found!" });
      // }

      res.status(200).send({
        status: 200,
        message: 'Success',
        user: result,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.UpdateEducationList = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  try {
    decoded = jwt.verify(token, config.secret);
    if (decoded) {
      const result = await adminService.UpdateEducationList(req.body);
      if (result < 1) {
        return res.status(404).send({status: 404, message: ' allredy exist!'});
      }

      res.status(200).send({
        status: 200,
        message: 'Success',
        user: result,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.deleteEducationList = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  const result = await adminService.fetchEducationByID(req.body.educationID);

  try {
    decoded = jwt.verify(token, config.secret);
    if (decoded) {
      // if (result.length < 1) {
      //     return res.status(404).send({status:404, message: "User not exist!" });
      // }else {
      const updateResult = await adminService.deleteEducationList(req.body);
      res.status(200).send({
        status: 200,
        message: 'delete Successful',
        result: updateResult,
      });
      // }
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.deleteFromPetList = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  // let result = await adminService.findPetById(req.body.petID);

  try {
    decoded = jwt.verify(token, config.secret);
    if (decoded) {
      // if (result.length < 1) {
      //     return res.status(404).send({status:404, message: "Pet not exist!" });
      // }else {
      const updateResult = await adminService.deleteFromPetList(req.body);
      res.status(200).send({
        status: 200,
        message: 'delete Successful',
        result: updateResult,
      });
      // }
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.UpdateUserPetList = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  try {
    decoded = jwt.verify(token, config.secret);
    if (decoded) {
      const result = await adminService.UpdateUserPetList(req.body);
      if (result < 1) {
        return res.status(404).send({status: 404, message: ' allredy exist!'});
      }

      res.status(200).send({
        status: 200,
        message: 'Success',
        user: result,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.updateUserOccupationList = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  const result = await adminService.UserOccupationListByID(
    req.body.occupationName,
  );
  try {
    decoded = jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        const updateResult = await adminService.updateUserOccupationList(
          req.body,
        );
        res.status(200).send({
          status: 200,
          message: ' Successful',
          result: updateResult,
        });
      } else {
        res.status(200).send({
          status: 400,
          message: ' occupation Already Exist',
          // result: updateResult,
        });
      }
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.deleteUserOccupationList = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  const result = await adminService.UserOccupationListByID(
    req.body.occupationID,
  );
  console.log(result);
  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      // if (result.length < 1) {
      //     return res.status(404).send({status:404, message: "occupation not exist!" });
      // }else {
      const updateResult = await adminService.deleteUserOccupationList(
        req.body,
      );
      res.status(200).send({
        status: 200,
        message: 'delete Successful',
        result: updateResult,
      });
      //  }
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.fetchallHobbyList = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      const result = await adminService.fetchallHobbyList();
      res.status(200).send({
        status: 200,
        message: 'Success',
        result,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.updateallHobbyList = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  try {
    const result1 = await adminService.findHobbyByName(req.body.hobbyName);
    decoded = jwt.verify(token, config.secret);
    if (decoded) {
      if (result1.length < 1) {
        const updateResult = await adminService.updateallHobbyList(req.body);
        res.status(200).send({
          status: 200,
          message: ' Successful',
          result: updateResult,
        });
      } else {
        res.status(200).send({
          status: 400,
          message: ' Religion Already Exist',
          // result: updateResult,
        });
      }
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.deleteallHobbyList = async (req, res) => {
  const {token} = req.body;
  let decoded = '';

  try {
    decoded = jwt.verify(token, config.secret);
    if (decoded) {
      const updateResult = await adminService.deleteallHobbyList(
        req.body.hobbyName,
      );
      res.status(200).send({
        status: 200,
        message: 'delete Successful',
        result: updateResult,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.updatePetList = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  const result = await adminService.findPetById(req.body.petType);

  try {
    decoded = jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        const updateResult = await adminService.updatePetList(req.body);
        res.status(200).send({
          status: 200,
          message: ' Successful',
          result: updateResult,
        });
      } else {
        res.status(200).send({
          status: 400,
          message: ' Religion Already Exist',
          // result: updateResult,
        });
      }
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.fetchallMovieList = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      const result = await adminService.fetchallMovieList();
      res.status(200).send({
        status: 200,
        message: 'Success',
        result,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.addAllMovieList = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  const result = await adminService.findMovieByName(req.body.movieName);
  try {
    decoded = jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        const updateResult = await adminService.addAllMovieList(req.body);
        res.status(200).send({
          status: 200,
          message: ' Successful',
          result: updateResult,
        });
      } else {
        res.status(200).send({
          status: 400,
          message: ' Religion Already Exist',
          // result: updateResult,
        });
      }
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.deleteAllMovieList = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  // let result = await adminService.findReligionById(req.body.religionID);

  try {
    decoded = jwt.verify(token, config.secret);
    if (decoded) {
      // if (result.length < 1) {
      //     return res.status(404).send({status:404, message: "User not exist!" });
      // }else {
      const updateResult = await adminService.deleteAllMovieList(req.body);
      res.status(200).send({
        status: 200,
        message: 'delete Successful',
        result: updateResult,
      });
      // }
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};
exports.fetchallMusicList = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      const result = await adminService.fetchallMusicList();
      res.status(200).send({
        status: 200,
        message: 'Success',
        result,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.addAllMusicList = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  const result = await adminService.findMusicByName(req.body.musicName);
  try {
    decoded = jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        const updateResult = await adminService.addAllMusicList(req.body);
        res.status(200).send({
          status: 200,
          message: ' Successful',
          result: updateResult,
        });
      } else {
        res.status(200).send({
          status: 400,
          message: ' Music Already Exist',
          // result: updateResult,
        });
      }
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.deleteAllMusicList = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  // let result = await adminService.findReligionById(req.body.religionID);

  try {
    decoded = jwt.verify(token, config.secret);
    if (decoded) {
      // if (result.length < 1) {
      //     return res.status(404).send({status:404, message: "User not exist!" });
      // }else {
      const updateResult = await adminService.deleteAllMusicList(req.body);
      res.status(200).send({
        status: 200,
        message: 'delete Successful',
        result: updateResult,
      });
      // }
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.fetchAllTravelStyleList = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      const result = await adminService.fetchAllTravelStyleList();
      res.status(200).send({
        status: 200,
        message: 'Success',
        result,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.addAllTravelStyleList = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  const result = await adminService.findTravelStyleByName(
    req.body.travelStyleName,
  );
  try {
    decoded = jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        const updateResult = await adminService.addAllTravelStyleList(req.body);
        res.status(200).send({
          status: 200,
          message: ' Successful',
          result: updateResult,
        });
      } else {
        res.status(200).send({
          status: 400,
          message: ' Religion Already Exist',
          // result: updateResult,
        });
      }
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};
exports.deleteAllTravelStyleList = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  // let result = await adminService.findReligionById(req.body.religionID);

  try {
    decoded = jwt.verify(token, config.secret);
    if (decoded) {
      // if (result.length < 1) {
      //     return res.status(404).send({status:404, message: "User not exist!" });
      // }else {
      const updateResult = await adminService.deleteAllTravelStyleList(
        req.body,
      );
      res.status(200).send({
        status: 200,
        message: 'delete Successful',
        result: updateResult,
      });
      // }
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.fetchJobsList = async (req, res) => {
  const {token} = req.body;
  let decoded = '';

  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      const data = await adminService.fetchJobsList();
      res.status(200).send({
        status: 200,
        message: 'Successful',
        result: data,
        // accessToken: token
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.addJobsList = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  try {
    decoded = jwt.verify(token, config.secret);
    if (decoded) {
      const result = await adminService.addJobsList(req.body);
      if (result < 1) {
        return res.status(404).send({status: 404, message: ' allredy exist!'});
      }

      res.status(200).send({
        status: 200,
        message: 'Success',
        user: result,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.deleteJobsList = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  //  let result = await adminService.fetchEducationByID(req.body.educationID);

  try {
    decoded = jwt.verify(token, config.secret);
    if (decoded) {
      // if (result.length < 1) {
      //     return res.status(404).send({status:404, message: "User not exist!" });
      // }else {
      const data = await adminService.deleteJobsList(req.body);
      res.status(200).send({
        status: 200,
        message: 'delete Successful',
        data,
      });
      // }
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.updateUser = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  //   let result = await adminService.findPetById(req.body.petType);

  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      const updateResult = await adminService.updateUser(req.body);
      res.status(200).send({
        status: 200,
        message: ' Successful',
        result: updateResult,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};
