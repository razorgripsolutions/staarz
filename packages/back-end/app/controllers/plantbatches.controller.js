const axios = require('axios');
const urlConfig = require('../config/urlConfig');
const sessionMasterService = require('../services/sessionMaster.service');
const plantbatchesService = require('../services/plantbatches.service');

exports.getActivePlantBatches = async (req, res) => {
  const result = await sessionMasterService.findSessionByUserId(req.userId);

  if (result.length > 0) {
    let data = [];

    await axios
      .get(
        `${urlConfig.METRC_URL}/plantbatches/v1/active?licenseNumber=${req.query.licenseNumber}`,
        {
          auth: {
            username: result[0].username,
            password: result[0].password,
          },
        },
      )
      .then(async (response) => {
        data = response.data;
        await plantbatchesService.deletePlantBatchesByLicenseNumber(
          req.query.licenseNumber,
        );
        data.forEach((d) => {
          plantbatchesService.savePlantBatches(
            req.userId,
            req.query.licenseNumber,
            d,
          );
        });
        res.status(200).send({
          data,
        });
      })
      .catch((error) =>
        res.status(401).send({
          message: 'Metrc Unauthorized!',
        }),
      );
  } else {
    return res.status(401).send({
      message: 'Metrc Unauthorized!',
    });
  }
};

exports.getPlantBatches = async (req, res) => {
  const {userId} = req;

  plantbatchesService
    .getPlantBatches(userId)
    .then((response) => {
      res.status(200).send({
        response,
      });
    })
    .catch((error) =>
      res.status(401).send({
        message: 'No Data Found...',
      }),
    );
};
