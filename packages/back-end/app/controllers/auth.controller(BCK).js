const axios = require('axios');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const config = require('../config/auth.config');
const userService = require('../services/user.service');
const adminService = require('../services/admin.service');
const urlConfig = require('../config/urlConfig');
const sessionMasterService = require('../services/sessionMaster.service');

exports.usersList = async (req, res) => {
  // const token = req.body.token;
  let decoded = '';
  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      const result = await userService.fetchAllUser();
      if (result.length < 1) {
        return res
          .status(404)
          .send({status: 404, message: ' Users Not Found!'});
      }

      res.status(200).send({
        status: 200,
        message: 'Success',
        user: result,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.deleteUserByEmail = async (req, res) => {
  // console.log(req.body)
  const {token} = req.body;
  let decoded = '';
  try {
    decoded = jwt.verify(token, config.secret);

    if (decoded) {
      const result1 = await userService.findUserByEmail(req.body.userID);
      if (result1.length < 1) {
        return res.status(404).send({status: 404, message: 'Invalid user!'});
      }

      const result = await userService.deleteUserByEmail(req.body.userID);
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'Invalid user!'});
      }

      res.status(200).send({
        status: 200,
        message: 'delete Successful',
        user: result,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.userSignIn = async (req, res) => {
  console.log(req.body);
  const result = await userService.findUserByEmail2(req.body.userEmail);
  if (result.length < 1) {
    return res.status(404).send({status: 404, message: 'Invalid user!'});
  }
  const passwordIsValid = bcrypt.compareSync(
    req.body.userPassword,
    result[0].userPassword,
  );
  let token = '';
  if (!passwordIsValid) {
    return res.status(404).send({status: 404, message: 'Invalid password!'});
  }
  if (req.body.rememberMe === true) {
    token = jwt.sign({id: result[0].userId}, config.secret);
    // console.log("rememberME")
    // console.log(token)
  } else {
    token = jwt.sign({id: result[0].userId}, config.secret, {
      expiresIn: 86400,
    });
    // console.log("not remember")
    // console.log(token)
  }
  // let authority = await roleService.findRoleById(result[0].roleId);
  result[0].userPassword = '';
  // result[0].authority = authority[0].roleType;
  res.status(200).send({
    status: 200,
    message: 'login Successful',
    user: result[0],
    accessToken: token,
  });
};
exports.userFBSignIn = async (req, res) => {
  console.log(req.body.facebookID);
  const result = await userService.findUserByfacebookID(req.body.facebookID);
  if (result.length < 1) {
    return res.status(200).send({status: 404, message: 'Invalid user!'});
  }

  const token = jwt.sign({id: result[0].userId}, config.secret, {
    expiresIn: 86400,
  });

  result[0].userPassword = '';
  // console.log(result);
  res.status(200).send({
    status: 200,
    message: 'login Successful',
    user: result[0],
    accessToken: token,
  });
};
exports.userGoogleSignIn = async (req, res) => {
  console.log(req.body.googleID);
  const result = await userService.findUserByGoogleID(req.body.googleID);
  if (result.length < 1) {
    return res.status(200).send({status: 404, message: 'Invalid user!'});
  }

  const token = jwt.sign({id: result[0].userId}, config.secret, {
    expiresIn: 86400,
  });

  result[0].userPassword = '';
  // console.log(result);
  res.status(200).send({
    status: 200,
    message: 'login Successful',
    user: result[0],
    accessToken: token,
  });
};

exports.userSignUp = async (req, res) => {
  console.log(JSON.stringify(req.body));
  const result = await userService.findUserByEmail2(req.body.userEmail);

  if (result.length > 0) {
    return res.status(404).send({status: 404, message: 'User already exist!'});
  }
  const {fullName} = req.body;
  const {userEmail} = req.body;
  const {userPassword} = req.body;
  const salt = bcrypt.genSaltSync(10);

  bcrypt.hash(userPassword, salt, async (err, hash) => {
    if (err) {
      return res.status(400).send({status: 400, message: 'Error', err});
    }
    const hashuserPassword = hash;
    const data = {
      fullName,
      userEmail,
      userPassword: hashuserPassword,
    };
    const registrationresult = await userService.userSignUp(data);
    return res
      .status(200)
      .send({status: 200, message: 'User registered', registrationresult});
  });
};

exports.userFBSignUp = async (req, res) => {
  // console.log(req.body);

  const result = await userService.findUserByfacebookID(req.body.facebookID);
  if (result.length > 0) {
    return res.status(404).send({status: 404, message: 'User already exist!'});
  }

  const registrationresult = await userService.userSignUpFB(req.body);
  return res
    .status(200)
    .send({status: 200, message: 'User registered', registrationresult});
};

exports.userGoogleSignUp = async (req, res) => {
  // console.log(req.body);

  const result = await userService.findUserByGoogleID(req.body.googleID);
  if (result.length > 1) {
    return res.status(404).send({status: 404, message: 'User already exist!'});
  }

  const registrationresult = await userService.userSignUpGoogle(req.body);
  return res
    .status(200)
    .send({status: 200, message: 'User registered', registrationresult});
};

exports.adminSignIn = async (req, res) => {
  // console.log(req.body);
  const result = await adminService.findUserByEmail(req.body.adminEmail);
  // console.log(result);

  if (result.length < 1) {
    return res.status(200).send({status: 404, message: 'Invalid user!'});
  }
  const passwordIsValidAdmin = bcrypt.compareSync(
    req.body.adminPassword,
    result[0].adminPassword,
  );
  if (!passwordIsValidAdmin) {
    return res.status(200).send({status: 404, message: 'Invalid password!'});
  }
  const token = jwt.sign({id: result[0].userId}, config.secret, {
    expiresIn: 86400, // 24 hours
  });
  // let authority = await roleService.findRoleById(result[0].roleId);
  result[0].userPassword = '';
  // result[0].authority = authority[0].roleType;
  res.status(200).send({
    status: 200,
    message: 'login Successful',
    user: result,
    accessToken: token,
  });
};

exports.adminSignUp = async (req, res) => {
  // console.log(req.body)

  const result = await adminService.findUserByEmail(req.body.adminEmail);
  if (result.length > 0) {
    return res.status(404).send({status: 404, message: 'User already exist!'});
  }
  // const adminID = 12;
  // const fullName = req.body.fullName;
  const {adminEmail} = req.body;
  const {adminPassword} = req.body;
  const salt = bcrypt.genSaltSync(10);

  bcrypt.hash(adminPassword, salt, async (err, hash) => {
    if (err) throw err;
    const hashadminPassword = hash;
    const data = {
      // "adminID":adminID,
      adminEmail,
      adminPassword: hashadminPassword,
    };
    const registrationresult = await adminService.adminSignUp(data);
    return res
      .status(200)
      .send({status: 200, message: 'Admin registered', registrationresult});
  });
};

exports.logout = async (req, res) => {
  sessionMasterService
    .deleteByUserId(req.userId)
    .then((response) => {
      res.status(200).send({
        status: 200,
        message: 'logout Successful',
        response,
      });
    })
    .catch((error) =>
      res.status(400).send({
        status: 400,
        message: 'Already Logout...',
        err: error,
      }),
    );
};

exports.fetchUserWithID = async (req, res) => {
  const result = await userService.findUserByEmail(req.body.userID);
  if (result.length < 1) {
    return res.status(404).send({status: 404, message: 'Invalid user!'});
  }

  res.status(200).send({
    status: 200,
    message: 'Successful',
    user: result[0],
  });
};

exports.socialProfile = async (req, res) => {
  const {token} = req.body;
  let decoded = '';
  const result = await userService.findUserByEmail(req.body.userID);
  // console.log("auth cont");
  // console.log(JSON.stringify(req.body));
  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      if (result.length < 1) {
        return res.status(404).send({status: 404, message: 'User not exist!'});
      }

      const updateResult = await userService.registerUserProfile(req.body);
      res.status(200).send({
        status: 200,
        message: 'update Successful',
        result: updateResult,
        // accessToken: token
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};

exports.usersListFilter = async (req, res) => {
  // const token = req.body.token;
  let decoded = '';
  // console.log("req.body")
  // console.log(req.body)
  try {
    decoded = 1; // jwt.verify(token, config.secret);
    if (decoded) {
      const result = await userService.fetchAllFilterUser(req.body);
      // if (result.length < 1) {
      //     return res.status(200).send({status:404, message: " Data Not Found!" });
      // }

      res.status(200).send({
        status: 200,
        message: 'Success',
        user: result,
      });
    }
  } catch (err) {
    res.json({
      success: false,
      msg: 'Oops Error!!',
      res: err,
      status: 400,
    });
  }
};
