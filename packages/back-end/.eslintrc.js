module.exports = {
  ...require('../../.eslintrc'),
  // root: true,
  env: {
    node: true,
    'jest/globals': true,
  },
};
