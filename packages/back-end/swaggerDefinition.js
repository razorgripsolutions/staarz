const swaggerDefinition = {
  openapi: '3.0.0',
  info: {
    title: 'Staarz',
    version: '1.0.0',
    description: 'Staarz.',
    license: {
      name: 'Licensed Under MIT',
      url: 'https://spdx.org/licenses/MIT.html',
    },
    contact: {
      name: 'admin@rgt.com',
      url: '',
    },
  },
  servers: [
    {
      url: 'http://localhost:7070',
      description: 'Api server',
    },
  ],
};

module.exports = swaggerDefinition;
