const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');
const RocketChatApi = require('rocketchat-api');
const passport = require('passport');
const dotenv = require('dotenv');
const fileUpload = require('express-fileupload');

const swaggerUi = require('swagger-ui-express');
const swaggerJsdoc = require('swagger-jsdoc');
const swaggerDocument = require('./swagger.json');
const swaggerDefinition = require('./swaggerDefinition');

dotenv.config();

const {UserEntity, AuthModule} = require('./app/auth');
const {PaymentModule} = require('./app/payment');
const {UserModule, LikesEntity} = require('./app/users');
const {ChartModule} = require('./app/chat');
const AppDataSource = require('./app/config/datasource');
const {HttpService} = require('./app/core');

const dataSource = new AppDataSource([UserEntity, LikesEntity]).initialize();

passport.serializeUser((user, done) => {
  done(null, user);
});

passport.deserializeUser((user, done) => {
  done(null, user);
});

const app = express();

app.use(bodyParser.json({limit: '500mb', extended: true}));

app.use(express.json());

// Add headers
app.use((req, res, next) => {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');

  // Request methods you wish to allow
  res.setHeader(
    'Access-Control-Allow-Methods',
    'GET, POST, OPTIONS, PUT, PATCH, DELETE',
  );

  // Request headers you wish to allow
  res.setHeader(
    'Access-Control-Allow-Headers',
    'X-Requested-With,content-type',
  );

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});

app.use(cors());
app.options('*', cors());

app.use(
  fileUpload({
    limits: {fileSize: 4 * 1024 * 1024},
    createParentPath: true,
  }),
);

// parse requests of content-type - application/x-www-form-urlencoded
app.use(
  bodyParser.urlencoded({
    limit: '500mb',
    extended: true,
    parameterLimit: 50000,
  }),
);

// require('./app/routes/auth.routes')(app);
// require('./app/routes/inventoryFacilities.routes')(app);
// require('./app/routes/items.routes')(app);
// require('./app/routes/plantbatches.routes')(app);

const options = {
  swaggerDefinition,
  apis: ['./apiDef/*.js'],
};

const swaggerSpec = swaggerJsdoc(options);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

new AuthModule().init({app, dataSource});
new PaymentModule().init({app, dataSource});
new ChartModule().init({HttpService, dataSource});
new UserModule().init({app, dataSource});

app.use(express.static(path.join(__dirname, 'build'))); // Handle React routing, return all requests to React app
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

// set port, listen for requests
const PORT = process.env.PORT || 7070;

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
