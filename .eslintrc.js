module.exports = {
  extends: ['airbnb', 'prettier'],
  plugins: ['prettier', 'unused-imports', 'jest'],
  parser: '@babel/eslint-parser',
  parserOptions: {
    babelOptions: {
      presets: ['@babel/preset-react'],
    },
    requireConfigFile: false,
  },
  rules: {
    'react/sort-comp': 'off',
    'class-methods-use-this': ['off', {enforceForClassFields: false}],
    'no-plusplus': 'off',
    'no-use-before-define': 'off', // TODO - probably requires fixing at some point
    'import/prefer-default-export': 'off', // TODO - probably requires fixing at some point
    'react/prop-types': 'off',
    'react/jsx-props-no-spreading': 'off',
    'react/destructuring-assignment': 'off', // TODO - consider getting it back
    'react/jsx-no-useless-fragment': 'off', // TODO - consider getting it back
    'no-alert': 'off', // TODO - probably requires fixing at some point
    'global-require': 'off',
    'consistent-return': 'off',
    'react-native/no-inline-styles': 'off',
    'prettier/prettier': ['error'],
    'no-unused-vars': 'off',
    'unused-imports/no-unused-imports': 'error',
    'no-console': 'off',
    'no-underscore-dangle': 'off',
    'vars-on-top': 'off', // TODO - probably requires fixing at some point
    'func-names': 'off', // TODO - probably requires fixing at some point
    camelcase: 'off',
    radix: 'off',
    'unused-imports/no-unused-vars': [
      // TODO - Should update when we clean a lot of the unused imports
      'off',
      {
        vars: 'all',
        varsIgnorePattern: '^_',
        args: 'after-used',
        argsIgnorePattern: '^_',
      },
    ],
  },
};
