-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: אפריל 18, 2022 בזמן 10:47 AM
-- גרסת שרת: 5.7.37
-- PHP Version: 7.4.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `producti_staarz`
--

-- --------------------------------------------------------

--
-- מבנה טבלה עבור טבלה `adminUsers`
--


CREATE TABLE `adminUsers` (
  `adminID` int(11) NOT NULL,
  `adminEmail` varchar(255) NOT NULL,
  `adminPassword` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `userRole` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- הוצאת מידע עבור טבלה `adminUsers`
--

INSERT INTO `adminUsers` (`adminID`, `adminEmail`, `adminPassword`, `status`, `userRole`) VALUES
(1, 'admin@mail.com', '$2a$10$Kll4JEgjoS8pgr2W/Rse3udxPOTavWU1Phn7H2BtjEGvJ6XOEKOPS', 1, 1),
(2, 'admin123@mail.com', '$2a$10$3UO3atOw8v7NJuouJRtzf.U/dsmav/Cj2A8to3gBjvpwWrUH/SMMG', 1, 1),
(3, 'raj@raj.raj', '$2a$10$.5gBeWV29Cbv7ADtfnsJ.utDFKLGWyXaKGdn2u7tNq6B2DDZgD9zG', 1, 1),
(4, 'test@test.com', '$2a$10$4XGJX/5SQlnT/K3D8deRn.ZQ7esAh32CYdHdB0SiCGt6O7Fvsv/jW', 1, 1);

-- --------------------------------------------------------

--
-- מבנה טבלה עבור טבלה `allEducationList`
--

CREATE TABLE `allEducationList` (
  `educationID` int(11) NOT NULL,
  `educationName` text NOT NULL,
  `universityName` text NOT NULL,
  `universityLocation` text NOT NULL,
  `universityLat` double NOT NULL,
  `universityLong` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- הוצאת מידע עבור טבלה `allEducationList`
--

INSERT INTO `allEducationList` (`educationID`, `educationName`, `universityName`, `universityLocation`, `universityLat`, `universityLong`) VALUES
(1, 'High School', '', '', 0, 0),
(2, 'Bachelor', '', '', 0, 0),
(3, 'Master', '', '', 0, 0),
(4, 'Doctor', '', '', 0, 0),
(5, 'Prof.', '', '', 0, 0);

-- --------------------------------------------------------

--
-- מבנה טבלה עבור טבלה `allHobbiesList`
--

CREATE TABLE `allHobbiesList` (
  `hobbyID` int(11) NOT NULL,
  `hobbyName` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- הוצאת מידע עבור טבלה `allHobbiesList`
--

INSERT INTO `allHobbiesList` (`hobbyID`, `hobbyName`) VALUES
(2, 'dance'),
(3, 'gym');

-- --------------------------------------------------------

--
-- מבנה טבלה עבור טבלה `allJobsList`
--

CREATE TABLE `allJobsList` (
  `jobID` int(11) NOT NULL,
  `jobName` varchar(255) NOT NULL,
  `jobCompanyName` text NOT NULL,
  `jobLocation` text NOT NULL,
  `jobLat` double NOT NULL,
  `jobLong` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- מבנה טבלה עבור טבלה `allMovieList`
--

CREATE TABLE `allMovieList` (
  `movieID` int(11) NOT NULL,
  `movieName` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- הוצאת מידע עבור טבלה `allMovieList`
--

INSERT INTO `allMovieList` (`movieID`, `movieName`) VALUES
(1, 'Action'),
(2, 'Adventure'),
(3, 'Animated'),
(4, 'Comedy'),
(5, 'Drama'),
(6, 'Fantasy'),
(7, 'Historical'),
(8, 'Horror'),
(9, 'Science Fiction'),
(10, 'Documentary');

-- --------------------------------------------------------

--
-- מבנה טבלה עבור טבלה `allMusicList`
--

CREATE TABLE `allMusicList` (
  `musicID` int(11) NOT NULL,
  `musicName` text NOT NULL,
  `singerName` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- הוצאת מידע עבור טבלה `allMusicList`
--

INSERT INTO `allMusicList` (`musicID`, `musicName`, `singerName`) VALUES
(1, 'Pop', ''),
(2, 'Rock', ''),
(3, 'EDM', ''),
(4, 'Jazz', ''),
(5, 'R&B', ''),
(6, 'Classical', ''),
(7, 'Hip Hop', ''),
(8, 'Country', ''),
(9, 'Blues', ''),
(10, 'Flok', '');

-- --------------------------------------------------------

--
-- מבנה טבלה עבור טבלה `allPetList`
--

CREATE TABLE `allPetList` (
  `petID` int(11) NOT NULL,
  `petType` text NOT NULL,
  `totalPets` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- הוצאת מידע עבור טבלה `allPetList`
--

INSERT INTO `allPetList` (`petID`, `petType`, `totalPets`) VALUES
(1, 'Dog', 0),
(2, 'Cat', 0),
(3, 'tiger', 20);

-- --------------------------------------------------------

--
-- מבנה טבלה עבור טבלה `allTravelStyleList`
--

CREATE TABLE `allTravelStyleList` (
  `travelStyleID` int(11) NOT NULL,
  `travelStyleName` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- הוצאת מידע עבור טבלה `allTravelStyleList`
--

INSERT INTO `allTravelStyleList` (`travelStyleID`, `travelStyleName`) VALUES
(1, 'Hiking'),
(2, 'Luxury'),
(3, 'Safari & Wildlife'),
(4, 'Culinary'),
(5, 'Religious & Spiritual'),
(6, 'Urban'),
(7, 'Hight');

-- --------------------------------------------------------

--
-- מבנה טבלה עבור טבלה `answersList`
--

CREATE TABLE `answersList` (
  `userID` int(11) NOT NULL,
  `questionID` int(11) NOT NULL,
  `answerText` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- מבנה טבלה עבור טבלה `blockList`
--

CREATE TABLE `blockList` (
  `id` bigint(20) NOT NULL,
  `userID` bigint(20) NOT NULL,
  `blockedUser` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- הוצאת מידע עבור טבלה `blockList`
--

INSERT INTO `blockList` (`id`, `userID`, `blockedUser`) VALUES
(2, 530, 432);

-- --------------------------------------------------------

--
-- מבנה טבלה עבור טבלה `chatList`
--

CREATE TABLE `chatList` (
  `chatID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `username` text NOT NULL,
  `channelID` text NOT NULL,
  `channelName` text NOT NULL,
  `lastMsg` text NOT NULL,
  `lastMsgTS` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- הוצאת מידע עבור טבלה `chatList`
--

INSERT INTO `chatList` (`chatID`, `userID`, `username`, `channelID`, `channelName`, `lastMsg`, `lastMsgTS`, `status`) VALUES
(59, 535, 'n0760981', 'ddTHPd22amFrdZ4f6', '5287_1621831852915', 'Hello', 2147483647, 1),
(60, 532, 'n0479395', 'ddTHPd22amFrdZ4f6', '5287_1621831852915', 'Hello', 2147483647, 1),
(61, 535, 'n0760981', '3nhEZjm4YGZWdf9FP', '3666_1621831858465', '0', 0, 1),
(62, 537, 'n02280241', '3nhEZjm4YGZWdf9FP', '3666_1621831858465', '0', 0, 1),
(63, 535, 'n0760981', 'LexWbCHY2ig4sw5uC', '4842_1621831864193', '0', 0, 1),
(64, 538, 'n06592883', 'LexWbCHY2ig4sw5uC', '4842_1621831864193', '0', 0, 1);

-- --------------------------------------------------------

--
-- מבנה טבלה עבור טבלה `identifyList`
--

CREATE TABLE `identifyList` (
  `identifyID` int(11) NOT NULL,
  `identifyText` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- מבנה טבלה עבור טבלה `interests`
--

CREATE TABLE `interests` (
  `interestsID` int(11) NOT NULL,
  `interestsName` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- הוצאת מידע עבור טבלה `interests`
--

INSERT INTO `interests` (`interestsID`, `interestsName`) VALUES
(1, 'Travel'),
(2, 'Outdoors'),
(3, 'Smoke'),
(4, 'Tattoo'),
(5, 'Music'),
(6, 'Books'),
(7, 'Hiking'),
(8, 'Adventure'),
(9, 'Fishing'),
(10, 'Hunting'),
(11, 'Camping'),
(12, 'Beach'),
(13, 'Eat'),
(14, 'Cook'),
(15, 'Video Games'),
(16, 'Netflix'),
(17, 'Movies'),
(18, 'Write'),
(19, 'Dancing'),
(20, 'Football'),
(21, 'Concert'),
(22, 'Active'),
(23, 'Work'),
(24, 'Beer'),
(25, 'Drink'),
(26, 'Food'),
(27, 'Coffee'),
(28, 'Wine'),
(29, 'Pizza'),
(30, 'Family'),
(31, 'Jokes'),
(32, 'Lough'),
(33, 'God'),
(34, 'Animals'),
(35, 'Dog'),
(36, 'Pet'),
(37, 'Cat'),
(38, 'Sports'),
(39, 'Gym'),
(40, 'Walks'),
(41, 'Nerd'),
(42, 'Studying');

-- --------------------------------------------------------

--
-- מבנה טבלה עבור טבלה `introVideoList`
--

CREATE TABLE `introVideoList` (
  `introVideoID` int(11) NOT NULL,
  `videoLink` text NOT NULL,
  `videoSize` int(11) NOT NULL,
  `approvalMessage` text NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- מבנה טבלה עבור טבלה `matchingTable`
--

CREATE TABLE `matchingTable` (
  `id` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `matchUserID` int(11) NOT NULL,
  `matchStatus` int(11) NOT NULL,
  `timestamp` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- הוצאת מידע עבור טבלה `matchingTable`
--

INSERT INTO `matchingTable` (`id`, `userID`, `matchUserID`, `matchStatus`, `timestamp`) VALUES
(4, 532, 531, 1, '1621146897385'),
(5, 532, 534, 1, '1621147731110'),
(6, 532, 535, 1, '1621147856205'),
(7, 537, 531, 1, '1621151131643'),
(8, 537, 535, 1, '1621153681787'),
(9, 537, 534, 1, '1621153745002'),
(51, 538, 531, 1, '1621331467425'),
(52, 538, 534, 1, '1621331475558'),
(53, 538, 535, 1, '1621331481597'),
(55, 531, 529, 1, '1621331762405'),
(56, 531, 530, 1, '1621331769569'),
(57, 531, 532, 1, '1621331782603'),
(58, 531, 537, 1, '1621331800580'),
(59, 531, 538, 1, '1621331815641'),
(126, 535, 530, 1, '1621831848547'),
(127, 535, 532, 1, '1621831852366'),
(128, 535, 537, 1, '1621831857932'),
(129, 535, 538, 1, '1621831863770'),
(130, 535, 539, 1, '1621831869332');

-- --------------------------------------------------------

--
-- מבנה טבלה עבור טבלה `questionsList`
--

CREATE TABLE `questionsList` (
  `questionID` int(11) NOT NULL,
  `questionText` text NOT NULL,
  `questionScreen` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- מבנה טבלה עבור טבלה `relationShipType`
--

CREATE TABLE `relationShipType` (
  `relationShipID` int(11) NOT NULL,
  `relationShipName` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- הוצאת מידע עבור טבלה `relationShipType`
--

INSERT INTO `relationShipType` (`relationShipID`, `relationShipName`) VALUES
(1, 'Long term'),
(2, 'Casual'),
(3, 'Friendship'),
(4, 'Friends with benefits'),
(5, 'Not sure');

-- --------------------------------------------------------

--
-- מבנה טבלה עבור טבלה `religionList`
--

CREATE TABLE `religionList` (
  `religionID` int(11) NOT NULL,
  `religionName` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- הוצאת מידע עבור טבלה `religionList`
--

INSERT INTO `religionList` (`religionID`, `religionName`) VALUES
(1, 'Jewish'),
(2, 'Christain'),
(3, 'other');

-- --------------------------------------------------------

--
-- מבנה טבלה עבור טבלה `userEducationList`
--

CREATE TABLE `userEducationList` (
  `educationID` int(11) NOT NULL,
  `userID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- מבנה טבלה עבור טבלה `userHobbiesList`
--

CREATE TABLE `userHobbiesList` (
  `hobbyID` int(11) NOT NULL,
  `userID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- מבנה טבלה עבור טבלה `userJobList`
--

CREATE TABLE `userJobList` (
  `jobID` int(11) NOT NULL,
  `userID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- מבנה טבלה עבור טבלה `userMediaList`
--

CREATE TABLE `userMediaList` (
  `mediaID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `mediaOriginalName` text NOT NULL,
  `mediaSystemName` text NOT NULL,
  `mediaSize` double NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- מבנה טבלה עבור טבלה `userMovieList`
--

CREATE TABLE `userMovieList` (
  `movieID` int(11) NOT NULL,
  `userID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- הוצאת מידע עבור טבלה `userMovieList`
--

INSERT INTO `userMovieList` (`movieID`, `userID`) VALUES
(2, 353),
(2, 353),
(2, 353),
(3, 360),
(3, 384),
(3, 384),
(2, 385),
(4, 404),
(4, 404),
(4, 404),
(1, 410),
(3, 411),
(4, 412),
(2, 415),
(6, 416),
(4, 417),
(4, 418),
(2, 420),
(2, 421),
(3, 425),
(2, 427),
(2, 430),
(3, 433),
(6, 437),
(2, 474),
(3, 482),
(3, 483),
(6, 486),
(3, 487),
(3, 488),
(3, 489),
(3, 495),
(3, 496),
(2, 497),
(4, 498),
(4, 499),
(4, 500),
(3, 501),
(4, 502),
(4, 521),
(2, 529),
(2, 530),
(2, 531),
(1, 532),
(3, 533),
(3, 534),
(2, 535),
(2, 537),
(4, 538);

-- --------------------------------------------------------

--
-- מבנה טבלה עבור טבלה `userMusicList`
--

CREATE TABLE `userMusicList` (
  `musicID` int(11) NOT NULL,
  `userID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- הוצאת מידע עבור טבלה `userMusicList`
--

INSERT INTO `userMusicList` (`musicID`, `userID`) VALUES
(1, 353),
(4, 353),
(4, 353),
(3, 360),
(3, 364),
(2, 384),
(2, 384),
(4, 386),
(4, 385),
(0, 404),
(2, 404),
(3, 410),
(4, 411),
(2, 412),
(3, 415),
(3, 416),
(4, 417),
(2, 418),
(4, 420),
(2, 421),
(2, 425),
(1, 427),
(4, 430),
(2, 433),
(7, 437),
(4, 474),
(1, 482),
(1, 483),
(6, 486),
(2, 487),
(2, 488),
(3, 489),
(3, 495),
(3, 496),
(2, 497),
(2, 498),
(1, 499),
(6, 500),
(1, 501),
(3, 502),
(6, 521),
(1, 529),
(1, 530),
(1, 531),
(2, 532),
(1, 533),
(2, 534),
(2, 535),
(2, 537),
(1, 538);

-- --------------------------------------------------------

--
-- מבנה טבלה עבור טבלה `userOccupationList`
--

CREATE TABLE `userOccupationList` (
  `occupationID` int(11) NOT NULL,
  `occupationName` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- הוצאת מידע עבור טבלה `userOccupationList`
--

INSERT INTO `userOccupationList` (`occupationID`, `occupationName`) VALUES
(1, 'Bussiness owner'),
(2, 'Doctor'),
(3, 'Lawyer'),
(4, 'Teacher'),
(5, 'Driver');

-- --------------------------------------------------------

--
-- מבנה טבלה עבור טבלה `userPetList`
--

CREATE TABLE `userPetList` (
  `petID` int(11) NOT NULL,
  `userID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- הוצאת מידע עבור טבלה `userPetList`
--

INSERT INTO `userPetList` (`petID`, `userID`) VALUES
(4, 353),
(1, 353),
(1, 353),
(2, 360),
(2, 384),
(2, 384),
(1, 385),
(1, 404),
(1, 404),
(1, 404),
(2, 410),
(2, 411),
(1, 412),
(1, 415),
(1, 416),
(2, 417),
(1, 418),
(1, 420),
(2, 421),
(1, 425),
(2, 427),
(2, 430),
(2, 433),
(1, 437),
(2, 474),
(2, 482),
(3, 483),
(1, 486),
(2, 487),
(1, 488),
(2, 489),
(2, 495),
(2, 496),
(3, 497),
(3, 498),
(3, 499),
(1, 500),
(3, 501),
(3, 502),
(3, 521),
(3, 529),
(3, 530),
(3, 531),
(1, 532),
(2, 533),
(3, 534),
(2, 535),
(3, 537),
(3, 538);

-- --------------------------------------------------------

--
-- מבנה טבלה עבור טבלה `users`
--

CREATE TABLE `users` (
  `userID` int(11) NOT NULL,
  `userEmail` varchar(255) NOT NULL,
  `userPassword` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `facebookToken` text NOT NULL,
  `googleToken` text NOT NULL,
  `facebookID` text NOT NULL,
  `googleID` text NOT NULL,
  `RCID` text NOT NULL,
  `fbAuthStatus` int(11) NOT NULL,
  `googleAuthStatus` int(11) NOT NULL,
  `fullName` text NOT NULL,
  `firstName` text NOT NULL,
  `lastName` text NOT NULL,
  `dob` date NOT NULL,
  `locationText` text NOT NULL,
  `lat` double NOT NULL,
  `lon` double NOT NULL,
  `gender` int(11) NOT NULL,
  `religionID` int(11) NOT NULL,
  `introVideoID` int(11) NOT NULL,
  `profilePicture` text NOT NULL,
  `height` double NOT NULL,
  `weight` double NOT NULL,
  `alternateLocation` text NOT NULL,
  `haveCar` int(11) NOT NULL,
  `doesSmoke` int(11) NOT NULL,
  `aboutMe` text NOT NULL,
  `relationshipType` int(11) NOT NULL,
  `packageID` int(11) NOT NULL,
  `genderInterest` int(11) NOT NULL,
  `identifyID` int(11) NOT NULL,
  `emailVerified` int(11) NOT NULL,
  `isImageVerified` int(11) NOT NULL,
  `isVideoVerified` int(11) NOT NULL,
  `isProfileVerified` int(11) NOT NULL,
  `photoVerificationPath` text NOT NULL,
  `videoVerificationPath` text NOT NULL,
  `identifyWith` int(11) NOT NULL,
  `galleryPhotos1` text NOT NULL,
  `galleryPhotos2` text NOT NULL,
  `galleryPhotos3` text NOT NULL,
  `galleryPhotos4` text NOT NULL,
  `galleryPhotos5` text NOT NULL,
  `galleryPhotos6` text NOT NULL,
  `galleryVideo1` text NOT NULL,
  `galleryVideo2` text NOT NULL,
  `galleryVideo3` text NOT NULL,
  `fbProfileLink` text NOT NULL,
  `instaProfileLink` text NOT NULL,
  `favoritSong` text NOT NULL,
  `bestHobby` text NOT NULL,
  `favoritePet` int(11) NOT NULL,
  `travelToLike` int(11) NOT NULL,
  `occupation` int(11) NOT NULL,
  `job` text NOT NULL,
  `company` text NOT NULL,
  `education` int(11) NOT NULL,
  `school` text NOT NULL,
  `interest1` int(11) NOT NULL,
  `interest2` int(11) NOT NULL,
  `interest3` int(11) NOT NULL,
  `interest4` int(11) NOT NULL,
  `interest5` int(11) NOT NULL,
  `yourHobby` text NOT NULL,
  `yourMusic` int(11) NOT NULL,
  `yourMovie` int(11) NOT NULL,
  `anyPet` text NOT NULL,
  `lookingFor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- הוצאת מידע עבור טבלה `users`
--

INSERT INTO `users` (`userID`, `userEmail`, `userPassword`, `username`, `facebookToken`, `googleToken`, `facebookID`, `googleID`, `RCID`, `fbAuthStatus`, `googleAuthStatus`, `fullName`, `firstName`, `lastName`, `dob`, `locationText`, `lat`, `lon`, `gender`, `religionID`, `introVideoID`, `profilePicture`, `height`, `weight`, `alternateLocation`, `haveCar`, `doesSmoke`, `aboutMe`, `relationshipType`, `packageID`, `genderInterest`, `identifyID`, `emailVerified`, `isImageVerified`, `isVideoVerified`, `isProfileVerified`, `photoVerificationPath`, `videoVerificationPath`, `identifyWith`, `galleryPhotos1`, `galleryPhotos2`, `galleryPhotos3`, `galleryPhotos4`, `galleryPhotos5`, `galleryPhotos6`, `galleryVideo1`, `galleryVideo2`, `galleryVideo3`, `fbProfileLink`, `instaProfileLink`, `favoritSong`, `bestHobby`, `favoritePet`, `travelToLike`, `occupation`, `job`, `company`, `education`, `school`, `interest1`, `interest2`, `interest3`, `interest4`, `interest5`, `yourHobby`, `yourMusic`, `yourMovie`, `anyPet`, `lookingFor`) VALUES
(530, 'n02@tec.com', '$2a$10$Izh978HzYXA3B7/d0r7YVOB6c88P7KgMGzHIX1CeXZYF7tDe5OEBq', 'n0276800', '', '', '', '', 'moSRKaJf9BJhJLqbP', 0, 0, 'N2', 'N2', '', '1991-08-22', 'Tel Yosef, Israel', 32.556704, 35.398914, 0, 3, 0, '', 10.5, 0, '', 1, 0, 'Bmmbhmfgmy hyhrthrh', 0, 0, 2, 0, 1, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '', '', '', 'N', '', '', '', 0, 0, 0, '', 'Gfhg', 3, '0', 18, 6, 9, 10, 5, '', 0, 0, '', 0),
(531, 'n03@tec.com', '$2a$10$fXzFa4M9ag5Q/VLM.5SUBO1w/M9OMGghk.qiolm76n8oQVKJRNE5K', 'n038308', '', '', '', '', 'iN4wezuBQTQ4Hw2Wq', 0, 0, 'N3', 'N3', '', '1991-08-22', 'Tel Yosef, Israel', 32.556704, 35.398914, 1, 3, 0, '', 0, 0, '', 0, 0, 'Gang tut. Jet jr fthfth', 0, 0, 0, 0, 1, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '', '', '', 'J', '', '', '', 0, 0, 0, '', 'Dress dr', 3, '0', 4, 2, 3, 7, 18, '', 0, 0, '', 1),
(532, 'n04@tec.com', '$2a$10$JTP.Ur9i.VsjNp9lmtFDA.0oYCKsCvgYJDDZGPwkN1CPazskEBM02', 'n0479395', '', '', '', '', '4EscjfrESe5ts4ZMi', 0, 0, 'N4', 'N4', '', '1991-08-22', 'Tel Yosef, Israel', 32.556704, 35.398914, 0, 1, 0, '', 0, 0, '', 0, 0, 'Uioyuoybrrt. Try y they jtyj by jtr rat', 0, 0, 1, 0, 1, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '', '', '', 'L', '', '', '', 0, 0, 0, '', 'Ghjgjgj gy', 3, '0', 3, 8, 7, 18, 14, '', 0, 0, '', 1),
(534, 'n06@tec.com', '$2a$10$B5FkHJHZKQcpdKxhJmY.SuxNo9wNh10JZ2NBnhhGRqQoNargQjLwK', 'n0662035', '', '', '', '', 'NoLxeYHWMZz3YmApW', 0, 0, 'N', 'N', '', '1991-08-22', 'Tel Yosef, Israel', 32.556704, 35.398914, 1, 1, 0, '', 0, 0, '', 0, 0, 'Veggie t jftjftjftjfgj fj ', 0, 0, 0, 0, 1, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '', '', '', 'H', '', '', '', 0, 0, 1, '', 'Ghhg', 4, '1', 19, 18, 15, 5, 1, '', 0, 0, '', 1),
(535, 'n07@tec.com', '$2a$10$/WvZgFGVlC14ddQoi9bRhu9aybnFuQjyEJfuqDNbbbjkpBwSBhUdq', 'n0760981', '', '', '', '', 'PPWuR8fXH7uuPE8x8', 0, 0, 'G', 'G', '', '1991-08-22', 'Tel Yosef, Israel', 32.556704, 35.398914, 1, 1, 0, '', 0, 0, '', 0, 0, ',kgjkfjtshvrvr', 0, 0, 0, 0, 1, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '', '', '', 'K', '', '', '', 0, 0, 2, '', 'Did d gesg', 2, '0', 33, 34, 5, 9, 18, '', 0, 0, '', 2),
(537, 'n022@tec.com', '$2a$10$swZo/cIp8stOkOD//yNjuO47ypH/vQLf74jmJ7gz/9f27dyraKaFG', 'n02280241', '', '', '', '', '8RJbR7SQxx72378qK', 0, 0, 'Nitin', 'Nitin', '', '1992-08-22', 'Tel Yosef, Israel', 32.556704, 35.398914, 0, 2, 0, '', 0, 0, '', 0, 0, 'Y Saturday ftyyntnii tyg6b7kj t7bv6b6b7bk77b7bni7i', 0, 0, 1, 0, 1, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '', '', '', 'K', '', '', '', 0, 0, 1, '', 'Tyeryrt', 3, '0', 27, 28, 4, 9, 14, '', 0, 0, '', 1),
(538, 'n065@tec.com', '$2a$10$pkSUVr1hM86UPCySBfEhEeaatkQHgJCogJZOfylTUv1qdgY8gdKCi', 'n06592883', '', '', '', '', 'FSu7vMrK8txHCgau5', 0, 0, 'Shubham', 'Shubham', '', '1996-08-19', 'Tel Yosef, Israel', 32.556704, 35.398914, 0, 1, 0, '', 0, 0, '', 0, 0, 'Er Gergen they’d get fee fe fe feet fed. Eyed ewf Hewitt egg ref Gregg rut fe fe', 0, 0, 1, 0, 1, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '', '', '', 'L', '', '', '', 0, 0, 1, '', 'Fdsfdfd', 3, '1', 12, 8, 11, 14, 13, '', 0, 0, '', 3),
(539, 'n098@tec.com', '$2a$10$vloto/LO9FV7iwwhkW0TYeMI2Vas4.gAbVQWe.F34AAyDoND0gBIe', 'n09851032', '', '', '', '', 'm649pdmpQX7e7q76q', 0, 0, 'Neha', 'Neha', '', '1996-08-22', 'Unnamed Road, Tel Yosef, Israel', 32.5529009, 35.3956043, 0, 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, 0, 1, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', 0, '', 0, 0, 0, 0, 0, '', 0, 0, '', 0),
(544, 'holly@gmail.com', '$2a$10$oPpl0iL7nrLQxmMYdFN4YOEAJ1ExZtaCbkYPizNqsCuTTGRP8cyvq', 'holly22624', '', '', '', '', 'BWJHkr7i8Kon9TxeR', 0, 0, 'holly dhakad', 'holly ', 'dhakad', '1997-02-13', 'Mandsaur, Madhya Pradesh, India', 24.0734356, 75.0679018, 0, 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, 0, 1, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', 0, '', 0, 0, 0, 0, 0, '', 0, 0, '', 0),
(545, 'yash@gmail.com', '$2a$10$6pb6O3fAGCBze4H9TcAfQOvaCvIC7WcknMnJYpk2evHMGVxF5JX5C', 'yash5677', '', '', '', '', 'J3t4R8onEw8bRpszb', 0, 0, '', '', '', '0000-00-00', '', 0, 0, 0, 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, 0, 1, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', 0, '', 0, 0, 0, 0, 0, '', 0, 0, '', 0),
(546, 'usee@gmail.com', '$2a$10$xumrD5VJpMMvQel34D7MReNb4eeVojTsyTvkfemaJUNlclnfcalNm', 'usee90835', '', '', '', '', 'onp6cSrjHT8JnRc37', 0, 0, '', '', '', '1997-08-22', '', 0, 0, 0, 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, 0, 1, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', 0, '', 0, 0, 0, 0, 0, '', 0, 0, '', 0),
(547, 'asd@gmail.com', '$2a$10$bMADSDA/7NOf4g214nDT8.rTgN3l.9yDVYT4f3ROPHHU3jYSczO4C', 'asd19370', '', '', '', '', 'G3Gy5ErTbrFDxENZD', 0, 0, '', '', '', '1997-08-22', '', 0, 0, 0, 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, 0, 1, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '', '', '', 'https://m.facebook.com/photo.php?fbid=139161091578472&id=100064537442479&set=a.139161111578470&source=11&refid=17&ref=m_upload_pic&__tn__=H-R', '', '', '', 0, 0, 0, '', '', 0, '', 0, 0, 0, 0, 0, '', 0, 0, '', 0),
(548, 'dinesh@gmail.com', '$2a$10$NlymZECR38odaHa/w8eL4OCi92UMhBfelzhJG7ROoAKux7JC/UmQ.', 'dinesh38306', '', '', '', '', '6vB3rRBZd9Sq52L7K', 0, 0, '', '', '', '1997-08-22', '', 0, 0, 0, 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, 0, 1, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '', '', '', 'https://m.facebook.com/photo.php?fbid=139161091578472&id=100064537442479&set=a.139161111578470&source=11&refid=17&ref=m_upload_pic&__tn__=H-R', '', '', '', 0, 0, 0, '', '', 0, '', 0, 0, 0, 0, 0, '', 0, 0, '', 0),
(549, 'vk@gmail.com', '$2a$10$krvWnNY3OGm0/GMYggmlE.yEa4xjajhBDnzjqujfKEdW4C60ZgXYm', 'vk72928', '', '', '', '', 'FpagbohKg2znAWdsM', 0, 0, '', '', '', '0000-00-00', '', 0, 0, 0, 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, 0, 1, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', 0, '', 0, 0, 0, 0, 0, '', 0, 0, '', 0),
(550, 'kkkkk@gmail.com', '$2a$10$io4upCDy/y6bhF0n0z76yOpqDJdld0Rp84YpBG6brjnCPVKE1/mlq', 'kkkkk85703', '', '', '', '', 'TjC687qStRznerRkq', 0, 0, '', '', '', '1997-08-22', '', 0, 0, 0, 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, 0, 1, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '', '', '', 'https://m.facebook.com/photo.php?fbid=139161091578472&id=100064537442479&set=a.139161111578470&source=11&refid=17', '', '', '', 0, 0, 0, '', '', 0, '', 0, 0, 0, 0, 0, '', 0, 0, '', 0),
(551, 'kk@gmail.com', '$2a$10$zKcPECmfbEMWLzEhfqG/Y.BmFVd8FOJ9X264iSgre05sQnZsjJ7Ge', 'kk69343', '', '', '', '', 'xe627HgzKBnzSoXr7', 0, 0, '', '', '', '1997-08-22', '', 0, 0, 0, 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, 0, 1, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '', '', '', 'https://m.facebook.com/photo.php?fbid=139161091578472&id=100064537442479&set=a.139161111578470&source=11&refid=17', '', '', '', 0, 0, 0, '', '', 0, '', 0, 0, 0, 0, 0, '', 0, 0, '', 0),
(552, 'qwe@gmail.com', '$2a$10$LZCWDLUsaLH2T3Zm.Uuo0eA6qYj97FyUwTsUfeFcD0v2XbqMbhGCy', 'qwe33440', '', '', '', '', 'CP84TCy9cNo3RN23u', 0, 0, '', '', '', '1997-08-22', '', 0, 0, 0, 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, 0, 1, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '', '', '', 'https://m.facebook.com/photo.php?fbid=139161091578472&id=100064537442479&set=a.139161111578470&source=11&refid=17', '', '', '', 0, 0, 0, '', '', 0, '', 0, 0, 0, 0, 0, '', 0, 0, '', 0),
(553, 'mn@gmail.com', '$2a$10$CEGfXNnj5m/fo3gItIsT8e2tNWJ.eNjF6cJVUBJk7fTruP8E5wjNa', 'mn68382', '', '', '', '', '2XkMeQ8tt4b9sFPfN', 0, 0, '', '', '', '1997-08-22', '', 0, 0, 0, 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, 0, 1, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '', '', '', 'https://m.facebook.com/photo.php?fbid=139161091578472&id=100064537442479&set=a.139161111578470&source=11&refid=17', '', '', '', 0, 0, 0, '', '', 0, '', 0, 0, 0, 0, 0, '', 0, 0, '', 0),
(554, 'mm@gmail.com', '$2a$10$UcbrZ7QcyzkcYdYA6qMeluTJFe47jJUGrZI8Sj0SJvCJ8MkE/gNXS', 'mm73950', '', '', '', '', '5CdNNkbwFhHPLeMsd', 0, 0, '', '', '', '0000-00-00', '', 0, 0, 0, 0, 0, '', 0, 0, 'Sheikh Zayed Rd - Al BarshaAl Barsha 1 - Dubai - United Arab Emirates', 0, 1, '', 0, 0, 0, 0, 1, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '', '', '', 'https://m.facebook.com/photo.php?fbid=139161091578472&id=100064537442479&set=a.139161111578470&source=11&refid=17', '', '', '', 0, 0, 0, '', '', 0, '', 0, 0, 0, 0, 0, '', 0, 0, '', 0),
(555, 'rk@gmail.com', '$2a$10$R7lrffYNLxdrxxw9/QrMbOLcdA7rlyI8KHp7NMXs5I2Khkgw5u76W', 'rk54677', '', '', '', '', '3d9qnoymdGcQM7ZzT', 0, 0, '', '', '', '1997-08-22', '', 0, 0, 0, 0, 0, '', 0, 0, '', 1, 0, '', 0, 0, 0, 0, 1, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '', '', '', 'https://m.facebook.com/photo.php?fbid=139161091578472&id=100064537442479&set=a.139161111578470&source=11&refid=17', '', '', '', 0, 0, 0, '', '', 0, '', 0, 0, 0, 0, 0, '', 0, 0, '', 0),
(556, 'mmm@gmail.com', '$2a$10$c01eE9lmWKpbb.L5vGYDf.qlEvjJ2TeWlsQIdpPpCvrx9Qj4jPeXq', 'mmm39668', '', '', '', '', 'cfG9Jh7bHZKezETqK', 0, 0, '', '', '', '0000-00-00', '', 0, 0, 0, 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, 0, 1, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '', '', '', 'https://m.facebook.com/photo.php?fbid=139161091578472&id=100064537442479&set=a.139161111578470&source=11&refid=17', '', '', '', 0, 0, 0, '', '', 0, '', 0, 0, 0, 0, 0, '', 0, 0, '', 0),
(557, 'rkrk@gmail.com', '$2a$10$mbBIxd1HZC.RFjNqjnp8Dugwp8mVxfn0AIaKTjrqm.I7a99o/72AO', 'rkrk31004', '', '', '', '', 'bxkcEhfwf32S9Fwnu', 0, 0, '', '', '', '0000-00-00', '', 0, 0, 0, 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, 0, 1, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '', '', '', 'https://m.facebook.com/photo.php?fbid=139161091578472&id=100064537442479&set=a.139161111578470&source=11&refid=17', '', '', '', 0, 0, 0, '', '', 0, '', 0, 0, 0, 0, 0, '', 0, 0, '', 0),
(558, 'kr@gmail.com', '$2a$10$NK76hJISdglJkrU76v0KOO0/MBBbo/WZQDoMQSEkFSDuE72lawq.y', 'kr669', '', '', '', '', 'Teu8yPyeJHgrna2JX', 0, 0, '', '', '', '0000-00-00', '', 0, 0, 0, 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, 0, 1, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '', '', '', 'https://m.facebook.com/photo.php?fbid=139161091578472&id=100064537442479&set=a.139161111578470&source=11&refid=17', '', '', '', 0, 0, 0, '', '', 0, '', 0, 0, 0, 0, 0, '', 0, 0, '', 0),
(559, 'mmkk@gmail.com', '$2a$10$W4nKNK.Yy7pwojKWYICocuzLdrh.SP8ieIX3gANNv12ly2XjIvmaK', 'mmkk80065', '', '', '', '', '2hc4rvvgorax5xpaz', 0, 0, '', '', '', '0000-00-00', '', 0, 0, 0, 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, 0, 1, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '', '', '', 'https://m.facebook.com/photo.php?fbid=139161091578472&id=100064537442479&set=a.139161111578470&source=11&refid=17', '', '', '', 0, 0, 0, '', '', 0, '', 0, 0, 0, 0, 0, '', 0, 0, '', 0),
(560, 'll@gmail.com', '$2a$10$OpHTAtjH7AGYZ3wO10vlkuYtMaTNA2i72AMTo.ScuHcDP4Q4EmAZS', 'll59274', '', '', '', '', 'usjTrFuLy9HMRhBvH', 0, 0, '', '', '', '0000-00-00', '', 0, 0, 0, 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, 0, 1, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '', '', '', 'https://m.facebook.com/photo.php?fbid=139161091578472&id=100064537442479&set=a.139161111578470&source=11&refid=17', '', '', '', 0, 0, 0, '', '', 0, '', 2, 0, 0, 0, 0, '', 0, 0, '', 0),
(561, 'oo@gmail.com', '$2a$10$blX0SIBJttcIOwqgFEZ.2O.zpVoK1cQrJyEjZS1Qgs6fUcXp.m1eu', 'oo82730', '', '', '', '', 'rsXFEoXakSuqGGG7Y', 0, 0, '', '', '', '0000-00-00', '', 0, 0, 0, 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, 0, 1, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', 0, '', 0, 0, 0, 0, 0, '', 0, 0, '', 0),
(562, 'uu@gmail.com', '$2a$10$pHA7sEGlVTIjBk8Ol/ts.uHE47AGsWVE/Y27fURlpKvahA2Dx2gXi', 'uu82966', '', '', '', '', 'gjXdvd5eze9dJMsnG', 0, 0, '', '', '', '0000-00-00', '', 0, 0, 0, 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, 0, 1, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', 0, '', 0, 0, 0, 0, 0, '', 0, 0, '', 0),
(563, 'nn@gmail.com', '$2a$10$tnfZWThwW2P6XYIY6mPJhugnn2yebC.lupR1MOT5cpy.mJYFNbvqO', 'nn1101', '', '', '', '', 'tCvWKbx5pAvkEdAGR', 0, 0, '', '', '', '0000-00-00', '', 0, 0, 0, 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, 0, 1, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', 0, '', 0, 0, 0, 0, 0, '', 0, 0, '', 0),
(564, 'vv@gmail.com', '$2a$10$6FzAvQp7qSSJpLhpKtL9oOZT5rr8lYUWNdsMACY7MFjv7rsfAJa3.', 'vv24634', '', '', '', '', 'xwfjK8Emiz466cSnM', 0, 0, '', '', '', '0000-00-00', '', 0, 0, 0, 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, 0, 1, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', 0, '', 0, 0, 0, 0, 0, '', 0, 0, '', 0),
(565, 'hello@gmail.com', '$2a$10$yBEguXfXEXY/FFJbun/AeubluBxLd7T.GUF.cvV.hmWWIIAXSVBrO', 'hello56987', '', '', '', '', 'aEFe9awcydyRawweo', 0, 0, 'hello', 'hello', '', '0000-00-00', '', 0, 0, 0, 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, 0, 1, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '', '', '', 'https://m.facebook.com/photo.php?fbid=139161091578472&id=100064537442479&set=a.139161111578470&source=11&refid=17', '', '', '', 0, 0, 0, '', '', 0, '', 0, 0, 0, 0, 0, '', 0, 0, '', 0);

-- --------------------------------------------------------

--
-- מבנה טבלה עבור טבלה `userTravelList`
--

CREATE TABLE `userTravelList` (
  `travelID` int(11) NOT NULL,
  `userID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- הוצאת מידע עבור טבלה `userTravelList`
--

INSERT INTO `userTravelList` (`travelID`, `userID`) VALUES
(3, 353);

-- --------------------------------------------------------

--
-- מבנה טבלה עבור טבלה `userTravelStyleList`
--

CREATE TABLE `userTravelStyleList` (
  `travelStyleID` int(11) NOT NULL,
  `userID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- הוצאת מידע עבור טבלה `userTravelStyleList`
--

INSERT INTO `userTravelStyleList` (`travelStyleID`, `userID`) VALUES
(3, 353),
(3, 360),
(2, 384),
(2, 384),
(3, 386),
(3, 385),
(0, 404),
(2, 404),
(3, 410),
(3, 411),
(3, 412),
(1, 415),
(2, 416),
(3, 417),
(3, 418),
(1, 420),
(1, 421),
(3, 425),
(2, 427),
(3, 430),
(2, 433),
(2, 437),
(3, 474),
(2, 482),
(2, 483),
(4, 486),
(1, 487),
(2, 488),
(1, 489),
(2, 495),
(2, 496),
(3, 497),
(2, 498),
(2, 499),
(2, 500),
(4, 501),
(3, 502),
(5, 521),
(1, 529),
(1, 530),
(1, 531),
(2, 532),
(3, 533),
(1, 534),
(2, 535),
(3, 537),
(3, 538);

--
-- Indexes for dumped tables
--

--
-- אינדקסים לטבלה `adminUsers`
--
ALTER TABLE `adminUsers`
  ADD UNIQUE KEY `adminID` (`adminID`);

--
-- אינדקסים לטבלה `allEducationList`
--
ALTER TABLE `allEducationList`
  ADD UNIQUE KEY `educationID` (`educationID`);

--
-- אינדקסים לטבלה `allHobbiesList`
--
ALTER TABLE `allHobbiesList`
  ADD UNIQUE KEY `hobbyID` (`hobbyID`);

--
-- אינדקסים לטבלה `allJobsList`
--
ALTER TABLE `allJobsList`
  ADD UNIQUE KEY `jobID` (`jobID`);

--
-- אינדקסים לטבלה `allMovieList`
--
ALTER TABLE `allMovieList`
  ADD UNIQUE KEY `genreID` (`movieID`);

--
-- אינדקסים לטבלה `allMusicList`
--
ALTER TABLE `allMusicList`
  ADD UNIQUE KEY `favouriteMusicStyleID` (`musicID`);

--
-- אינדקסים לטבלה `allPetList`
--
ALTER TABLE `allPetList`
  ADD UNIQUE KEY `petID` (`petID`);

--
-- אינדקסים לטבלה `allTravelStyleList`
--
ALTER TABLE `allTravelStyleList`
  ADD UNIQUE KEY `travelStyleID` (`travelStyleID`);

--
-- אינדקסים לטבלה `blockList`
--
ALTER TABLE `blockList`
  ADD UNIQUE KEY `id` (`id`);

--
-- אינדקסים לטבלה `chatList`
--
ALTER TABLE `chatList`
  ADD UNIQUE KEY `chatID` (`chatID`);

--
-- אינדקסים לטבלה `interests`
--
ALTER TABLE `interests`
  ADD UNIQUE KEY `interestsID` (`interestsID`);

--
-- אינדקסים לטבלה `matchingTable`
--
ALTER TABLE `matchingTable`
  ADD PRIMARY KEY (`id`);

--
-- אינדקסים לטבלה `relationShipType`
--
ALTER TABLE `relationShipType`
  ADD UNIQUE KEY `relationShipID` (`relationShipID`);

--
-- אינדקסים לטבלה `religionList`
--
ALTER TABLE `religionList`
  ADD UNIQUE KEY `religionID` (`religionID`);

--
-- אינדקסים לטבלה `userOccupationList`
--
ALTER TABLE `userOccupationList`
  ADD UNIQUE KEY `occupationID` (`occupationID`);

--
-- אינדקסים לטבלה `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adminUsers`
--
ALTER TABLE `adminUsers`
  MODIFY `adminID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `allEducationList`
--
ALTER TABLE `allEducationList`
  MODIFY `educationID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `allHobbiesList`
--
ALTER TABLE `allHobbiesList`
  MODIFY `hobbyID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `allJobsList`
--
ALTER TABLE `allJobsList`
  MODIFY `jobID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `allMovieList`
--
ALTER TABLE `allMovieList`
  MODIFY `movieID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `allMusicList`
--
ALTER TABLE `allMusicList`
  MODIFY `musicID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `allPetList`
--
ALTER TABLE `allPetList`
  MODIFY `petID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `allTravelStyleList`
--
ALTER TABLE `allTravelStyleList`
  MODIFY `travelStyleID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `blockList`
--
ALTER TABLE `blockList`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `chatList`
--
ALTER TABLE `chatList`
  MODIFY `chatID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `interests`
--
ALTER TABLE `interests`
  MODIFY `interestsID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `matchingTable`
--
ALTER TABLE `matchingTable`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT for table `relationShipType`
--
ALTER TABLE `relationShipType`
  MODIFY `relationShipID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `religionList`
--
ALTER TABLE `religionList`
  MODIFY `religionID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `userOccupationList`
--
ALTER TABLE `userOccupationList`
  MODIFY `occupationID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=566;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
