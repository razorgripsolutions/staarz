# ✨ ✨ Staarz ✨ ✨ #

### What is this repository for? ###

This monorepo holds both the server code and application code for Staarz application
### Used technologies

- [Lerna](https://github.com/lerna/lerna) for monorepo management
- [React native](https://reactnative.dev/) for the application

### How do I get set up? ###

Make sure to install [`yarn`](https://yarnpkg.com/)

Run 

```sh
yarn install
```

#### IOS

- To work on IOS, you must use a Mac computer ([or a docker version of Mac](https://hub.docker.com/r/sickcodes/docker-osx))
- Make sure to [install an IOS simulator using XCode](https://developer.apple.com/documentation/xcode/running-your-app-in-the-simulator-or-on-a-device)
- Make sure to run `pod install` within `packages/front-end/ios` on clone/pull to install the dependencies

#### Android
 
- To work on the android version, you need to [create an android emulator](https://developer.android.com/studio/run/managing-avds). You can use [IntelliJ](https://www.jetbrains.com/idea/) or [Android studio](https://developer.android.com/studio)

### Running local server and application

You can start both the app and server running:



```sh
yarn start
```

You can start the (React native JS) app only, by running:

```
yarn start-frontend
```

You can start the server only, by running:

```
yarn start-server
```

*You can use `yarn` for the commands as well. 

### Running tests 

#### Unit tests

To run all of the tests (both client and server), run the following command:

```sh
yarn test
```

Tests are being run using `jest`, you can pass [options](https://jestjs.io/docs/cli) to the command (such as running a specific test)

#### E2E tests

**
Make sure to have a correct ENV set up with correct simulators as stated in the [detoxrc file](./packages/front-end/.detoxrc.json)**

To run E2E tests locally (**choose between android OR ios**): 

1. In a first terminal run `yarn build:e2e:{android|ios}:debug` (wait for the command to show metro running)
2. In a second terminal run `yarn test:e2e:{android|ios}:debug` - This will run e2e tests.

For more information regarding [test api](https://wix.github.io/Detox/docs/api/matchers), read all about [detox](https://wix.github.io/Detox/docs/guide/developing-while-writing-tests)
### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Use the proper [Slack channel](https://really-great-tech.slack.com/archives/C039UC9GMUH) for any questions or remarks. 